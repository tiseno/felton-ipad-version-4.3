//
//  SalesOrderType.h
//  KDS
//
//  Created by Jermin Bazazian on 12/8/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//
typedef enum 
{
    SalesOrderMain,
    SalesOrderBlind
    //SalesOrderBlindA
} SalesOrderType;