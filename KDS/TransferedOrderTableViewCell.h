//
//  TransferedOrderTableViewCell.h
//  KDS
//
//  Created by Tiseno on 11/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KDSDataSalesOrder.h"
#import "KDSAppDelegate.h"
#import "SalesOrder.h"
#import "TransferedOrderDeleteDelegate.h"
#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1

@interface TransferedOrderTableViewCell : UITableViewCell {
    
    UIColor *lineColor;
    
    BOOL topCell;
    BOOL isRemoved;
    
    UIButton *editButton;
    UIButton *pdfButton;
    UIButton *deleteButton;
    
    UILabel *codeLabel;
    UILabel *nameLabel;
    UILabel *priceLabel;
    
    SalesOrder* salesOrder;
    id<TransferedOrderDeleteDelegate> delegate;
}
@property (nonatomic) BOOL topCell;
@property (nonatomic) BOOL isRemoved;
@property (nonatomic, retain)  UIButton *editButton;
@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic, retain) UIButton *pdfButton;
@property (nonatomic, retain) UIButton *deleteButton;
@property (nonatomic, retain) UILabel *codeLabel;
@property (nonatomic, retain) UILabel *nameLabel;
@property (nonatomic, retain) UILabel *lblTransferredDate;
@property (nonatomic, retain) UILabel *lblCreatedDate;
@property (nonatomic, retain) UILabel *priceLabel;
@property (nonatomic, retain) UILabel *PackageIDLabel;
@property (nonatomic, retain) SalesOrder* salesOrder;
@property (nonatomic, retain) id<TransferedOrderDeleteDelegate> delegate;
@property (nonatomic, retain)  UIButton *pdfquoButton;

@end
