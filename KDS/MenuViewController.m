
//
//  MenuViewController.m
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MenuViewController.h"


@implementation MenuViewController
@synthesize navController, loadingView,superViewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [navController release];
    [loadingView release];
    [superViewController release];
    [super dealloc];
}
-(IBAction)syncDataButtonTapped
{
    SynchDataViewController *syncDataViewController=[[SynchDataViewController alloc] initWithNibName:@"SynchDataViewController" bundle:nil];
    syncDataViewController.title=@"Felton";
    [self.navigationController pushViewController:syncDataViewController animated:YES];
    [syncDataViewController release];
}
-(IBAction)yourCustomerButtonListTapped
{
    if(((KDSAppDelegate*)[UIApplication sharedApplication].delegate).salesOrder != nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your current cart is still open, kindly proceed to close the order before you can choose client for new order." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert autorelease];
        [alert show];
        /*CustomerListViewController *customerListViewController=[[CustomerListViewController alloc] initWithNibName:@"CustomerListViewController" bundle:nil];
        customerListViewController.title=@"Felton";
        [self.navigationController pushViewController:customerListViewController animated:YES];
        [customerListViewController release];*/
    }
    else
    {
        CustomerListViewController *customerListViewController=[[CustomerListViewController alloc] initWithNibName:@"CustomerListViewController" bundle:nil];
        customerListViewController.title=@"Felton";
        [self.navigationController pushViewController:customerListViewController animated:YES];
        [customerListViewController release];
    }
    
}
-(IBAction)listOfProductButtonTapped
{
    ListOfProductsViewController *listOfProductsViewController=[[ListOfProductsViewController alloc] initWithNibName:@"ListOfProductsViewController" bundle:nil];
    listOfProductsViewController.title=@"Felton";
    [self.navigationController pushViewController:listOfProductsViewController animated:YES];
    [listOfProductsViewController release];
}
-(IBAction)newOrderButtonTapped
{
    if(((KDSAppDelegate*)[UIApplication sharedApplication].delegate).currentCustomer == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Customer is selected, Please select a Customer to continue ordering." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert autorelease];
        [alert show];
        CustomerListViewController *customerListViewController=[[CustomerListViewController alloc] initWithNibName:@"CustomerListViewController" bundle:nil];
        customerListViewController.title=@"Felton";
        [self.navigationController pushViewController:customerListViewController animated:YES];
        [customerListViewController release];
    }
    else
    {
    /*NewOrderCategoryViewController *orderCategoryViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];
    orderCategoryViewController.title=@"Felton";
    [self.navigationController pushViewController:orderCategoryViewController animated:YES];
    [orderCategoryViewController release];
        
    NewOrderViewController *newOrderViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
    newOrderViewController.title=@"Felton";
    [self.navigationController pushViewController:newOrderViewController animated:YES];
    [newOrderViewController release];*/
        
        KDSAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
        
        NSString *listgalllery=[[NSString alloc]init];
        
        listgalllery=appDelegate.loadListGallerySatatus;
        
        
        if ([listgalllery length]==0) {
            listgalllery=@"list";
            
            KDSDataListGallery *dataItem=[[KDSDataListGallery alloc] init];
            [dataItem insertItem:listgalllery];
            
            [self listView];
        }
        else if([listgalllery isEqualToString:@"list"])
        {
            
            
            [self listView];
        }
        else if([listgalllery isEqualToString:@"gallery"])
        {
            
            
            [self galleryView];
        }  
        
        
        
        
        
        
        [listgalllery release];
        
        
        /*NewOrderCategoryViewController *newOrderViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];     
        newOrderViewController.title=@"Felton";
        [self.navigationController pushViewController:newOrderViewController animated:YES];
        [newOrderViewController release]; */
        
    }
}

-(void)listView
{
    NewOrderViewController *newOrderViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
    newOrderViewController.title=@"Felton";
    
    [self.navigationController pushViewController:newOrderViewController animated:YES];
    [newOrderViewController release];
}

-(void)galleryView
{
    /*KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
     SalesOrder* tempSalesOrder=appDelegate.salesOrder;
     [tempSalesOrder retain];
     
     NewOrderCategoryViewController *newOrderViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];
     //NewOrderViewController *newOrderCategoryViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
     appDelegate.salesOrder=tempSalesOrder;
     [tempSalesOrder release];
     
     */NewOrderCategoryViewController *newOrderViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];     
    newOrderViewController.title=@"Felton";
    [self.navigationController pushViewController:newOrderViewController animated:YES];
    [newOrderViewController release]; 
}

-(IBAction)orderListButtonTapped
{
    OrderMenupage *orderListViewController=[[OrderMenupage alloc] initWithNibName:@"OrderMenupage" bundle:nil];
    orderListViewController.title=@"Felton";
    [self.navigationController pushViewController:orderListViewController animated:YES];
    [orderListViewController release];
}
-(IBAction)transferOrderButtonTapped
{
    /*OrderListTransfered *orderListTransferedViewController=[[OrderListTransfered alloc] initWithNibName:@"OrderListTransfered" bundle:nil];
    orderListTransferedViewController.title=@"Felton";
    [self.navigationController pushViewController:orderListTransferedViewController animated:YES];
    [orderListTransferedViewController release];*/
    
    OrderListTransferedsystem *orderListTransferedViewController=[[OrderListTransferedsystem alloc] initWithNibName:@"OrderListTransferedsystem" bundle:nil];
    orderListTransferedViewController.title=@"Felton";
    [self.navigationController pushViewController:orderListTransferedViewController animated:YES];
    [orderListTransferedViewController release];

}
-(IBAction)logoutButtonTapped
{
    /*LoginViewController *loginViewController=[[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil] autorelease];
    loginViewController.view.frame=CGRectMake(0, 0, 1024, 748);
    for(UIView *subview in self.navigationController.view.superview.subviews)
    {
        [subview removeFromSuperview];
    }
    [self.navigationController.view.superview addSubview:loginViewController.view];*/
    [self.superViewController loadLogin];
    //[menuViewController release];
}

-(IBAction)myCartView
{
    /*KDSAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    YourCartViewController *launchView=[[YourCartViewController alloc] initWithNibName:@"YourCartViewController" bundle:nil];
    //launchView.orderItemsArr=[appDelegate.salesOrder order_Items];
    NSArray *orderItemsArr=[appDelegate.salesOrder order_Items];
    if(orderItemsArr.count==0)
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Cart Alert!" message:@"Empty!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message show]; 
        [message release];
        [launchView release];
    }
    else
    { 
        launchView.title=@"Felton";
        [self.navigationController pushViewController:launchView animated:YES];
        [launchView release];
    }*/
    KDSAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    YourCartViewController *launchView=[[YourCartViewController alloc] initWithNibName:@"YourCartViewController" bundle:nil];
    launchView.orderItemsArr=[appDelegate.salesOrder order_Items];
    if(launchView.orderItemsArr.count==0)
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Cart Alert!" message:@"Empty!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message show];
        [message release];
    }
    else
    {
        launchView.title=@"Felton";
        [self.navigationController pushViewController:launchView animated:YES];
        [launchView release];
    }
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
