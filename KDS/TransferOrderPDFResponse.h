//
//  TransferOrderPDFResponse.h
//  KDS
//
//  Created by Tiseno Mac 2 on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMLResponse.h"

@interface TransferOrderPDFResponse : XMLResponse{
    NSString *getPDFPath;
}

@property (nonatomic,retain) NSString *getPDFPath;

@end
