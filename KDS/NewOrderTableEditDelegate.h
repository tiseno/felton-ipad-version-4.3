//
//  NewOrderTableEditDelegate.h
//  KDS
//
//  Created by Tiseno on 12/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol NewOrderTableEditDelegate <NSObject>
-(void)editSaleOrderForEditButton;
-(void)reloadNewOrderTable;
-(void)editSaleOrderPDFButton:(SalesOrder*)isaleOrder SalesPersonCode:(SalesPerson*)isalesPersonCode;
-(void)editSaleOrderPDFQuoButton:(SalesOrder *)isaleOrder SalesPersonCode:(SalesPerson *)isalesPersonCode;
@end
  