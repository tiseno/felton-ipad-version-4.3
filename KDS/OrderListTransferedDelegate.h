//
//  OrderListTransferedDelegate.h
//  KDS
//
//  Created by Jermin Bazazian on 5/28/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

/*#ifndef KDS_OrderListTransferedDelegate_h
#define KDS_OrderListTransferedDelegate_h
#endif*/

#import <Foundation/Foundation.h>


@protocol OrderListTransferedDelegate <NSObject>
//-(void)editSaleOrderForEditButton;
//-(void)reloadNewOrderTable;
//-(void)editSaleOrderPDFButton:(SalesOrder*)isaleOrder SalesPersonCode:(NSString*)isalesPersonCode;
//-(void)TransferSaleOrderButton;
-(void)TransferSaleOrderButton:(SalesOrder *)isaleOrder SalesPersonCode:(SalesPerson *)isalesPersonCode ;
@end

