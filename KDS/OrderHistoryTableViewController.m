//
//  OrderHistoryTableViewController.m
//  KDS
//
//  Created by Tiseno on 12/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OrderHistoryTableViewController.h"

@implementation OrderHistoryTableViewController
@synthesize orderHistoryArr, loggedInSalesPerson;
-(id)init
{
    self=[super init];
    if(self)
    {
        /*KDSAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
        if(loggedInSalesPerson == nil)
        {
            self.loggedInSalesPerson = appDelegate.loggedInSalesPerson;
        }*/
    }
    return self;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
-(NSInteger) tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
	if(orderHistoryArr!=nil)
    {
        return [self.orderHistoryArr count];
    }
    return 0;
}
-(void)orderHistoryForCustomer:(Customer*)customer
{
        KDSDataSalesOrder* dataSaleOrder = [[KDSDataSalesOrder alloc] init];
        NSArray *tSalesOrderArr = [dataSaleOrder selectSalesOrderForCustomer:customer IsDeleted:NO];
        self.orderHistoryArr = tSalesOrderArr;
        [dataSaleOrder release];
}
- (void)dealloc
{
    [loggedInSalesPerson release];
    [orderHistoryArr release];
    [super dealloc];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    OrderHistoryTableCellViewController *cell = (OrderHistoryTableCellViewController*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    SalesOrder* saleOrder = [orderHistoryArr objectAtIndex:indexPath.row];
    if(cell == nil)
    {
        cell = [[[OrderHistoryTableCellViewController alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]autorelease];
    }
        cell.lineColor = [UIColor blackColor];
        cell.orderIdPrefix.text = saleOrder.systemOrderId;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSString* grandPrice = [NSString stringWithFormat:@"RM %.2f", saleOrder.grand_Price];
        cell.orderPriceLabel.text = grandPrice;
        NSDateFormatter* formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy/MM/dd"];
        NSString* currentDateStr=[formatter stringFromDate:saleOrder.Date];
        [formatter release];
        cell.orderDateLabel.text = currentDateStr;
        
        /*if(indexPath.row == 0)
            cell.topCell = YES; 
        else 
            cell.topCell = NO;*/
    
    return cell;
}


@end
