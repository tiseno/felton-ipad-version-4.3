//
//  OrderListTransferedsystem.m
//  KDS
//
//  Created by Jermin Bazazian on 5/28/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "OrderListTransferedsystem.h"

//@interface OrderListTransferedsystem ()

//@end

@implementation OrderListTransferedsystem
@synthesize btnBigTransfer,_tableViewHeader; 
@synthesize _tableView, newolistSalesOrderArr,transferedOrderTableView;
@synthesize loadingView,ssalesOrder,TnewOrderTableViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //[self initializeTableData];
        [self reloadSalesOrderArray];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    btnBigTransfer.hidden=YES;
    
    if(TnewOrderTableViewController == nil)
    {
        TransferOrderTableViewController *trNewOrderTableViewController = [[TransferOrderTableViewController alloc] init];
        self.TnewOrderTableViewController = trNewOrderTableViewController;
        self.TnewOrderTableViewController.orderListViewControllersystm=self;
        [trNewOrderTableViewController release];
    } 
    
    [_tableView setDataSource:self.TnewOrderTableViewController];
    
    self.TnewOrderTableViewController.view = self.TnewOrderTableViewController.tableView;
    
    if(_tableViewHeader == nil)
    {
        if(_tableViewHeader == nil)
        {
            self._tableView.rowHeight = 45;
            UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y-30, self._tableView.frame.size.width, 30)];
            tableHeaderView.backgroundColor = [UIColor colorWithRed:0.0196 green:0.513 blue:0.949 alpha:1.0];
            
            UILabel *tableHeaderCustomerNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(93, 4, 653, 21)];
            tableHeaderCustomerNameLabel.text = @"Customer Name";
            tableHeaderCustomerNameLabel.backgroundColor = [UIColor clearColor];
            tableHeaderCustomerNameLabel.textColor = [UIColor whiteColor];
            tableHeaderCustomerNameLabel.textAlignment = UITextAlignmentLeft;
            tableHeaderCustomerNameLabel.font=[UIFont boldSystemFontOfSize:17];
            [tableHeaderView addSubview:tableHeaderCustomerNameLabel];
            [tableHeaderCustomerNameLabel release];
            
            UILabel *tableHeaderPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(663, 4, 191, 21)];
            tableHeaderPriceLabel.text = @"Amount";
            tableHeaderPriceLabel.backgroundColor = [UIColor clearColor];
            tableHeaderPriceLabel.textColor = [UIColor whiteColor];
            tableHeaderPriceLabel.textAlignment = UITextAlignmentLeft;
            tableHeaderPriceLabel.font=[UIFont boldSystemFontOfSize:17];
            [tableHeaderView addSubview:tableHeaderPriceLabel];
            [tableHeaderPriceLabel release];

            _tableViewHeader = tableHeaderView;
            [self.view addSubview:_tableViewHeader];
            [tableHeaderView release];
  
        }
    }
    
    
    
} 

- (void)viewDidUnload
{
    [self set_tableView:nil];
    [self setBtnBigTransfer:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)dealloc
{
    [_tableViewHeader release];
    [TnewOrderTableViewController release];
    [transferedOrderTableView release];
    [newolistSalesOrderArr release];
    [loadingView release];
    [ssalesOrder release];
    [_tableView release];
    [btnBigTransfer release];
    [super dealloc];
    
}

-(BOOL)NetworkStatus
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus==NotReachable);
}

//-(void)editSaleOrderPDFButton:(SalesOrder *)isaleOrder SalesPersonCode:(NSString *)isalesPersonCode
-(void)TransferSaleOrderButton:(SalesOrder *)isaleOrder SalesPersonCode:(SalesPerson *)isalesPersonCode 
{
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }else if (appDelegate.salesOrder != nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Please clear your cart \n before transfer order. \n Thank you." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else 
    {
        //NSLog(@"hello2~");
        
        UIView *selfView=self.view;
        LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
        self.loadingView=temploadingView;

        ssalesOrder= isaleOrder;
        
        KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
        //KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        //NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:isaleOrder];
        //self.newSalesOrderArr=[dataSalesOrderItem selectOrderItemsForOrder:isaleOrder];
        /*
        NewOrderTransferRequest* request=[[NewOrderTransferRequest alloc] initWithSalesOrder:isaleOrder SalesPersonCode:isalesPersonCode];
        NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
        [networkHandler setDelegate:self];
        [networkHandler request:request];
        [request release];
        [networkHandler release];  */ 
        
        
        /**/
            NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:ssalesOrder];
            if([ssalesOrder NumberOfOderItems]==0)
            {
                for(SalesOrderItem* orderItem in orderItemArr)
                {
                    [ssalesOrder addOrderItem:orderItem];
                }
            } 
            //NewOrderTransferRequest* request=[[NewOrderTransferRequest alloc] initWithSalesOrder:salesOrder SalesPersonCode:appdelegate.loggedInSalesPerson.SalesPerson_Code];
            NewOrderTransferRequest* request=[[NewOrderTransferRequest alloc] initWithSalesOrder:isaleOrder SalesPersonCode:isalesPersonCode];
            NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
            [networkHandler setDelegate:self];
            [networkHandler request:request];
            [request release];
            [networkHandler release];
        //}
        [dataSalesOrderItem release];
        
        KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        KDSDataSalesOrder *dataSalesOrder=[[KDSDataSalesOrder alloc] init];
        NSArray* tNewSalesOrderArr=[dataSalesOrder selectOneSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:NO IsDeleted:NO IsSalesOrderID:ssalesOrder];
        self.newolistSalesOrderArr=tNewSalesOrderArr;
        [dataSalesOrder release];

        
    }
}


-(void)handleRecievedResponseMessage:(XMLResponse *)responseMessage
{    
    if(responseMessage!=nil && [responseMessage isKindOfClass:[NewOrderTransferResponse class]])
    {
        NSString *feedBackResult=((NewOrderTransferResponse*)responseMessage).getOrderResponse;
        NSLog(@"%@",feedBackResult);
        
        if ([feedBackResult isEqualToString:@"okay"]) 
        {
            KDSDataSalesOrder* dataSalesOrder=[[KDSDataSalesOrder alloc] init];
            [dataSalesOrder openConnection];
            dataSalesOrder.runBatch=YES;
            for (SalesOrder* salesOrder in self.newolistSalesOrderArr) {
                
                /*for(SalesOrderItem* orderItem in orderItemArr)
                {
                    [salesOrder addOrderItem:orderItem];
                }*/
                
                NSDateFormatter *formatter;
                NSString        *dateString;
                
                formatter = [[NSDateFormatter alloc] init];
                //[formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                [formatter setDateFormat:@"dd-MM-yyyy"];
                
                dateString = [formatter stringFromDate:[NSDate date]];
                
                [formatter release]; 
                
                NSString *transferDatereff=[[NSString alloc]initWithFormat:@"Transferred %@ %@",dateString,salesOrder.Order_Reference];
                
                salesOrder.Order_Reference=transferDatereff;
                
                [dataSalesOrder markSalesOrderAsUploaded:salesOrder]; 
                [transferDatereff release];
            }
            
            //for(SalesOrderItem* orderItem in self.newSalesOrderArr)
            //{
              //  [salesOrder addOrderItem:orderItem];
            //}
            
            [dataSalesOrder closeConnection];
            [dataSalesOrder release];
            [self reloadSalesOrderArray];
            //[self initializeTableData];
             
            /*KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            KDSDataSalesOrder *dataSalesOrderss=[[KDSDataSalesOrder alloc] init];
            NSArray* tNewSalesOrderArr=[dataSalesOrderss selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:NO IsDeleted:NO];
            self.newSalesOrderArr=tNewSalesOrderArr;
            [dataSalesOrder release];*/
            [self reloadTransferedTable];
            [_tableView reloadData];
            [self.loadingView removeView];
            
            /*KDSDataSalesOrder* dataSalesOrder=[[KDSDataSalesOrder alloc] init];
            [dataSalesOrder openConnection];
            dataSalesOrder.runBatch=YES;
            for (SalesOrder* salesOrder in self.newOrdersNotTransferedArr) {
                [dataSalesOrder markSalesOrderAsUploaded:salesOrder];
            }
            [dataSalesOrder closeConnection];
            [dataSalesOrder release];
            [self reloadSalesOrderArray];
            [_tableView reloadData];*/
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Transfer" message:@"Order transferred successful." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=kConfirmedAlertTag;
            [alert show];
            [alert release];

                        
        }else {
            [self.loadingView removeView];
            NSLog(@"server down2~");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Server Down. Please try again later. Thank you." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            
        }
        
    }else {
        [self.loadingView removeView];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Server Down. Please try again later. Thank you." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        
        NSLog(@"server down1~");
    }
}

-(void)reloadTransferedTable
{
    //[self initializeTableData];
    [self.TnewOrderTableViewController initializeTableData];
    
    [transferedOrderTableView reloadData];
}

-(void)initializeTableData
{
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KDSDataSalesOrder *dataSalesOrder;
    if(appDelegate.salesOrder.Type==SalesOrderMain)
    {
        dataSalesOrder = [[KDSDataSalesOrder alloc] init];
    }
    else //if(appDelegate.salesOrder.Type==SalesOrderBlind)
    {
        dataSalesOrder = [[KDSDataBlindSalesOrder alloc] init];
    }
    NSArray* tNewSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:NO IsDeleted:NO];
    self.newolistSalesOrderArr=tNewSalesOrderArr;
    [dataSalesOrder release];
}

-(void)reloadSalesOrderArray
{
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KDSDataSalesOrder *dataSalesOrder=[[KDSDataSalesOrder alloc] init];
    NSArray* tNewSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:NO IsDeleted:NO];
    self.newolistSalesOrderArr=tNewSalesOrderArr;
    [dataSalesOrder release];
}

@end
