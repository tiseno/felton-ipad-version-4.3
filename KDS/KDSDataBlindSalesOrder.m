//
//  KDSDataBlindSalesOrder.m
//  KDS
//
//  Created by Tiseno on 12/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KDSDataBlindSalesOrder.h"


@implementation KDSDataBlindSalesOrder
-(NSString*)generateInsertQueryForSaleOrder:(SalesOrder*)salesOreder AndSalesperson:(SalesPerson*)salesPerson
{
    int orderType=1;
    if(salesOreder.Type==SalesOrderBlind)
    {
        orderType=2;
    }
    
    //NSLog(@"blind orderType-->%d",orderType);
    NSDateFormatter* formatter=[[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"YYYY/MM/dd hh:mm:ss"];
    [formatter setDateFormat:@"yyyyMMdd"];
    NSDate* currentDate=[NSDate date];
    NSString* currentDateStr=[formatter stringFromDate:currentDate];
    [formatter release];
    BlindSalesOrder* blindSalesOrder = (BlindSalesOrder*)salesOreder;
    
    NSString *orderRemark=[blindSalesOrder.remark stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];    
    NSString *orderOrder_CashRemarks=[salesOreder.CashRemarks stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *orderDeliveryDate=[salesOreder.DeliveryDate stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *blindSalesOrderInstallationorder_Address=[blindSalesOrder.Installationorder_Address stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *orderOrder_Reference=[salesOreder.Order_Reference stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *Installation_Email=[blindSalesOrder.Installation_Email stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
     
    NSString *Installation_Fax=[blindSalesOrder.Installation_Fax stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *Installation_Phone=[blindSalesOrder.Installation_Phone stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *Installation_State=[blindSalesOrder.Installation_State stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *Installation_City=[blindSalesOrder.Installation_City stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    
    NSString *Installation_Name=[blindSalesOrder.Installation_Name stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *OrderDescription=[salesOreder.OrderDescription stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    
    
    NSString *insertSQL = [NSString stringWithFormat:@"insert into SalesOrder(customer_id,salesperson_code,order_type,order_date,order_location,is_salesorder_uploaded,grand_price,Installation_name,installation_address,installation_city,installation_state,installation_phone,installation_fax,installation_email,installation,transportation,order_discount, remark,OrderDescription, DeliveryDate, CashRemark, orderReference,is_deleted) values('%@','%@',%d,'%@','%@',%d,%f,'%@','%@','%@','%@','%@','%@','%@',%f,%f,%f,'%@','%@','%@','%@','%@', %d);", salesOreder.customer.Customer_Id,salesPerson.SalesPerson_Code,orderType,currentDateStr,@"",salesOreder.Is_Uploaded,blindSalesOrder.grand_Price,Installation_Name,blindSalesOrderInstallationorder_Address,Installation_City,Installation_State,Installation_Phone,Installation_Fax, Installation_Email,blindSalesOrder.installation,blindSalesOrder.transportation,blindSalesOrder.order_discount,orderRemark,OrderDescription, orderDeliveryDate, orderOrder_CashRemarks, orderOrder_Reference ,salesOreder.isDeleted];
    //NSLog(@"blindSalesOrder.Installation_Name------>%@", blindSalesOrder.Installation_Name);  
    
    return insertSQL;
} 

-(void)insertOrderItems:(NSArray*)orderItemArr forSalesOrderID:(int)salesOrderID
{
    KDSDataBlindSalesOrderItem* dataItem=[[KDSDataBlindSalesOrderItem alloc] initWithDataBase:dbKDS];
    dataItem.runBatch=YES;
    for(BlindSalesOrderItem* item in orderItemArr)
    {
        [dataItem insertSalesOrderItem:item ForSalesOrderID:salesOrderID];
    }
    [dataItem release];
}
@end
