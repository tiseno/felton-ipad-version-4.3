//
//  TransferedOrderDeleteDelegate.h
//  KDS
//
//  Created by Tiseno on 3/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TransferedOrderDeleteDelegate <NSObject>
-(void)editDeleteSaleOrderForEditButton;
-(void)reloadTransferedTable;
-(void)editTransferredSaleOrderPDFButton:(SalesOrder*)isaleOrder SalesPersonCode:(SalesPerson*)isalesPersonCode;
-(void)editTransferredSaleOrderPDFQuoButton:(SalesOrder *)isaleOrder SalesPersonCode:(SalesPerson *)isalesPersonCode;
@end 
 