//
//  GridDeletedTableView.m
//  KDS
//
//  Created by Tiseno Mac 2 on 5/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GridDeletedTableView.h"
#define cell1Width 80
#define cell2Width 300
#define cell3Width 60
#define cell4Width 70
#define cell5Width 90

@implementation GridDeletedTableView

@synthesize lineColor, topCell, deleteButton, productImageView, changeDiscountTypeButton;
@synthesize quantityLabel, productNameLabel, priceLabel, descriptionLabel,discountLabel,discountTypeLabel;
@synthesize quantityTextField, priceTextField, descriptionTextView, discountTextField,isDiscountAmount,equivalantOrderItemIndex,delegate,salesOrderItem,blindOrderItem,TotalLabel;

@synthesize controlblind,colorblind,sqftblind,totalsqftblind,remarkblind,productNameLabelmain;

@synthesize productNameLabelblind,quantityLabelblind,descriptionLabelblind,priceLabelblind,discountLabelblind,discountTypeLabelblind,TotalLabelblind;
- (void)dealloc
{
    [TotalLabel release];
    [discountTypeLabel release];
    [discountLabel release];
    [lineColor release];
    [deleteButton release];
    [productImageView release];
    [changeDiscountTypeButton release];
    [quantityLabel release];
    [productNameLabel release];
    [priceLabel release];
    [descriptionLabel release];
    [quantityTextField release];
    [priceTextField release];
    [descriptionTextView release];
    [discountTextField release];
    [delegate release];
    [super dealloc];
    
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        isDiscountAmount=YES;
        topCell = NO;
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 692, 142);
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 692, 142);
        
        UIImage *deleteButtonImage = [UIImage imageNamed:@"btn_delete.png"];
        UIButton *tdeleteButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 14, 58, 30)];
        [tdeleteButton setBackgroundImage:deleteButtonImage forState:UIControlStateNormal];
        [tdeleteButton addTarget:self action:@selector(delet_ButtonTapped:)forControlEvents:UIControlEventTouchDown];
        self.deleteButton = tdeleteButton;
        [tdeleteButton release];
        [self addSubview:deleteButton];
        
        
        
        /*UITextField *tquantityTextField = [[UITextField alloc] initWithFrame:CGRectMake(10, 45, 89, 34)];
         tquantityTextField.delegate = self;
         tquantityTextField.textColor = [UIColor blackColor];
         tquantityTextField.textAlignment = UITextAlignmentCenter;
         tquantityTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
         tquantityTextField.backgroundColor = [UIColor lightGrayColor];
         self.quantityTextField = tquantityTextField;
         [tquantityTextField release];
         [self addSubview:quantityTextField];*/
        
        /*UIImageViewWithImageFromURL *tproductImageView = [[UIImageViewWithImageFromURL alloc] init];
         tproductImageView.frame = CGRectMake(100, 0, 50, 50);
         self.productImageView = tproductImageView;
         [tproductImageView release];
         [self addSubview:productImageView];*/
        
        UILabel *tproductNameLabelmain = [[UILabel alloc] initWithFrame:CGRectMake(130, 16, 260, 25)];
        //tproductNameLabel.text = @"productNameLabel:";
        tproductNameLabelmain.textAlignment = UITextAlignmentLeft;
        tproductNameLabelmain.adjustsFontSizeToFitWidth = YES;
        tproductNameLabelmain.backgroundColor = [UIColor clearColor];
        tproductNameLabelmain.textColor = [UIColor blackColor];
        self.productNameLabelmain = tproductNameLabelmain;
        [tproductNameLabelmain release];
        [self addSubview:productNameLabelmain];
        
        
         
        
        UILabel *tquantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(452, 16, 50, 25)];
        //tquantityLabel.text = @"Quantity:";
        tquantityLabel.textAlignment = UITextAlignmentCenter;
        tquantityLabel.adjustsFontSizeToFitWidth = YES;
        tquantityLabel.backgroundColor = [UIColor clearColor];
        tquantityLabel.textColor = [UIColor blackColor];
        self.quantityLabel = tquantityLabel;
        [tquantityLabel release];
        [self addSubview:quantityLabel];
        
        UILabel *tpriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(563, 16, 50, 25)];
        //tpriceLabel.text = @"Price (RM):";
        tpriceLabel.textAlignment = UITextAlignmentCenter;
        tpriceLabel.adjustsFontSizeToFitWidth = YES;
        tpriceLabel.backgroundColor = [UIColor clearColor];
        tpriceLabel.textColor = [UIColor blackColor];
        self.priceLabel = tpriceLabel;
        [tpriceLabel release];
        [self addSubview:priceLabel];
        
        
        UILabel *tDiscountLabel = [[UILabel alloc] initWithFrame:CGRectMake(693, 16, 70, 25)];
        //tDiscountLabel.text = @"Discount:";
        tDiscountLabel.textAlignment = UITextAlignmentCenter;
        tDiscountLabel.adjustsFontSizeToFitWidth = YES;
        tDiscountLabel.backgroundColor = [UIColor clearColor];
        tDiscountLabel.textColor = [UIColor blackColor];
        self.discountLabel = tDiscountLabel;
        [tDiscountLabel release];
        [self addSubview:discountLabel];
        
        UILabel *tTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(822, 16, 83, 25)];
        //tDiscountLabel.text = @"TotalLabel:";
        tTotalLabel.textAlignment = UITextAlignmentCenter;
        tTotalLabel.adjustsFontSizeToFitWidth = YES;
        tTotalLabel.backgroundColor = [UIColor clearColor];
        tTotalLabel.textColor = [UIColor blackColor];
        self.TotalLabel = tTotalLabel;
        [tTotalLabel release];
        [self addSubview:TotalLabel];
        
        /*-=============blind===============-*/
        
        UILabel *tproductNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 8, 200, 25)];
        //tproductNameLabel.text = @"productNameLabel:";
        tproductNameLabel.textAlignment = UITextAlignmentLeft;
        tproductNameLabel.adjustsFontSizeToFitWidth = YES;
        tproductNameLabel.backgroundColor = [UIColor clearColor];
        tproductNameLabel.textColor = [UIColor blackColor];
        self.productNameLabel = tproductNameLabel;
        [tproductNameLabel release];
        [self addSubview:productNameLabel];
        
        UILabel *tcolorLabelblind = [[UILabel alloc] initWithFrame:CGRectMake(100, 26, 280, 25)];
        tcolorLabelblind.textAlignment = UITextAlignmentLeft;
        tcolorLabelblind.adjustsFontSizeToFitWidth = YES;
        tcolorLabelblind.backgroundColor = [UIColor clearColor];
        tcolorLabelblind.textColor = [UIColor blackColor];
        self.colorblind = tcolorLabelblind;
        [tcolorLabelblind release];
        [self addSubview:colorblind];
        
        UILabel *tremarkblindlbl = [[UILabel alloc] initWithFrame:CGRectMake(100, 44, 280, 25)];
        tremarkblindlbl.textAlignment = UITextAlignmentLeft;
        tremarkblindlbl.adjustsFontSizeToFitWidth = YES;
        tremarkblindlbl.backgroundColor = [UIColor clearColor];
        tremarkblindlbl.textColor = [UIColor blackColor];
        self.remarkblind = tremarkblindlbl;
        [tremarkblindlbl release];
        [self addSubview:remarkblind];
        
        UILabel *tcontrolblind = [[UILabel alloc] initWithFrame:CGRectMake(310, 16, 100, 25)];
        tcontrolblind.textAlignment = UITextAlignmentCenter;
        tcontrolblind.adjustsFontSizeToFitWidth = YES;
        tcontrolblind.backgroundColor = [UIColor clearColor];
        tcontrolblind.textColor = [UIColor blackColor];
        self.controlblind = tcontrolblind;
        [tcontrolblind release];
        [self addSubview:controlblind];
        
        
        UILabel *tquantityLabelblind = [[UILabel alloc] initWithFrame:CGRectMake(420, 16, 50, 25)];
        tquantityLabelblind.textAlignment = UITextAlignmentCenter;
        tquantityLabelblind.adjustsFontSizeToFitWidth = YES;
        tquantityLabelblind.backgroundColor = [UIColor clearColor];
        tquantityLabelblind.textColor = [UIColor blackColor];
        
        self.quantityLabelblind = tquantityLabelblind;
        [tquantityLabelblind release];
        [self addSubview:quantityLabelblind];
        
        
        UILabel *tsqftblind = [[UILabel alloc] initWithFrame:CGRectMake(485, 16, 50, 25)];
        tsqftblind.textAlignment = UITextAlignmentCenter;
        tsqftblind.adjustsFontSizeToFitWidth = YES;
        tsqftblind.backgroundColor = [UIColor clearColor];
        tsqftblind.textColor = [UIColor blackColor];
        self.sqftblind = tsqftblind;
        [tsqftblind release];
        [self addSubview:sqftblind];
        
        UILabel *ttotalsqftblind = [[UILabel alloc] initWithFrame:CGRectMake(565, 16, 50, 25)];
        ttotalsqftblind.textAlignment = UITextAlignmentCenter;
        ttotalsqftblind.adjustsFontSizeToFitWidth = YES;
        ttotalsqftblind.backgroundColor = [UIColor clearColor];
        ttotalsqftblind.textColor = [UIColor blackColor];
        self.totalsqftblind = ttotalsqftblind;
        [ttotalsqftblind release];
        [self addSubview:totalsqftblind];
        
        
        UILabel *tpriceLabelblind = [[UILabel alloc] initWithFrame:CGRectMake(660, 16, 50, 25)];
        tpriceLabelblind.textAlignment = UITextAlignmentCenter;
        tpriceLabelblind.adjustsFontSizeToFitWidth = YES;
        tpriceLabelblind.backgroundColor = [UIColor clearColor];
        tpriceLabelblind.textColor = [UIColor blackColor];
        self.priceLabelblind = tpriceLabelblind;
        [tpriceLabelblind release];
        [self addSubview:priceLabelblind];
        
        
        UILabel *tDiscountLabelblind = [[UILabel alloc] initWithFrame:CGRectMake(750, 16, 70, 25)];
        tDiscountLabelblind.textAlignment = UITextAlignmentCenter;
        tDiscountLabelblind.adjustsFontSizeToFitWidth = YES;
        tDiscountLabelblind.backgroundColor = [UIColor clearColor];
        tDiscountLabelblind.textColor = [UIColor blackColor];
        self.discountLabelblind = tDiscountLabelblind;
        [tDiscountLabelblind release];
        [self addSubview:discountLabelblind];
        
        UILabel *tTotalLabelblind = [[UILabel alloc] initWithFrame:CGRectMake(845, 16, 83, 25)];
        tTotalLabelblind.textAlignment = UITextAlignmentCenter;
        tTotalLabelblind.adjustsFontSizeToFitWidth = YES;
        tTotalLabelblind.backgroundColor = [UIColor clearColor];
        tTotalLabelblind.textColor = [UIColor blackColor];
        self.TotalLabelblind = tTotalLabelblind;
        [tTotalLabelblind release];
        [self addSubview:TotalLabelblind];
        
        
    }
    return self;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [delegate updateAlertText];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField selectAll:nil];
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [delegate updateAlertText];
    
}
-(IBAction)changeDiscountType_ButtonTapped:(id)sender
{
    if(isDiscountAmount)
    {
        [((UIButton*)sender) setBackgroundImage:[UIImage imageNamed:@"Btn_Discount_Amount.png"] forState:UIControlStateNormal];
        self.discountTypeLabel.text = @"In %";
        isDiscountAmount = NO;
        
    }
    else
    {
        [((UIButton*)sender) setBackgroundImage:[UIImage imageNamed:@"Button_Amount_Percentage.png"] forState:UIControlStateNormal];
        self.discountTypeLabel.text = @"In amount";
        isDiscountAmount = YES;
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context, lineColor.CGColor);       
    
	// CGContextSetLineWidth: The default line width is 1 unit. When stroked, the line straddles the path, with half of the total width on either side.
	// Therefore, a 1 pixel vertical line will not draw crisply unless it is offest by 0.5. This problem does not seem to affect horizontal lines.
	CGContextSetLineWidth(context, 1.0);
    
	// Add the vertical lines
	//CGContextMoveToPoint(context, cell1Width, 0);
	//CGContextAddLineToPoint(context, cell1Width, rect.size.height);
    
	CGContextMoveToPoint(context, cell1Width+cell2Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+0.5, rect.size.height);
    
    CGContextMoveToPoint(context, cell1Width+cell2Width+cell3Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+cell3Width+0.5, rect.size.height);
    
    CGContextMoveToPoint(context, cell1Width+cell2Width+cell3Width+cell4Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+cell3Width+cell4Width+0.5, rect.size.height);
    
    CGContextMoveToPoint(context, cell1Width+cell2Width+cell3Width+cell4Width+cell5Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+cell3Width+cell4Width+cell5Width+0.5, rect.size.height);
    
    
	// Add bottom line
	CGContextMoveToPoint(context, 0, rect.size.height);
	CGContextAddLineToPoint(context, rect.size.width, rect.size.height-0.5);
	
	// If this is the topmost cell in the table, draw the line on top
	if (topCell)
	{
		CGContextMoveToPoint(context, 0, 0);
		CGContextAddLineToPoint(context, rect.size.width, 0);
	}
	
	// Draw the lines
	CGContextStrokePath(context); 
}
-(void)setTopCell:(BOOL)newTopCell
{
    topCell = newTopCell;
}
-(void)setIsDiscountAmount:(BOOL)tisDiscountAmount
{
    isDiscountAmount=tisDiscountAmount;
    NSString *imageName=@"Btn_Discount_Amount.png";
    if(isDiscountAmount)
    {
        imageName=@"Button_Amount_Percentage.png";
    }
    [self.changeDiscountTypeButton setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}


@end
