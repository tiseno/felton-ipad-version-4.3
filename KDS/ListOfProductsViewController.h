//
//  ListOfProductsViewController.h
//  KDS
//
//  Created by Tiseno on 12/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListOfProductsTableViewCellController.h"
#import "NSDictionary-MutableDeepCopy.h"
#import "KDSAppDelegate.h"
#import "LoadingView.h"

@interface ListOfProductsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    
    UITableView *_tableView;
    
    UISearchBar *search;
    
    NSMutableDictionary *names;
    NSDictionary *allNames;

    NSMutableArray *keys;
    UISegmentedControl *searchMethodSc;
    
    BOOL searchById, searchByName;
    
    UISearchBar* searchBar;
    LoadingView *loadingView;
    
    int counterA;
    bool startA;
    NSString *_CountDownTime;
    NSTimer *timerA;
    IBOutlet UILabel *secondsA;
    BOOL loadData;
}
@property (nonatomic) BOOL searchById, searchByName;
@property (nonatomic, retain) IBOutlet UISearchBar* searchBar;
@property (nonatomic, retain) IBOutlet UISegmentedControl *searchMethodSc;
@property (nonatomic, retain) IBOutlet UITableView *_tableView;
@property (nonatomic, retain) IBOutlet UISearchBar *search;
@property (nonatomic, retain) NSMutableDictionary *names;
@property (nonatomic, retain) NSMutableArray *keys;
@property (nonatomic, retain) NSDictionary *allNames;
@property (retain, nonatomic) IBOutlet UISwitch *PrefixOnOff;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic,retain) NSString *_CountDownTime;
@property (retain, nonatomic) IBOutlet UILabel *secondsA;
@end
