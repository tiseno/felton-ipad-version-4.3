//
//  KDSAppDelegate.m
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KDSAppDelegate.h"

#import "KDSViewController.h"

@implementation KDSAppDelegate 

@synthesize apporderPackaging;
@synthesize orderPackagingArr;
@synthesize window=_window, currentCustomer,itemCategoryDictionary, currentItem, salesOrder,loggedInSalesPerson,isSynced,currentUpdateOrderItem,TransferOrdersalesOrder,OrderTransfercurrentCustomer,PdfEmailOrdersalesOrder,PDFTransfercurrentCustomer;

@synthesize viewController=_viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
     
    self.window.rootViewController = self.viewController;
    isSynced=NO;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [itemCategoryDictionary release];
    [currentCustomer release];
    [salesOrder release];
    [apporderPackaging release];
    [orderPackagingArr release];
    [TransferOrdersalesOrder release];
    [PdfEmailOrdersalesOrder release];
    [PDFTransfercurrentCustomer release];
    [OrderTransfercurrentCustomer release];
    [super dealloc];
}

/**/-(NSDictionary*)loadProductsUpdate:(NSString *)item
{
    KDSDataItem *dataItem=[[KDSDataItem alloc] init];
    NSArray *items=[dataItem selectEditItem:item];
    [self fillItemCateogryDictionaryWithItemArr:items];
    [dataItem release];
    return self.itemCategoryDictionary;
}

-(NSDictionary*)loadProducts
{
    KDSDataItem *dataItem=[[KDSDataItem alloc] init];
    NSArray *items=[dataItem selectItem];
    [self fillItemCateogryDictionaryWithItemArr:items];
    [dataItem release];
    return self.itemCategoryDictionary;
}
-(void)fillItemCateogryDictionaryWithItemArr:(NSArray*)itemarr
{
    if(self.itemCategoryDictionary==nil)
    {
        NSMutableDictionary *titemCategoryDictionary=[[NSMutableDictionary alloc] init];
        self.itemCategoryDictionary=titemCategoryDictionary;
        [titemCategoryDictionary release];
    }
    for(Item* item in itemarr)
    {
        NSArray* keys=[self.itemCategoryDictionary allKeys];
        int keyIndex=-1;
        if(keys!=nil)
        {
            for(int i=0;i<keys.count;i++)
            {
                NSString* key=[keys objectAtIndex:i];
                NSString *catTrimmedStr=[item.Category stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                if([key isEqualToString:catTrimmedStr])
                {
                    keyIndex=i;
                    break;
                }
            }
        }
        if(keyIndex==-1)
        {
            NSMutableArray *valueItemArr=[[NSMutableArray alloc] init];
            [valueItemArr addObject:item];
            NSString *catTrimmedStr=[item.Category stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [self.itemCategoryDictionary setObject:valueItemArr forKey:catTrimmedStr];
            [valueItemArr release];
        }
        else
        {
            NSString *catTrimmedStr=[item.Category stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSMutableArray *valueItemArr=[self.itemCategoryDictionary objectForKey:catTrimmedStr];
            [valueItemArr addObject:item];
        }
    }
}
-(void)clearSalesOrder
{
    if(salesOrder!=nil)
    {
        [salesOrder release];
        salesOrder=nil; 
    }
}

-(void)clearPDFEmailSalesOrder
{
    if(PdfEmailOrdersalesOrder!=nil)
    {
        [PdfEmailOrdersalesOrder release];
        PdfEmailOrdersalesOrder=nil;
    }
}

-(void)loginWithSalePerson:(SalesPerson*)iSalesPerosn
{
    loggedInSalesPerson=iSalesPerosn;
    [loggedInSalesPerson retain];
}
-(void)logout
{
    if(self.loggedInSalesPerson!=nil)
    {
        [loggedInSalesPerson release];
    }
}
-(NSMutableDictionary*)itemCategoryDictionary
{
    if(self.isSynced)
    {
        isSynced=NO;
        [self loadProducts];
    }
    return itemCategoryDictionary;
}

-(void)loadOrderPackage
{
    KDSDateOrderPackage *dataSalesOrder=[[KDSDateOrderPackage alloc] init];
    //NSArray* tTransferecSalesOrderPackageArr=[[NSArray alloc]init];
    
    
    
   /**/if (self.orderPackagingArr ==nil) {
        NSArray* tTransferecSalesOrderPackageArr=[dataSalesOrder selectOrderPackageID];
        //tTransferecSalesOrderPackageArr=[dataSalesOrder selectOrderPackageID];
        
        //KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        self.orderPackagingArr=tTransferecSalesOrderPackageArr;
        
        //[apporderPackaging addOrderPackage:tTransferecSalesOrderPackageArr];
        NSLog(@"self.orderPackagingArr~%@",self.orderPackagingArr);
       [orderPackagingArr retain];
        
    }else {

        //[apporderPackaging Order_Package];
        NSLog(@"not nil~");
        
        NSLog(@"self.orderPackagingArr~%@",self.orderPackagingArr);
    }
    
     
    //[orderPackagingArr retain];
    //[tTransferecSalesOrderPackageArr release];
    [dataSalesOrder release];
}

-(void)clearOrderPackage
{
    if(orderPackagingArr!=nil)
    {
        [orderPackagingArr release];
        orderPackagingArr=nil; 
    }
}

-(NSString*)loadListGallerySatatus
{ 
    KDSDataListGallery *dataItem=[[KDSDataListGallery alloc] init];
    NSArray *items=[dataItem selectItem];
    //NSLog(@"items--->%@",items);
    
    NSString *glistgallerystatus=[[[NSString alloc]init]autorelease];
    for(listgallery* item in items)
    {
        
        
        glistgallerystatus=item.listgallerystatus;
        //NSLog(@"glistgallerystatus-->%@",glistgallerystatus);
        
    }
    
    [dataItem release];
    
    return glistgallerystatus;
}

@end
