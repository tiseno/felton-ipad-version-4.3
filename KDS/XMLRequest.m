//
//  XMLRequest.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "XMLRequest.h"


@implementation XMLRequest
@synthesize webserviceURL,SOAPAction,requestType;
-(id)init
{
    self=[super init];
    if(self)
    {
        self.webserviceURL=@"http://219.94.43.102/KDSWebservice/KDSService.asmx";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(void)dealloc
{
    [webserviceURL release];
    [SOAPAction release];
    [super dealloc];
}
-(NSString*) generateHTTPPostMessage
{
    [NSException raise:NSInternalInconsistencyException 
				format:@"Implementation is not provided", NSStringFromSelector(_cmd)];
	return nil;
}
@end
