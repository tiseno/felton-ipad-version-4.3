//
//  YourCartViewController.h
//  KDS
//
//  Created by Tiseno on 11/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KDSAppDelegate.h"
#import "SalesOrder.h"
#import "SalesOrderItem.h"
#import "NewOrderCategoryViewController.h"
#import "ChangeDiscountTypeButtonDelegate.h"
#import "YourCartTableCellDelegate.h"
#import "KDSDataSalesOrder.h"
#import "KDSDataBlindSalesOrder.h"
#import "MenuViewController.h"
#import "MenuViewController.h"
#import "UITableViewLoadingDelegate.h"
#import "UITableViewWithLoadingNotification.h"
#import "ReviewTableViewController.h"
#import "UpdateSelectedCartView.h"

#import "NewOrderUpdateConfirmView.h"
#import "Customer.h"
#import "KDSDataCustomer.h"
#import "BlindSalesOrderItem.h"

#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1
#define kCancelAlertTag 2
#define kUnsavedAlertTag 3

#define kHeightTextField 0
#define kWidthTextField 1
#define kQuantityTextField 2
#define kPriceTextFeield 3

#define kTotalDiscount 10

#define MAXLENGTHto 200
#define MAXLENGTHo 100
#define MAXQTYthirty 30
#define MAXSIZEtwelve 12
#define MAXBRANDsixty 60
#define MAXBRANDtwo 2
#define MAXBRANDfifty 50 

@class UITableView;
@interface YourCartViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate, UITableViewLoadingDelegate> {
    
    UITableViewWithLoadingNotification *_tableView;
    UIView *tableViewHeader;
    UIView *reviewUIView;
    UIView *UpdateCartUIView; 
    
    UITextField *installationNameTextField;
    UITextField *cityTextField;
    UITextField *stateTextField;
    UITextField *dateTextField;
    UITextField *timeTextField;
    UITextField *telTextField;
    UITextField *faxTextField;
    UITextField *emailTextField;
    UITextField *transportationTextField;
    UITextField *installationTextField;
    UITextField *totalDiscountTextField;
    
    UIButton *backButton;
    //SalesOrder* CurrentSaleOrder;
    ReviewTableViewController* reviewTableViewController;
    
    UILabel *grandTotalLabel;
    UILabel *totalPriceLabel;
    UILabel *orderIdLabel;
    UILabel *orderIdValueLabel;
    
    UITableView *reviewTableView;
    UIView *EditCartSmallView;
    
    UIImageView* backgroundImageView;
    
    UITextView *installationAddressTextView;
    UITextView *ShippingAddressTextView;
    UITextView *remarkTextView;
    UITextView *alertUser;
    
    int numberOfRows;
    int heightOfEditedArea;
    int heightOffset;
    BOOL iskeyboardDisplayed;
    BOOL isTableLoaded;
    BOOL isDiscountAmount;
    float totalPricce;
    float grandPrice;
    float totalDiscountOfItems;
    float discountOnTotalPrice;
    int numberOfCellsGenerated;
    int numberOfBlindOrders;
    BOOL shouldUpdateCart;
    NSArray *orderItemsArr;
    SalesOrderItem* salesOrderItem;
    //NSMutableArray *allsalesItem;
    UITextField *txtorderref;
    NSString *UpdaCartPrice;
    int UpdateOrderItemIndex;
    integer_t *ctype;
    int salesoderTypes;
}
@property (nonatomic, retain) NSString *lstGallery;
@property (retain, nonatomic) IBOutlet UIScrollView *mainScrolView;
@property (retain, nonatomic) IBOutlet UILabel *lblcustomerid;
@property (retain, nonatomic) IBOutlet UILabel *lblcustomername;
@property (retain, nonatomic) IBOutlet UIImageView *imgline;

@property (nonatomic) int salesoderTypes;
//@property (nonatomic,retain) NSString *currenttype;
@property (retain, nonatomic) IBOutlet UILabel *lblcustomerCompanyName;
@property (retain, nonatomic) IBOutlet UILabel *lblcustomerTel;
@property (retain, nonatomic) IBOutlet UILabel *shippingAddress;
@property (nonatomic, retain) Customer* currentCustomer;
@property (nonatomic,retain) NSArray *orderItemsArr;
@property (nonatomic, retain) UIButton *backButton;
@property (nonatomic, retain) UITableView *reviewTableView;
@property (nonatomic, retain) UIView *EditCartSmallView;
@property (nonatomic, retain) UIView *reviewUIView;
@property (nonatomic, retain) UIView *UpdateCartUIView;
@property (nonatomic) float totalDiscountOfItems;
@property (nonatomic) float discountOnTotalPrice;
@property (nonatomic, retain) IBOutlet UITableViewWithLoadingNotification *_tableView;
@property (nonatomic, retain) UIView *tableViewHeader;
//@property (nonatomic, retain) SalesOrder* CurrentSaleOrder;
@property (nonatomic, retain) ReviewTableViewController* reviewTableViewController;
//@property (nonatomic, retain) UpdateSelectedCartView* UpdateSelectedCartViewset;

//@property (nonatomic, retain) IBOutlet UIImageView* backgroundImageView;
@property (nonatomic, retain) IBOutlet UITextField *installationNameTextField;
@property (nonatomic, retain) IBOutlet UITextField *cityTextField;
@property (nonatomic, retain) IBOutlet UITextField *stateTextField;
@property (nonatomic, retain) IBOutlet UITextField *dateTextField;
@property (nonatomic, retain) IBOutlet UITextField *timeTextField;
@property (nonatomic, retain) IBOutlet UITextField *telTextField;
@property (nonatomic, retain) IBOutlet UITextField *faxTextField;
@property (nonatomic, retain) IBOutlet UITextField *emailTextField;
@property (nonatomic, retain) IBOutlet UITextField *transportationTextField;
@property (nonatomic, retain) IBOutlet UITextField *installationTextField;
@property (nonatomic, retain) IBOutlet UITextField *totalDiscountTextField;
@property (nonatomic, retain) IBOutlet UILabel *grandTotalLabel;
@property (nonatomic, retain) IBOutlet UILabel *totalPriceLabel;
@property (nonatomic, retain) IBOutlet UILabel *orderIdLabel;
@property (nonatomic, retain) IBOutlet UILabel *orderIdValueLabel;
@property (nonatomic, retain) IBOutlet UITextView *alertUser;
@property (nonatomic, retain) IBOutlet UITextView *installationAddressTextView;
//-->
@property (nonatomic, retain) IBOutlet UITextView *remarkTextView;
@property (retain, nonatomic) IBOutlet UITextField *DeliveryDateTextField;
@property (retain, nonatomic) IBOutlet UILabel *lbldeliveryDate;
@property (retain, nonatomic) IBOutlet UITextView *DescripttionTextView;
@property (retain, nonatomic) IBOutlet UILabel *lblDescription;
-(void)updateCart;
-(void)manageReviewView;
-(void)manageUpdateCartView;
-(IBAction)UpdateScrollToItem:(id)sender;
@property (nonatomic,retain) SalesOrderItem* orderItem;
//@property (nonatomic, retain) NSMutableArray *allsalesItem;

@property (nonatomic,retain) UITextField *gpriceTextField;

@property (nonatomic,retain) NSString *UpdaCartPrice;
-(void)tableCell:(UITableViewCell*)cell UpdateItemAtIndex:(int)orderItemIndex;

//@property (nonatomic) int UpdateOrderItemIndex;
//@property (nonatomic,retain) NSArray *getsumOfOrderItemPrice;
@property (nonatomic,retain) NSString *EditTag;

@property (retain, nonatomic) IBOutlet UILabel *lbltransportation;
@property (retain, nonatomic) IBOutlet UILabel *lblinstallation;
@property (retain, nonatomic) IBOutlet UILabel *lblinstallationName;
@property (retain, nonatomic) IBOutlet UILabel *lblinstallationAddress;
@property (retain, nonatomic) IBOutlet UILabel *lbldate;
@property (retain, nonatomic) IBOutlet UILabel *lbltime;
@property (retain, nonatomic) IBOutlet UILabel *lbltel;
@property (retain, nonatomic) IBOutlet UILabel *lblfax;
@property (retain, nonatomic) IBOutlet UILabel *lblcontact;
@property (retain, nonatomic) IBOutlet UILabel *lblorderref;
@property (retain, nonatomic) IBOutlet UITextField *txtorderref;

@property (retain, nonatomic) IBOutlet UITextField *txtPO;
@property (retain, nonatomic) IBOutlet UILabel *lblPO;

@property (retain, nonatomic) IBOutlet UILabel *lblcusRemark;
@property (retain, nonatomic) IBOutlet UITextField *txtcusRemark;
@property (retain, nonatomic) IBOutlet UITextView *ShippingAddressTextView;

@property (retain, nonatomic) IBOutlet UIButton *changeDiscountTypeButton;
@property (retain, nonatomic) IBOutlet UITextField *txtDiscountValue;
@property (retain, nonatomic) IBOutlet UILabel *lblsymbolpercent;
@property (retain, nonatomic) IBOutlet UILabel *lblCalculator;
@property (retain, nonatomic) IBOutlet UIButton *BtnCalculator;
@property (retain, nonatomic) IBOutlet UIImageView *Calculatorbg;
@property (retain, nonatomic) IBOutlet UILabel *lblremark;
@property (retain, nonatomic) IBOutlet UILabel *lbltotal;
@property (retain, nonatomic) IBOutlet UILabel *lbldiscount;
@property (retain, nonatomic) IBOutlet UILabel *lblgrandprin;

@property (retain, nonatomic) IBOutlet UIButton *btnconfirm;
@property (retain, nonatomic) IBOutlet UIButton *btncancel;

@property (retain, nonatomic) IBOutlet UIButton *btnupdatecart;
@property (retain, nonatomic) IBOutlet UIButton *btnContinueOrder;
@property (retain, nonatomic) IBOutlet UIImageView *logoyourcart;
@property (retain, nonatomic) IBOutlet UIButton *btnordersummary;
@property (retain, nonatomic) IBOutlet UIImageView *bluebar;

@property (retain, nonatomic) IBOutlet UILabel *lbldescRemark;
@property (retain, nonatomic) IBOutlet UILabel *lblcontrolblind;
@property (retain, nonatomic) IBOutlet UILabel *lblquantityblind;
@property (retain, nonatomic) IBOutlet UILabel *lblsqftblind;
@property (retain, nonatomic) IBOutlet UILabel *lbltotalsqftblind;
@property (retain, nonatomic) IBOutlet UILabel *lblunitprincblind;
@property (retain, nonatomic) IBOutlet UILabel *lbldiscountblind;
@property (retain, nonatomic) IBOutlet UILabel *lbltotaltbl;

@property (retain, nonatomic) IBOutlet UILabel *lbldeliveryblind;
@property (retain, nonatomic) IBOutlet UITextField *txtdeliveryblind;

@property (retain, nonatomic) IBOutlet UILabel *lblremarkdescmain;
@property (retain, nonatomic) IBOutlet UILabel *lblqtymain;
@property (retain, nonatomic) IBOutlet UILabel *lblunitpricemain;
@property (retain, nonatomic) IBOutlet UILabel *lbldiscountmain;
@property (retain, nonatomic) IBOutlet UILabel *lbltotalmain;
-(void)listView;
-(void)galleryView;
@end




