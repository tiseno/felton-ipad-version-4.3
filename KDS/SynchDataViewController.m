//
//  SynchDataViewController.m
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SynchDataViewController.h"


@implementation SynchDataViewController
@synthesize DownloadImagesSwitch;
@synthesize CountDownTime,productTypes;
@synthesize lbldlimg,lblproduct,lblcustomerimg;
@synthesize customerSwitch,productSwitch,loadingView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //productSelected=NO;
        customerSelected=NO;
        productRecieved=NO;
        customerRecieved=NO;
        //DownloadImageSelected=NO;
        DownloadImageRecieved=NO;
        
        OrderPackageSelected=NO;
        OrderPackageRecieved=NO;
        
    }
    return self;
}
-(BOOL)NetworkStatus
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus==NotReachable);
}
- (void)dealloc
{
    [productTypes release];
    [lbldlimg release];
    [lblproduct release];
    [lblcustomerimg release];
    [productSwitch release];
    [customerSwitch release];
    [loadingView release];
    [DownloadImagesSwitch release];
    [super dealloc];
}

-(void)loadingViewshow
{
    BOOL isLoadingDisplayed=NO;
    for(UIView *subView in self.view.subviews)
    {
        if ([subView class]==[LoadingView class]) {
            isLoadingDisplayed=YES;
        }
    }
    if(!isLoadingDisplayed)
    {
        UIView *selfView=self.view;
        LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
        self.loadingView=temploadingView;
    }

}


- (void)updateTimer{ //Happens every time updateTimer is called. Should occur every second.
    
    counterA -= 1;
    CountDownTime = [NSString stringWithFormat:@"%i", counterA];
    
    if (counterA < 0) // Once timer goes below 0, reset all variables.
    {
        [timerA invalidate];
        startA = TRUE;
        //counterA = 1;
        loadData=YES;
        //NSLog(@"%@",_CountDownTime);
        
        if (loadData==YES) 
        {
            if ([productTypes isEqualToString:@"Blind"]) 
            {
                [self requestBlindProducts];
            }
            else if ([productTypes isEqualToString:@"Main"]) 
            {
                [self requestMainProducts];
            }
            
        }
        
        //[self.loadingView removeView];
    }
    
    
}

-(IBAction)startButtonTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        /*if(productSwitch.on || customerSwitch.on || DownloadImagesSwitch.on)
        {
            
            [self loadingViewshow];
            //Set the flags
            if(productSwitch.on)
            {
                                
                //if (dataItem) {
                    [self requestProducts];
                //}
                
            }
            else {
                productSelected=NO;
            }
            
            
            if(customerSwitch.on)
            {
                customerSelected=YES;
                [self requestCustomers];
            }else {
                customerSelected=NO;
            }
            
            if (DownloadImagesSwitch.on) {
                DownloadImageSelected=YES;
                [self requestDownloadImages];
            }else {
                DownloadImageSelected=NO;
            }
        
        
        }
        else {
            NSLog(@"no sync");
        }
       */
        [self loadingViewshow];
        counterA = 1;
        startA = TRUE;
        
        if(startA == TRUE) //Check that another instance is not already running.
        {
            CountDownTime = @"1";
            startA = FALSE;
            timerA = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
            
            
            
        }
        

        
        /*
        
        if(productSwitch.on)
        {
            [self requestProducts];
        }
        else if(customerSwitch.on)
        {
            [self requestCustomers];
        }
        KDSDataItem *dataItem=[[KDSDataItem alloc] init];
        [dataItem deleteAllitems];
        [dataItem release];*/
    }
    
}
-(IBAction)productCatalogueTapped
{
    
    
}
-(IBAction)customerInfoTapped
{
    
}

-(IBAction)startproductTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        [self loadingViewshow];
        counterA = 1;
        startA = TRUE;
        
        if(startA == TRUE) //Check that another instance is not already running.
        {
            CountDownTime = @"1";
            startA = FALSE;
            timerA = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
            
            productTypes=@"Main";
        }
    }
}

-(IBAction)startproductBlindTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        [self loadingViewshow];
        counterA = 1;
        startA = TRUE;
        
        if(startA == TRUE) //Check that another instance is not already running.
        {
            CountDownTime = @"1";
            startA = FALSE;
            timerA = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
            
            productTypes=@"Blind";
        }
    }
}


-(IBAction)startcustomerTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        [self loadingViewshow];
        [self requestCustomers];
        

    }
}

-(IBAction)startproductimageTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        [self loadingViewshow];
        [self requestDownloadImages];
    }
}

-(IBAction)startproductimageTappedblind
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        [self loadingViewshow];
        [self requestDownloadImagesblind];
    }
}

-(IBAction)startPackageIDTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        [self loadingViewshow];
        [self requestOrderPackage];
    }
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    productSwitch.hidden=YES;
    customerSwitch.hidden=YES;
    DownloadImagesSwitch.hidden=YES;
    
    lblproduct.text=@"Product Info";
    lbldlimg.text=@"|";
    lblcustomerimg.text=@"Customer Info";
    
    
} 


-(void)deleteitemsload
{
    KDSDataItem *dataItem=[[KDSDataItem alloc] init];
    [dataItem deleteAllitems];
    [dataItem release];
}

-(IBAction)removeMainProductTapped
{
    [self loadingViewshow];
    
    [self performSelector:@selector(delayremoveMain) withObject:nil afterDelay:1];
}

-(void)delayremoveMain
{
    KDSDataItem *dataItem=[[KDSDataItem alloc] init];
    [dataItem deleteMainitems];
    [dataItem release];
    
    
    [self performSelector:@selector(removeLoading) withObject:nil afterDelay:5];
}

-(IBAction)removeBlindProductTapped
{
    [self loadingViewshow];
    
    
    [self performSelector:@selector(delayremoveBlind) withObject:nil afterDelay:1];
}

-(void)delayremoveBlind
{
    KDSDataItem *dataItem=[[KDSDataItem alloc] init];
    [dataItem deleteBlinditems];
    [dataItem release];
    
    [self performSelector:@selector(removeLoading) withObject:nil afterDelay:5];
}

-(void)removeLoading
{
    [self.loadingView removeView];
    [self alertcloseapp];
}

- (void)viewDidUnload
{
    [self setDownloadImagesSwitch:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(void)requestProducts
{
    /**/
    
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    [networkHandler setDelegate:self];
    //AllItemDetailsRequest *request2=[[AllItemDetailsRequest alloc] init];
    ActiveCaseItemsRequest *request2=[[ActiveCaseItemsRequest alloc] init];
    
    [networkHandler request:request2];
    [request2 release];
    [networkHandler release];
}

-(void)requestBlindProducts
{
    /**/
    
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    [networkHandler setDelegate:self];
    //AllItemDetailsRequest *request2=[[AllItemDetailsRequest alloc] init];
    ActiveCaseblindItemsRequest *request2=[[ActiveCaseblindItemsRequest alloc] init];
    
    [networkHandler request:request2];
    [request2 release];
    [networkHandler release];
}

-(void)requestMainProducts
{    
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    [networkHandler setDelegate:self];
    //AllItemDetailsRequest *request2=[[AllItemDetailsRequest alloc] init];
    ActiveCasemainItemsRequest *request2=[[ActiveCasemainItemsRequest alloc] init];
    
    [networkHandler request:request2];
    [request2 release];
    [networkHandler release];
}

-(void)requestCustomers
{
    KDSAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    [networkHandler setDelegate:self];
    CustomersBySalespersonIDRequest *request=[[CustomersBySalespersonIDRequest alloc] initWithSalesPersonID:appDelegate.loggedInSalesPerson.SalesPerson_Code];
    [networkHandler request:request];
    [request release];
}
-(void)requestDownloadImages
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    [networkHandler setDelegate:self];
    DownloadItemImagesRequest *request=[[DownloadItemImagesRequest alloc]initWithItemTypes:@"Main"];
    [networkHandler request:request];
    [request release];
}

-(void)requestDownloadImagesblind
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    [networkHandler setDelegate:self];
    DownloadItemImagesRequest *request=[[DownloadItemImagesRequest alloc]initWithItemTypes:@"Blinds"];
    [networkHandler request:request];
    [request release];
}

-(void)requestOrderPackage
{
    KDSAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    [networkHandler setDelegate:self];

    OrderPackagingRequest *request2=[[OrderPackagingRequest alloc] initWithSalespersonID:appDelegate.loggedInSalesPerson.SalesPerson_Code Salesdatestart:@"20100101" Salesdateend:@"20151231"];
    
    [networkHandler request:request2];
    [request2 release];
    [networkHandler release];
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
         if(responseMessage!=nil && [responseMessage isKindOfClass:[CustomersBySalespersonIDResponse class]])
        {
            KDSAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
            KDSDataCustomer *dataCustomer=[[KDSDataCustomer alloc] init];
            [dataCustomer insertCustomerArray:((CustomersBySalespersonIDResponse*)responseMessage).customers forSalespersonID:appDelegate.loggedInSalesPerson.SalesPerson_Code];
            [dataCustomer release];
            customerRecieved=YES;
        }
        else if(responseMessage!=nil && [responseMessage isKindOfClass:[AllItemDetailsResponse class]])
        {
            //[self deleteitemsload];
            KDSDataItem *dataItem=[[KDSDataItem alloc] init];
            
                [dataItem insertItemArray:((AllItemDetailsResponse*)responseMessage).items];
                [dataItem release];
                
                if (dataItem) {
                    productRecieved=YES;
                    
                }else {
                    productRecieved=NO;
                }
                //if(customerSelected){
                    [self requestCustomers];
                //}

            
                        
        }else if (responseMessage!=nil && [responseMessage isKindOfClass:[DownloadItemImagesReaponse class]]) 
        {
            /*NSString *feedBackResult=((NewOrderTransferResponse*)responseMessage).getOrderResponse;*/
            NSLog(@"ok");
            
            DownloadImageRecieved=YES;
            //[self.loadingView removeView];
        }
        else if (responseMessage!=nil && [responseMessage isKindOfClass:[OrderPackagingResponse class]]) 
        {
            
            /*KDSDateOrderPackage *OrderPackage=[[KDSDateOrderPackage alloc]init];
             [OrderPackage deletOrderpackage];            
             [OrderPackage insertOrderPackageArray:((OrderPackagingResponse*)responseMessage).OrderPackage];
             OrderPackageRecieved=YES;
             [OrderPackage release];*/
            
            
            KDSDataSalesOrder *OrderPackage=[[KDSDataSalesOrder alloc]init];
          
            [OrderPackage updateOrderPackageArray:((OrderPackagingResponse*)responseMessage).OrderPackage];
            
            OrderPackageRecieved=YES;
            [OrderPackage release];
            
            
            
            NSLog(@"ok");
            
            
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Server Down. Please try again later. Thank you." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            [self.loadingView removeView];
            NSLog(@"server down1~");
        }
    
    
        
    /*else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Server Down. Please try again later. Thank you." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        
        [self.loadingView removeView];
        NSLog(@"server down1~");
    }*/
    
    
    /*if((productRecieved && !customerSelected && !DownloadImageSelected) || (customerRecieved && !productSelected && !DownloadImageSelected) ||
       (productRecieved && customerSelected && !DownloadImageSelected) || (!productRecieved && !customerSelected && DownloadImageRecieved) ||
       (!productSelected && customerSelected && DownloadImageRecieved) || (customerRecieved && productRecieved && DownloadImageRecieved))
    if ((customerRecieved && productRecieved && DownloadImageRecieved))
    {
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        appDelegate.isSynced=YES;
        [self.loadingView removeView];
    }*/
    
    
    if (productRecieved )
    {
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        appDelegate.isSynced=YES;
        [self.loadingView removeView];
        [self alertcloseapp];
        
    }else if (customerRecieved)
    {
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        appDelegate.isSynced=YES;
        [self.loadingView removeView];
        [self alertcloseapp];
        
    }else if ( DownloadImageRecieved)
    {
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        appDelegate.isSynced=YES;
        [self.loadingView removeView];
        [self alertcloseapp];
        
    }else if ( OrderPackageRecieved)
    {
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        appDelegate.isSynced=YES;
        [self.loadingView removeView];
        [self alertcloseapp];
    }

}

-(void)alertcloseapp
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Kindly close the APP after sync - Double click home button to close Felton APP. Thank you." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

@end
