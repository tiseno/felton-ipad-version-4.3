//
//  ItemSelecteddelegate.h
//  KDS
//
//  Created by Tiseno Mac 2 on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Item.h"

@protocol ItemSelecteddelegate <NSObject>
//-(void)editSaleOrderForEditButton;
//-(void)reloadNewOrderTable;
//-(void)editSaleOrderPDFButton:(SalesOrder*)isaleOrder SalesPersonCode:(SalesPerson*)isalesPersonCode;
//-(void)editSaleOrderPDFButton:(SalesOrder *)isaleOrder SalesPersonCode:(NSString *)isalesPersonCode;
-(void)productImageselected:(Item *)iitem ;
@end
 