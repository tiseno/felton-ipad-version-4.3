//
//  BlindSalesOrderItem.m
//  KDS
//
//  Created by Jermin Bazazian on 12/8/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "BlindSalesOrderItem.h"


@implementation BlindSalesOrderItem
@synthesize Width=_Width,Height=_Height,Quantity_in_Set=_Quantity_in_Set,Control=_Control,Color=_Color;
//@synthesize blindItems;
@synthesize SalesOrderItems;

-(void)dealloc
{
    //[blindItems release];
    
    [_Control release];
    [_Color release];
    [super dealloc];
}
-(void)addInheritanceDetails:(GDataXMLElement*)salesOrderItemNode
{
    GDataXMLElement* widthElement=[GDataXMLNode elementWithName:@"Width" stringValue:[NSString stringWithFormat:@"%.2f",self.Width]];
    [salesOrderItemNode addChild:widthElement];
    GDataXMLElement* heightElement=[GDataXMLNode elementWithName:@"Height" stringValue:[NSString stringWithFormat:@"%.2f",self.Height]];
    [salesOrderItemNode addChild:heightElement];
    GDataXMLElement* quanityInSetElement=[GDataXMLNode elementWithName:@"QuantityInSet" stringValue:[NSString stringWithFormat:@"%.2f",self.Quantity_in_Set]];
    [salesOrderItemNode addChild:quanityInSetElement];
    NSString *Controls=[self.Control stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* controlElement=[GDataXMLNode elementWithName:@"Control" stringValue:Controls];
    [salesOrderItemNode addChild:controlElement];
    NSString *Colors=[self.Color stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* colorElement=[GDataXMLNode elementWithName:@"Color" stringValue:Colors];
    [salesOrderItemNode addChild:colorElement];
    //GDataXMLElement* SpecialItemDesc=[GDataXMLNode elementWithName:@"SpeDesc" stringValue:SalesOrderItems.SpecialItemDescription];
    //[salesOrderItemNode addChild:SpecialItemDesc];
}

-(void)addInheritancePDFDetails:(GDataXMLElement*)salesOrderItemNode
{
    GDataXMLElement* widthElement=[GDataXMLNode elementWithName:@"Width" stringValue:[NSString stringWithFormat:@"%.2f",self.Width]];
    
    GDataXMLElement* heightElement=[GDataXMLNode elementWithName:@"Height" stringValue:[NSString stringWithFormat:@"%.2f",self.Height]];
    
    GDataXMLElement* quanityInSetElement=[GDataXMLNode elementWithName:@"QuantityInSet" stringValue:[NSString stringWithFormat:@"%.2f",self.Quantity_in_Set]];
    NSString *Controls=[self.Control stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* controlElement=[GDataXMLNode elementWithName:@"Control" stringValue:Controls];
    NSString *Colors=[self.Color stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* colorElement=[GDataXMLNode elementWithName:@"Color" stringValue:Colors];
    
    
    /**/[salesOrderItemNode addChild:widthElement];
    [salesOrderItemNode addChild:heightElement];
    [salesOrderItemNode addChild:quanityInSetElement];
    [salesOrderItemNode addChild:controlElement];
    [salesOrderItemNode addChild:colorElement];
}
@end
