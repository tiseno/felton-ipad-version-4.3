//
//  KDSDataSalesPerson.h
//  KDS
//
//  Created by Jermin Bazazian on 12/12/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KDSDataLayerBase.h"
#import "SalesPerson.h"


@interface KDSDataSalesPerson : KDSDataLayerBase {
    
    BOOL runBatch;
}
@property (nonatomic)BOOL runBatch;
-(DataBaseInsertionResult)insertSalesPerson:(SalesPerson*)salesPerson;
-(NSArray*)selectSalespeople;
-(SalesPerson*)selectSalesPersonWithUsername:(NSString*)username Password:(NSString*)password;
-(DataBaseUpdateResult)updateSalesPerson:(SalesPerson*)salesPerson;
-(DataBaseInsertionResult)insertSalesPersonArray:(NSArray*)salesPersonArr;
-(DataBaseUpdateResult)updateSalesPersonId:(SalesPerson*)salesPerson;

@end
