//
//  OrderTransferedViewController.h
//  KDS
//
//  Created by Tiseno Mac 2 on 6/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransferedOrderTableViewController.h"
#import "NewOrderTableEditDelegate.h"
#import "TransferedOrderDeleteDelegate.h"
#import "YourCartViewController.h"
#import "SalesOrder.h"
#import "SalesOrderType.h"
#import "NetworkHandler.h"
#import "TransferOrderPdfRequest.h"
#import "TransferOrderPDFResponse.h"
#import "QuotationFormPdfRequest.h"
#import "QuotationFormPdfResponse.h"
#import "XMLResponse.h"
#import <MessageUI/MessageUI.h>
#import "LoadingView.h"
#import "Reachability.h"
#import "DeletedCartViewController.h"

#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1
@class UIDatePicker;


@interface OrderTransferedViewController : UIViewController<NetworkHandlerDelegate, TransferedOrderDeleteDelegate, UIAlertViewDelegate,MFMailComposeViewControllerDelegate>{
      
}  

@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic) BOOL isPickerHidden;

@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) SalesOrder* salesOrder;
@property (nonatomic, retain) SalesOrder* selectedOrder;
@property (nonatomic, retain) UIView *newOrderTableViewHeader;
@property (nonatomic, retain) UIView *transferedOrderTableViewHeader;
@property (retain, nonatomic) IBOutlet UITableView *transferedOrderTableView;
@property (nonatomic, retain) NSArray* salesOrderArr;
@property (retain, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, retain) TransferedOrderTableViewController *transferedOrderViewController;
-(void)initializeTableDataR;
@property (retain, nonatomic) IBOutlet UIButton *BtnDeleteOrderByDate;


@end
