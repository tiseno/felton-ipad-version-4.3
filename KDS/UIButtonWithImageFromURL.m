//
//  UIButtonWithImageFromURL.m
//  Holiday Villa
//
//  Created by Jermin Bazazian on 5/18/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "UIButtonWithImageFromURL.h"


@implementation UIButtonWithImageFromURL

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}
-(void)handleRecieveImage:(UIImage*)image sender:(AsyncImageView*)asyncImageView
{
    [self setImage:image forState:UIControlStateNormal];
}
@end
