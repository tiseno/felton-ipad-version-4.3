//
//  Item.m
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Item.h"


@implementation Item

@synthesize Item_No = _Item_No, Description = _Description, FMT_Item_No = _FMT_Item_No, Category = _Category, Stock_Unit = _Stock_Unit, Image_Path = _Image_Path;
@synthesize UOMAndConversion = _UOMAndConversion, Default_Price = _Default_Price, Item_id = _Item_id, LastSellingPrices=_LastSellingPrices;
@synthesize edit_min_Height,edit_min_SqFt,edit_total_SqFt;
-(void)dealloc
{
    [_Item_No release];
    [_Description release];
    [_FMT_Item_No release];
    [_Category release];
    [_Stock_Unit release];
    [_Image_Path release];
    [_UOMAndConversion release];
    [_LastSellingPrices release];
    [_Default_Price release];
    [super dealloc];
}
-(NSArray*)lastSellingPriceForCustomer:(NSString*)customerCode
{
    if(self.LastSellingPrices != nil)  
    {
        NSArray* lastSellingPriceForCustomer=[self.LastSellingPrices objectForKey:customerCode];
        return  lastSellingPriceForCustomer; 
    }
    return  nil;
} 

-(void)addLastSellingPriceArr:(NSArray*)lastSellingPriceArr forCustomerCode:(NSString*)customerCode
{
    if(self.LastSellingPrices==nil)
    {
        NSMutableDictionary *tlastSellingPrice=[[NSMutableDictionary alloc] init];
        self.LastSellingPrices=tlastSellingPrice;
        [tlastSellingPrice release];
    }
    NSArray* lastSellingPriceForCustomer=[self lastSellingPriceForCustomer:customerCode];
    if(lastSellingPriceForCustomer==nil)
    {
        [self.LastSellingPrices setObject:lastSellingPriceArr forKey:customerCode];
    }
    else
    {
        [self.LastSellingPrices removeObjectForKey:customerCode];
        [self.LastSellingPrices setObject:lastSellingPriceArr forKey:customerCode];
    }
}
-(void)handleRecieveImage:(UIImage *)image sender:(AsyncImageView *)sender
{ 
    if(image != nil)
    {
        NSData* pngData = UIImagePNGRepresentation(image);
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsPath = [paths objectAtIndex:0];
        NSString* filePath = [documentsPath stringByAppendingPathComponent:self.Image_Path];
        [pngData writeToFile:filePath atomically:YES];
        NSLog(@"Image Saved");
    }
    
}
@end
