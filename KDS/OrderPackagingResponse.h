//
//  OrderPackagingResponse.h
//  KDS
//
//  Created by Tiseno Mac 2 on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"
@interface OrderPackagingResponse : XMLResponse
{
    NSArray *OrderPackage;
}

@property (nonatomic, retain) NSArray *OrderPackage;
@end
