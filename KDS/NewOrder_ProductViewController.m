//
//  NewOrder_ProductViewController.m
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewOrder_ProductViewController.h"
@interface NewOrder_ProductViewController ()

@end

@implementation NewOrder_ProductViewController
@synthesize PrefixOnOff;
@synthesize listGallery;
@synthesize btnViewYourCart;
@synthesize btnCancelOrder;
@synthesize customerDetailtable,tableViewHeader;
@synthesize SelectedItem;

@synthesize customerNameLabel, companyNameLabel, telLabel, faxLabel, emailLabel, currentSelectedCustomer,category,itemScrollView, selectedItem, search, productsArr, productsArr_Copied, searchById, searchMethodSc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.currentSelectedCustomer = ((KDSAppDelegate*)[UIApplication sharedApplication].delegate).currentCustomer;
    }
    return self;
}

- (void)dealloc
{
    //[tableViewHeader release];
    //[productsArr release];
    [SelectedItem release];
    [searchMethodSc release];
    [productsArr_Copied release];
    [search release];
    [customerNameLabel release];
    [companyNameLabel release];
    [telLabel release];
    [faxLabel release];
    [emailLabel release];
    [category release];
    [selectedItem release];
    [currentSelectedCustomer release];
    [PrefixOnOff release];
    [listGallery release];
    [btnViewYourCart release];
    [btnCancelOrder release];
    [customerDetailtable release];
    [super dealloc];
}
-(void)initializeCategoryButtons:(NSArray*) itemArr
{
    //NSLog(@"%@",itemArr);
    int widthOffset = 0;
    int yOffset=0;
    int xOffset=2;
    for(int i=0;i<itemArr.count;i++) 
    {
        
        if(i!=0 && (i%2)==0)
        {
            yOffset=0;
            xOffset+=180;
        } 
        Item* item=[itemArr objectAtIndex:i];
        ButtonWithProduct *categoryButton_up = [[ButtonWithProduct alloc] init];
        //UIImage* image=nil;
        NSString* filePath=nil;
        if(item.Image_Path!=nil && ![item.Image_Path isEqualToString:@""])
        {
            NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString* documentsPath = [paths objectAtIndex:0];
            filePath = [documentsPath stringByAppendingPathComponent:item.Image_Path];
            NSData* pngData = [NSData dataWithContentsOfFile:filePath];
            UIImage* image = [UIImage imageWithData:pngData]; 
            
            [categoryButton_up setBackgroundImage:image forState:UIControlStateNormal];
            //NSLog(@"image loaded");
            
        }
        else {
            UIImage* image = [UIImage imageNamed:@"blinds.png"];
            filePath=@"blinds.png";
            [categoryButton_up setBackgroundImage:image forState:UIControlStateNormal];
        }
        
        categoryButton_up.item = item;
        //categoryButton_up.frame = CGRectMake(1+widthOffset*xOffset, yOffset, 137, 137);
        categoryButton_up.frame = CGRectMake(xOffset+3, 2+widthOffset*yOffset+2, 131, 133);
        
        [categoryButton_up addTarget:self action:@selector(productImageTapped:) forControlEvents:UIControlEventTouchUpInside];
        //[categoryButton_up performSelector:@selector(productImageTapped:) withObject:filePath afterDelay:0.1 ];
        
        
        UIImageView *imgview =[[UIImageView alloc]initWithFrame:CGRectMake(xOffset, 2+widthOffset*yOffset, 137, 137)];
        imgview.image=[UIImage imageNamed:@"image_base.png"];
        
        /**/UILabel *orderIdLabel_up = [[UILabel alloc] initWithFrame:CGRectMake(xOffset, 2+widthOffset*yOffset+137, 137, 21)];
        orderIdLabel_up.adjustsFontSizeToFitWidth = YES;
        orderIdLabel_up.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
        orderIdLabel_up.minimumFontSize=7;
        orderIdLabel_up.textAlignment=UITextAlignmentCenter;
        orderIdLabel_up.textColor = [UIColor blackColor];
        orderIdLabel_up.backgroundColor = [UIColor clearColor];
        orderIdLabel_up.text=item.Item_No; 
        
        UILabel *categoryLabel_up = [[UILabel alloc] initWithFrame:CGRectMake(xOffset, 2+widthOffset*yOffset+155, 137, 21)];
        categoryLabel_up.adjustsFontSizeToFitWidth = NO;
        categoryLabel_up.font= [UIFont fontWithName:@"Helvetica" size:12];
        categoryLabel_up.textAlignment=UITextAlignmentCenter;
        categoryLabel_up.textColor = [UIColor blackColor];
        categoryLabel_up.backgroundColor = [UIColor clearColor];
        categoryLabel_up.text=item.Description;
        
        
        
        widthOffset=185;
        //xOffset++;
        yOffset++;
        
        [self.itemScrollView addSubview:imgview];
        [self.itemScrollView addSubview:categoryButton_up];
        /**/[self.itemScrollView addSubview:orderIdLabel_up];
        [self.itemScrollView addSubview:categoryLabel_up];
        
        //[categoryLabel_up release];
        //[imgview release];
        //[orderIdLabel_up release];
        //[categoryButton_up release];
        
    }
    self.itemScrollView.contentSize=CGSizeMake(xOffset+180, self.itemScrollView.frame.size.height);
    
}

/*-(void)initializeCategoryButtons:(NSArray*) itemArr
 {*/
/*KDSAppDelegate* appDelegate = (KDSAppDelegate*)[UIApplication sharedApplication].delegate;
 if(appDelegate.itemCategoryDictionary == nil)
 {
 [appDelegate loadProducts];
 }
 NSArray *itemArr = [appDelegate.itemCategoryDictionary objectForKey:self.category];*/
/*int widthOffset = 0;
 int yOffset=5;
 int xOffset=0;
 for(int i=0;i<itemArr.count;i++)
 {
 if(i!=0 && (i%5)==0)
 {
 yOffset+=182;
 xOffset=0;
 }
 Item* item=[itemArr objectAtIndex:i];
 ButtonWithProduct *categoryButton_up = [[[ButtonWithProduct alloc] init] autorelease];
 if(item.Image_Path!=nil && ![item.Image_Path isEqualToString:@""])
 {
 NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString* documentsPath = [paths objectAtIndex:0];
 NSString* filePath = [documentsPath stringByAppendingPathComponent:item.Image_Path];
 NSData* pngData = [NSData dataWithContentsOfFile:filePath];
 UIImage* image = [UIImage imageWithData:pngData];*/

/*if(![item.Image_Path isEqualToString:@""])
 {
 AsyncImageView *asyncImageView=[[AsyncImageView alloc] init];
 NSString *itemImageLocalPath=[item.Image_Path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
 NSString *itemImageURLPath=[NSString stringWithFormat:@"http://mobilesolutions.com.my/KDSCMS/images/products/%@",itemImageLocalPath];
 [asyncImageView setDelegate:categoryButton_up];
 [asyncImageView loadImageFromPath:itemImageURLPath];
 [asyncImageView release];
 }
 else
 {
 [categoryButton_up setTitle:item.Description forState:UIControlStateNormal];
 }*/
/*[categoryButton_up setBackgroundImage:image forState:UIControlStateNormal];
 NSLog(@"image loaded");
 }
 categoryButton_up.item = item;
 categoryButton_up.frame = CGRectMake(5+widthOffset*xOffset, yOffset, 137, 137);
 [categoryButton_up addTarget:self action:@selector(productImageTapped:) forControlEvents:UIControlEventTouchDown];
 
 UILabel *orderIdLabel_up = [[UILabel alloc] initWithFrame:CGRectMake(5+widthOffset*xOffset, yOffset+137, 137, 21)];
 orderIdLabel_up.adjustsFontSizeToFitWidth = YES;
 orderIdLabel_up.font = [UIFont fontWithName:@"Helvetica-Bold" size:9];
 orderIdLabel_up.minimumFontSize=7;
 orderIdLabel_up.textAlignment=UITextAlignmentCenter;
 orderIdLabel_up.textColor = [UIColor blackColor];
 orderIdLabel_up.backgroundColor = [UIColor clearColor];
 orderIdLabel_up.text=item.Item_No;
 
 UILabel *categoryLabel_up = [[UILabel alloc] initWithFrame:CGRectMake(5+widthOffset*xOffset, yOffset+160, 137, 21)];
 categoryLabel_up.adjustsFontSizeToFitWidth = YES;
 categoryLabel_up.minimumFontSize=7;
 categoryLabel_up.textAlignment=UITextAlignmentCenter;
 categoryLabel_up.textColor = [UIColor blackColor];
 categoryLabel_up.backgroundColor = [UIColor clearColor];
 categoryLabel_up.text=item.Description;
 widthOffset=187;
 xOffset++;
 
 [self.itemScrollView addSubview:categoryButton_up];
 [self.itemScrollView addSubview:orderIdLabel_up];
 [self.itemScrollView addSubview:categoryLabel_up];
 
 [categoryLabel_up release];
 //[orderIdLabel_up release];
 //[categoryButton_up release];
 
 }
 //self.itemScrollView.contentSize=CGSizeMake(self.itemScrollView.frame.size.width, yOffset+150);
 self.itemScrollView.contentSize=CGSizeMake(self.itemScrollView.frame.size.width, yOffset+150);
 
 }*/

-(IBAction)viewYourCartButtonTapped
{
    YourCartViewController *yourCartViewController=[[YourCartViewController alloc] initWithNibName:@"YourCartViewController" bundle:nil];
    yourCartViewController.title=@"Felton";
    [self.navigationController pushViewController:yourCartViewController animated:YES];
    [yourCartViewController release];
}
-(IBAction)cancelYourCartButtonTapped
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"Please Confirm cancelling your order." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
    [alert release];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        [appDelegate clearSalesOrder];
        
        UIViewController* newOrderCategoryViewController=nil;
        for(UIViewController* viewController in self.navigationController.viewControllers)
        {
            if([viewController isKindOfClass:[MenuViewController class]])
            {
                newOrderCategoryViewController=viewController;
                break;
            }
        }
        [self.navigationController popToViewController:newOrderCategoryViewController animated:YES];
    }
    
} 

-(IBAction)searchMethodScTapped
{
    if(self.searchMethodSc.selectedSegmentIndex==0)
    {
        searchById = YES;
        if(![search.text isEqualToString:@""] && search.text != nil)
        {
            [self searchBarSearchButtonClicked:search];
        }
    }
    else if(self.searchMethodSc.selectedSegmentIndex==1)
    {
        searchById = NO;
        //NSLog(@"%@", search.text);
        
        if(![search.text isEqualToString:@""] && search.text != nil)
        {
            [self searchBarSearchButtonClicked:search];
        }
    }
}

-(IBAction)ListGalleryTapped
{
    if(self.listGallery.selectedSegmentIndex==0)
    {
        
        
    }
    else if(self.listGallery.selectedSegmentIndex==1)
    {
        
        
    }
}

-(IBAction)productImageSelectedTapped:(id)sender
{
    /**/NewOrder_ConfirmViewController *orderConfirmViewController=[[NewOrder_ConfirmViewController alloc] initWithNibName:@"NewOrder_ConfirmViewController" bundle:nil];
     orderConfirmViewController.currentSelectedItem = ((ButtonWithProduct*)sender).item;
     orderConfirmViewController.title=@"Felton";
     [self.navigationController pushViewController:orderConfirmViewController animated:YES];
     [orderConfirmViewController release];
}

-(void)productImageselected:(Item *)iitem
{
    NSLog(@"clickkkk");
    NewOrder_ConfirmViewController *orderConfirmViewController=[[NewOrder_ConfirmViewController alloc] initWithNibName:@"NewOrder_ConfirmViewController" bundle:nil];
    orderConfirmViewController.currentSelectedItem = iitem;
    orderConfirmViewController.title=@"Felton";
    [self.navigationController pushViewController:orderConfirmViewController animated:YES];
    [orderConfirmViewController release];
}

-(IBAction)openPopup:(id)sender 
{
    //[MTPopupWindow showWindowWithHTMLFile:@"testContent.html" insideView:self.view];
}

-(IBAction)productImageTapped:(id)sender
{
    //[MTPopupWindow showWindowWithHTMLFile:((ButtonWithProduct*)sender).item insideView:self.view];
 
    
    /**/NewOrder_ConfirmViewController *orderConfirmViewController=[[NewOrder_ConfirmViewController alloc] initWithNibName:@"NewOrder_ConfirmViewController" bundle:nil];
    orderConfirmViewController.currentSelectedItem = ((ButtonWithProduct*)sender).item;
    orderConfirmViewController.title=@"Felton";
    [self.navigationController pushViewController:orderConfirmViewController animated:YES];
    [orderConfirmViewController release];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    listGallery.hidden=YES;
    btnViewYourCart.hidden=YES;
    btnCancelOrder.hidden=YES;
    searchById = YES;
    customerNameLabel.text = currentSelectedCustomer.Contact_Name;
    companyNameLabel.text = currentSelectedCustomer.Customer_Name;
    telLabel.text = currentSelectedCustomer.Phone_1;
    faxLabel.text = currentSelectedCustomer.Phone_2;
    emailLabel.text = currentSelectedCustomer.Email;
    listGallery.hidden=YES;
    
    KDSAppDelegate* appDelegate = (KDSAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.itemCategoryDictionary == nil)
    {
        [appDelegate loadProducts];
    } 
    NSArray *itemArr = [appDelegate.itemCategoryDictionary objectForKey:self.category];
    NSLog(@"%@",self.category);
    if(productsArr == nil)
    {
        productsArr = itemArr;
    }
    if(productsArr_Copied==nil)
    {
        NSMutableArray* tProductArr_Copied = [[NSMutableArray alloc]init];
        self.productsArr_Copied = tProductArr_Copied;
        [tProductArr_Copied release];
    }
    for(int i=0;i<[self.productsArr count];i++)
    {
        [productsArr_Copied addObject:[productsArr objectAtIndex:i]];
    }
    [self initializeCategoryButtons:itemArr];
    
    
    /**/UIView *TransferedOrdertableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(customerDetailtable.frame.origin.x, customerDetailtable.frame.origin.y-30, self.customerDetailtable.frame.size.width, 30)];
    TransferedOrdertableHeaderView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0];
    
    UILabel *TransferedOrderTableHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 5, self.customerDetailtable.frame.size.width/2, 21)];
    TransferedOrderTableHeaderLabel.text = @"Customer";
    TransferedOrderTableHeaderLabel.backgroundColor = [UIColor clearColor];
    TransferedOrderTableHeaderLabel.textColor = [UIColor whiteColor];
    TransferedOrderTableHeaderLabel.textAlignment = UITextAlignmentLeft;
    TransferedOrderTableHeaderLabel.font=[UIFont boldSystemFontOfSize:17];
    [TransferedOrdertableHeaderView addSubview:TransferedOrderTableHeaderLabel];
    tableViewHeader = TransferedOrdertableHeaderView;
    [self.view addSubview:tableViewHeader];
    
    
    [TransferedOrdertableHeaderView release];
    [TransferedOrderTableHeaderLabel release];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
}

- (void)viewDidUnload
{
    //[self setPrefixOnOff:nil];
    //[self setListGallery:nil];
    //[self setBtnViewYourCart:nil];
    //[self setBtnCancelOrder:nil];
    //[self setCustomerDetailtable:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

/**/-(void)viewDidDisappear:(BOOL)animated
{
    if([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        //order information will be removed here
        //order information will be removed here
        //order information will be removed here
        //order information will be removed here
        NSLog(@"order information will be removed here11");
        
    }
    //else {
    //    NSLog(@"order information will be removed here22");
    //}
    //[self.navigationController viewDidDisappear:animated];
    [super viewDidDisappear:animated];
}
-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(void)handleSearchForTerm:(NSString*)searchTerm
{
    
    [productsArr_Copied removeAllObjects];
    for(Item* item in productsArr)
    {
        if (PrefixOnOff.on) 
        {
            
            if(!searchById)
            {
                if([item.Description rangeOfString:searchTerm options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].location != NSNotFound)                    [productsArr_Copied addObject:item];
            }
            else 
            {
                if([item.Item_No rangeOfString:searchTerm options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].location != NSNotFound)                    [productsArr_Copied addObject:item];
            }
            
        }else 
        {
            
            if(!searchById)
            {
                if([item.Description rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location != NSNotFound)
                    [productsArr_Copied addObject:item];
            }
            else 
            {
                if([item.Item_No rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location != NSNotFound)
                    [productsArr_Copied addObject:item];
            }
            
        }
        
    }
    /**/for(UIView* view in itemScrollView.subviews)
    {
        [view removeFromSuperview];
    }
    [self initializeCategoryButtons:productsArr_Copied];
    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchTerm = [searchBar text];
    [self handleSearchForTerm:searchTerm];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText isEqualToString:@""] || searchText == nil)
    {
        /**/for(UIView* view in itemScrollView.subviews)
        {
            [view removeFromSuperview];
        }
        [self initializeCategoryButtons:productsArr];
        return;
    }
    [self handleSearchForTerm:searchText];
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    heightOfEditedView = searchBar.frame.size.height;
    heightOffset = searchBar.frame.origin.y;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, -250, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
}

@end
