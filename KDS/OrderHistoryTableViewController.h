//
//  OrderHistoryTableViewController.h
//  KDS
//
//  Created by Tiseno on 12/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KDSAppDelegate.h"
#import "KDSDataSalesOrder.h"
#import "OrderHistoryTableCellViewController.h"
#import "KDSDataBlindSalesOrder.h"

@interface OrderHistoryTableViewController : UITableViewController <UITableViewDelegate,
UITableViewDataSource> {
    
    NSArray* orderHistoryArr;
    SalesPerson* loggedInSalesPerson;
}
@property (nonatomic,retain) NSArray* orderHistoryArr;
@property (nonatomic, retain) SalesPerson* loggedInSalesPerson;
-(void)orderHistoryForCustomer:(Customer*)customer;
@end
