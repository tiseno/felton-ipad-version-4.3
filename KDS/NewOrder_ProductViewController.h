//
//  NewOrder_ProductViewController.h
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewOrder_ConfirmViewController.h"
#import "YourCartViewController.h"
#import "KDSAppDelegate.h"
#import "Item.h"
#import "ButtonWithProduct.h"
//#import "ProductCatalogViewController.h"
#import "MTPopupWindow.h"
#import "ItemSelecteddelegate.h"

@interface NewOrder_ProductViewController : UIViewController <UISearchBarDelegate,ItemSelecteddelegate>{
    
    UILabel *customerNameLabel;
    UILabel *companyNameLabel;
    UILabel *telLabel;
    UILabel *faxLabel;
    UILabel *emailLabel; 
    
    Customer *currentSelectedCustomer;
    NSString* category;
    
    int heightOfEditedView;
    int heightOffset;
    BOOL searchById;
    
    UISearchBar* search;
    UIScrollView *itemScrollView;
    
    UISegmentedControl* searchMethodSc;
    
    Item* selectedItem;
    
    NSArray* productsArr;
    NSMutableArray* productsArr_Copied;
}
@property (nonatomic) BOOL searchById;
@property (nonatomic, retain) IBOutlet UISegmentedControl* searchMethodSc;
@property (nonatomic, retain) NSArray* productsArr;
@property (nonatomic, retain) NSMutableArray* productsArr_Copied;
@property (nonatomic, retain) IBOutlet UISearchBar* search;
@property (nonatomic, retain) IBOutlet UILabel *customerNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *companyNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *telLabel;
@property (nonatomic, retain) IBOutlet UILabel *faxLabel;
@property (nonatomic, retain) IBOutlet UILabel *emailLabel;
@property (nonatomic, retain) Customer *currentSelectedCustomer;
@property (nonatomic, retain) Item* selectedItem;
@property (nonatomic, retain) NSString* category;
@property (nonatomic, retain) IBOutlet UIScrollView *itemScrollView;
//@property (retain, nonatomic) IBOutlet UISwitch *PrefixOnOff;
@property (retain, nonatomic) IBOutlet UISwitch *PrefixOnOff;
@property (retain, nonatomic) IBOutlet UISegmentedControl *listGallery;
@property (retain, nonatomic) IBOutlet UIButton *btnViewYourCart;
@property (retain, nonatomic) IBOutlet UIButton *btnCancelOrder;
@property (retain, nonatomic) IBOutlet UITableView *customerDetailtable;
@property (nonatomic, retain) UIView *tableViewHeader;
@property (nonatomic, retain) Item *SelectedItem;

@end
