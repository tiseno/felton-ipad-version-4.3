//
//  NewOrderViewController.m
//  KDS
//
//  Created by Tiseno on 1/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "NewOrderViewController.h"
#import <QuartzCore/QuartzCore.h>


@implementation NewOrderViewController
@synthesize listGallery;
@synthesize customerDetailtable,tableViewHeader;
@synthesize _CountDownTime,selfView;
//@synthesize secondsA;
@synthesize companyNameLabel, customerNameLabel, telLabel, faxLabel, emailLabel, productTableView, search, searchMethodSC, allNames, keys, names, searchById, searchByName;
@synthesize PrefixOnOff,loadingView;
//@synthesize appDelegate;
//@synthesize lstGallery;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        loadData=NO;
    }
    return self; 
}

- (void)dealloc
{
    //[lstGallery release];
    //[appDelegate release];
    //[selfView release];
    //[_CountDownTime release];
    [loadingView release];
    //[allNames release];
    [names release];
    [keys release];
    [search release];
    [searchMethodSC release];
    [productTableView release]; 
    [companyNameLabel release];
    [customerNameLabel release];
    [telLabel release];
    [faxLabel release];
    [emailLabel release];
    [PrefixOnOff release];
    [customerDetailtable release];
    //[tableViewHeader release];
    //NSLog(@"Address of 'keys' is after release is: %@", keys);
    //NSLog(@"Address of 'productTableView' is after release is: %@", productTableView);
    //NSLog(@"Address of 'searchMethodSC' is after release is: %@", searchMethodSC);
    //NSLog(@"Address of 'names' is after release is: %@", names);
    [listGallery release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    //if (keys !=nil) {
     //   keys=nil;
    //}
    
    
    
    // Release any cached data, images, etc that aren't in use.
}
-(IBAction)searchMethodScTapped
{
    /**/if(self.searchMethodSC.selectedSegmentIndex==0)
    {
        searchByName = YES;
        searchById = NO;
        if(![search.text isEqualToString:@""] && search.text != nil)
        {
            [self searchBarSearchButtonClicked:search];
            [productTableView reloadData];
        }
        
        
    }
    else if(self.searchMethodSC.selectedSegmentIndex==1)
    {
        searchByName = NO;
        searchById = YES;
        if(![search.text isEqualToString:@""] && search.text != nil)
        {
            [self searchBarSearchButtonClicked:search];
            [productTableView reloadData];
        }
    }
    
    /*if(self.searchMethodSC.selectedSegmentIndex == 0)
    {
        searchByName = NO;
        searchById = YES;
        
        if(![search.text isEqualToString:@""] && search.text != nil)
        {
            [self searchBarSearchButtonClicked:search];
            [productTableView reloadData];
        }
        
    }
    else if(self.searchMethodSC.selectedSegmentIndex == 1)
    {
        searchById = NO;
        searchByName = YES;
        
        if(![search.text isEqualToString:@""] && search.text != nil)
        {
            [self searchBarSearchButtonClicked:search];
            [productTableView reloadData];
        }
    }
    else if(self.searchMethodSC.selectedSegmentIndex == 2)//added new code here
    {
        searchById = NO;
        searchByName = NO;
        
        if(![search.text isEqualToString:@""] && search.text != nil)
        {
            [self searchBarSearchButtonClicked:search];
            [productTableView reloadData];
        }
    }*/
}
-(void)resetSearch
{
    NSMutableDictionary *allNamesCopy = [self.allNames MutableDeepCopy];
    self.names = allNamesCopy;
    //[allNamesCopy release];
    NSMutableArray *keyArray = [[NSMutableArray alloc] init];
    [keyArray addObjectsFromArray:[[self.allNames allKeys] sortedArrayUsingSelector:@selector(compare:)]];
    self.keys = keyArray;
    [keyArray release]; 
}
-(void)handleSearchForTerm:(NSString*)searchTerm
{
    NSMutableArray *sectionsToRemove = [[NSMutableArray alloc] init];
    [self resetSearch];
    
    for(NSString *key in self.keys)
    {
        NSMutableArray *array = [names valueForKey:key];
        NSMutableArray *toRemove = [[NSMutableArray alloc] init];
        
        for(Item* name in array)
        {
         /*   if(!searchById)
            {
                if([name.Description rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location == NSNotFound)
                    [toRemove addObject:name];
            }
            else
            {
                if([name.Item_No rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location == NSNotFound)
                    [toRemove addObject:name];
            }*/
            
            
            if (PrefixOnOff.on) 
            {
                if(searchById == NO && searchByName == YES)
                {
                    if([name.Description rangeOfString:searchTerm options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].location == NSNotFound)
                        [toRemove addObject:name];
                }
                else if(searchById == YES && searchByName == NO)
                {
                    if([name.Item_No rangeOfString:searchTerm options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].location == NSNotFound)
                        [toRemove addObject:name];
                }
                
            }else 
            {
                
                if(searchByName == NO && searchById == YES)
                {
                    if([name.Item_No rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location == NSNotFound)
                        [toRemove addObject:name];
                }
                else if(searchByName == YES && searchById == NO)
                {
                    if([name.Description rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location == NSNotFound)
                        [toRemove addObject:name];
                }
                 
                
            }
            
            
        }
        
        if([array count] == [toRemove count])
            
            [sectionsToRemove addObject:key];
        
        [array removeObjectsInArray:toRemove];
         
        [toRemove release];
        
    }
    [self.keys removeObjectsInArray:sectionsToRemove];
    [sectionsToRemove release];
    [productTableView reloadData];
}

- (void)updateTimer{ //Happens every time updateTimer is called. Should occur every second.
    
    counterA -= 1;
    _CountDownTime = [NSString stringWithFormat:@"%i", counterA];
    
    if (counterA < 0) // Once timer goes below 0, reset all variables.
    {
        [timerA invalidate];
        startA = TRUE;
        //counterA = 1;
        loadData=YES;
        NSLog(@"%@",_CountDownTime); 
        
        if (loadData==YES) 
        {
            //searchById = NO;
            KDSAppDelegate* appDelegate = (KDSAppDelegate*)[UIApplication sharedApplication].delegate;
            if(appDelegate.itemCategoryDictionary == nil)
            {
                
                [appDelegate loadProducts];
                
                 
            } 
            NSDictionary *dict = appDelegate.itemCategoryDictionary;
            self.allNames = dict;
            //[dict release];
            
            [self resetSearch];
            [productTableView reloadData];
            [productTableView setContentOffset:CGPointMake(0.0, 44.0) animated:NO];
        }
        
        [self.loadingView removeView];
        
        
    }
    
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    //searchById = YES;
    searchByName = YES;
    searchById = NO;
    listGallery.hidden=YES;
     
    KDSAppDelegate* appDelegate = (KDSAppDelegate*)[UIApplication sharedApplication].delegate; 
    //appDelegate = (KDSAppDelegate*)[UIApplication sharedApplication].delegate;
    customerNameLabel.text = appDelegate.currentCustomer.Contact_Name;
    companyNameLabel.text = appDelegate.currentCustomer.Customer_Name;
    telLabel.text = appDelegate.currentCustomer.Phone_1;
    faxLabel.text = appDelegate.currentCustomer.Phone_2;
    emailLabel.text = appDelegate.currentCustomer.Email;
    
    
    //lstGallery=appDelegate.loadListGallerySatatus;
    
    counterA = 1;
    startA = TRUE;
    
    if(startA == TRUE) //Check that another instance is not already running.
    {
        _CountDownTime = @"1";
        startA = FALSE;
        timerA = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
        
        
        
    }  
    //UIView *selfView=self.view;
    selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;

    
    
    UIView *TransferedOrdertableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(customerDetailtable.frame.origin.x, customerDetailtable.frame.origin.y-30, self.customerDetailtable.frame.size.width, 30)];
    TransferedOrdertableHeaderView.backgroundColor = [UIColor colorWithRed:0.0196 green:0.513 blue:0.949 alpha:1.0];
    
    UILabel *TransferedOrderTableHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 5, self.customerDetailtable.frame.size.width/2, 21)];
    TransferedOrderTableHeaderLabel.text = @"Customer";
    TransferedOrderTableHeaderLabel.backgroundColor = [UIColor clearColor];
    TransferedOrderTableHeaderLabel.textColor = [UIColor whiteColor];
    TransferedOrderTableHeaderLabel.textAlignment = UITextAlignmentLeft;
    TransferedOrderTableHeaderLabel.font=[UIFont boldSystemFontOfSize:17];
    [TransferedOrdertableHeaderView addSubview:TransferedOrderTableHeaderLabel];
    tableViewHeader = TransferedOrdertableHeaderView;
    [self.view addSubview:tableViewHeader];
    
    
    [TransferedOrdertableHeaderView release];
    [TransferedOrderTableHeaderLabel release];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
}

- (void)viewDidUnload
{
    /*[loadingView release];
    //[allNames release];
    [names release];
    [keys release];
    [search release];
    [searchMethodSC release];
    [productTableView release]; 
    [companyNameLabel release];
    [customerNameLabel release];
    [telLabel release];
    [faxLabel release];
    [emailLabel release];
    [PrefixOnOff release];
    [customerDetailtable release];*/

    //self.loadingView=nil;
    /*self.companyNameLabel=nil;
    self.customerNameLabel=nil;
    self.customerDetailtable=nil;
    self.telLabel=nil;
    self.faxLabel=nil;
    self.emailLabel=nil;*/
    //self 
    
    //[self setLoadingView:nil];
    
    /*
    [self setNames:nil];
    
    [self setPrefixOnOff:nil];
    [self setCompanyNameLabel:nil];
    [self setCustomerNameLabel:nil];
    //[self setCustomerDetailtable:nil];
    [self setTelLabel:nil];
    [self setFaxLabel:nil];
    [self setEmailLabel:nil];
    [self setSearch:nil];
    [self setCustomerDetailtable:nil];
    //[self setTableViewHeader:nil];
    //[self setAllNames:nil];
    [self setKeys:nil];
    [self setProductTableView:nil];
    [self setSearchMethodSC:nil];*/
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    //[self setListGallery:nil];
    [super viewDidUnload];
    
    // e.g. self.myOutlet = nil;
}
-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ([keys count] > 0)?[keys count] :1;
} 

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([keys count] == 0)
        return 0;
    NSString *key = [self.keys objectAtIndex:section];
    NSArray *nameSection = [self.names objectForKey:key];
    return [nameSection count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row]; 
    NSString *key = [self.keys objectAtIndex:section];
    NSArray *nameSection = [self.names objectForKey:key];
    
    static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
    
    ProductTableCell *cell = (ProductTableCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
    //ProductTableCell *cell = [[[ProductTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]retain];
    //ProductTableCell *cell = [[ProductTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier];
     
    if(cell == nil)
    {  
        //cell = [[ProductTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier];
        cell = [[[ProductTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
    }

    
    Item *item=[nameSection objectAtIndex:row];
    if(item.Default_Price!=nil &&item.Default_Price.count>0)
    {
        DefaultPriceAndUOM *defaultPrice=[item.Default_Price objectAtIndex:0];
        double dprice=[defaultPrice.Default_Price doubleValue];
        NSString *price=[NSString stringWithFormat:@"RM %.2f/ unit",dprice];
        cell.priceLabel.text = price;
    }else {
        NSString *price=[NSString stringWithFormat:@"RM 0.00/ unit"];
        cell.priceLabel.text = price;
    }
    
    cell.itemm = item;
    cell.productIdLabel.text = item.Item_No;
    cell.productNameLabel.text = item.Description;
    cell.selectionStyle = UITableViewCellEditingStyleNone;
    //[cell autorelease];
    return cell;// [cell autorelease];

}

-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if([keys count] == 0)
        return nil;
    NSString *key = [self.keys objectAtIndex:section];
    return key;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductTableCell *selectedcell= (ProductTableCell*)[tableView cellForRowAtIndexPath:indexPath]; 
    
    //NSLog(@"%@",selectedcell.itemm.Item_No);
    //NSLog(@"%@",selectedcell.itemm.Image_Path);
    
    /**/NewOrder_ConfirmViewController *orderConfirmViewController=[[NewOrder_ConfirmViewController alloc] initWithNibName:@"NewOrder_ConfirmViewController" bundle:nil];
    //ProductTableCell *cell = [[tableView visibleCells] objectAtIndex:indexPath.row];
     //NSLog(@"Row:%d - %@ - Name:%@",indexPath.row,cell.itemm.Category,cell.itemm.Description);
    //NSUInteger section = [indexPath section];
    //NSUInteger row = [indexPath row];
    //NSString *key = [self.keys objectAtIndex:section];
    //NSArray *nameSection = [self.names objectForKey:key];
    //Item *item=[nameSection objectAtIndex:row];
    //NSLog(@"Row:%d - %@ - Name:%@",indexPath.row,item.Category,item.Description);
    
    
    orderConfirmViewController.currentSelectedItem = selectedcell.itemm;
    orderConfirmViewController.title=@"Felton";
    [self.navigationController pushViewController:orderConfirmViewController animated:YES]; 
    [orderConfirmViewController release];
}
-(NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return keys;
}
-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [search resignFirstResponder];
    return indexPath;
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    heightOfEditedView = searchBar.frame.size.height;
    heightOffset = searchBar.frame.origin.y;
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, -250, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
}
#pragma mark -
#pragma mark Search Bar Delegate Methods
-(void)searchBarSearchButtonClicked:(UISearchBar *)searcchBar
{
    NSString *searchTerm = [searcchBar text];
    [self handleSearchForTerm:searchTerm];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0)
    {
        [self resetSearch];
        [productTableView reloadData];
        return;
    }
    [self handleSearchForTerm:searchText];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searcchBar
{
    search.text = @"";
    [self resetSearch];
    [productTableView reloadData];
    [searcchBar resignFirstResponder];
}

-(IBAction)GalleryTapped
{ 
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    KDSDataListGallery *dataItem=[[KDSDataListGallery alloc] init];
    [dataItem updateLogin:@"gallery"];
    
    SalesOrder* tempSalesOrder=appDelegate.salesOrder;
    [tempSalesOrder retain];
    
    NewOrderCategoryViewController *newOrderViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];
    appDelegate.salesOrder=tempSalesOrder;
    [tempSalesOrder release];
    
    newOrderViewController.title=@"Felton";
    [self.navigationController pushViewController:newOrderViewController animated:YES];

    [newOrderViewController release]; 

}

-(IBAction)ListGalleryTapped
{
    if(self.listGallery.selectedSegmentIndex==0)
    {
        /*UIViewController* newOrderCategoryViewController=nil;
        for(UIViewController* viewController in self.navigationController.viewControllers)
        {
            if([viewController isKindOfClass:[NewOrderCategoryViewController class]])
            {
                
                newOrderCategoryViewController=viewController;
                break;
            }
        }
        
        for (UIView* uiview in self.view.subviews) {
            [uiview removeFromSuperview];
        }*/

        
        NewOrderViewController *newOrderViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
        newOrderViewController.title=@"Felton";
        [self.navigationController pushViewController:newOrderViewController animated:YES];
        [newOrderViewController release];
        
                 /*//[self.navigationController popToViewController:newOrderCategoryViewController animated:YES];
         
         [self.navigationController pushViewController:newOrderCategoryViewController animated:YES];*/
        
    }
    else if(self.listGallery.selectedSegmentIndex==1)
    {
        
        
        /*
         
         */
        
         /*//[self.navigationController popToViewController:newOrderCategoryViewController animated:YES];
         [self.navigationController pushViewController:newOrderCategoryViewController animated:YES];
         
        
        
        for (UIView* uiview in self.view.subviews) {
            [uiview removeFromSuperview];
        }
        */
        
        //UIViewController *view1=[[UIViewController alloc]initWithNibName:@"ViewController1" bundle:nil];
        //[uiview removeFromSuperview];

        
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        SalesOrder* tempSalesOrder=appDelegate.salesOrder;
        [tempSalesOrder retain];

        NewOrderCategoryViewController *newOrderViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];
        //NewOrderViewController *newOrderCategoryViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
        appDelegate.salesOrder=tempSalesOrder;
        [tempSalesOrder release];

        newOrderViewController.title=@"Felton";
        
        
        /*UIViewController* newOrderCategoryViewController=nil;
        for(UIViewController* viewController in self.navigationController.viewControllers)
        {
            if([viewController isKindOfClass:[NewOrderCategoryViewController class]])
            {
                newOrderCategoryViewController=viewController;
                break;
            }
        }
        
        
        UIViewController* newOrderCategoryViewController=nil;
        for(UIViewController* viewController in self.navigationController.viewControllers)
        {
            if([viewController isKindOfClass:[MenuViewController class]])
            {
                newOrderCategoryViewController=viewController;
                break;
            }
        }
        [self.navigationController popToViewController:newOrderCategoryViewController animated:YES];
        */
        
        
        
        //[self.view addSubview:newOrderViewController.view];
        [self.navigationController pushViewController:newOrderViewController animated:YES];
        
        
        
        //[self presentModalViewController:newOrderViewController animated:YES];
        
        [newOrderViewController release];
        
        
        
    }
}

@end
