//
//  AllSalesPeopleRequest.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "AllSalesPeopleRequest.h"


@implementation AllSalesPeopleRequest
-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"GetArrayListOfAllSalespeople";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}
@end
