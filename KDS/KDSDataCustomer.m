//
//  KDSDataCustomer.m
//  KDS
//
//  Created by Tiseno on 12/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KDSDataCustomer.h"


@implementation KDSDataCustomer
@synthesize runBatch; 
-(void)dealloc
{
    [super dealloc]; 
}
-(DataBaseInsertionResult)insertCustomer:(Customer*)tCustomer
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *customerName=[tCustomer.Customer_Name stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *customerAddressStreet1=[tCustomer.Address.Street_1 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *customerAddressStreet2=[tCustomer.Address.Street_2 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *customerAddressStreet3=[tCustomer.Address.Street_3 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *customerAddressStreet4=[tCustomer.Address.Street_4 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *City=[tCustomer.Address.City stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        //NSString *customer_remarkss=[tCustomer.customer_remark stringByReplacingOccurrencesOfString:@"'" withString:@" "];
        
        NSString *customerCountry=[tCustomer.Address.Country stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *customerContactName=[tCustomer.Contact_Name stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *ZipCode=[tCustomer.Address.ZipCode stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        
        NSString *Phone_1=[tCustomer.Phone_1 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Phone_2=[tCustomer.Phone_2 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        
        NSString *PostalCode=[tCustomer.Address.PostalCode stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Email=[tCustomer.Email stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        
        
        NSString *insertSQL = [NSString stringWithFormat:@"insert into Customer(salesperson_code,city,contact_name,country,email,_id,name,phone_1,phone_2,postal_code,street_1,street_2,street_3,street_4,zip_code) values('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@');",tCustomer.salesperson_Code,City,customerContactName,customerCountry,Email,tCustomer.Customer_Id,customerName,Phone_1,Phone_2,PostalCode,customerAddressStreet1,customerAddressStreet2,customerAddressStreet3,customerAddressStreet4,ZipCode];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}
-(DataBaseUpdateResult)updateCustomer:(Customer*)tCustomer
{ 
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *Email=[tCustomer.Email stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *City=[tCustomer.Address.City stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Contact_Name=[tCustomer.Contact_Name stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Country=[tCustomer.Address.Country stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Customer_Name=[tCustomer.Customer_Name stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Phone_1=[tCustomer.Phone_1 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Phone_2=[tCustomer.Phone_2 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *PostalCode=[tCustomer.Address.PostalCode stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Street_1=[tCustomer.Address.Street_1 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Street_2=[tCustomer.Address.Street_2 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Street_3=[tCustomer.Address.Street_3 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *Street_4=[tCustomer.Address.Street_4 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *ZipCode=[tCustomer.Address.ZipCode stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        
        
        NSString *updateSQL = [NSString stringWithFormat:@"update Customer set salesperson_code='%@',city='%@',contact_name='%@',country='%@',email='%@',name='%@',phone_1='%@',phone_2='%@',postal_code='%@',street_1='%@',street_2='%@',street_3='%@',street_4='%@',zip_code='%@' where _id='%@';",tCustomer.salesperson_Code,City,Contact_Name,Country,Email,Customer_Name,Phone_1,Phone_2,PostalCode,Street_1,Street_2,Street_3,Street_4,ZipCode,tCustomer.Customer_Id];
        sqlite3_stmt *statement;
        const char *update_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, update_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            { 
                [self closeConnection];
            }
            return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseUpdateFailed;
}

/*-(void)updateCustomerRemark:(Customer*)tCustomer
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update Customer set Remarks='%@' where _id='%@';",tCustomer.customer_remark,tCustomer.Customer_Id];
        sqlite3_stmt *statement;
        const char *update_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, update_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            //return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        { 
            [self closeConnection];
        }
    }
    //return DataBaseUpdateFailed;
}

-(void)deleteAllitems
{
    NSArray *existingItems=[self selectItem];
    
    for(Item* eItem in existingItems)
    {
        [self deletItem:eItem];
    }
    
}*/

-(DataBaseDeletionResult)deletItem:(Customer*)tcustomer
{
    //NSString *insertSQL = [NSString stringWithFormat:@"delete from Item where item_id=%d;",tItem.Item_id];
    NSString *insertSQL = [NSString stringWithFormat:@"delete from Customer where _id=%d;",tcustomer.Customer_Id];
    sqlite3_stmt *statement;
    const char *insert_stmt = [insertSQL UTF8String];
    
    sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
    if(sqlite3_step(statement) == SQLITE_DONE)
    {
        sqlite3_finalize(statement);
        
        return DataBaseDeletionSuccessful;
    }
    NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
    sqlite3_finalize(statement);
    return DataBaseDeletionFailed;
}

-(DataBaseInsertionResult)insertCustomerArray:(NSArray*)customerArr forSalespersonID:(NSString*)salesPersonID
{
    self.runBatch=YES;
    DataBaseInsertionResult insertionResult=DataBaseInsertionSuccessful;
    NSArray *existingCustomers=[self selectCustomerWithSalespersonID:salesPersonID];
    [self openConnection];
    for(Customer* customer in customerArr)
    {
        BOOL foundAMatch=NO;
        for(Customer* eCustomer in existingCustomers)
        {
            if([customer.Customer_Id isEqualToString:eCustomer.Customer_Id])
            {
                customer.salesperson_Code=salesPersonID;
                [self updateCustomer:customer];
                foundAMatch=YES;
                break;
            }
        }
        if(!foundAMatch)
        {
            customer.salesperson_Code=salesPersonID;
            insertionResult=[self insertCustomer:customer];
            if(insertionResult==DataBaseInsertionFailed)
            {
                break;
            }
        }
    }
    [self closeConnection];
    return insertionResult;
}
-(NSArray*)selectCustomerWithSalespersonID:(NSString*)salespersonID
{
    Customer* tCustomer = nil;
    Address* tAddress = nil;
    NSMutableArray *customerArray = [[[NSMutableArray alloc] init] autorelease];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select salesperson_code,city,contact_name,country,email,_id,name,phone_1,phone_2,postal_code,street_1,street_2,street_3,street_4,zip_code from Customer where salesperson_code='%@'",salespersonID];
        //NSString *selectSQL=@"select count(_id) from Customer";
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            tCustomer=[[Customer alloc] init];
            tAddress=[[Address alloc]init];
            NSString *salesperson_code=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 0)];
            NSString *salesperson_codes=[salesperson_code stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tCustomer.salesperson_Code = salesperson_codes;
            
            NSString *city=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
            NSString *citys=[city stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tAddress.City = citys;
            
            NSString *contact_name=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            NSString *contact_names=[contact_name stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tCustomer.Contact_Name = contact_names;
            
            NSString *country=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
            NSString *countrys=[country stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tAddress.Country = countrys;
            
            NSString *email=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 4)];
            NSString *emails=[email stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tCustomer.Email = emails;
            
            NSString *_id=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 5)];
            
            tCustomer.Customer_Id = _id;
            
            NSString *name=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)];
            NSString *names=[name stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tCustomer.Customer_Name = names;
            
            NSString *phone_1=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
            NSString *phone_1s=[phone_1 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tCustomer.Phone_1 = phone_1s;
            
            NSString *phone_2=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
            NSString *phone_2s=[phone_2 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tCustomer.Phone_2 = phone_2s;
            
            NSString *postal_code=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 9)];
            NSString *postal_codes=[postal_code stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tAddress.PostalCode = postal_codes;
            
            NSString *street_1=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 10)];
            NSString *street_1s=[street_1 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tAddress.Street_1 = street_1s;
            
            NSString *street_2=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 11)];
            NSString *street_2s=[street_2 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tAddress.Street_2 = street_2s;
            
            NSString *street_3=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 12)];
            NSString *street_3s=[street_3 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tAddress.Street_3 = street_3s;
            
            NSString *street_4=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 13)];
            NSString *street_4s=[street_4 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tAddress.Street_4 = street_4s;
            
            NSString *zip_code=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 14)];
            NSString *zip_codes=[zip_code stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            tAddress.ZipCode = zip_codes;
            
                        
            /*if((char*)sqlite3_column_text(statement, 15)!=NULL)
            {
                NSString *cusRemark=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 15)];
                tCustomer.customer_remark = cusRemark;
            }  */

            
            tCustomer.Address = tAddress;
            [tAddress release];
            [customerArray addObject:tCustomer];
            [tCustomer release];
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    if([customerArray count]==0)
    {
        customerArray=nil;
    }
    return customerArray;
}
@end
