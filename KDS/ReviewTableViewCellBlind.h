//
//  ReviewTableViewCellBlind.h
//  KDS
//
//  Created by Tiseno on 4/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ReviewTableViewCellBlind : UITableViewCell {
    
    UILabel *itemIdLabel;
    UILabel *itemNameLabel;
    UILabel *itemWidthDrop;
    UILabel *itemSqrFeet;
    UILabel *itemQuantityLabel;
    UILabel *orderPriceLabel;
    UILabel *totalPriceLabel;
    UILabel *itemArea;
    UILabel *orderDiscount;
    UILabel *orderControl;
    UILabel *orderColor;
}
@property(nonatomic, retain) UILabel *itemIdLabel;
@property(nonatomic, retain) UILabel *itemNameLabel;
@property(nonatomic, retain) UILabel *itemWidthDrop;
@property(nonatomic, retain) UILabel *itemSqrFeet;
@property(nonatomic, retain) UILabel *itemQuantityLabel;
@property(nonatomic, retain) UILabel *orderPriceLabel;
@property(nonatomic, retain) UILabel *totalPriceLabel;
@property(nonatomic, retain) UILabel *itemArea;
@property(nonatomic, retain) UILabel *orderDiscount;
@property(nonatomic, retain) UILabel *orderControl;
@property(nonatomic, retain) UILabel *orderColor;

@end
