//
//  NewOrderUpdateConfirmView.m
//  KDS
//
//  Created by Tiseno Mac 2 on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NewOrderUpdateConfirmView.h"
#import <QuartzCore/QuartzCore.h>

//@interface NewOrderUpdateConfirmView ()

//@end

@implementation NewOrderUpdateConfirmView
@synthesize blindremarktextview;
@synthesize lblitemdesc,currentSelectedBlindSalesOrderItem;
@synthesize blinditemDesctextView;
@synthesize lblTypeofMeasurement,Bspecialitemdesc;
@synthesize customerdetaintable;
@synthesize lblprice;
@synthesize lbldescription;
@synthesize lblwidth;
@synthesize lblX;
@synthesize lblHeight;
@synthesize lblequal;
@synthesize lblcontrol;
@synthesize lblcolor;
@synthesize lblquantity;
@synthesize lblremark;
@synthesize discountTextField;
@synthesize discountTypeLabel;
@synthesize changeDiscountTypeButton;
@synthesize customerNameLabel, companyNameLabel, telLabel, faxLabel, emailLabel, totalLengthLabel, standardPriceLabel, productNameLabel, descriptionLabel, typesOfmeasurementsLabel,lblSelectedUOM, currentSelectedBlindItem, quantityOrderedLabel, formulaLabel;
@synthesize priceTextField, heightTextField, widthTextField, controlTextField, colorTextField, quantityTextField, remarkTextView,isOrderMixed,isOverAmount;
@synthesize orderTypeSegmentedControl, typesOfmeasurementPickerView, changeMeasurementButton,currentSelectedCustomer, currentSelectedItem, totalLength;
@synthesize backGroundImageView, productImageView , measurementArray, isPickerHidden;
@synthesize SelectedItemUpdate,UpDatetag;
@synthesize PriceUpdate,RemarkUpdate,QuantityUpdate;
@synthesize updateindexPath,totalDiscountOfItems,BHeight,BWidth,BColor,BControl,gHeight,gWidth,itemUom,tableViewHeader;
@synthesize delegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        isPickerHidden = YES;
        self.currentSelectedCustomer = ((KDSAppDelegate*)[UIApplication sharedApplication].delegate).currentCustomer;
        currentConversionRate=defaultConversionRate=-1;
        currentPrice=0.0f;
        isDiscountAmount = YES;
        isOverAmount=NO;
    }
    return self;
}

- (void)dealloc
{
    [delegate release];
    [PriceUpdate release];
    [QuantityUpdate release];
    [RemarkUpdate release];
    [UpDatetag release];
    [formulaLabel release];
    [quantityOrderedLabel release];
    [customerNameLabel release];
    [companyNameLabel release];
    [telLabel release];
    [faxLabel release];
    [emailLabel release];
    [totalLengthLabel release];
    [standardPriceLabel release];
    [productNameLabel release];
    [descriptionLabel release];
    [typesOfmeasurementsLabel release];
    [lblSelectedUOM release];
    [currentSelectedBlindItem release];
    [priceTextField release];
    [heightTextField release];
    [widthTextField release];
    [controlTextField release];
    [colorTextField release];
    [quantityTextField release];
    [remarkTextView release];
    [itemUom release];
    
    [orderTypeSegmentedControl release];
    [typesOfmeasurementPickerView release];
    [changeMeasurementButton release];
    [measurementArray release];
    [Bspecialitemdesc release];
    [backGroundImageView release];
    [productImageView release];
    [currentSelectedItem release];
    [currentSelectedCustomer release];
    if(currentUOM!=nil) [currentUOM release];
    [discountTextField release];
    [discountTypeLabel release];
    [changeDiscountTypeButton release];
    [lblTypeofMeasurement release];
    [customerdetaintable release];
    [lblprice release];
    [lbldescription release];
    [lblwidth release];
    [lblX release];
    [lblHeight release];
    [lblequal release];
    [lblcontrol release];
    [lblcolor release];
    [lblquantity release];
    [lblremark release];
    [blindremarktextview release];
    [lblitemdesc release];
    [blinditemDesctextView release];
    [currentSelectedBlindSalesOrderItem release];
    [super dealloc];
}

-(IBAction)viewYourCartButtonTapped
{
    YourCartViewController *yourCartViewController=[[YourCartViewController alloc] initWithNibName:@"YourCartViewController" bundle:nil];
    yourCartViewController.title=@"Felton";
    [self.navigationController pushViewController:yourCartViewController animated:YES];
    [yourCartViewController release];
}
-(IBAction)cancelYourCartButtonTapped
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"Please Confirm cancelling your order." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
    [alert release];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        [appDelegate clearSalesOrder];
        
        UIViewController* newOrderCategoryViewController=nil;
        for(UIViewController* viewController in self.navigationController.viewControllers)
        {
            if([viewController isKindOfClass:[MenuViewController class]])
            {
                newOrderCategoryViewController=viewController;
                break;
            }
        }
        [self.navigationController popToViewController:newOrderCategoryViewController animated:YES];
    }
    
}
-(IBAction)changeMeasurementButton_Tapped
{
    [self hidePicker];
}
-(IBAction)confirmButtonTapped
{
    /*if([quantityTextField.text intValue]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"The Quantity field can't be zero!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }else if([priceTextField.text floatValue]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"The Unit Price field can't be zero!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else*/
        if([currentSelectedItem.Category isEqualToString:kBlindCategory] && [widthTextField.text intValue]==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"The Width field can't be zero!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        else if([discountTypeLabel.text isEqualToString:@"In %"] && [discountTextField.text floatValue] == 100)
        {
            /**/UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"100% Discount is not allowed." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            discountTextField.text = [NSString stringWithFormat:@"%d", 0];
        }else if((([quantityTextField.text floatValue]==0) || ([priceTextField.text floatValue]==0)) && ([discountTextField.text floatValue] != 0))
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@" Discount is not allowed. \n While Price or Quantity is 0." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            discountTextField.text = [NSString stringWithFormat:@"%d", 0];
        }else
        {
            if([currentSelectedItem.Category isEqualToString:kBlindCategory] || [currentSelectedItem.Category isEqualToString:kblindACategoryA])
            {
                KDSAppDelegate* appDelegate=(KDSAppDelegate*)[UIApplication sharedApplication].delegate;
                if(appDelegate.salesOrder==nil)
                {
                    BlindSalesOrder* tBlindSalesOrder=[[BlindSalesOrder alloc] init];
                    tBlindSalesOrder.customer=currentSelectedCustomer;
                    appDelegate.salesOrder=tBlindSalesOrder;
                    
                    [tBlindSalesOrder release];
                }
                BlindSalesOrderItem* salesOrderItem=[[BlindSalesOrderItem alloc] init];
                salesOrderItem.item=currentSelectedItem;
                salesOrderItem.Unit_Price=[priceTextField.text floatValue];
                //UOMAndConversion *uom=[currentSelectedItem.UOMAndConversion objectAtIndex:[typesOfmeasurementPickerView selectedRowInComponent:0]];
                //salesOrderItem.Order_UOM=uom.Uom;
                salesOrderItem.Order_UOM=lblSelectedUOM.text;
                salesOrderItem.Quantity=[quantityTextField.text floatValue];
                salesOrderItem.Comment=blindremarktextview.text;
                salesOrderItem.Height=[heightTextField.text floatValue];
                salesOrderItem.Width=[widthTextField.text floatValue];
                salesOrderItem.Control=controlTextField.text;
                salesOrderItem.Color=colorTextField.text;
                salesOrderItem.SpecialItemDescription=blinditemDesctextView.text;
                currentSelectedBlindItem.total_SqFt = [totalLengthLabel.text floatValue];
                salesOrderItem.Quantity_in_Set = currentSelectedBlindItem.total_SqFt;
                
                
                float sumOfOrderItemPrice=0.0;
                totalDiscountOfItems=0.0;
                
                if (salesOrderItem) 
                {
                    
                    float orderQuantity = [totalLengthLabel.text floatValue];
                    float orderPrice = [priceTextField.text floatValue];
                    sumOfOrderItemPrice = (orderQuantity*orderPrice);
                    //salesOrderItem.TotalPrice=sumOfOrderItemPrice;
                    
                    if ([currentSelectedItem.Category isEqualToString:kBlindCategory]) 
                    {
                        if(isDiscountAmount)
                        {
                            
                            salesOrderItem.Discount_Percent=0.0;
                            
                            NSString* alertStr;
                            if((![quantityTextField.text floatValue]==0) && (![priceTextField.text floatValue]==0) && ([discountTextField.text floatValue] >= salesOrderItem.Quantity_in_Set*salesOrderItem.Unit_Price))
                            {
                                isOverAmount=YES;
                                alertStr = [NSString stringWithFormat:@"The Discount Amount Order Item can't be more than total price of that order item."];
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:alertStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                [alert show];
                                [alert release];
                                discountTextField.text = [NSString stringWithFormat:@"%d", 0];
                            }
                            else
                            {
                                isOverAmount=NO;
                                salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                                totalDiscountOfItems+=[discountTextField.text floatValue];
                                salesOrderItem.TotalPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                            }
                        }
                        else //if(!isDiscountAmount)
                        {
                            salesOrderItem.Discount_Amount=0.0;
                            if([discountTextField.text floatValue] == 100)
                            {
                                /*UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"100% Discount is not allowed." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                 [alert show];
                                 [alert release];*/
                                discountTextField.text = [NSString stringWithFormat:@"%d", 0];
                            }
                            else
                            {
                                isOverAmount=NO;
                                salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                                totalDiscountOfItems+=(salesOrderItem.Discount_Percent*salesOrderItem.Unit_Price*salesOrderItem.Quantity_in_Set)/100;
                                salesOrderItem.TotalPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                            }
                        }
                    }else 
                    {
                        if(isDiscountAmount)
                        {
                            
                            salesOrderItem.Discount_Percent=0.0;
                            
                            NSString* alertStr;
                            if((![quantityTextField.text floatValue]==0) && (![priceTextField.text floatValue]==0) && ([discountTextField.text floatValue] >= salesOrderItem.Quantity*salesOrderItem.Unit_Price))
                            {
                                isOverAmount=YES;
                                alertStr = [NSString stringWithFormat:@"The Discount Amount Order Item can't be more than total price of that order item."];
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:alertStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                [alert show];
                                [alert release];
                                discountTextField.text = [NSString stringWithFormat:@"%d", 0];
                            }
                            else
                            {
                                isOverAmount=NO;
                                salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                                totalDiscountOfItems+=[discountTextField.text floatValue];
                                salesOrderItem.TotalPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                            }
                        }
                        else //if(!isDiscountAmount)
                        {
                            salesOrderItem.Discount_Amount=0.0;
                            if([discountTextField.text floatValue] == 100)
                            {
                                /*UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"100% Discount is not allowed." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                 [alert show];
                                 [alert release];*/
                                discountTextField.text = [NSString stringWithFormat:@"%d", 0];
                            }
                            else
                            {
                                isOverAmount=NO;
                                salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                                totalDiscountOfItems+=(salesOrderItem.Discount_Percent*salesOrderItem.Unit_Price*salesOrderItem.Quantity)/100;
                                salesOrderItem.TotalPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                            }
                        }
                    }
                    
                    
                    
                }

                if(![appDelegate.salesOrder replaceOrderItemAtIndexblind:updateindexPath withObject:salesOrderItem])
                {
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Blind Items can't be added to Normal Orders" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    [alert release];
                    isOrderMixed = YES;
                }
                [salesOrderItem release];
                
                
                //[self deleteRowOrderItemblind];
            }
            else
            {
                KDSAppDelegate* appDelegate=(KDSAppDelegate*)[UIApplication sharedApplication].delegate;
                if(appDelegate.salesOrder==nil)
                {
                    SalesOrder* tSalesOrder=[[SalesOrder alloc] init];
                    tSalesOrder.customer=currentSelectedCustomer;
                    appDelegate.salesOrder=tSalesOrder;
                    [tSalesOrder release];
                }
                SalesOrderItem* salesOrderItem=[[SalesOrderItem alloc] init];
                salesOrderItem.item=currentSelectedItem;
                salesOrderItem.Unit_Price=[priceTextField.text floatValue];
                //salesOrderItem.Order_UOM=currentUOM;
                salesOrderItem.Order_UOM=lblSelectedUOM.text;
                salesOrderItem.Quantity=[quantityTextField.text floatValue];
                salesOrderItem.Comment=remarkTextView.text;
                
                float sumOfOrderItemPrice=0.0;
                totalDiscountOfItems=0.0;
                
                if (salesOrderItem) 
                {
                    
                    float orderQuantity = [quantityTextField.text floatValue];
                    float orderPrice = [priceTextField.text floatValue];
                    sumOfOrderItemPrice = (orderQuantity*orderPrice);
                    //salesOrderItem.TotalPrice=sumOfOrderItemPrice;
                    
                    if(isDiscountAmount)
                    {
                        
                        salesOrderItem.Discount_Percent=0.0;
                        
                        NSString* alertStr;
                        if((![quantityTextField.text floatValue]==0) && (![priceTextField.text floatValue]==0) && ([discountTextField.text floatValue] >= salesOrderItem.Quantity*salesOrderItem.Unit_Price))
                        {
                            isOverAmount=YES;
                            /*if(i==0)
                            {
                                alertStr=[NSString stringWithFormat:@"The Discount Amount for First Order Item can't be more than total price of that order item."];
                            }
                            else
                            {
                                alertStr = [NSString stringWithFormat:@"The Discount Amount for %d%@ Order Item can't be more than total price of that order item.", i+1, @"nd"];
                            }*/
                            alertStr = [NSString stringWithFormat:@"The Discount Amount Order Item can't be more than total price of that order item."];
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:alertStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                            [alert show];
                            [alert release];
                            discountTextField.text = [NSString stringWithFormat:@"%d", 0];
                        }
                        else
                        {
                            isOverAmount=NO;
                            salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                            totalDiscountOfItems+=[discountTextField.text floatValue];
                            salesOrderItem.TotalPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                        }
                    }
                    else
                    {
                        salesOrderItem.Discount_Amount=0.0;
                        if([discountTextField.text floatValue] == 100)
                        {
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"100% Discount is not allowed." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                            [alert show];
                            [alert release];
                            discountTextField.text = [NSString stringWithFormat:@"%d", 0];
                        }
                        else
                        {
                            isOverAmount=NO;
                            salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                            totalDiscountOfItems+=(salesOrderItem.Discount_Percent*salesOrderItem.Unit_Price*salesOrderItem.Quantity)/100;
                            salesOrderItem.TotalPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                        }
                    }
                    
                    
                    
                    
                }
                
                if(![appDelegate.salesOrder replaceOrderItemAtIndex:updateindexPath withObject:salesOrderItem])
                {
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Normal Items can't be added to Blind Orders" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    [alert release];
                    isOrderMixed =YES;
                }
                [salesOrderItem release];
                
                //[self deleteRowOrderItem];
            }
            if(isOrderMixed==NO && isOverAmount==NO)
            {
                YourCartViewController *yourCartViewController=[[YourCartViewController alloc] initWithNibName:@"YourCartViewController" bundle:nil];
                yourCartViewController.title=@"Felton";
                [self.navigationController pushViewController:yourCartViewController animated:YES];
                [yourCartViewController release];
            }
            
        }
    
        
    
}

-(void)deleteRowOrderItemblind
{ 
    //KDSAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
    //NSLog(@"equivalantOrderItemIndex--->%d",equivalantOrderItemIndex);
    //NSString *orderidreplace=[[NSString alloc]initWithFormat:@"%d",appDelegate.salesOrder];
    //[appDelegate.salesOrder replaceOrderItemAtIndex:updateindexPath withObject:appDelegate.salesOrder.b]; 
    
}

-(void)deleteRowOrderItem 
{ 
    //KDSAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
    //NSLog(@"equivalantOrderItemIndex--->%d",equivalantOrderItemIndex);
    //NSString *orderidreplace=[[NSString alloc]initWithFormat:@"%d",appDelegate.salesOrder];
    //[appDelegate.salesOrder replaceOrderItemAtIndex:updateindexPath withObject:appDelegate.salesOrder]; 
 
}

-(void)calculateSquareFeet
{
    
    if ([currentSelectedItem.Category isEqualToString:kBlindCategory]) 
    {
        /*float heightInput = [heightTextField.text floatValue];
         float widthInput = [widthTextField.text floatValue];
         float quantityInput = [quantityTextField.text floatValue];
         if(quantityInput != 0 && widthInput !=0 && heightInput != 0)
         {
         formulaLabel.alpha = 1.0;
         totalLengthLabel.alpha = 1.0;
         NSString* totalLengthStr;
         if(heightInput < currentSelectedBlindItem.min_Height)
         {
         heightInput=currentSelectedBlindItem.min_Height;
         }
         totalLength = ((widthInput*heightInput)/144)*quantityInput;
         if(totalLength <= currentSelectedBlindItem.min_SqFt)
         {
         totalLength = currentSelectedBlindItem.min_SqFt;
         //self.currentSelectedBlindItem.total_SqFt= totalLength;
         totalLengthStr = [NSString stringWithFormat:@"%.2f",totalLength];
         totalLengthLabel.text = totalLengthStr;
         }
         else
         {
         totalLengthStr = [NSString stringWithFormat:@"%.2f",totalLength];
         //currentSelectedBlindItem.total_SqFt=totalLength;
         totalLengthLabel.text = totalLengthStr;
         }
         NSString *formulaStr = [NSString stringWithFormat:@"((%.2f * %.2f)/144) * %.2f", widthInput ,heightInput , quantityInput];
         formulaLabel.text = formulaStr;
         totalLengthLabel.text = totalLengthStr;
         }*/
        
        float heightInput = [heightTextField.text floatValue];
        float widthInput = [widthTextField.text floatValue];
        //float quantityInput = [quantityTextField.text floatValue];
        //if(quantityInput != 0 && widthInput !=0 && heightInput != 0)
        if( widthInput !=0 && heightInput != 0)
        {
            formulaLabel.alpha = 1.0;
            totalLengthLabel.alpha = 1.0;
            NSString* totalLengthStr;
            
            totalLength = widthInput*heightInput/144;
            
            if(totalLength < currentSelectedBlindItem.min_SqFt)
            {
                
                totalLength = currentSelectedBlindItem.min_SqFt;                
                totalLengthStr = [NSString stringWithFormat:@"%.2f",totalLength];
                totalLengthLabel.text = totalLengthStr;
                
            }else if(totalLength >= currentSelectedBlindItem.min_SqFt)
            {
                if(heightInput < currentSelectedBlindItem.min_Height)
                {                                    
                    heightInput=currentSelectedBlindItem.min_Height;
                    
                }else 
                {
                    heightInput=heightInput;
                }                                        
                totalLength = widthInput*heightInput/144;                    
                totalLengthStr = [NSString stringWithFormat:@"%.2f",totalLength];                    
                totalLengthLabel.text = totalLengthStr;
                
            }
            else 
            {                
                totalLengthStr = [NSString stringWithFormat:@"%.2f",totalLength];                
                totalLengthLabel.text = totalLengthStr;
            }
            
            NSString *formulaStr = [NSString stringWithFormat:@"(%.2f * %.2f)/144", widthInput ,heightInput];
            formulaLabel.text = formulaStr;
            totalLengthLabel.text = totalLengthStr;
        }
        
        
    }
}
-(IBAction)measurementControllerTapped
{
    
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView*)pickerView numberOfRowsInComponent:(NSInteger)component
{
    //NSLog(@"currentSelectedItem.UOMAndConversion--->%@",currentSelectedItem.UOMAndConversion);
    if(currentSelectedItem.UOMAndConversion!=nil)
        return [currentSelectedItem.UOMAndConversion count];
    else
        return 0;
}
-(NSString*)pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    UOMAndConversion* uom=[currentSelectedItem.UOMAndConversion objectAtIndex:row];
    //NSLog(@"uom--->%@",uom);
    return uom.Uom;
}
-(void)pickerView:(UIPickerView*)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    UOMAndConversion* uom=[currentSelectedItem.UOMAndConversion objectAtIndex:row];
    [self setCurrentUOM:uom.Uom];
    /**/LastSellingPrice* matchedLastSellingPrice=nil;
    NSArray* lastSellingPriceArr=[currentSelectedItem lastSellingPriceForCustomer:currentSelectedCustomer.Customer_Id];
    if(lastSellingPriceArr!=nil)
    {
        for(LastSellingPrice* lastSellingPrice in lastSellingPriceArr)
        {
            if([lastSellingPrice.UOM isEqualToString:uom.Uom])
            {
                matchedLastSellingPrice=lastSellingPrice;
                break;
            }
        }
    }
    if(matchedLastSellingPrice==nil)
    {
        float nextConversionRate = [uom.Converstion floatValue];
        currentPrice=fabsf(currentPrice*(nextConversionRate/currentConversionRate));
        NSString *nextPrice=[NSString stringWithFormat:@"%.2f",currentPrice];
        priceTextField.text=nextPrice;
        currentConversionRate=nextConversionRate;
    }
    else
    {
        NSString *nextPrice=[NSString stringWithFormat:@"%.2f",matchedLastSellingPrice.Price];
        priceTextField.text=nextPrice;
    }
    [self hidePicker];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag == kWidthTextField || textField.tag ==kHeightTextField || textField.tag == kQuantityTextField)
    {
        [self calculateSquareFeet];
        if([textField.text floatValue] == 0)
        {
            formulaLabel.alpha = 0.0;
            totalLengthLabel.alpha = 0.0;
        }
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    heightOfEditedView = textField.frame.size.height;
    heightOffset = textField.frame.origin.y+10;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    if(textField.tag == kPriceTextFeield)
    {
        [textField selectAll:nil];
    }
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    heightOfEditedView = textView.frame.size.height;
    heightOffset = textView.frame.origin.y+10;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    
}

-(void)showDiscountButton
{
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([currentSelectedItem.Category isEqualToString:kBlindCategory] ) 
    {
        if (textField == discountTextField) {
            [textField resignFirstResponder];
            [priceTextField becomeFirstResponder];
        }
        else if (textField == priceTextField) {
            [textField resignFirstResponder];
            [widthTextField becomeFirstResponder];
        }
        else if (textField == widthTextField) {
            [textField resignFirstResponder];
            [heightTextField becomeFirstResponder];
        }
        else if (textField == heightTextField) {
            [textField resignFirstResponder];
            [controlTextField becomeFirstResponder];
        }
        else if (textField == controlTextField) {
            [textField resignFirstResponder];
            [colorTextField becomeFirstResponder];
        }
        else if (textField == colorTextField) {
            [textField resignFirstResponder];
            [quantityTextField becomeFirstResponder];
        }else if (textField == quantityTextField) {
            [textField resignFirstResponder];
            [blindremarktextview becomeFirstResponder];
        }
        else if (textField == blindremarktextview) {
            [textField resignFirstResponder];
            [blinditemDesctextView becomeFirstResponder];
        }
        else if (textField == blinditemDesctextView) {
            [textField resignFirstResponder];
        }
        
    }else 
    {
        if (textField == discountTextField) {
            [textField resignFirstResponder];
            [priceTextField becomeFirstResponder];
        }
        else if (textField == priceTextField) {
            [textField resignFirstResponder];
            [quantityTextField becomeFirstResponder];
        }
        else if (textField == quantityTextField) {
            [textField resignFirstResponder];
            [remarkTextView becomeFirstResponder];
        }
        else if (textField == remarkTextView) {
            [textField resignFirstResponder];
            //[quantityTextField becomeFirstResponder];
        }
    }
    
	return NO;
}


-(IBAction)changeDiscountType_ButtonTapped:(id)sender
{
    if(isDiscountAmount)
    {
        [((UIButton*)sender) setBackgroundImage:[UIImage imageWithContentsOfFile:@"Btn_Discount_Amount.png"] forState:UIControlStateNormal];
        self.discountTypeLabel.textColor=[UIColor blackColor];
        self.discountTypeLabel.text = @"In %";
        isDiscountAmount = NO;
        
    }
    else
    {
        [((UIButton*)sender) setBackgroundImage:[UIImage imageWithContentsOfFile:@"Button_Amount_Percentage.png"] forState:UIControlStateNormal];
        self.discountTypeLabel.textColor=[UIColor blackColor];
        self.discountTypeLabel.text = @"In amount";
        isDiscountAmount = YES;
    }
    
}

-(void)setIsDiscountAmount:(BOOL)tisDiscountAmount
{
    isDiscountAmount=tisDiscountAmount;
    NSString *imageName=@"Btn_Discount_Amount.png";
    if(isDiscountAmount)
    {
        imageName=@"Button_Amount_Percentage.png";
    }
    [self.changeDiscountTypeButton setBackgroundImage:[UIImage imageWithContentsOfFile:imageName] forState:UIControlStateNormal];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == kPriceTextFeield) {
        int lengtha = [priceTextField.text length];
        //NSLog(@"lenghta = %d",lengtha);
        if (lengtha >= MAXLENGTHtwelve && ![string isEqualToString:@""]) {
            priceTextField.text = [priceTextField.text substringToIndex:MAXLENGTHtwelve];
            return NO;
        }
        return YES;
    } else if (textField.tag == kHeightTextField) {
        int lengthb = [heightTextField.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHfive && ![string isEqualToString:@""]) {
            heightTextField.text = [heightTextField.text substringToIndex:MAXLENGTHfive];
            return NO;
        }
        return YES;
    } else if (textField.tag == kWidthTextField) {
        int lengthb = [widthTextField.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHfive && ![string isEqualToString:@""]) {
            widthTextField.text = [widthTextField.text substringToIndex:MAXLENGTHfive];
            return NO;
        }
        return YES;
    } else if (textField.tag == 304) {
        int lengthb = [controlTextField.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHCOLORCONTROL && ![string isEqualToString:@""]) {
            controlTextField.text = [controlTextField.text substringToIndex:MAXLENGTHCOLORCONTROL];
            return NO;
        }
        return YES;
    } else if (textField.tag == 306) {
        int lengthb = [colorTextField.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHCOLORCONTROL && ![string isEqualToString:@""]) {
            colorTextField.text = [colorTextField.text substringToIndex:MAXLENGTHCOLORCONTROL];
            return NO;
        }
        return YES;
    } else if (textField.tag == kQuantityTextField) {
        int lengthb = [quantityTextField.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHsix && ![string isEqualToString:@""]) {
            quantityTextField.text = [quantityTextField.text substringToIndex:MAXLENGTHsix];
            return NO;
        }
        return YES;
    } else if (textField.tag == 307) {
        int lengthb = [discountTextField.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHtwelve && ![string isEqualToString:@""]) {
            discountTextField.text = [discountTextField.text substringToIndex:MAXLENGTHtwelve];
            return NO;
        }
        return YES;
    } 
    
    
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)string
{
    if (textView.tag == 501) {
        int lengtha = [blindremarktextview.text length];
        //NSLog(@"lenghta = %d",lengtha);
        if (lengtha >= MAXLENGTHfifty && ![string isEqualToString:@""]) {
            blindremarktextview.text = [blindremarktextview.text substringToIndex:MAXLENGTHfifty];
            return NO;
        }
        return YES;
    } else if (textView.tag == 502) {
        int lengthb = [blinditemDesctextView.text length];
        // NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHfifty && ![string isEqualToString:@""]) {
            blinditemDesctextView.text = [blinditemDesctextView.text substringToIndex:MAXLENGTHfifty];
            return NO;
        }
        return YES;
    } else if (textView.tag == 303) {
        int lengthb = [remarkTextView.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHfifty && ![string isEqualToString:@""]) {
            remarkTextView.text = [remarkTextView.text substringToIndex:MAXLENGTHfifty];
            return NO;
        }
        return YES;
    } 
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //heightTextField.tag=302;
    //widthTextField.tag=303;
    controlTextField.tag=304;
    colorTextField.tag=306;
    discountTextField.tag=307;
    
    blindremarktextview.tag=501;
    blinditemDesctextView.tag=502;
    remarkTextView.tag=503;
    
    [self initializeItem];
    
    [self initializeVariables];
    
    [[remarkTextView layer] setBorderWidth:0.3];
	[[remarkTextView layer] setCornerRadius:6];
    
    [[blindremarktextview layer] setBorderWidth:0.3];
	[[blindremarktextview layer] setCornerRadius:6];
    
    [[blinditemDesctextView layer] setBorderWidth:0.3];
	[[blinditemDesctextView layer] setCornerRadius:6];
    
    isOrderMixed = NO;
    customerNameLabel.text = currentSelectedCustomer.Contact_Name;
    companyNameLabel.text = currentSelectedCustomer.Customer_Name;
    telLabel.text = currentSelectedCustomer.Phone_1;
    faxLabel.text = currentSelectedCustomer.Phone_2;
    emailLabel.text = currentSelectedCustomer.Email;
    quantityTextField.delegate=self;
    discountTextField.delegate=self;
    NSString *fquantityTextField=[[NSString alloc]initWithFormat:@"%.2f",SelectedItemUpdate.Quantity];
    quantityTextField.text=fquantityTextField;
    NSString *fpriceTextField=[[NSString alloc]initWithFormat:@"%.2f",SelectedItemUpdate.Unit_Price];
    priceTextField.text=fpriceTextField;
    blindremarktextview.text=SelectedItemUpdate.Comment;
    remarkTextView.text=SelectedItemUpdate.Comment;
    blinditemDesctextView.text=SelectedItemUpdate.SpecialItemDescription; 
    controlTextField.text=BControl;
    colorTextField.text=BColor;
    lblSelectedUOM.text=itemUom;
    lblTypeofMeasurement.hidden=YES;
    changeMeasurementButton.hidden=YES;
    
    colorTextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    controlTextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    
    //discountTypeLabel.textColor=[UIColor redColor];
    discountTypeLabel.text=@"In amount";
    
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    //appDelegate.loggedInSalesPerson.;
    NSLog(@"SalesPerson_Code---->>>>%@",appDelegate.loggedInSalesPerson.SalesPerson_Username);
    
    /**/
    NSString *STRSalesPerson_Code=[[NSString alloc]initWithFormat:@"%@",appDelegate.loggedInSalesPerson.SalesPerson_Username];
    
    
    BOOL result;
    result = [STRSalesPerson_Code hasPrefix: @"88"];
    [STRSalesPerson_Code release];
    
    if (result)
    {
        //priceTextField.editing=NO;
        
        [priceTextField setEnabled:NO];
        
    }
    else
    {
        
        [priceTextField setEnabled:YES];
    }
    
    NSString *fdiscountAmount=[[NSString alloc]initWithFormat:@"%.2f",SelectedItemUpdate.Discount_Amount];
    NSString *fdiscountPercent=[[NSString alloc]initWithFormat:@"%.2f",SelectedItemUpdate.Discount_Percent];
    
    if (![fdiscountAmount isEqualToString:@""]) {
        discountTextField.text=fdiscountAmount;
    }else if (![fdiscountPercent isEqualToString:@""]) {
        discountTextField.text=fdiscountPercent;
    }
    
    quantityTextField.tag=kQuantityTextField;
    priceTextField.tag = kPriceTextFeield;
    
    SelectedItemUpdate.item=currentSelectedItem;
    
    //NSLog(@"SelectedItemUpdate.item.Item_id--->%d",SelectedItemUpdate.item.Item_id);
                     
    if (currentSelectedItem.Category!=nil) 
    {
        heightTextField.text=BHeight;
        widthTextField.text=BWidth;
    
    if((![currentSelectedItem.Category isEqualToString:kBlindCategory]) && (![currentSelectedItem.Category isEqualToString:kblindACategoryA]))
    {
        
        //backGroundImageView.image = [UIImage imageNamed:@"subpage_background.png"];
        //productImageView.image = [UIImage imageNamed:@"plastic.png"];
        
        lbldescription.hidden=NO;
        lblwidth.hidden=YES;
        lblX.hidden=YES;
        lblHeight.hidden=YES;
        lblequal.hidden=YES;
        lblcontrol.hidden=YES;
        lblcolor.hidden=YES;
        lblitemdesc.hidden=YES;
        blinditemDesctextView.hidden=YES;
        blindremarktextview.hidden=YES;

        
        widthTextField.alpha = 0.0f;
        heightTextField.alpha = 0.0f;
        totalLengthLabel.alpha = 0.0f;
        controlTextField.alpha = 0.0f;
        colorTextField.alpha = 0.0f;
        productNameLabel.alpha = 1.0f;
        descriptionLabel.alpha = 1.0;
        quantityOrderedLabel.alpha =1.0;
        formulaLabel.alpha = 0.0;
        quantityOrderedLabel.alpha = 0.0;
        descriptionLabel.text=currentSelectedItem.Description;
        productNameLabel.text=[NSString stringWithFormat:@"%d - %@", currentSelectedItem.Item_id, currentSelectedItem.Description];
    }
    else
    {
        if ([currentSelectedItem.Category isEqualToString:kBlindCategory]) {
            if(currentSelectedBlindItem == nil)
            {
                self.currentSelectedBlindItem = (BlindItem*) currentSelectedItem;
            }
            //backGroundImageView.image = [UIImage imageNamed:@"subpage_background.png"];
            //productImageView.image = [UIImage imageNamed:@"blinds.png"];
            
            lbldescription.hidden=YES;
            lblwidth.hidden=NO;
            lblX.hidden=NO;
            lblHeight.hidden=NO;
            lblequal.hidden=NO;
            lblcontrol.hidden=NO;
            lblcolor.hidden=NO;
            remarkTextView.hidden=YES;
            lblitemdesc.hidden=NO;
            blinditemDesctextView.hidden=NO;
            blindremarktextview.hidden=NO;

            
            
            widthTextField.alpha = 1.0;
            widthTextField.tag = kWidthTextField;
            heightTextField.alpha = 1.0;
            heightTextField.tag=kHeightTextField;
            totalLengthLabel.alpha = 1.0;
            controlTextField.alpha = 1.0;
            colorTextField.alpha = 1.0;
            descriptionLabel.alpha=0.0;
            productNameLabel.alpha =1.0;
            quantityOrderedLabel.alpha = 0.0;
            productNameLabel.text=[NSString stringWithFormat:@"%d - %@", currentSelectedItem.Item_id, currentSelectedItem.Description];
            typesOfmeasurementsLabel.alpha = 1.0;
            quantityOrderedLabel.alpha = 1.0;
        }else 
        {
            if(currentSelectedBlindItem == nil)
            {
                self.currentSelectedBlindItem = (BlindItem*) currentSelectedItem;
            }
            //backGroundImageView.image = [UIImage imageNamed:@"subpage_background.png"];
            //productImageView.image = [UIImage imageNamed:@"blinds.png"];
            
            lbldescription.hidden=YES;
            lblwidth.hidden=YES;
            lblX.hidden=YES;
            lblHeight.hidden=YES;
            lblequal.hidden=YES;
            lblcontrol.hidden=YES;
            lblcolor.hidden=YES;
            
            lblitemdesc.hidden=NO;
            remarkTextView.hidden=YES;
            blinditemDesctextView.hidden=NO;
            blindremarktextview.hidden=NO;
            
            
            widthTextField.alpha = 0.0f;
            widthTextField.tag = kWidthTextField;
            heightTextField.alpha = 0.0f;
            heightTextField.tag=kHeightTextField;
            totalLengthLabel.alpha = 0.0f;
            controlTextField.alpha = 0.0f;
            colorTextField.alpha = 0.0f;
            descriptionLabel.alpha=0.0;
            productNameLabel.alpha =1.0;
            quantityOrderedLabel.alpha = 0.0;
            productNameLabel.text=[NSString stringWithFormat:@"%d - %@", currentSelectedItem.Item_id, currentSelectedItem.Description];
            typesOfmeasurementsLabel.alpha = 1.0;
            quantityOrderedLabel.alpha = 0.0f;
        }
        
    }
    }else if (currentSelectedItem.Category == nil) 
    {
        NSString *heightg=[[NSString alloc]initWithFormat:@"%.2f",gHeight];
        NSString *widthg=[[NSString alloc]initWithFormat:@"%.2f",gWidth];
        
        heightTextField.text=heightg; 
        widthTextField.text=widthg;
        
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        
        if(appDelegate.salesOrder.Type==0)
        {
            
            //backGroundImageView.image = [UIImage imageNamed:@"subpage_background.png"];
            //productImageView.image = [UIImage imageNamed:@"plastic.png"];
            widthTextField.alpha = 0.0f;
            heightTextField.alpha = 0.0f;
            totalLengthLabel.alpha = 0.0f;
            controlTextField.alpha = 0.0f;
            colorTextField.alpha = 0.0f;
            productNameLabel.alpha = 1.0f;
            descriptionLabel.alpha = 1.0;
            quantityOrderedLabel.alpha =1.0;
            formulaLabel.alpha = 0.0;
            quantityOrderedLabel.alpha = 0.0;
            descriptionLabel.text=currentSelectedItem.Description;
            productNameLabel.text=[NSString stringWithFormat:@"%d - %@", currentSelectedItem.Item_id, currentSelectedItem.Description];
        }
        else if(appDelegate.salesOrder.Type==1)
        {
            if(currentSelectedBlindItem == nil)
            {
                self.currentSelectedBlindItem = (BlindItem*) currentSelectedItem;
            }
            //backGroundImageView.image = [UIImage imageNamed:@"subpage_background.png"];
            //productImageView.image = [UIImage imageNamed:@"blinds.png"];
            widthTextField.alpha = 1.0;
            widthTextField.tag = kWidthTextField;
            heightTextField.alpha = 1.0;
            heightTextField.tag=kHeightTextField;
            totalLengthLabel.alpha = 1.0;
            controlTextField.alpha = 1.0;
            colorTextField.alpha = 1.0;
            descriptionLabel.alpha=0.0;
            productNameLabel.alpha =1.0;
            quantityOrderedLabel.alpha = 0.0;
            productNameLabel.text=[NSString stringWithFormat:@"%d - %@", currentSelectedItem.Item_id, currentSelectedItem.Description];
            typesOfmeasurementsLabel.alpha = 1.0;
            quantityOrderedLabel.alpha = 1.0;
        }
    }
    [self initializeProductImage];
    [self calculateSquareFeet];
    
    
    UIView *TransferedOrdertableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(customerdetaintable.frame.origin.x, customerdetaintable.frame.origin.y-30, self.customerdetaintable.frame.size.width, 30)];
    TransferedOrdertableHeaderView.backgroundColor = [UIColor colorWithRed:0.0196 green:0.513 blue:0.949 alpha:1.0];
    
    UILabel *TransferedOrderTableHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 5, self.customerdetaintable.frame.size.width/2, 21)];
    TransferedOrderTableHeaderLabel.text = @"Customer";
    TransferedOrderTableHeaderLabel.backgroundColor = [UIColor clearColor];
    TransferedOrderTableHeaderLabel.textColor = [UIColor whiteColor];
    TransferedOrderTableHeaderLabel.textAlignment = UITextAlignmentLeft;
    TransferedOrderTableHeaderLabel.font=[UIFont boldSystemFontOfSize:17];
    [TransferedOrdertableHeaderView addSubview:TransferedOrderTableHeaderLabel];
    tableViewHeader = TransferedOrdertableHeaderView;
    [self.view addSubview:tableViewHeader];
    
    
    [TransferedOrdertableHeaderView release];
    [TransferedOrderTableHeaderLabel release];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];

}

-(void)initializeProductImage
{
    UIImageViewWithImageFromURL *tproductImageView = [[UIImageViewWithImageFromURL alloc] init];
    tproductImageView.frame = CGRectMake(77, 399, 233, 233);

    if(currentSelectedItem.Image_Path!=nil && ![currentSelectedItem.Image_Path isEqualToString:@""])
    {
         NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
         NSString* documentsPath = [paths objectAtIndex:0];
         NSString* filePath = [documentsPath stringByAppendingPathComponent:currentSelectedItem.Image_Path];
         NSData* pngData = [NSData dataWithContentsOfFile:filePath];
         UIImage* image = [UIImage imageWithData:pngData];
         
         tproductImageView.image = image;
         self.productImageView=tproductImageView;
         
        
        /*NSString *imgurl=[[NSString alloc]initWithFormat:@"http://219.94.43.102/KDSCMS/images/products/%@",currentSelectedItem.Image_Path];
        
        UIImageView *imgview =[[UIImageView alloc]initWithFrame:CGRectMake(69, 348, 250, 280)];
        imgview.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgurl]]];
        [self.view addSubview:imgview];*/
        
    }  
    else //if (currentSelectedItem.Image_Path==nil && [currentSelectedItem.Image_Path isEqualToString:@""])
    {
        
        UIImage* image = [UIImage imageNamed:@"blinds.png"];
        
        //UIImageViewWithImageFromURL *tproductImageView = [[UIImageViewWithImageFromURL alloc] init];
        //tproductImageView.frame = CGRectMake(77, 399, 233, 233);
        tproductImageView.image = image;
        self.productImageView=tproductImageView;
        
        
        
    }

    [tproductImageView release];
    [self.view addSubview:self.productImageView];
    
    /*if(![currentSelectedItem.Image_Path isEqualToString:@""])
     {
     AsyncImageView *asyncImageView=[[AsyncImageView alloc] init];
     NSString *itemImageLocalPath=[currentSelectedItem.Image_Path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
     NSString *itemImageURLPath=[NSString stringWithFormat:@"http://mobilesolutions.com.my/KDSCMS/images/products/%@",itemImageLocalPath];
     [asyncImageView setDelegate:self.productImageView];
     [asyncImageView loadImageFromPath:itemImageURLPath];
     [asyncImageView release];
     }*/
}

- (void)viewDidUnload
{
    /*[self setDiscountTextField:nil];
    [self setDiscountTypeLabel:nil];
    [self setChangeDiscountTypeButton:nil];
    [self setLblTypeofMeasurement:nil];
    [self setCustomerdetaintable:nil];
    [self setLblprice:nil];
    [self setLbldescription:nil];
    [self setLblwidth:nil];
    [self setLblX:nil];
    [self setLblHeight:nil];
    [self setLblequal:nil];
    [self setLblcontrol:nil];
    [self setLblcolor:nil];
    [self setLblquantity:nil];
    [self setLblremark:nil];
    [self setBlindremarktextview:nil];
    [self setLblitemdesc:nil];
    [self setBlinditemDesctextView:nil];*/
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(void)hidePicker
{
    if(isPickerHidden)
    {
        typesOfmeasurementPickerView.hidden = NO;
        isPickerHidden = NO;
    }
    else
    {
        typesOfmeasurementPickerView.hidden = YES;
        isPickerHidden = YES;
    }
}
-(void)initializeVariables
{
    /**/LastSellingPrice* matchedLastSellingPrice=nil;
    NSArray* lastSellingPriceArr=[currentSelectedItem lastSellingPriceForCustomer:currentSelectedCustomer.Customer_Id];
    if(lastSellingPriceArr!=nil)
    {
        for(LastSellingPrice* lastSellingPrice in lastSellingPriceArr)
        {
            if([lastSellingPrice.UOM isEqualToString:currentSelectedItem.Stock_Unit])
            {
                matchedLastSellingPrice=lastSellingPrice;
                break;
            }
        }
    }
    BOOL found=NO;
    for(DefaultPriceAndUOM* defaultPrice in currentSelectedItem.Default_Price)
    {
        if(defaultPrice.Default_Price!=0)
        {
            found=YES;
            currentPrice=[defaultPrice.Default_Price floatValue];
            for(UOMAndConversion* uom in currentSelectedItem.UOMAndConversion)
            {
                if([defaultPrice.Uom isEqualToString:uom.Uom])
                {
                    [self setCurrentUOM:uom.Uom];
                    defaultConversionRate=currentConversionRate=[uom.Converstion floatValue];
                    break;
                }
            }
        /**/}
        if(found)
        {
            break;
        }
    }
    float tempCurrentPrice=currentPrice;
    if(matchedLastSellingPrice!=nil)
    {
        tempCurrentPrice=matchedLastSellingPrice.Price;
    }
    NSString *priceStr=[NSString stringWithFormat:@"%.2f",tempCurrentPrice];
    priceTextField.text=priceStr;
}
-(void)setCurrentUOM:(NSString*)iCurrentUOM
{
    if(currentUOM!=nil)
    {
        [currentUOM release];
    }
    currentUOM=iCurrentUOM;
    [currentUOM retain];
    self.lblSelectedUOM.text=currentUOM;
}
-(void)initializeItem
{
    //KDSDataItem* dataItem=[[KDSDataItem alloc] init];
    /*KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    
    //NSLog(@"initializeItem currentSelectedItem--->%@",currentSelectedItem.Item_No);
    
    NSArray* lastSellingPrice=[dataItem selectLastSellingPriceForItem:currentSelectedItem andCustomer:currentSelectedCustomer andSalesperson:appDelegate.loggedInSalesPerson];
    
    //NSLog(@"initializeItem lastSellingPrice--->%@",lastSellingPrice);
    [dataItem release];
    if(lastSellingPrice!=nil)
    {
        [currentSelectedItem addLastSellingPriceArr:lastSellingPrice forCustomerCode:currentSelectedCustomer.Customer_Id];
    }
    //selectItemUOM
    //KDSDataItem* dataItem=[[KDSDataItem alloc] init];
    
    Item* itemUom=[dataItem selectItemUOM:currentSelectedItem];
    NSLog(@"itemUom--->%@",itemUom);
    
    NSMutableArray *arrayUOM = [[NSMutableArray alloc] init];
    arrayUOM=itemUom.UOMAndConversion;
    
    UOMAndConversion* getitemUOm=[[UOMAndConversion alloc]init];
    getitemUOm=arrayUOM;
    NSLog(@"arrayUOM--->%@",getitemUOm);
    
    [dataItem release];*/
    
}
@end