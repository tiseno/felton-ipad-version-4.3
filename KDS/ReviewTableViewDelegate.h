//
//  ReviewTableViewDelegate.h
//  KDS
//
//  Created by Tiseno on 1/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ReviewTableViewDelegate <NSObject>
-(void)scrollToItem:(NSIndexPath*) indexPath;
-(void)focusOnCell:(NSIndexPath*)indexPath;
@end
