//
//  QuotationFormPdfResponse.h
//  KDS
//
//  Created by Tiseno Mac 2 on 10/17/12.
//
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"

@interface QuotationFormPdfResponse : XMLResponse{
    NSString *getPDFPath;
}

@property (nonatomic,retain) NSString *getPDFPath;

@end
