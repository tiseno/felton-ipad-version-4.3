//
//  NewOrderTableViewCell.m
//  KDS
//
//  Created by Tiseno on 11/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewOrderTableViewCell.h"
#define cell1Width 71
#define cell2Width 115
#define cell3Width 70
#define orderIdCellWidth 140
#define cell4Width 364
#define cell5Width 75

#define cellHeight 45

@implementation NewOrderTableViewCell

@synthesize lineColor, topCell, delegate;
@synthesize editButton, pdfButton, deleteButton;
@synthesize codeLabel, nameLabel, priceLabel, currencyLabel, salesOrder, orderIdLabel;
@synthesize pdfquoButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) 
        {
            topCell = NO;
            
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 846, 183);
            
            
        
            UIImage *editButtonImage = [UIImage imageNamed:@"btn_edit.png"];
            UIButton *teditButton = [[UIButton alloc] initWithFrame:CGRectMake(20, (cellHeight/2)-(editButtonImage.size.height/2), 51, 30)];
            [teditButton setBackgroundImage:editButtonImage forState:UIControlStateNormal];
            [teditButton addTarget:self action:@selector(edit_ButtonTapped:)
                    forControlEvents:UIControlEventTouchDown];
            self.editButton = teditButton;
            [teditButton release];
            [self addSubview:editButton]; 
            
            UIImage *pdfButtonImage = [UIImage imageNamed:@"btn_po.png"];
            UIButton *tPdfButton = [[UIButton alloc] initWithFrame:CGRectMake(100, (cellHeight/2)-(pdfButtonImage.size.height/2), 35, 35)];
            [tPdfButton setBackgroundImage:pdfButtonImage forState:UIControlStateNormal];
            [tPdfButton addTarget:self action:@selector(pdf_ButtonTapped:)
                  forControlEvents:UIControlEventTouchDown];
            self.pdfButton = tPdfButton;
            [tPdfButton release];
            [self addSubview:pdfButton];
            
            UIImage *deleteButtonImage = [UIImage imageNamed:@"btn_delete_2.png"];
            UIButton *tDeleteButton = [[UIButton alloc] initWithFrame:CGRectMake(150, (cellHeight/2)-(deleteButtonImage.size.height/2), 30, 30)];
            [tDeleteButton setBackgroundImage:deleteButtonImage forState:UIControlStateNormal];
            [tDeleteButton addTarget:self action:@selector(delete_ButtonTapped:)
                 forControlEvents:UIControlEventTouchDown];
            self.deleteButton = tDeleteButton;
            [tDeleteButton release];
            [self addSubview:deleteButton];

            UILabel *tOrderIdLabel = [[UILabel alloc] initWithFrame:CGRectMake(190, (cellHeight/2)-cellHeight/2, orderIdCellWidth,cellHeight)];
            tOrderIdLabel.textAlignment = UITextAlignmentLeft;
            tOrderIdLabel.adjustsFontSizeToFitWidth = YES;
            tOrderIdLabel.backgroundColor = [UIColor clearColor];
            tOrderIdLabel.textColor = [UIColor blackColor];
            tOrderIdLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
            self.orderIdLabel = tOrderIdLabel;
            [tOrderIdLabel release];
            [self addSubview:tOrderIdLabel];
            
            UIImage *pdfquoButtonImage = [UIImage imageNamed:@"btn_quo.png"];
            UIButton *tPdfquoButton = [[UIButton alloc] initWithFrame:CGRectMake(350, (cellHeight/2)-(pdfButtonImage.size.height/2), 35, 35)];
            [tPdfquoButton setBackgroundImage:pdfquoButtonImage forState:UIControlStateNormal];
            [tPdfquoButton addTarget:self action:@selector(pdfQuo_ButtonTapped:)
                 forControlEvents:UIControlEventTouchDown];
            self.pdfquoButton = tPdfquoButton;
            [tPdfquoButton release];
            [self addSubview:pdfquoButton];
            
            UILabel *tNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(400, (cellHeight/2)-cellHeight/2, 300,cellHeight)];
            tNameLabel.textAlignment = UITextAlignmentLeft;
            tNameLabel.adjustsFontSizeToFitWidth = YES;
            tNameLabel.backgroundColor = [UIColor clearColor];
            tNameLabel.textColor = [UIColor blackColor];
            tNameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
            self.nameLabel = tNameLabel;
            [tNameLabel release];
            [self addSubview:nameLabel];
            
            UILabel *tPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(734, 0, cell5Width, cellHeight)];
            tPriceLabel.textAlignment = UITextAlignmentLeft;
            tPriceLabel.adjustsFontSizeToFitWidth = YES;
            tPriceLabel.backgroundColor = [UIColor clearColor];
            tPriceLabel.textColor = [UIColor blackColor];
            self.priceLabel = tPriceLabel;
            [tPriceLabel release];
            [self addSubview:priceLabel];
}

    return self;
}

-(IBAction)edit_ButtonTapped:(id)sender
{
    KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
    NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:self.salesOrder];
    [dataSalesOrderItem release];
    if([self.salesOrder NumberOfOderItems]==0)
    {
        for(SalesOrderItem* orderItem in orderItemArr)
        {
            [self.salesOrder addOrderItem:orderItem];
        }
    }
    
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    appDelegate.salesOrder=self.salesOrder;
    appDelegate.currentCustomer=self.salesOrder.customer;
    //NSLog(@"%d", self.salesOrder.Order_id);
    [self.delegate editSaleOrderForEditButton];
}  
-(IBAction)pdf_ButtonTapped:(id)sender
{
    KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:self.salesOrder];
    
    [dataSalesOrderItem release];
    
    if([self.salesOrder NumberOfOderItems]==0)
    {
        for(SalesOrderItem* orderItem in orderItemArr)
        {
            [self.salesOrder addOrderItem:orderItem];
        }
        
        //NSLog(@"pdf tapped!!");
        
        appDelegate.PdfEmailOrdersalesOrder=self.salesOrder;
        appDelegate.PDFTransfercurrentCustomer=self.salesOrder.customer;
        
        ///NSLog(@"%d", self.salesOrder.Order_id);
        
        [self.delegate editSaleOrderPDFButton:(SalesOrder*)salesOrder SalesPersonCode:appDelegate.loggedInSalesPerson];
        
    }
    else
    {
        appDelegate.PdfEmailOrdersalesOrder=self.salesOrder;
        appDelegate.PDFTransfercurrentCustomer=self.salesOrder.customer;
        
        //NSLog(@"%d", self.salesOrder.Order_id);
        
        [self.delegate editSaleOrderPDFButton:(SalesOrder*)salesOrder SalesPersonCode:appDelegate.loggedInSalesPerson];
    }
}

-(IBAction)pdfQuo_ButtonTapped:(id)sender
{
    KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:self.salesOrder];
    
    [dataSalesOrderItem release];
    
    if([self.salesOrder NumberOfOderItems]==0)
    {
        for(SalesOrderItem* orderItem in orderItemArr)
        {
            [self.salesOrder addOrderItem:orderItem];
        }
        
        //NSLog(@"pdf tapped!!");
        
        appDelegate.PdfEmailOrdersalesOrder=self.salesOrder;
        appDelegate.PDFTransfercurrentCustomer=self.salesOrder.customer;
        
        ///NSLog(@"%d", self.salesOrder.Order_id);
        
        [self.delegate editSaleOrderPDFQuoButton:(SalesOrder*)salesOrder SalesPersonCode:appDelegate.loggedInSalesPerson];
        
    }
    else
    {
        appDelegate.PdfEmailOrdersalesOrder=self.salesOrder;
        appDelegate.PDFTransfercurrentCustomer=self.salesOrder.customer;
        
        //NSLog(@"%d", self.salesOrder.Order_id);
        
        [self.delegate editSaleOrderPDFQuoButton:(SalesOrder*)salesOrder SalesPersonCode:appDelegate.loggedInSalesPerson];
    }
}

-(IBAction)delete_ButtonTapped:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingAlertTag;
    [alert show];
    [alert release];
    
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == kConfirmingAlertTag)
    {
        if(buttonIndex == 1)
        {
            KDSDataSalesOrder* dataSaleOrder=[[KDSDataSalesOrder alloc] init];
            self.salesOrder.isDeleted=YES;
            [dataSaleOrder updateSaleOrderIsDeleted:salesOrder];
            [dataSaleOrder release];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"The order has been deleted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=kConfirmedAlertTag;
            [alert show];
            [alert release];
            [delegate reloadNewOrderTable];
        }
    }
     
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [pdfquoButton release];
    [orderIdLabel release];
    [delegate release];
    [salesOrder release];
    [editButton release];
    [pdfButton release];
    [deleteButton release];
    [lineColor release];
    [codeLabel release];
    [nameLabel release];
    [priceLabel release];
    [currencyLabel release];
    [super dealloc];

}
-(void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context, lineColor.CGColor);       
    
	// CGContextSetLineWidth: The default line width is 1 unit. When stroked, the line straddles the path, with half of the total width on either side.
	// Therefore, a 1 pixel vertical line will not draw crisply unless it is offest by 0.5. This problem does not seem to affect horizontal lines.
	CGContextSetLineWidth(context, 1.0);
    
	// Add the vertical lines
	/*CGContextMoveToPoint(context, cell1Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+0.5, rect.size.height);
    
	CGContextMoveToPoint(context, cell1Width+cell2Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+0.5, rect.size.height);
    
    CGContextMoveToPoint(context, cell1Width+cell2Width+cell3Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+cell3Width+0.5, rect.size.height);
    
    CGContextMoveToPoint(context, cell1Width+cell2Width+cell3Width+cell4Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+cell3Width+cell4Width+0.5, rect.size.height);
    
	// Add bottom line
	CGContextMoveToPoint(context, 0, rect.size.height);
	CGContextAddLineToPoint(context, rect.size.width, rect.size.height-0.5);
	
	// If this is the topmost cell in the table, draw the line on top
	if (topCell)
	{
		CGContextMoveToPoint(context, 0, 0);
		CGContextAddLineToPoint(context, rect.size.width, 0);
	}*/
	
	// Draw the lines
	CGContextStrokePath(context); 
}
-(void)setTopCell:(BOOL)newTopCell
{
    topCell = newTopCell;
}


@end
