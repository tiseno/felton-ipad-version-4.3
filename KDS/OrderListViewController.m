//
//  OrderListViewController.m
//  KDS
//
//  Created by Tiseno on 11/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OrderListViewController.h"

@implementation OrderListViewController
@synthesize BtnDeleteOrderByDate;

@synthesize newOrderTabelView, transferedOrderTableView, salesOrderArr;
@synthesize  transferedOrderViewController,salesOrder;
@synthesize newOrderTableViewHeader, transferedOrderTableViewHeader, datePicker,isPickerHidden, cancelButton;
@synthesize loadingView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        isPickerHidden=YES;
        [self initializeTableData];
    }
    return self;
}

- (void)dealloc
{
    [cancelButton release];
    [newOrderTabelView release];
    [transferedOrderTableView release];
    //[newOrderTableViewController release];
    [transferedOrderViewController release];
    [salesOrderArr release];
    //[newOrderTableViewController release];
    [transferedOrderViewController release];
    [salesOrder release];
    [datePicker release];
    [BtnDeleteOrderByDate release];
    [super dealloc];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)editSaleOrderForEditButton
{
    YourCartViewController *yourCartViewController=[[YourCartViewController alloc] initWithNibName:@"YourCartViewController" bundle:nil];
    yourCartViewController.title=@"Felton";
    yourCartViewController.EditTag=@"edittag";
    [self.navigationController pushViewController:yourCartViewController animated:YES];
    [yourCartViewController release];
}  

-(void)editDeleteSaleOrderForEditButton
{
    DeletedCartViewController *yourCartViewController=[[DeletedCartViewController alloc] initWithNibName:@"DeletedCartViewController" bundle:nil];
    yourCartViewController.title=@"Felton";
    yourCartViewController.EditTag=@"edittag";
    [self.navigationController pushViewController:yourCartViewController animated:YES];
    [yourCartViewController release];
}

-(BOOL)NetworkStatus
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus==NotReachable);
}

-(void)editSaleOrderPDFButton:(SalesOrder *)isaleOrder SalesPersonCode:(SalesPerson *)isalesPersonCode
//-(void)editSaleOrderPDFButton:(SalesOrder*)isaleOrder SalesPersonCode:(NSString*)isalesPersonCode
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }else 
    {
        //-(id)initWithSalesOrder:(SalesOrder*)isaleOrder SalesPersonCode:(NSString*)isalesPersonCode;
        
        /**/TransferOrderPdfRequest* request=[[TransferOrderPdfRequest alloc] initWithSalesOrder:isaleOrder SalesPersonTransfer:isalesPersonCode];
        NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
        [networkHandler setDelegate:self];
        [networkHandler request:request];
        [request release];
        [networkHandler release]; 
        
        UIView *selfView=self.view;
        LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
        self.loadingView=temploadingView;
    } 
} 

-(void)editTransferredSaleOrderPDFButton:(SalesOrder *)isaleOrder SalesPersonCode:(SalesPerson *)isalesPersonCode
//-(void)editSaleOrderPDFButton:(SalesOrder*)isaleOrder SalesPersonCode:(NSString*)isalesPersonCode
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }else 
    {
        //-(id)initWithSalesOrder:(SalesOrder*)isaleOrder SalesPersonCode:(NSString*)isalesPersonCode;
        
        /**/TransferOrderPdfRequest* request=[[TransferOrderPdfRequest alloc] initWithSalesOrder:isaleOrder SalesPersonTransfer:isalesPersonCode];
        NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
        [networkHandler setDelegate:self];
        [networkHandler request:request];
        [request release];
        [networkHandler release]; 
        
        UIView *selfView=self.view;
        LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
        self.loadingView=temploadingView;
    } 
}


-(IBAction)cancelButtonTapped
{
    datePicker.hidden=YES;
    isPickerHidden = YES;
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.3];
    cancelButton.frame=CGRectMake(504, cancelButton.frame.origin.y, cancelButton.frame.size.width, cancelButton.frame.size.height);
    cancelButton.alpha = 0.0;
    [UIView commitAnimations];
}
-(IBAction)deleteOrderButton_Tapped
{    
    if(isPickerHidden)
    {
        datePicker.hidden=NO;
        isPickerHidden=NO;
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.3];
        cancelButton.frame=CGRectMake(604, cancelButton.frame.origin.y, cancelButton.frame.size.width, cancelButton.frame.size.height);
        cancelButton.alpha = 1.0;
        [UIView commitAnimations];
    }
    else if(!isPickerHidden)
    {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        NSString* alertStr = [NSString stringWithFormat:@"You're about to delete all the orders up to %@", [formatter stringFromDate:[datePicker date]]];
        [formatter release];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Deletion" message:alertStr delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        alert.tag= kConfirmingAlertTag;
        [alert show];
        [alert release];
        
        /*NSDate* pickerDate = [datePicker date];
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.3];
        cancelButton.frame=CGRectMake(504, cancelButton.frame.origin.y, cancelButton.frame.size.width, cancelButton.frame.size.height);
        cancelButton.alpha = 0.0;
        [UIView commitAnimations];
        datePicker.hidden=YES;
        isPickerHidden=YES;
        for(SalesOrder* order in salesOrderArr)
        {
            NSDate* orderDate = order.Date;
            switch([orderDate compare:pickerDate])
            {
                case NSOrderedAscending:
                    order.isDeleted = YES;
                    break;
                case NSOrderedSame:
                    order.isDeleted = YES;
                    break;
                case NSOrderedDescending:break;
            }
            KDSDataSalesOrder* tSaleOrder = [[KDSDataSalesOrder alloc] init];
            [tSaleOrder updateSaleOrderIsDeleted:order];
            [tSaleOrder release];
        }
        [self reloadNewOrderTable];*/
    }
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    int deletedOrders=0;
    if(alertView.tag == kConfirmingAlertTag)
    {
        if(buttonIndex == 1)
        {
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            [formatter setDateFormat:@"yyyy/MM/dd"];
            NSString* pickerDateStr = [formatter stringFromDate:[datePicker date]];
            NSDate* pickerDate = [formatter dateFromString:pickerDateStr];
            [formatter release];
            
            [UIView beginAnimations:@"" context:nil];
            [UIView setAnimationDuration:0.3];
            cancelButton.frame=CGRectMake(504, cancelButton.frame.origin.y, cancelButton.frame.size.width, cancelButton.frame.size.height);
            cancelButton.alpha = 0.0;
            [UIView commitAnimations];
            datePicker.hidden=YES;
            isPickerHidden=YES;
            
            [self initializeTableDataR];
            
            for(SalesOrder* order in self.salesOrderArr)
            {
                NSDate* orderDate = order.Date;
                switch([orderDate compare:pickerDate])
                {
                    case NSOrderedAscending:
                        order.isDeleted = YES;
                        deletedOrders++;
                        break;
                    case NSOrderedSame:
                        order.isDeleted = YES;
                        deletedOrders++;
                        break;
                    case NSOrderedDescending:break;
                }
                KDSDataSalesOrder* tSaleOrder = [[KDSDataSalesOrder alloc] init];
                [tSaleOrder updateSaleOrderIsDeleted:order];
                [tSaleOrder release];
            }
            [self reloadTransferedTable];
            NSString* alertStr = [NSString stringWithFormat:@"%d order(s) has been deleted successfully", deletedOrders];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Deletion Completed" message:alertStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alert.tag=kConfirmedAlertTag;
            [alert show];
            [alert release];
        }
        else 
        {
            [UIView beginAnimations:@"" context:nil];
            [UIView setAnimationDuration:0.3];
            cancelButton.frame=CGRectMake(504, cancelButton.frame.origin.y, cancelButton.frame.size.width, cancelButton.frame.size.height);
            cancelButton.alpha = 0.0;
            datePicker.hidden=YES;
            isPickerHidden=YES;
            [UIView commitAnimations];
        }
        
    }
}

-(void)initializeTableData
{
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KDSDataSalesOrder *dataSalesOrder;
    if(appDelegate.salesOrder.Type==SalesOrderMain)
    {
        dataSalesOrder = [[KDSDataSalesOrder alloc] init];
    }
    else //if(appDelegate.salesOrder.Type==SalesOrderBlind)
    {
        dataSalesOrder = [[KDSDataBlindSalesOrder alloc] init];
    }
    NSArray* tNewSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:NO IsDeleted:NO];
    self.salesOrderArr=tNewSalesOrderArr;
    [dataSalesOrder release];
}

-(void)initializeTableDataR
{
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KDSDataSalesOrder *dataSalesOrder;
    if(appDelegate.TransferOrdersalesOrder.Type==SalesOrderMain)
    {
        dataSalesOrder = [[KDSDataSalesOrder alloc] init];
    }
    else //if(appDelegate.TransferOrdersalesOrder.Type==SalesOrderBlind)
    {
        dataSalesOrder = [[KDSDataBlindSalesOrder alloc] init];
    }
    NSArray* tNewSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:YES IsDeleted:NO];
    self.salesOrderArr=tNewSalesOrderArr;
    [dataSalesOrder release];
}

 
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    //BtnDeleteOrderByDate.hidden=YES;
    cancelButton.alpha = 0.0;
    datePicker.hidden = YES;
    datePicker.datePickerMode =UIDatePickerModeDate;
    [datePicker setDate:[NSDate date] animated:NO];
    [super viewDidLoad];
    /*if(newOrderTableViewController == nil)
    {
        NewOrderTableViewController *tNewOrderTableViewController = [[NewOrderTableViewController alloc] init];
        self.newOrderTableViewController = tNewOrderTableViewController;
        self.newOrderTableViewController.orderListViewController=self;
        [tNewOrderTableViewController release];
    } 
    if(transferedOrderViewController == nil)
    {
        TransferedOrderTableViewController *tTransferedOrderTableViewController = [[TransferedOrderTableViewController alloc] init];
        self.transferedOrderViewController = tTransferedOrderTableViewController;
        self.transferedOrderViewController.orderListViewController=self;
        [tTransferedOrderTableViewController release];
    }
    
    [newOrderTabelView setDataSource:self.newOrderTableViewController];
    [newOrderTabelView setDelegate:self.newOrderTableViewController];
    
    [transferedOrderTableView setDataSource:self.transferedOrderViewController];
    [transferedOrderTableView setDelegate:self.transferedOrderViewController];
    
    self.newOrderTableViewController.view = self.newOrderTableViewController.tableView;
    self.transferedOrderViewController.view = self.transferedOrderViewController.tableView;
    
    UIView *NewOrdeertableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(newOrderTabelView.frame.origin.x, newOrderTabelView.frame.origin.y-30, self.newOrderTabelView.frame.size.width, 30)];
    NewOrdeertableHeaderView.backgroundColor = [UIColor colorWithRed:0.0196 green:0.513 blue:0.949 alpha:1.0];
    
    UILabel *NewOrderTableHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 5, self.newOrderTabelView.frame.size.width/2, 21)];
    NewOrderTableHeaderLabel.text = @"New Order";
    NewOrderTableHeaderLabel.backgroundColor = [UIColor clearColor];
    NewOrderTableHeaderLabel.textColor = [UIColor whiteColor];
    NewOrderTableHeaderLabel.textAlignment = UITextAlignmentLeft;
    NewOrderTableHeaderLabel.font=[UIFont boldSystemFontOfSize:17];
    [NewOrdeertableHeaderView addSubview:NewOrderTableHeaderLabel];
    newOrderTableViewHeader = NewOrdeertableHeaderView;
    [self.view addSubview:newOrderTableViewHeader];
    
    UIView *TransferedOrdertableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(transferedOrderTableView.frame.origin.x, transferedOrderTableView.frame.origin.y-30, self.transferedOrderTableView.frame.size.width, 30)];
    TransferedOrdertableHeaderView.backgroundColor = [UIColor colorWithRed:0.0196 green:0.513 blue:0.949 alpha:1.0];
    
    UILabel *TransferedOrderTableHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 5, self.transferedOrderTableView.frame.size.width/2, 21)];
    TransferedOrderTableHeaderLabel.text = @"Transfered Order";
    TransferedOrderTableHeaderLabel.backgroundColor = [UIColor clearColor];
    TransferedOrderTableHeaderLabel.textColor = [UIColor whiteColor];
    TransferedOrderTableHeaderLabel.textAlignment = UITextAlignmentLeft;
    TransferedOrderTableHeaderLabel.font=[UIFont boldSystemFontOfSize:17];
    [TransferedOrdertableHeaderView addSubview:TransferedOrderTableHeaderLabel];
    transferedOrderTableViewHeader = TransferedOrdertableHeaderView;
    [self.view addSubview:transferedOrderTableViewHeader];
    
    [NewOrdeertableHeaderView release];
    [NewOrderTableHeaderLabel release];
    [TransferedOrdertableHeaderView release];
    [TransferedOrderTableHeaderLabel release];*/
}

- (void)viewDidUnload
{
    [self setBtnDeleteOrderByDate:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(void)reloadNewOrderTable
{
    //[self initializeTableData];
    //[self.newOrderTableViewController initializeTableData];
    [newOrderTabelView reloadData];
}
-(void)reloadTransferedTable
{ 
    //[self initializeTableData];
    [self.transferedOrderViewController initializeTableDataR];
    
    [transferedOrderTableView reloadData];
}


-(void)handleRecievedResponseMessage:(XMLResponse *)responseMessage
{
    if([responseMessage isKindOfClass:[TransferOrderPDFResponse class]])
    {
        NSString *feedBackResult=((TransferOrderPDFResponse*)responseMessage).getPDFPath;
        
        
        
        if (![feedBackResult isEqualToString:@""]) 
        {
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
                
                mailer.mailComposeDelegate = self;
                
                [mailer setSubject:@"A Message from Felton"];
                
                NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
                [mailer setToRecipients:toRecipients];
                
                //NSString *PDFpath=[[NSString alloc]initWithString:@"http://www.mobilesolutions.com.my/TUPPERWARE/AgendaSlides/Carhiremarket&Suncars_Documentation.pdf"];	
                NSString *PDFpath=[[NSString alloc]initWithFormat:@"http://219.94.43.102/KDSOrderPDFConvertor/PDF/%@",feedBackResult];
                //NSString *emailBody = @"Have you seen the MobileTuts+ web site?";
                
                NSString *emailBody = [[NSString alloc]initWithFormat:@"You got a new pdf, click here to view---> %@",PDFpath];
                [mailer setMessageBody:emailBody isHTML:NO];
                
                // only for iPad
                // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
                
                [self presentModalViewController:mailer animated:YES];
                
                [mailer release];
                
                [self.loadingView removeView];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure" 
                                                                message:@"Your device doesn't support the composer sheet" 
                                                               delegate:nil 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles: nil];
                [alert show];
                [alert release];
            }
            
            //[self.loadingView removeView];
        }
    } 
}

- (IBAction)openMail:(id)sender 
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:@"A Message from Felton"];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
        [mailer setToRecipients:toRecipients];
        
        UIImage *myImage = [UIImage imageNamed:@"mobiletuts-logo.png"];
        NSData *imageData = UIImagePNGRepresentation(myImage);
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mobiletutsImage"];	
        
        NSString *emailBody = @"Have you seen the MobileTuts+ web site?";
        [mailer setMessageBody:emailBody isHTML:NO];
        
        // only for iPad 
        // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentModalViewController:mailer animated:YES];
        
        [mailer release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure" 
                                                        message:@"Your device doesn't support the composer sheet" 
                                                       delegate:nil 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    
}



#pragma mark - MFMailComposeController delegate


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the Drafts folder");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was nog saved or queued, possibly due to an error");
			break;
		default:
			NSLog(@"Mail not sent");
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}

@end
