//
//  ListOfProductsTableViewCellController.h
//  KDS
//
//  Created by Tiseno on 12/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ListOfProductsTableViewCellController : UITableViewCell {
    
    UILabel *productIdLabel;
    UILabel *productNameLabel;
    UILabel *priceLabel;
    UIColor *lineColor;
}
@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic,retain) UILabel *productNameLabel;
@property (nonatomic, retain) UILabel *priceLabel;
@property (nonatomic, retain) UILabel *productIdLabel;
@end
