//
//  NewOrderTableViewController.h
//  KDS
//
//  Created by Tiseno on 11/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewOrderTableViewCell.h"
#import "KDSDataSalesOrder.h"
#import "KDSAppDelegate.h"
#import "SalesOrderType.h"
#import "KDSDataBlindSalesOrder.h"
//#import "OrdereditViewController.h"

//@class OrderListViewController;
@class OrdereditViewController;
@interface NewOrderTableViewController : UITableViewController <UITableViewDelegate,UITableViewDataSource>{

    NSArray *neworderSalesOrderArray;
    //OrdereditViewController* orderListViewController;
    OrdereditViewController* ordereditViewController;
} 
@property (nonatomic, retain) NSArray *neworderSalesOrderArray;
//@property (nonatomic, retain) OrderListViewController* orderListViewController;
//@property (nonatomic, retain) OrdereditViewController* orderListViewController;
@property (nonatomic, retain) OrdereditViewController* ordereditViewController;
-(void)initializeTableData;
@end
