//
//  NewOrderCategoryViewController.h
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewOrder_ProductViewController.h"
#import "YourCartViewController.h"
#import "Customer.h"
#import "KDSAppDelegate.h"
#import "Item.h"
#import "ButtonWithProduct.h"
//#import "ProductCatalogViewController.h"
#import "NewOrderViewController.h"
//#import "KDSSuperViewController.h"
#import "KDSDataListGallery.h"
#import "LoadingView.h"

@interface NewOrderCategoryViewController : UIViewController<UISearchBarDelegate> {
    
    UILabel *customerNameLabel;
    UILabel *companyNameLabel;
    UILabel *telLabel;
    UILabel *faxLabel;
    UILabel *emailLabel;
    UISegmentedControl* scCategories;
    
    UIScrollView *categoryScrollView;
    Customer *currentSelectedCustomer;
    
    NSString* categoryitem;
    
    int heightOfEditedView;
    int heightOffset;
    BOOL searchById;
    
    UISearchBar* search;
    UIScrollView *itemScrollView;
    
    UISegmentedControl* searchMethodSc;
    
    Item* selectedItem;
    
    NSArray* productsArr;
    NSMutableArray* productsArr_Copied;
    
    
    LoadingView *loadingView;
    
    int counterA;
    bool startA;
    NSString *_CountDownTime;
    NSTimer *timerA; 
    //IBOutlet UILabel *secondsA;
    BOOL loadData;
    
    
}
//@property (nonatomic, retain) id<KDSSuperViewController> superViewController;
@property (nonatomic,retain) UIView *selfView;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic,retain) NSString *_CountDownTime;
@property (nonatomic, retain) IBOutlet UILabel *customerNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *companyNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *telLabel;
@property (nonatomic, retain) IBOutlet UILabel *faxLabel;
@property (nonatomic, retain) IBOutlet UILabel *emailLabel;
@property (nonatomic, retain) IBOutlet UISegmentedControl* scCategories;
@property (nonatomic, retain) Customer *currentSelectedCustomer;
@property (nonatomic, retain) IBOutlet UIScrollView *categoryScrollView;
-(void)initializeCategoryButtonsWithCategoryType:(NSString*)categoryType;
-(BOOL)doesCategory:(NSString*)category belongToCategoryType:(NSString*)categoryType;
-(IBAction) segmentedControlIndexChanged;
@property (retain, nonatomic) IBOutlet UISegmentedControl *listGallery;


@property (nonatomic) BOOL searchById;
@property (nonatomic, retain) NSArray* productsArr;
@property (nonatomic, retain) Item* selectedItem;
@property (nonatomic, retain) NSString* categoryitem;
@property (nonatomic, retain) NSMutableArray* productsArr_Copied;
@property (nonatomic, retain) NSMutableArray* toRemove;
@property (retain, nonatomic) IBOutlet UISegmentedControl *searchMethodSc;
@property (retain, nonatomic) IBOutlet UISearchBar *search;
@property (retain, nonatomic) IBOutlet UILabel *lblprefix;
@property (retain, nonatomic) IBOutlet UISwitch *prefixonoff;

-(void)searchView;
@property (nonatomic, retain) NSDictionary *allNames;
@property (nonatomic, retain) NSMutableDictionary *names;
@property (nonatomic, retain) NSMutableArray *keyss;
@property (retain, nonatomic) IBOutlet UIButton *btnViewYourCart;
@property (retain, nonatomic) IBOutlet UIButton *btnCancelOrder;
@property (retain, nonatomic) IBOutlet UITableView *customerDetailtable;
@property (nonatomic, retain) UIView *tableViewHeader;
-(IBAction)ListTapped;

@end
