//
//  NewOrderTransferResponse.h
//  KDS
//
//  Created by Tiseno Mac 2 on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"

@interface NewOrderTransferResponse : XMLResponse{
    NSString *getOrderResponse;
}
@property (nonatomic,retain) NSString *getOrderResponse;

@end
