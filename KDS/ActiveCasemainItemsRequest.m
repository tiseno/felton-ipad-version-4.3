//
//  ActiveCasemainItemsRequest.m
//  KDS
//
//  Created by Tiseno Mac 2 on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ActiveCasemainItemsRequest.h"

@implementation ActiveCasemainItemsRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.webserviceURL=@"http://219.94.43.102/KDSMainItem/service.asmx";
        self.SOAPAction=@"GetActiveMainItem";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}
@end
