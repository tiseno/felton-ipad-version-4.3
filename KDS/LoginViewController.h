//
//  LoginViewController.h
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KDSSuperViewController.h"

#import "MenuViewController.h"
#import "NetworkHandler.h"
#import "NetworkHandlerDelegate.h"
#import "AllSalesPeopleRequest.h"
#import "AllSalesPeopleResponse.h"
#import "KDSDataSalesPerson.h"
#import "KDSAppDelegate.h"
#import "LoadingView.h"
#import "Reachability.h"


@class Reachability;
@interface LoginViewController : UIViewController<NetworkHandlerDelegate, UITextFieldDelegate> {
    
    UITextField *userNameTextView;
    UITextField *passwordTextView;
    
    UINavigationController *navController;
    
    LoadingView *loadingView;
    id<KDSSuperViewController> superViewController;
}
@property (nonatomic, retain) IBOutlet UITextField *userNameTextView;
@property (nonatomic, retain) IBOutlet UITextField *passwordTextView;
@property (nonatomic, retain) UINavigationController *navController;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) id<KDSSuperViewController> superViewController;

-(BOOL)NetworkStatus;
@end
