//
//  OrderHistoryTableGeneratorDelegate.h
//  KDS
//
//  Created by Tiseno on 12/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol OrderHistoryTableGeneratorDelegate <NSObject>

-(void)generateOderHistoryTable:(id)sender;

@end
