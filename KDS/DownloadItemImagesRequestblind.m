//
//  DownloadItemImagesRequestblind.m
//  KDS
//
//  Created by Tiseno Mac 2 on 5/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DownloadItemImagesRequestblind.h"

@implementation DownloadItemImagesRequestblind
@synthesize ItemType;

-(id)initWithItemTypes:(NSString*)iItemType;
{
    self=[super init];
    if(self)
    {
        self.webserviceURL=@"http://219.94.43.102/KDSItemImage/service.asmx";
        self.SOAPAction=@"getItemImage";
        self.requestType=WebserviceRequest;
        self.ItemType=iItemType;
    }
    return self;
}

-(void)dealloc
{
    [ItemType release];
    
    [super dealloc]; 
}

-(NSString*) generateHTTPPostMessage
{
    NSString *postMsg = [NSString stringWithFormat:@"ItemType=%@",self.ItemType];
    
    return postMsg;
    
}
@end
