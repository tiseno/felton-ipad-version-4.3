//
//  NewOrderTransferResponse.m
//  KDS
//
//  Created by Tiseno Mac 2 on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NewOrderTransferResponse.h"

@implementation NewOrderTransferResponse
@synthesize getOrderResponse;

-(void)dealloc
{
    [getOrderResponse release];
    [super dealloc];
}

@end
