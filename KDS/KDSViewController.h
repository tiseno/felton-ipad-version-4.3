//
//  KDSViewController.h
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KDSSuperViewController.h"
#import "LoginViewController.h"
#import "MenuViewController.h"
//#import "NewOrderCategoryViewController.h"

@interface KDSViewController : UIViewController<KDSSuperViewController> {
    LoginViewController *logingInViewController;
    MenuViewController *menuViewController;
    NewOrderCategoryViewController *newOrderCatViewCtrr;
    UINavigationController* navController;
}
@property (nonatomic,retain) LoginViewController *logingInViewController;
@property (nonatomic,retain) MenuViewController *menuViewController;
@property (nonatomic,retain) NewOrderCategoryViewController *newOrderCatViewCtrr;
@property (nonatomic,retain) UINavigationController* navController;
-(void)loadLogin;
-(void)loadMenu;
-(void)loadProductCatalog;
@end
