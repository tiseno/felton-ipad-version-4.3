//
//  TransferedOrderTableViewController.h
//  KDS
//
//  Created by Tiseno on 11/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KDSAppDelegate.h"
#import "TransferedOrderTableViewCell.h"
#import "KDSDataSalesOrder.h"
#import "KDSDataBlindSalesOrder.h"
//#import "KDSDateOrderPackage.h"
//#import "OrderPackaging.h"

@class OrderTransferedViewController;
@interface TransferedOrderTableViewController : UITableViewController <UITableViewDelegate,
UITableViewDataSource>{
    
    NSArray* transferedOrderArr;
    OrderTransferedViewController* OrdertransferedViewController;
}
@property (nonatomic, retain) NSArray* transferedOrderArr;
@property (nonatomic, retain) OrderTransferedViewController* OrdertransferedViewController;
@property (nonatomic, retain) NSArray* tTransferecSalesOrderPackageArr;
-(void)initializeTableDataR;
@end
