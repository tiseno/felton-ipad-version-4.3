//
//  DownloadImages.h
//  KDS
//
//  Created by Tiseno Mac 2 on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AsyncImageViewDelegate.h"

@interface DownloadImages : NSObject<AsyncImageViewDelegate> {
    NSString *Image_Path;
}
@property (nonatomic,retain) NSString *Image_Path;

@end
