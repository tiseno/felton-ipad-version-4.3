//
//  QuotationFormPdfRequest.h
//  KDS
//
//  Created by Tiseno Mac 2 on 10/17/12.
//
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"
#import "SalesPerson.h"
#import "SalesOrder.h"

@interface QuotationFormPdfRequest : XMLRequest {
    SalesOrder* newTransferSalesOrder;
    SalesPerson* salesPersonTransfer;
}
@property(nonatomic, retain) SalesOrder* newTransferSalesOrder;
@property(nonatomic, retain) SalesPerson* salesPersonTransfer;

-(id)initWithSalesOrder:(SalesOrder*)isaleOrder SalesPersonTransfer:(SalesPerson*)isalesPersonTransfer;@end
