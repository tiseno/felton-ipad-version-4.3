//
//  KDSDataItem.h
//  KDS
//
//  Created by Tiseno on 12/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KDSDataLayerBase.h"
#import "Item.h"
#import "UOMAndConversion.h"
#import "DefaultPriceAndUOM.h"
#import "Customer.h"
#import "SalesPerson.h"
#import "LastSellingPrice.h"
#import "AppWideConstants.h"
#import "BlindItem.h"

@interface KDSDataItem : KDSDataLayerBase {
    
    BOOL runBatch;
}
@property (nonatomic)BOOL runBatch;
-(NSArray*)selectUpdateItem:(Item*)item;
-(NSArray*)selectItem;
-(NSArray*)selectLastSellingPriceForItem:(Item*)item andCustomer:(Customer*)customer andSalesperson:(SalesPerson*)salesperson;
-(void)addDefaultPriceToItem:(Item*)tItem;
-(DataBaseInsertionResult)insertItem:(Item*)tItem;
-(DataBaseInsertionResult)insertUomAndConversion:(UOMAndConversion*)tUOM forItem:(int)tItem_id;
-(DataBaseInsertionResult)insertDefaultPriceAndUOM:(DefaultPriceAndUOM*)tDefaultPrice forItem:(int)tItem_id;
-(DataBaseDeletionResult)deletItem:(Item*)tItem;
-(DataBaseDeletionResult)deletUOMAndConversionForItem:(Item*)tItem;
-(DataBaseDeletionResult)deletDefaultPriceAndUOMForItem:(Item*)tItem;
-(DataBaseInsertionResult)insertItemArray:(NSArray*)itemArr;
-(NSArray*)selectEditItem:(NSString *)editItem;
-(void)deleteAllitems;
-(NSArray*)selectItemUOM:(Item*)item;
-(NSArray*)selectremoveItemMain;
-(void)deleteMainitems;
-(void)deleteBlinditems;

@end
