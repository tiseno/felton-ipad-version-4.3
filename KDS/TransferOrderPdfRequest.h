//
//  TransferOrderPdfRequest.h
//  KDS
//
//  Created by Tiseno Mac 2 on 3/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"
#import "SalesPerson.h"
#import "SalesOrder.h"

@interface TransferOrderPdfRequest : XMLRequest {
    SalesOrder* newTransferSalesOrder;
    SalesPerson* salesPersonTransfer;
}
@property(nonatomic, retain) SalesOrder* newTransferSalesOrder;
@property(nonatomic, retain) SalesPerson* salesPersonTransfer;

-(id)initWithSalesOrder:(SalesOrder*)isaleOrder SalesPersonTransfer:(SalesPerson*)isalesPersonTransfer;

@end
