//
//  NetworkHandler.m
//  Holiday Villa
//
//  Created by Jermin Bazazian on 4/14/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "NetworkHandler.h"


@implementation NetworkHandler
@synthesize webData,soapResults,conn;
-(id)init
{
    self=[super init];
    if(self)
    {
    }
    return self;
}
-(void) dealloc
{
    [webData release];
    [soapResults release];
    [conn release];
    [super dealloc];
}

- (id)delegate
{
	return _delegate;
}
-(void)setDelegate:(id)new_delegate
{
	if (_delegate)
	{
		[_delegate release];
	}
	[new_delegate retain];
    _delegate=new_delegate;
}

-(void)request:(XMLRequest*)xmlRequest
{
    NSString *urlString=[NSString stringWithFormat:@"%@/%@",xmlRequest.webserviceURL,xmlRequest.SOAPAction];
    if (xmlRequest.requestType==FileRequest) {
        urlString=[NSString stringWithFormat:@"%@",xmlRequest.webserviceURL];
    }
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    NSString *postMsg=[xmlRequest generateHTTPPostMessage];
    NSLog(@"%@",postMsg);
    NSString *msgLength = [NSString stringWithFormat:@"%d", [postMsg length]];
    [req addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [req addValue:msgLength forHTTPHeaderField:@"Content-Length"];
    if(xmlRequest.requestType==WebserviceRequest)
    {
        [req setHTTPMethod:@"POST"];
    }
    else if(xmlRequest.requestType==FileRequest)
    {
        [req setHTTPMethod:@"GET"];
    }
    [req setHTTPBody: [postMsg dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLConnection *tconn = [[NSURLConnection alloc] initWithRequest:req delegate:self];
    if(tconn)
    {
        self.conn=tconn;
        [tconn release];
        NSMutableData *twebData=[NSMutableData data];
        self.webData=twebData;
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
    [self.webData setLength: 0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{
    [webData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
    //propagate the error message
}
-(void) connectionDidFinishLoading:(NSURLConnection *) connection {
    NSLog(@"DONE. Received Bytes: %d", [webData length]);
    NSString *theXML = [[NSString alloc] 
                        initWithBytes: [webData mutableBytes] 
                        length:[webData length] 
                        encoding:NSUTF8StringEncoding];
    //---shows the XML---
    NSLog(@"%@",theXML);
    [theXML release];
    ResponseTranslator *translator=[[ResponseTranslator alloc] init];
    XMLResponse *responseMessage=[translator translate:webData];
    [translator release];
    if ([_delegate respondsToSelector:@selector(handleRecievedResponseMessage:)]) {
        [_delegate handleRecievedResponseMessage:responseMessage];
    }
}
@end
