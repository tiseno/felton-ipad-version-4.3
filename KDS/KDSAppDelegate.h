//
//  KDSAppDelegate.h
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Customer.h"
#import "Item.h"
#import "KDSDataItem.h"
#import "SalesOrder.h"
#import "SalesPerson.h"
#import "AppWideConstants.h"
#import "BlindSalesOrder.h"
#import "BlindItem.h"
#import "OrderPackaging.h"
#import "KDSDataListGallery.h"
#import "listgallery.h"

@class KDSViewController;
@class NewOrderCategoryViewController;
@class LoginViewController;
@interface KDSAppDelegate : NSObject <UIApplicationDelegate> {
    Customer *currentCustomer;
    Customer *OrderTransfercurrentCustomer;
    Item* currentItem;
    Item* currentUpdateOrderItem;
    SalesOrder* salesOrder;
    SalesOrder* TransferOrdersalesOrder;
    SalesOrder* PdfEmailOrdersalesOrder;
    SalesPerson* loggedInSalesPerson;
    Customer *PDFTransfercurrentCustomer;
    bool isSynced;
    
    NSMutableDictionary *itemCategoryDictionary;
     
}
 
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) Customer *currentCustomer;
@property (nonatomic, retain) Customer *OrderTransfercurrentCustomer;
@property (nonatomic, retain) Customer *PDFTransfercurrentCustomer;
@property (nonatomic, retain) Item* currentItem;
@property (nonatomic, retain) Item* currentUpdateOrderItem;
@property (nonatomic, retain) NSMutableDictionary *itemCategoryDictionary;
@property (nonatomic, retain) IBOutlet KDSViewController *viewController;
@property (nonatomic, retain) SalesOrder* salesOrder;
@property (nonatomic, retain) SalesOrder* TransferOrdersalesOrder;
@property (nonatomic, retain) SalesOrder* PdfEmailOrdersalesOrder;
@property (nonatomic, readonly) SalesPerson* loggedInSalesPerson;
@property (nonatomic) bool isSynced;
@property (nonatomic, retain) OrderPackaging* apporderPackaging;
@property (nonatomic, retain) NSArray *orderPackagingArr;

-(void)fillItemCateogryDictionaryWithItemArr:(NSArray*)itemarr;
-(NSDictionary*)loadProductsUpdate:(NSString*)item;
-(NSDictionary*)loadProducts;
-(void)clearSalesOrder;
-(void)loginWithSalePerson:(SalesPerson*)iSalesPerosn;
-(void)logout;
-(void)loadOrderPackage;
-(NSString*)loadListGallerySatatus;

@end
