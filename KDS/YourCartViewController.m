//
//  YourCartViewController.m
//  KDS
//
//  Created by Tiseno on 11/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "YourCartViewController.h"
#import "GridTableView.h"
#import "SalesOrderType.h"
#import <QuartzCore/QuartzCore.h>
#import "KDSAppDelegate.h"


@implementation YourCartViewController
@synthesize mainScrolView;
@synthesize lblcustomerid;
@synthesize lblcustomername;
@synthesize imgline;
@synthesize ShippingAddressTextView;
@synthesize changeDiscountTypeButton;
@synthesize txtDiscountValue;
@synthesize lblsymbolpercent;
@synthesize lblCalculator;
@synthesize BtnCalculator;
@synthesize Calculatorbg;
@synthesize lblremark;
@synthesize lbltotal;
@synthesize lbldiscount;
@synthesize lblgrandprin;
@synthesize btnconfirm;
@synthesize btncancel;
@synthesize btnupdatecart;
@synthesize btnContinueOrder;
@synthesize logoyourcart;
@synthesize btnordersummary;
@synthesize bluebar;
@synthesize lbldescRemark;
@synthesize lblcontrolblind;
@synthesize lblquantityblind;
@synthesize lblsqftblind;
@synthesize lbltotalsqftblind;
@synthesize lblunitprincblind;
@synthesize lbldiscountblind;
@synthesize lbltotaltbl;
@synthesize lbldeliveryblind;
@synthesize txtdeliveryblind;
@synthesize lblremarkdescmain;
@synthesize lblqtymain;
@synthesize lblunitpricemain;
@synthesize lbldiscountmain;
@synthesize lbltotalmain;
@synthesize lblcustomerCompanyName;
@synthesize lblcustomerTel;
@synthesize shippingAddress;
@synthesize DeliveryDateTextField;
@synthesize lbldeliveryDate;
@synthesize DescripttionTextView;
@synthesize lblDescription;
@synthesize _tableView, tableViewHeader;
@synthesize  installationNameTextField, cityTextField, stateTextField, dateTextField, timeTextField, telTextField, faxTextField, emailTextField, transportationTextField, installationTextField, totalDiscountTextField, discountOnTotalPrice,totalDiscountOfItems, alertUser, backButton;
@synthesize installationAddressTextView, remarkTextView, reviewUIView, reviewTableViewController, reviewTableView;
@synthesize grandTotalLabel, totalPriceLabel, orderIdLabel, orderIdValueLabel,orderItemsArr,orderItem;
@synthesize UpdateCartUIView;

@synthesize EditCartSmallView;
@synthesize UpdaCartPrice;
@synthesize gpriceTextField;
@synthesize salesoderTypes;
@synthesize EditTag;
@synthesize lbltransportation;
@synthesize lblinstallation;
@synthesize lblinstallationName;
@synthesize lblinstallationAddress;
@synthesize lbldate;
@synthesize lbltime;
@synthesize lbltel;
@synthesize lblfax,currentCustomer;
@synthesize lblcontact;
@synthesize lblorderref;
@synthesize txtorderref;
@synthesize txtPO;
@synthesize lblPO;
@synthesize lblcusRemark;
@synthesize txtcusRemark;//,currenttype;
@synthesize lstGallery;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]autorelease];
    if (self) {
        isTableLoaded = NO;
        numberOfCellsGenerated=0;
        isDiscountAmount = YES;
        shouldUpdateCart=NO;
        
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        lstGallery=appDelegate.loadListGallerySatatus;
        
        //NSLog(@"lstGallery-->%@",lstGallery);

        
    }
    return self;
}

- (void)dealloc
{

    [lstGallery release];
    [EditTag release];
    [gpriceTextField release];
    [UpdaCartPrice release];
    [EditCartSmallView release];
    //[installationNameTextField release];
    [UpdateCartUIView release];

    //[currentCustomer release];
    [orderItem release];
    [orderItemsArr release];
    [backButton release];
    [reviewTableView release];
    [reviewTableViewController release];
    [reviewUIView release];
    //[_tableView release];
    //[backgroundImageView release];
    [tableViewHeader release];
    //[installationTextField release];
    [cityTextField release];
    [stateTextField release];
    [dateTextField release];
    [timeTextField release];
    [telTextField release];
    [faxTextField release];
    [emailTextField release];
    [transportationTextField release];
    //[installationAddressTextView release];
    [remarkTextView release];
    [grandTotalLabel release];
    [orderIdLabel release];
    [orderIdValueLabel release];
    [totalPriceLabel release];
    //[CurrentSaleOrder release];
    [totalDiscountTextField release];
    [lbltransportation release];
    [lblinstallation release];
    [lblinstallationName release];
    [lblinstallationAddress release];
    [lbldate release];
    [lbltime release];
    [lbltel release];
    [lblfax release];
    [lblcontact release];
    [DeliveryDateTextField release];
    [lbldeliveryDate release];
    [DescripttionTextView release];
    [lblDescription release];
    [lblcustomerCompanyName release];
    [lblcustomerTel release];
    [shippingAddress release];    
    [lblorderref release];
    [txtorderref release];
    [lblcusRemark release];
    [txtcusRemark release];
    [ShippingAddressTextView release];
    [changeDiscountTypeButton release];
    [txtDiscountValue release];
    [lblsymbolpercent release];
    [lblCalculator release];
    [BtnCalculator release];
    [Calculatorbg release];
    [txtPO release];
    [lblPO release];
    [mainScrolView release];
    [lblcustomerid release];
    [lblcustomername release];
    [imgline release];
    [lblremark release];
    [lbltotal release];
    [lbldiscount release];
    [lblgrandprin release];
    [btnconfirm release];
    [btncancel release];
    [btnupdatecart release];
    [btnContinueOrder release];
    [logoyourcart release];
    [btnordersummary release];
    [bluebar release];
    [lbldescRemark release];
    [lblcontrolblind release];
    [lblquantityblind release];
    [lblsqftblind release];
    [lbltotalsqftblind release];
    [lblunitprincblind release];
    [lbldiscountblind release];
    [lbltotaltbl release];
    [lbldeliveryblind release];
    [txtdeliveryblind release];
    [lblremarkdescmain release];
    [lblqtymain release];
    [lblunitpricemain release];
    [lbldiscountmain release];
    [lbltotalmain release];
    //NSLog(@"Address of 'orderItemsArr' is after release is: %@", orderItemsArr);
    //NSLog(@"Address of 'UpdaCartPrice' is after release is: %@", UpdaCartPrice);
    [super dealloc];
}
-(NSString*)generateOrderIdPrefixforOrderId:(int)orderId
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
    NSString *dateString = [dateFormat stringFromDate:today];
    //NSLog(@"date: %@", dateString);
    [dateFormat release];

    
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    NSString* orderIdStr = [[NSString alloc] initWithFormat:@"%d",orderId ];
    int numberOfOrdersLength = [orderIdStr length];
    int numberOfZeros = 5 - numberOfOrdersLength;
    NSString* numberOfZerosStr = @"";
    for(int i =0;i<numberOfZeros;i++)
    {
        numberOfZerosStr = [numberOfZerosStr stringByAppendingFormat:@"0"];
    }
    NSString* orderIdPrefixStr = [[[NSString alloc] initWithFormat:@"%@%@%@%@",dateString,appDelegate.loggedInSalesPerson.SalesPerson_Code ,numberOfZerosStr,orderIdStr]autorelease];
    
    [orderIdStr release];
    
    
    return orderIdPrefixStr;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{ 
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    if(appDelegate.salesOrder!=nil)
    {
        orderItemsArr=[appDelegate.salesOrder order_Items];
        numberOfRows = orderItemsArr.count;
        return numberOfRows;
    }
    return [orderItemsArr count];
} 

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    numberOfCellsGenerated++;
    static NSString *CellIdentifier = @"Cell";
    GridTableView *cell = (GridTableView*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    self.orderItemsArr=[appDelegate.salesOrder order_Items];

    if(cell == nil)  
    { 

        cell = [[[GridTableView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];

        
	} else if(cell != nil)
    {
        //NSLog(@"reused cell! (%d)", numberOfCellsGenerated);
    }
    //orderItem = [orderItemsArr objectAtIndex:[indexPath row]];
    
    orderItem=[self.orderItemsArr objectAtIndex:indexPath.row];
    cell.delegate=self;
    cell.lineColor = [UIColor blackColor];
    cell.equivalantOrderItemIndex=indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
    if (orderItem.item.Category!=nil) 
    {
        if((![orderItem.item.Category isEqualToString:kBlindCategory]) && (![orderItem.item.Category isEqualToString:kblindACategoryA]))
        {
            NSString* quantityStr=[[NSString alloc] initWithFormat:@"%.0f",orderItem.Quantity];
            cell.quantityLabel.text=quantityStr;
            
            [quantityStr release];
        }
        else
        {
            //BlindSalesOrderItem* blindOrderItem = (BlindSalesOrderItem*)orderItem;
            if ([orderItem.item.Category isEqualToString:kBlindCategory]) 
            {
                cell.blindOrderItem=(BlindSalesOrderItem*)orderItem;
                //NSString* quantitySetStr =[NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set*orderItem.Quantity];
                NSString* quantitySetStr=[[NSString alloc] initWithFormat:@"%.0f",orderItem.Quantity];
                cell.quantityLabelblind.text = quantitySetStr;
                
                [quantitySetStr release];
                
            }else 
            {
                cell.blindOrderItem=(BlindSalesOrderItem*)orderItem;
                NSString* quantityStr=[[NSString alloc] initWithFormat:@"%.0f",orderItem.Quantity];
                cell.quantityLabelblind.text=quantityStr;
                
                [quantityStr release];
            }
            
        }

    }
    /*else if (orderItem.item.Category == nil) 
    {
        if (appDelegate.salesOrder.Type==0) 
        {
            NSString* quantityStr=[NSString stringWithFormat:@"%.0f",orderItem.Quantity];
            cell.quantityLabel.text=quantityStr;
                
        } else  if(appDelegate.salesOrder.Type==1)     
        {
            cell.blindOrderItem=(BlindSalesOrderItem*)orderItem;
            //NSString* quantitySetStr =[NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set];
            NSString* quantitySetStr=[NSString stringWithFormat:@"%.0f",orderItem.Quantity];
            cell.quantityLabelblind.text = quantitySetStr;                        
        }
    }*/
    
    cell.descriptionTextView.text=orderItem.Comment;
    NSString* priceStr=[[NSString alloc] initWithFormat:@"%.2f",orderItem.Unit_Price];
    
    cell.salesOrderItem=orderItem;

    /*if(![orderItem.item.Image_Path isEqualToString:@""])
    {
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsPath = [paths objectAtIndex:0];
        NSString* filePath = [documentsPath stringByAppendingPathComponent:orderItem.item.Image_Path];
        NSData* pngData = [NSData dataWithContentsOfFile:filePath];
        UIImage* timage = [UIImage imageWithData:pngData];
        cell.productImageView.image=timage;
         
    }*/
    
    float tatalOrderItemPrice=0.0;
    float sumOfOrderItemPrice=0.0;
    if (priceStr!=nil) 
    {
        
        //if (![orderItem.item.Category isEqualToString:kBlindCategory]) 
        if ((![orderItem.item.Category isEqualToString:kBlindCategory]) && (![orderItem.item.Category isEqualToString:kblindACategoryA]))
        {
            sumOfOrderItemPrice = ([cell.quantityLabel.text floatValue]*[priceStr floatValue]);
            
            if (orderItem.Discount_Amount != 0.0) 
            {
                NSString *discountAmount =[[NSString alloc] initWithFormat:@"RM %.2f",orderItem.Discount_Amount];
                cell.discountLabel.text=discountAmount;
                
                NSString *fdiscountAmount =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Amount];
                
                float Discount_Amount=[fdiscountAmount floatValue];
                totalDiscountOfItems=Discount_Amount;
                tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                
                [fdiscountAmount release];
                [discountAmount release];
                
            }else if (orderItem.Discount_Percent != 0.0) 
            {
                NSString *discountPercent =[[NSString alloc] initWithFormat:@"%.2f %%",orderItem.Discount_Percent];
                cell.discountLabel.text=discountPercent;
                
                NSString *fdiscountPercent =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Percent];
                
                float Discount_Percent=[fdiscountPercent floatValue];
                totalDiscountOfItems=(Discount_Percent*[priceStr floatValue]*[cell.quantityLabel.text floatValue])/100;
                tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;/**/
                
                [discountPercent release];
                [fdiscountPercent release];
            }else 
            {
                NSString *discountPercent =[[NSString alloc] initWithFormat:@"0.00"];
                cell.discountLabel.text=discountPercent;
                
                tatalOrderItemPrice=sumOfOrderItemPrice;
                //--->
                [discountPercent release];
            }
            
        }else 
        {
            if (![orderItem.item.Category isEqualToString:kblindACategoryA])
                {
                    NSString* quantitySetStr =[[NSString alloc] initWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set*orderItem.Quantity];
                    sumOfOrderItemPrice = ([quantitySetStr floatValue]*[priceStr floatValue]);
                    
                    if (orderItem.Discount_Amount != 0.0) 
                    {
                        NSString *discountAmount =[[NSString alloc] initWithFormat:@"RM %.2f",orderItem.Discount_Amount];
                        cell.discountLabelblind.text=discountAmount;
                        
                        NSString *fdiscountAmount =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Amount];
                        
                        float Discount_Amount=[fdiscountAmount floatValue];
                        totalDiscountOfItems=Discount_Amount;
                        tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                        
                        [discountAmount release];
                        [fdiscountAmount release];
                        
                        
                    }else if (orderItem.Discount_Percent != 0.0) 
                    {
                        NSString *discountPercent =[[NSString alloc] initWithFormat:@"%.2f %%",orderItem.Discount_Percent];
                        cell.discountLabelblind.text=discountPercent;
                        
                        NSString *fdiscountPercent =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Percent];
                        
                        float Discount_Percent=[fdiscountPercent floatValue];
                        totalDiscountOfItems=(Discount_Percent*[priceStr floatValue]*[cell.quantityLabelblind.text floatValue])/100;
                        tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;/**/
                        
                        [discountPercent release];
                        [fdiscountPercent release];
                        
                    }else 
                    {
                        NSString *discountPercent =[[NSString alloc] initWithFormat:@"0.00"];
                        cell.discountLabelblind.text=discountPercent;
                        
                        tatalOrderItemPrice=sumOfOrderItemPrice;
                        
                        [discountPercent release];
                    }
                    
                    [quantitySetStr release];
                    
                }else 
                {
                    sumOfOrderItemPrice = ([cell.quantityLabelblind.text floatValue]*[priceStr floatValue]);
                    
                    if (orderItem.Discount_Amount != 0.0) 
                    {
                        NSString *discountAmount =[[NSString alloc] initWithFormat:@"RM %.2f",orderItem.Discount_Amount];
                        cell.discountLabelblind.text=discountAmount;
                        
                        NSString *fdiscountAmount =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Amount];
                        
                        float Discount_Amount=[fdiscountAmount floatValue];
                        totalDiscountOfItems=Discount_Amount;
                        tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                        
                        [discountAmount release];
                        [fdiscountAmount release];
                        
                    }else if (orderItem.Discount_Percent != 0.0) 
                    {
                        NSString *discountPercent =[[NSString alloc] initWithFormat:@"%.2f %%",orderItem.Discount_Percent];
                        cell.discountLabelblind.text=discountPercent;
                        
                        NSString *fdiscountPercent =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Percent];
                        
                        float Discount_Percent=[fdiscountPercent floatValue];
                        totalDiscountOfItems=(Discount_Percent*[priceStr floatValue]*[cell.quantityLabelblind.text floatValue])/100;
                        tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;/**/
                        
                        [discountPercent release];
                        [fdiscountPercent release];
                    }else 
                    {
                        NSString *discountPercent =[[NSString alloc] initWithFormat:@"0.00"];
                        cell.discountLabelblind.text=discountPercent;
                        
                        tatalOrderItemPrice=sumOfOrderItemPrice;
                        [discountPercent release];
                    }
                }
            
        }

        
        if ((![orderItem.item.Category isEqualToString:kBlindCategory]) && (![orderItem.item.Category isEqualToString:kblindACategoryA])) 
        //if (![orderItem.item.Category isEqualToString:kBlindCategory]) 
        {
            NSString *gTotalPrice =[[NSString alloc] initWithFormat:@"RM %.2f",tatalOrderItemPrice];
            cell.TotalLabel.text=gTotalPrice;
            cell.priceLabel.text=priceStr;
            cell.productNameLabelmain.text=orderItem.item.Description;
            [gTotalPrice release];
            
        }else 
        {
            cell.productNameLabel.text=orderItem.item.Description;
            if (![orderItem.item.Category isEqualToString:kblindACategoryA])
            {
                NSString *gTotalPrice =[[NSString alloc] initWithFormat:@"RM %.2f",tatalOrderItemPrice];
                cell.TotalLabelblind.text=gTotalPrice;
                cell.priceLabelblind.text=priceStr;
                 
                NSString *gWidthHeight =[[NSString alloc] initWithFormat:@"(%.2f x %.2f)",((BlindSalesOrderItem*)orderItem).Width, ((BlindSalesOrderItem*)orderItem).Height];
                NSString *gcolor =[[NSString alloc] initWithFormat:@"%@",((BlindSalesOrderItem*)orderItem).Color];
                NSString *gcontrol =[[NSString alloc] initWithFormat:@"%@",((BlindSalesOrderItem*)orderItem).Control];
                NSString *gSqft =[[NSString alloc] initWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set];
                NSString *gtotalSqft =[[NSString alloc] initWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set*((BlindSalesOrderItem*)orderItem).Quantity];
                
                cell.widthHeightblind.text=gWidthHeight;
                cell.colorblind.text=gcolor;
                cell.controlblind.text=gcontrol;
                cell.sqftblind.text=gSqft;
                cell.totalsqftblind.text=gtotalSqft;
                //cell.remarkblind.text=[NSString stringWithFormat:@"%@", ((BlindSalesOrderItem*)orderItem).Comment];
                
                [gTotalPrice release];
                [gWidthHeight release];
                [gcolor release];
                [gcontrol release];
                [gSqft release];
                [gtotalSqft release];
            }else 
            {
                NSString *gTotalPrice =[[NSString alloc] initWithFormat:@"RM %.2f",tatalOrderItemPrice];
                cell.TotalLabelblind.text=gTotalPrice;
                cell.priceLabelblind.text=priceStr;
                
                 NSString *gWidthHeight =[[NSString alloc] initWithFormat:@"" ];
                NSString *gcolor =[[NSString alloc] initWithFormat:@"" ];
                NSString *gcontrol =[[NSString alloc] initWithFormat:@"" ];
                NSString *gSqft =[[NSString alloc] initWithFormat:@"" ];
                NSString *gtotalSqft =[[NSString alloc] initWithFormat:@"" ];
                
                cell.widthHeightblind.text=gWidthHeight;
                cell.colorblind.text=gcolor;
                cell.controlblind.text=gcontrol;
                cell.sqftblind.text=gSqft;
                cell.totalsqftblind.text=gtotalSqft;
                //cell.remarkblind.text=[NSString stringWithFormat:@"%@", ((BlindSalesOrderItem*)orderItem).Comment];
                
                [gTotalPrice release];
                [gWidthHeight release];
                [gcolor release];
                [gcontrol release];
                [gSqft release];
                [gtotalSqft release];
                

            }

        }
        
        
    }
    
    
    [priceStr release];
    
    
    if(indexPath.row == 0)
            cell.topCell = YES;
        else
            cell.topCell = NO;
    

    
    return cell;
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    GridTableView *selectedcell= (GridTableView*)[tableView cellForRowAtIndexPath:indexPath];  
   
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    self.orderItemsArr=[appDelegate.salesOrder order_Items];
    
    NewOrderUpdateConfirmView *newOrderCategoryViewController=[[NewOrderUpdateConfirmView alloc] initWithNibName:@"NewOrderUpdateConfirmView" bundle:nil];
    newOrderCategoryViewController.title=@"Felton";

    NSString *priceupdate=selectedcell.priceLabel.text;
    
    NSString *descriptionTextView=selectedcell.descriptionTextView.text;
        
    NSString *indexupdateg=[[NSString alloc]initWithFormat:@"%d",indexPath.row];
    int *indexupdate=[indexupdateg intValue];
    newOrderCategoryViewController.updateindexPath=indexupdate;
    newOrderCategoryViewController.PriceUpdate=priceupdate;
    
    newOrderCategoryViewController.RemarkUpdate=descriptionTextView;
     
    NSString *itemuom=[[NSString alloc]initWithFormat:@"%@",selectedcell.salesOrderItem.Order_UOM];
    newOrderCategoryViewController.itemUom=itemuom;
    
    if (selectedcell.salesOrderItem.item.Category != nil) 
    {
        newOrderCategoryViewController.currentSelectedItem=selectedcell.salesOrderItem.item;
        
        if ([selectedcell.salesOrderItem.item.Category isEqualToString:kBlindCategory] || [selectedcell.salesOrderItem.item.Category isEqualToString:kblindACategoryA]) 
        {
            if ([selectedcell.salesOrderItem.item.Category isEqualToString:kBlindCategory]) 
            {
                NSString *quantityTextField=[[NSString alloc]initWithFormat:@"%.2f",selectedcell.blindOrderItem.Quantity];
                
                newOrderCategoryViewController.QuantityUpdate=quantityTextField;
                
                NSString *blindOrderItemHeight=[[NSString alloc]initWithFormat:@"%.2f",selectedcell.blindOrderItem.Height];
                NSString *blindOrderItemWidth=[[NSString alloc]initWithFormat:@"%.2f",selectedcell.blindOrderItem.Width];
                newOrderCategoryViewController.BHeight=blindOrderItemHeight;
                newOrderCategoryViewController.BWidth=blindOrderItemWidth;
                
                NSString *blindControl=selectedcell.blindOrderItem.Control;
                NSString *blindColor=selectedcell.blindOrderItem.Color;
                newOrderCategoryViewController.BControl=blindControl;
                newOrderCategoryViewController.BColor=blindColor;
                
                [quantityTextField release];
                [blindOrderItemHeight release];
                [blindOrderItemWidth release];
               
            }else 
            {
                NSString *quantityTextField=selectedcell.quantityLabel.text;
                newOrderCategoryViewController.QuantityUpdate=quantityTextField;
                
                                
            }
               
        }else 
        {
            NSString *quantityTextField=selectedcell.quantityLabel.text;
            newOrderCategoryViewController.QuantityUpdate=quantityTextField;
        }
    }else if (selectedcell.salesOrderItem.item.Category == nil) 
    {

        if (appDelegate.salesOrder.Type==0) 
        {
            NSString *quantityTextField=selectedcell.quantityLabel.text;
            newOrderCategoryViewController.QuantityUpdate=quantityTextField;
            
        }else if (appDelegate.salesOrder.Type==1)
        {
            NSString *quantityTextField=[[NSString alloc]initWithFormat:@"%.2f",selectedcell.blindOrderItem.Quantity];
            //NSString *Quantity_in_Set=[[NSString alloc]initWithFormat:@"%.2f",selectedcell.blindOrderItem.Quantity_in_Set];
            newOrderCategoryViewController.QuantityUpdate=quantityTextField;
            //newOrderCategoryViewController.totalLength
            
            NSString *blindOrderItemHeight=[[NSString alloc]initWithFormat:@"%.2f",selectedcell.blindOrderItem.Height];
            NSString *blindOrderItemWidth=[[NSString alloc]initWithFormat:@"%.2f",selectedcell.blindOrderItem.Width];
                        
            newOrderCategoryViewController.gHeight=[blindOrderItemHeight floatValue];
            newOrderCategoryViewController.gWidth=[blindOrderItemWidth floatValue];
            
            NSString *blindControl=selectedcell.blindOrderItem.Control;
            NSString *blindColor=selectedcell.blindOrderItem.Color;
            
            newOrderCategoryViewController.BControl=blindControl;
            newOrderCategoryViewController.BColor=blindColor;
            
            [quantityTextField release];
            [blindOrderItemHeight release];
            [blindOrderItemWidth release];
        }
        
    }
    
     
        
    newOrderCategoryViewController.SelectedItemUpdate=selectedcell.salesOrderItem;
    
    newOrderCategoryViewController.UpDatetag=@"UpdateProductOrder";
    [self.navigationController pushViewController:newOrderCategoryViewController animated:YES];
    
    [newOrderCategoryViewController release];
    [indexupdateg release];    
    [itemuom release];
    [_tableView release];
    //}
    
}
//-(IBAction)close_ButtonTapped:(id)sender
-(IBAction)UpdateScrollToItem:(id)sender
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.4];
    UpdateCartUIView.alpha = 0.0;
    [UIView commitAnimations];
    //[_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    NSLog(@"hello~~");
    
    
    
    
    NSLog(@"gpriceTextField--->%@",self.gpriceTextField.text);
    //NSLog(@"UpdaCartPrice--->%@",self.UpdaCartPrice);
    [UpdaCartPrice release];
    
    /*if (self.gpriceTextField.text !=nil) {
        
    
    KDSAppDelegate* appDelegate=(KDSAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.salesOrder==nil)
    {
        SalesOrder* tSalesOrder=[[SalesOrder alloc] init];
        //tSalesOrder.customer=currentSelectedCustomer;
        appDelegate.salesOrder=tSalesOrder;
        [tSalesOrder release];
    }
    SalesOrderItem* salesOrderItem2=[[SalesOrderItem alloc] init];
    //salesOrderItem.item=currentSelectedItem;
    salesOrderItem2.Unit_Price=[gpriceTextField.text floatValue];
    //salesOrderItem.Order_UOM=currentUOM;
    //salesOrderItem.Quantity=[quantityTextField.text floatValue];
    //salesOrderItem.Comment=remarkTextView.text;
    //[appDelegate.salesOrder addOrderItem:salesOrderItem];
    if(![appDelegate.salesOrder addOrderItem:salesOrderItem])
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Normal Items can't be added to Blind Orders" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        //isOrderMixed =YES;
    }
    [salesOrderItem2 release];*/
        /*if([appDelegate.salesOrder addOrderItem:salesOrderItem2])
         {
         
             NSLog(@"addOrderItem appDelegate--2-->");
             [_tableView reloadData];
         }*/
    


    
    /*KDSAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
    //NSLog(@"equivalantOrderItemIndex--->%d",equivalantOrderItemIndex);
    
    if([appDelegate.salesOrder updatecartOrderItemAtIndex:UpdateOrderItemIndex])
    {
        [sender tableCell:self._tableView UpdateItemAtIndex:UpdateOrderItemIndex];
        
        //NSLog(@"equivalantOrderItemIndex--2-->%d",equivalantOrderItemIndex);
    }
    }*/
    
    
    
}

-(void)tableCell:(UITableViewCell*)cell UpdateItemAtIndex:(int)orderItemIndex
{
    GridTableView* griViewCell=(GridTableView*)cell;
    float orderItemPrice=[griViewCell.priceTextField.text floatValue] * [griViewCell.quantityTextField.text floatValue];
    totalPricce+=orderItemPrice;
    shouldUpdateCart=YES;
    totalPricce=0.0;
    //[_tableView reloadData];
}

-(void)manageUpdateCartView
{
    UIView *tHeaderView = [[UIView alloc] initWithFrame:CGRectMake(142, 104, 740, 35)];
    tHeaderView.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *itemIdLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 35)];
    itemIdLabel.text = @"Product ID";
    itemIdLabel.textAlignment = UITextAlignmentCenter;
    itemIdLabel.backgroundColor = [UIColor clearColor];
    itemIdLabel.textColor = [UIColor whiteColor];
    [tHeaderView addSubview:itemIdLabel];
    [itemIdLabel release];
    
    UILabel *itemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, 0, 400, 35)];
    itemNameLabel.text = @"Product Description";
    itemNameLabel.textAlignment = UITextAlignmentCenter;
    itemNameLabel.backgroundColor = [UIColor clearColor];
    itemNameLabel.textColor = [UIColor whiteColor];
    [tHeaderView addSubview:itemNameLabel];
    [itemNameLabel release];
    
    UILabel *itemQuantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(550, 0, 90, 35)];
    itemQuantityLabel.text = @"Quantity";
    itemQuantityLabel.textAlignment = UITextAlignmentCenter;
    itemQuantityLabel.backgroundColor = [UIColor clearColor];
    itemQuantityLabel.textColor = [UIColor whiteColor];
    [tHeaderView addSubview:itemQuantityLabel];
    [itemQuantityLabel release];
    
    UILabel *orderPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(635, 0, 100, 35)];
    orderPriceLabel.text = @"Price";
    orderPriceLabel.textAlignment = UITextAlignmentCenter;
    orderPriceLabel.backgroundColor = [UIColor clearColor];
    orderPriceLabel.textColor = [UIColor whiteColor];
    [tHeaderView addSubview:orderPriceLabel];
    [orderPriceLabel release];
}

-(IBAction)close_ButtonTapped:(id)sender
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.4];
    UpdateCartUIView.alpha = 0.0;
    [UIView commitAnimations];
}



-(IBAction)updateCartButtonTapped
{
    alertUser.hidden=YES;
    [self updateCart];
}
-(IBAction)continueOrderButtonTapped
{
    if([lstGallery isEqualToString:@"list"])
    {
        
        [self listView];
    }
    else if([lstGallery isEqualToString:@"gallery"])
    {
        
        [self galleryView];
    } 
    
    
    
    
    [_tableView release];
    [orderItem release];
    [orderItemsArr release];
    //}
}

-(void)listView
{
    NewOrderViewController *newOrderViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
    newOrderViewController.title=@"Felton";
    
    [self.navigationController pushViewController:newOrderViewController animated:YES];
    [newOrderViewController release];
}

-(void)galleryView
{
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
     SalesOrder* tempSalesOrder=appDelegate.salesOrder;
     [tempSalesOrder retain];
     
     NewOrderCategoryViewController *newOrderViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];
     //NewOrderViewController *newOrderCategoryViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
     appDelegate.salesOrder=tempSalesOrder;
     [tempSalesOrder release];
     
     /**///NewOrderCategoryViewController *newOrderViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];     
    newOrderViewController.title=@"Felton";
    [self.navigationController pushViewController:newOrderViewController animated:YES];
    [newOrderViewController release]; 
}

-(IBAction)confirmButtonTapped
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Confirm" message:@"You are about to confirm your order. Please press ok." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alert.tag=kConfirmingAlertTag;
    [alert show];
    [alert release];
}
-(IBAction)cancelButtonTapped
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"Please Confirm cancelling your order." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alert.tag=kCancelAlertTag;
    [alert show];
    [alert release];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    NSLog(@"didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];

    
    // Release any cached data, images, etc that aren't in use.
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==kCancelAlertTag)
    {
        if(buttonIndex == 1)
        {
            KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            [appDelegate clearSalesOrder];
            
            [_tableView release];
            [orderItem release];
            [orderItemsArr release];
            
            UIViewController* newOrderCategoryViewController=nil;
            for(UIViewController* viewController in self.navigationController.viewControllers)
            {
                if([viewController isKindOfClass:[MenuViewController class]])
                {
                    newOrderCategoryViewController=viewController;
                    break;
                }
            }
            [self.navigationController popToViewController:newOrderCategoryViewController animated:YES];
        }
    }
    else if(alertView.tag==kConfirmingAlertTag)
    {
        if(buttonIndex == 1)
        {
            KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            appDelegate.salesOrder.grand_Price = grandPrice;
            appDelegate.salesOrder.order_discount = discountOnTotalPrice;
            appDelegate.salesOrder.remark = remarkTextView.text;
            KDSDataSalesOrder *dataSalesOrder;
            //KDSDataSalesPerson *dataSalesPerson = [[KDSDataSalesPerson alloc] init];
            //KDSDataCustomer *dataCustomer;
            appDelegate.currentCustomer.Customer_Id=lblcustomerTel.text;
            appDelegate.salesOrder.CashRemarks=txtcusRemark.text;
            if (appDelegate.salesOrder.Type==SalesOrderMain) 
            {
                dataSalesOrder=[[KDSDataSalesOrder alloc] init];
                
                

                appDelegate.salesOrder.DeliveryDate=DeliveryDateTextField.text;
                
                if(ShippingAddressTextView.text != nil)
                {
                    
                    NSString *installationAddressText=[[NSString alloc]initWithFormat:@"%@",ShippingAddressTextView.text];
                    appDelegate.salesOrder.Shipping_Address = installationAddressText;
                    
                    //[installationAddressText release];
                } 
                
                if(txtorderref.text != nil)
                {
                    
                    NSString *orderreftxt=[[NSString alloc]initWithFormat:@"%@",txtorderref.text];
                    appDelegate.salesOrder.Order_Reference = orderreftxt;
                    [orderreftxt release];
                }  
                
                
                
            }
            else if(appDelegate.salesOrder.Type==SalesOrderBlind)
            {
                //NSLog(@"%d",appDelegate.salesOrder.Type);
                appDelegate.salesOrder.OrderDescription=DescripttionTextView.text;
                appDelegate.salesOrder.DeliveryDate=txtdeliveryblind.text;
                if(txtPO.text != nil)
                {
                    
                    NSString *orderreftxt=[[NSString alloc]initWithFormat:@"%@",txtPO.text];
                    appDelegate.salesOrder.Order_Reference = orderreftxt;
                    
                    [orderreftxt release];
                }
                
                dataSalesOrder=[[KDSDataBlindSalesOrder alloc] init];
                float installationCost = 0.0f;
                float transportationCost = 0.0f;
                if(![transportationTextField.text isEqualToString:@""])
                {
                    transportationCost=[transportationTextField.text floatValue];
                }
                if(![installationTextField.text isEqualToString:@""])
                {
                    installationCost=[installationTextField.text floatValue];
                }
                ((BlindSalesOrder*)appDelegate.salesOrder).installation = installationCost;
                ((BlindSalesOrder*)appDelegate.salesOrder).transportation = transportationCost;
                if(installationNameTextField.text != nil)
                {
                    NSString *installationNameText=[[NSString alloc]initWithFormat:@"%@",installationNameTextField.text];
                    ((BlindSalesOrder*)appDelegate.salesOrder).Installation_Name = installationNameText;
                    
                    [installationNameText release];
                }
                
                if(installationAddressTextView.text != nil)
                {
                    //(((BlindSalesOrder*)appDelegate.salesOrder).Installation_Address).City = cityTextField.text;
                    NSString *installationAddressText=[[NSString alloc]initWithFormat:@"%@",installationAddressTextView.text];
                    ((BlindSalesOrder*)appDelegate.salesOrder).Installationorder_Address = installationAddressText;
                    [installationAddressText release];
                }
                
                if(![dateTextField.text isEqualToString:@""])
                {
                    //(((BlindSalesOrder*)appDelegate.salesOrder).Installation_Address).City = cityTextField.text;
                    ((BlindSalesOrder*)appDelegate.salesOrder).Installation_City = dateTextField.text;
                }else {
                    ((BlindSalesOrder*)appDelegate.salesOrder).Installation_City=@" ";
                }

                if(timeTextField.text != nil)
                {
                    ((BlindSalesOrder*)appDelegate.salesOrder).Installation_State = timeTextField.text;
                }
                
                
                if(telTextField.text != nil)
                {
                    ((BlindSalesOrder*)appDelegate.salesOrder).Installation_Phone = telTextField.text;
                }
                if(faxTextField.text != nil)
                {
                    ((BlindSalesOrder*)appDelegate.salesOrder).Installation_Fax = faxTextField.text;
                }
                if(emailTextField.text != nil)
                {
                    ((BlindSalesOrder*)appDelegate.salesOrder).Installation_Email = emailTextField.text;
                }
            }
            if(appDelegate.salesOrder.Order_id!=-1)
            {
                [dataSalesOrder deleteSaleOrder:appDelegate.salesOrder];
            }
            [dataSalesOrder insertSalesOrder:appDelegate.salesOrder forSalesperson:appDelegate.loggedInSalesPerson];
            appDelegate.loggedInSalesPerson.orderId_Prefix = [self generateOrderIdPrefixforOrderId:appDelegate.salesOrder.Order_id];
            [dataSalesOrder updateSaleOrder:appDelegate.salesOrder ForSaleOrderId:appDelegate.loggedInSalesPerson];
            //[dataSalesOrder  updateCustomerRemark:appDelegate.currentCustomer]; 
            [dataSalesOrder release];
            
            
            //[dataCustomer updateCustomerRemark:appDelegate.currentCustomer]; 
            //[dataCustomer release];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Confirmed" message:@"Your order is confirmed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=kConfirmedAlertTag;
            [alert show];
            [alert release];
        }
    }
    else if(alertView.tag==kConfirmedAlertTag)
    {
        KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        [appDelegate clearSalesOrder];
        
        [_tableView release];
        [orderItem release];
        [orderItemsArr release];
        
        UIViewController* menuViewController=nil;
        for(UIViewController* viewController in self.navigationController.viewControllers)
        {
            if([viewController isKindOfClass:[MenuViewController class]])
            {
                menuViewController=viewController;
                break;
            }
        }
        [self.navigationController popToViewController:menuViewController animated:YES];
    }
}
-(IBAction)reviewButtonTapped
{
    [self manageReviewView];
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.4];
    reviewUIView.alpha = 1;
    [UIView commitAnimations];
}
-(void)manageReviewView
{
    KDSAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    if(reviewUIView == nil)
    {
        UIView* tReviewUIView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
        UIImageView* bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tReviewUIView.frame.size.width, tReviewUIView.frame.size.height)];
        bgImageView.backgroundColor = [UIColor blackColor];
        bgImageView.alpha = 0.5;
        [tReviewUIView addSubview:bgImageView];
        [bgImageView release];
        self.reviewUIView = tReviewUIView;
        [tReviewUIView release];
    }
    if(reviewTableViewController == nil)
    {
        ReviewTableViewController *treviewTableViewController = [[ReviewTableViewController alloc] init];
        self.reviewTableViewController = treviewTableViewController;
        reviewTableViewController.scrollDelegate = self;
        [treviewTableViewController release];
    }
    if(reviewTableView == nil)
    {
        UITableView *tReviewTableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 104, 1015, 496) style:UITableViewStylePlain];
        self.reviewTableView = tReviewTableView;
        [tReviewTableView release];
    }
    [reviewTableView setDelegate:self.reviewTableViewController];
    [reviewTableView setDataSource:self.reviewTableViewController];
    self.reviewTableViewController.view = self.reviewTableViewController.tableView;
    [reviewTableView reloadData];
    
    UIView *tHeaderView = [[UIView alloc] initWithFrame:CGRectMake(reviewTableView.frame.origin.x, reviewTableView.frame.origin.y-35, reviewTableView.frame.size.width, 35)];
    tHeaderView.backgroundColor = [UIColor lightGrayColor];
    
    if(appDelegate.salesOrder.Type==SalesOrderBlind)
    {
        /*UILabel *itemIdLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 35)];
         itemIdLabel.text = @"Product ID";
         itemIdLabel.textAlignment = UITextAlignmentCenter;
         itemIdLabel.backgroundColor = [UIColor clearColor];
         itemIdLabel.textColor = [UIColor whiteColor];
         [tHeaderView addSubview:itemIdLabel];
         [itemIdLabel release];*/
        
        /*UILabel *itemSqFeetLabel = [[UILabel alloc] initWithFrame:CGRectMake(430, 0, 480, 35)];
         itemSqFeetLabel.text = @"Sq.Ft";
         itemSqFeetLabel.textAlignment = UITextAlignmentCenter;
         itemSqFeetLabel.backgroundColor = [UIColor clearColor];
         itemSqFeetLabel.textColor = [UIColor whiteColor];
         [tHeaderView addSubview:itemSqFeetLabel];
         [itemSqFeetLabel release];*/
        
        UILabel *itemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 35)];
        itemNameLabel.text = @"Product Description";
        itemNameLabel.textAlignment = UITextAlignmentCenter;
        itemNameLabel.backgroundColor = [UIColor clearColor];
        itemNameLabel.textColor = [UIColor whiteColor];
        itemNameLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemNameLabel];
        [itemNameLabel release];
        
        UILabel *itemWidth_DropLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, 210, 35)];
        itemWidth_DropLabel.text = @"Width x Drop";
        itemWidth_DropLabel.textAlignment = UITextAlignmentCenter;
        itemWidth_DropLabel.backgroundColor = [UIColor clearColor];
        itemWidth_DropLabel.textColor = [UIColor whiteColor];
        itemWidth_DropLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemWidth_DropLabel];
        [itemWidth_DropLabel release];
        
        UILabel *itemQuantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 0, 330, 35)];
        itemQuantityLabel.text = @"Quantity";
        itemQuantityLabel.textAlignment = UITextAlignmentCenter;
        itemQuantityLabel.backgroundColor = [UIColor clearColor];
        itemQuantityLabel.textColor = [UIColor whiteColor];
        itemQuantityLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemQuantityLabel];
        [itemQuantityLabel release];

        
        UILabel *itemAreaLabel = [[UILabel alloc] initWithFrame:CGRectMake(320, 0, 270, 35)];
        itemAreaLabel.text = @"Area";
        itemAreaLabel.textAlignment = UITextAlignmentCenter;
        itemAreaLabel.backgroundColor = [UIColor clearColor];
        itemAreaLabel.textColor = [UIColor whiteColor];
        itemAreaLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemAreaLabel];
        [itemAreaLabel release];
        
                
        UILabel *orderPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(340, 0, 390, 35)];
        orderPriceLabel.text = @"Unit Price";
        orderPriceLabel.textAlignment = UITextAlignmentCenter;
        orderPriceLabel.backgroundColor = [UIColor clearColor];
        orderPriceLabel.textColor = [UIColor whiteColor];
        orderPriceLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderPriceLabel];
        [orderPriceLabel release];
        
        UILabel *orderControlLabel = [[UILabel alloc] initWithFrame:CGRectMake(400, 0, 450, 35)];
        orderControlLabel.text = @"Control";
        orderControlLabel.textAlignment = UITextAlignmentCenter;
        orderControlLabel.backgroundColor = [UIColor clearColor];
        orderControlLabel.textColor = [UIColor whiteColor];
        orderControlLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderControlLabel];
        [orderControlLabel release];
        
        UILabel *orderColorLabel = [[UILabel alloc] initWithFrame:CGRectMake(460, 0, 510, 35)];
        orderColorLabel.text = @"Color";
        orderColorLabel.textAlignment = UITextAlignmentCenter;
        orderColorLabel.backgroundColor = [UIColor clearColor];
        orderColorLabel.textColor = [UIColor whiteColor];
        orderColorLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderColorLabel];
        [orderColorLabel release];
        
        UILabel *itemDiscountLabel = [[UILabel alloc] initWithFrame:CGRectMake(520, 0, 570, 35)];
        itemDiscountLabel.text = @"Discount";
        itemDiscountLabel.textAlignment = UITextAlignmentCenter;
        itemDiscountLabel.backgroundColor = [UIColor clearColor];
        itemDiscountLabel.textColor = [UIColor whiteColor];
        itemDiscountLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemDiscountLabel];
        [itemDiscountLabel release];
        
        UILabel *orderTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(580, 0, 630, 35)];
        orderTotalLabel.text = @"Total Price";
        orderTotalLabel.textAlignment = UITextAlignmentCenter;
        orderTotalLabel.backgroundColor = [UIColor clearColor];
        orderTotalLabel.textColor = [UIColor whiteColor];
        orderTotalLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderTotalLabel];
        [orderTotalLabel release];
        
        UIImage *backButtonImage = [UIImage imageNamed:@"btn_cancel_circle.png"];
        UIButton *tBackButton = [[UIButton alloc] initWithFrame:CGRectMake(985, 0, 29, 31)];
        [tBackButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
        [tBackButton addTarget:self action:@selector(back_ButtonTapped:)
              forControlEvents:UIControlEventTouchDown];
        [tHeaderView addSubview:tBackButton];
        [tBackButton release];
    }
    //else if(salesoderTypes==1)
    //{
    else if(appDelegate.salesOrder.Type==SalesOrderMain)
    {
        UILabel *itemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 350, 35)];
        itemNameLabel.text = @"Product Description";
        itemNameLabel.textAlignment = UITextAlignmentCenter;
        itemNameLabel.backgroundColor = [UIColor clearColor];
        itemNameLabel.textColor = [UIColor whiteColor];
        itemNameLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemNameLabel];
        [itemNameLabel release];
        
        UILabel *itemQuantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(360, 0, 410, 35)];
        itemQuantityLabel.text = @"Quantity";
        itemQuantityLabel.textAlignment = UITextAlignmentCenter;
        itemQuantityLabel.backgroundColor = [UIColor clearColor];
        itemQuantityLabel.textColor = [UIColor whiteColor];
        itemQuantityLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemQuantityLabel];
        [itemQuantityLabel release];
        
        UILabel *orderPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(420, 0, 470, 35)];
        orderPriceLabel.text = @"Unit Price";
        orderPriceLabel.textAlignment = UITextAlignmentCenter;
        orderPriceLabel.backgroundColor = [UIColor clearColor];
        orderPriceLabel.textColor = [UIColor whiteColor];
        orderPriceLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderPriceLabel];
        [orderPriceLabel release];
        
        UILabel *itemDiscountLabel = [[UILabel alloc] initWithFrame:CGRectMake(480, 0, 530, 35)];
        itemDiscountLabel.text = @"Discount";
        itemDiscountLabel.textAlignment = UITextAlignmentCenter;
        itemDiscountLabel.backgroundColor = [UIColor clearColor];
        itemDiscountLabel.textColor = [UIColor whiteColor];
        itemDiscountLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemDiscountLabel];
        [itemDiscountLabel release];
        
        UILabel *orderTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(540, 0, 590, 35)];
        orderTotalLabel.text = @"Total Price";
        orderTotalLabel.textAlignment = UITextAlignmentCenter;
        orderTotalLabel.backgroundColor = [UIColor clearColor];
        orderTotalLabel.textColor = [UIColor whiteColor];
        orderTotalLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderTotalLabel];
        [orderTotalLabel release];
        
        UIImage *backButtonImage = [UIImage imageNamed:@"btn_cancel_circle.png"];
        UIButton *tBackButton = [[UIButton alloc] initWithFrame:CGRectMake(985, 0, 29, 31)];
        [tBackButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
        [tBackButton addTarget:self action:@selector(back_ButtonTapped:)
              forControlEvents:UIControlEventTouchDown];
        [tHeaderView addSubview:tBackButton];
        [tBackButton release];
    }

    
    [reviewUIView addSubview:tHeaderView];
    [tHeaderView release];
    [reviewUIView addSubview:reviewTableView];
    [self.view addSubview:reviewUIView];
}

-(IBAction)back_ButtonTapped:(id)sender
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.4];
    reviewUIView.alpha = 0.0;
    [UIView commitAnimations];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
        heightOfEditedArea = textField.frame.size.height;
        heightOffset=textField.frame.origin.y+10;
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 652-(heightOffset+ heightOfEditedArea), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
        heightOfEditedArea = textView.frame.size.height;
        heightOffset=textView.frame.origin.y+10;
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 652-(heightOffset+ heightOfEditedArea), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    alertUser.hidden=NO;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag==kTotalDiscount) {
        alertUser.hidden=NO;
    }else {
        alertUser.hidden=NO;
    }
    //alertUser.hidden=NO;
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    //NSLog(@"yourcart~");
    [super viewDidLoad];
    _tableView.loadingDelegate = self;
    installationNameTextField.tag=90;
    dateTextField.tag=98;
    timeTextField.tag=99;
    telTextField.tag=101;
    faxTextField.tag=102;
    emailTextField.tag=103;
    txtDiscountValue.tag=104;
    
    transportationTextField.tag=106;
    installationTextField.tag=107;
    txtcusRemark.tag=108;
    DeliveryDateTextField.tag=109;
    txtorderref.tag=110;
    
    installationAddressTextView.tag=201;
    ShippingAddressTextView.tag=202;
    DescripttionTextView.tag=203;
    remarkTextView.tag=204;
    
    totalDiscountTextField.tag=kTotalDiscount;
    reviewUIView.alpha = 0.0;
    orderIdLabel.hidden=YES;
    orderIdValueLabel.hidden=YES;
    
    
    alertUser.hidden=YES;
    
    //KDSAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    KDSAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;

    if(appDelegate.salesOrder.remark != nil && ![appDelegate.salesOrder.remark isEqualToString:@""])
    {
        remarkTextView.text = appDelegate.salesOrder.remark;
    }
    
    NSString *customerName=[[NSString alloc]initWithFormat:@"%@",appDelegate.currentCustomer.Customer_Name];
    lblcustomerCompanyName.text=customerName;
    [customerName release];
    NSString *customerTel=[[NSString alloc]initWithFormat:@"%@",appDelegate.currentCustomer.Customer_Id];
    lblcustomerTel.text=customerTel;
    
    BOOL result; 
    result = [customerTel hasPrefix: @"CASH"];
    [customerTel release];
    
    txtDiscountValue.hidden=NO;
    lblsymbolpercent.hidden=NO;
    lblCalculator.hidden=NO;
    BtnCalculator.hidden=NO;
    Calculatorbg.hidden=YES;
    changeDiscountTypeButton.hidden=YES;
    
    
    if (result) {
        
        lblcusRemark.hidden=NO;
        txtcusRemark.hidden=NO;
        
        if(appDelegate.salesOrder.CashRemarks != nil && ![appDelegate.salesOrder.CashRemarks isEqualToString:@""])
        {
            if ([appDelegate.salesOrder.CashRemarks isEqualToString:@"(null)"]) 
            {
                txtcusRemark.text= @"";
            }else 
            {
                txtcusRemark.text = appDelegate.salesOrder.CashRemarks;
            }
        }
        
    } 
    else {
         
        lblcusRemark.hidden=YES;
        txtcusRemark.hidden=YES;
       
    }
    
    //KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    self.orderItemsArr=[appDelegate.salesOrder order_Items];
     
    float sumOfOrderItemPrice = 0.0;
    for(int i=0;i<orderItemsArr.count;i++)
    {
        sumOfOrderItemPrice += salesOrderItem.TotalPrice;
    }

    
    [[installationAddressTextView layer] setBorderWidth:0.3];
	[[installationAddressTextView layer] setCornerRadius:6];
    
    [[ShippingAddressTextView layer] setBorderWidth:0.3];
	[[ShippingAddressTextView layer] setCornerRadius:6];
    
    [[DescripttionTextView layer] setBorderWidth:0.3];
	[[DescripttionTextView layer] setCornerRadius:6];
    
    [[remarkTextView layer] setBorderWidth:0.3];
	[[remarkTextView layer] setCornerRadius:6];
    
    //[[PatientNameTextField layer] setBorderWidth:2.3];
	//[[PatientNameTextField layer] setCornerRadius:15];
    
    //NSLog(@"viewdidload appDelegate.salesOrder.Type-->%d",appDelegate.salesOrder.Type);
    
    NSString *totalDiscountStr=[[NSString alloc] initWithFormat:@"%.2f",appDelegate.salesOrder.order_discount];
    totalDiscountTextField.text=totalDiscountStr;
    [totalDiscountStr release];
    if (appDelegate.salesOrder.Type==SalesOrderBlind) 
    {
        if(appDelegate.salesOrder.Order_Reference != nil && ![appDelegate.salesOrder.Order_Reference isEqualToString:@""])
        {
            txtPO.text = appDelegate.salesOrder.Order_Reference;
        }
        //backgroundImageView.image = [UIImage imageNamed:@"subpage_background.png"];
        installationTextField.hidden=NO;
        transportationTextField.hidden=NO;
        installationAddressTextView.hidden = NO;
        ShippingAddressTextView.hidden=YES;
        shippingAddress.hidden=YES;
        lblorderref.hidden=YES;
        txtorderref.hidden=YES;
        cityTextField.hidden = YES;
        stateTextField.hidden = YES;
        dateTextField.hidden = NO;
        timeTextField.hidden = NO;
        telTextField.hidden = NO;
        faxTextField.hidden = NO;
        emailTextField.hidden = NO;
        lblDescription.hidden=NO;
        DescripttionTextView.hidden=NO;
        lbldeliveryblind.hidden=NO;
        txtdeliveryblind.hidden=NO;
        lbldeliveryDate.hidden=YES;
        DeliveryDateTextField.hidden=YES;
        txtPO.hidden=NO;
        lblPO.hidden=NO;
        DescripttionTextView.text=appDelegate.salesOrder.OrderDescription;
        
        lbldescRemark.hidden=NO;
        lblcontrolblind.hidden=NO;
        lblquantityblind.hidden=NO;
        lblsqftblind.hidden=NO;
        lbltotalsqftblind.hidden=NO;
        lblunitprincblind.hidden=NO;
        lbldiscountblind.hidden=NO;
        lbltotaltbl.hidden=NO;
        
        lblremarkdescmain.hidden=YES;
        lblqtymain.hidden=YES;
        lblunitpricemain.hidden=YES;
        lbldiscountmain.hidden=YES;
        lbltotalmain.hidden=YES;
        
        
        if(appDelegate.salesOrder.DeliveryDate != nil && ![appDelegate.salesOrder.DeliveryDate isEqualToString:@""])
        {
            if ([appDelegate.salesOrder.DeliveryDate isEqualToString:@"(null)"]) 
            {
                txtdeliveryblind.text= @"";
            }else 
            {
                txtdeliveryblind.text=appDelegate.salesOrder.DeliveryDate;
            }
        }
        
        if ([((BlindSalesOrder*)appDelegate.salesOrder).Installation_Name isEqualToString:@"(null)"]) {
            installationNameTextField.text= @"";
        }else {
            installationNameTextField.text= ((BlindSalesOrder*)appDelegate.salesOrder).Installation_Name;
        }
        
        
        installationAddressTextView.text = ((BlindSalesOrder*)appDelegate.salesOrder).Installationorder_Address;
        
        if ([((BlindSalesOrder*)appDelegate.salesOrder).Installation_City isEqualToString:@"(null)"]) {
            dateTextField.text=@"";
        }else {
            dateTextField.text = ((BlindSalesOrder*)appDelegate.salesOrder).Installation_City;
        }
        
        if ([((BlindSalesOrder*)appDelegate.salesOrder).Installation_State isEqualToString:@"(null)"]) {
            timeTextField.text=@"";
        }else {
            timeTextField.text = ((BlindSalesOrder*)appDelegate.salesOrder).Installation_State;
        }
        
        if ([((BlindSalesOrder*)appDelegate.salesOrder).Installation_Phone isEqualToString:@"(null)"]) {
            telTextField.text=@"";
        }else {
            telTextField.text=((BlindSalesOrder*)appDelegate.salesOrder).Installation_Phone;
        }
        
        if ([((BlindSalesOrder*)appDelegate.salesOrder).Installation_Fax isEqualToString:@"(null)"]) {
            faxTextField.text=@"";
        }else {
            faxTextField.text=((BlindSalesOrder*)appDelegate.salesOrder).Installation_Fax;
        }
        if ([((BlindSalesOrder*)appDelegate.salesOrder).Installation_Email isEqualToString:@"(null)"]) {
            emailTextField.text=@"";
        }else {
            emailTextField.text=((BlindSalesOrder*)appDelegate.salesOrder).Installation_Email;
        }

        
    }
    else
        if(appDelegate.salesOrder.Type==SalesOrderMain)
    {
        //backgroundImageView.image = [UIImage imageNamed:@"subpage_background.png"];
        installationNameTextField.hidden = YES;
        installationAddressTextView.hidden = YES;
        ShippingAddressTextView.hidden=NO;
        installationTextField.hidden=YES;
        transportationTextField.hidden=YES;
        shippingAddress.hidden=NO;
        lblorderref.hidden=NO;
        txtorderref.hidden=NO;
        cityTextField.hidden = YES;
        stateTextField.hidden = YES;
        dateTextField.hidden = YES;
        timeTextField.hidden = YES;
        telTextField.hidden = YES;
        faxTextField.hidden = YES;
        emailTextField.hidden = YES;
        
        lbltransportation.hidden=YES;
        lblinstallation.hidden=YES;
        lblinstallationName.hidden=YES;
        lblinstallationAddress.hidden=YES;
        lbldate.hidden=YES;
        lbltime.hidden=YES;
        lbltel.hidden=YES;
        lblfax.hidden=YES;
        lblcontact.hidden=YES;
        
        txtPO.hidden=YES;
        lblPO.hidden=YES;
        DescripttionTextView.hidden=YES;
        lblDescription.hidden=YES;
        lbldeliveryblind.hidden=YES;
        txtdeliveryblind.hidden=YES;
        lbldeliveryDate.hidden=NO;
        DeliveryDateTextField.hidden=NO;
        
        lbldescRemark.hidden=YES;
        lblcontrolblind.hidden=YES;
        lblquantityblind.hidden=YES;
        lblsqftblind.hidden=YES;
        lbltotalsqftblind.hidden=YES;
        lblunitprincblind.hidden=YES;
        lbldiscountblind.hidden=YES;
        lbltotaltbl.hidden=YES;
        
        lblremarkdescmain.hidden=NO;
        lblqtymain.hidden=NO;
        lblunitpricemain.hidden=NO;
        lbldiscountmain.hidden=NO;
        lbltotalmain.hidden=NO;
        
        
        
        
        if(appDelegate.salesOrder.DeliveryDate != nil && ![appDelegate.salesOrder.DeliveryDate isEqualToString:@""])
        {
            if ([appDelegate.salesOrder.DeliveryDate isEqualToString:@"(null)"]) 
            {
                DeliveryDateTextField.text= @"";
            }else 
            {
                DeliveryDateTextField.text=appDelegate.salesOrder.DeliveryDate;
            }
        }
        
        if(appDelegate.salesOrder.Shipping_Address != nil && ![appDelegate.salesOrder.Shipping_Address isEqualToString:@""])
        {
            ShippingAddressTextView.text = appDelegate.salesOrder.Shipping_Address;
        }
        
        if(appDelegate.salesOrder.Order_Reference != nil && ![appDelegate.salesOrder.Order_Reference isEqualToString:@""])
        {
            txtorderref.text = appDelegate.salesOrder.Order_Reference;
        }
        
    }
    
    
    
    if(tableViewHeader == nil)
    {
        self._tableView.rowHeight = 55;
        UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y-30, self._tableView.frame.size.width, 30)];
        tableHeaderView.backgroundColor = [UIColor colorWithRed:0.0196 green:0.513 blue:0.949 alpha:1.0];
        
        UILabel *itemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 400, 30)];
        itemNameLabel.text = @"Product Description";
        itemNameLabel.textAlignment = UITextAlignmentCenter;
        itemNameLabel.backgroundColor = [UIColor clearColor];
        itemNameLabel.textColor = [UIColor whiteColor];
        itemNameLabel.font=[UIFont boldSystemFontOfSize:17];
        [tableHeaderView addSubview:itemNameLabel];
        [itemNameLabel release];
        
        UILabel *itemQuantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(375, 0, 90, 30)];
        itemQuantityLabel.text = @"Quantity";
        itemQuantityLabel.textAlignment = UITextAlignmentCenter;
        itemQuantityLabel.backgroundColor = [UIColor clearColor];
        itemQuantityLabel.textColor = [UIColor whiteColor];
        itemQuantityLabel.font=[UIFont boldSystemFontOfSize:17]; 
        [tableHeaderView addSubview:itemQuantityLabel];
        [itemQuantityLabel release];
        
        UILabel *orderPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(435, 0, 100, 30)];
        orderPriceLabel.text = @"Price";
        orderPriceLabel.textAlignment = UITextAlignmentCenter;
        orderPriceLabel.backgroundColor = [UIColor clearColor];
        orderPriceLabel.textColor = [UIColor whiteColor];
        orderPriceLabel.font=[UIFont boldSystemFontOfSize:17];
        [tableHeaderView addSubview:orderPriceLabel];
        [orderPriceLabel release];
        
        UILabel *DiscountLabel = [[UILabel alloc] initWithFrame:CGRectMake(515, 0, 100, 30)];
        DiscountLabel.text = @"Discount";
        DiscountLabel.textAlignment = UITextAlignmentCenter;
        DiscountLabel.backgroundColor = [UIColor clearColor];
        DiscountLabel.textColor = [UIColor whiteColor];
        DiscountLabel.font=[UIFont boldSystemFontOfSize:17];
        [tableHeaderView addSubview:DiscountLabel];
        [DiscountLabel release];
        
        UILabel *TotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(605, 0, 100, 30)];
        TotalLabel.text = @"Total";
        TotalLabel.textAlignment = UITextAlignmentCenter;
        TotalLabel.backgroundColor = [UIColor clearColor];
        TotalLabel.textColor = [UIColor whiteColor];
        TotalLabel.font=[UIFont boldSystemFontOfSize:17];
        [tableHeaderView addSubview:TotalLabel];
        [TotalLabel release];
        
        //self.tableViewHeader = tableHeaderView;
        //[self.view addSubview:tableViewHeader];
        
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];

        [tableHeaderView release];
        //[tableHeaderLabel release];
    }
    if(appDelegate.salesOrder.systemOrderId != nil)
    {
        orderIdLabel.hidden = NO;
        orderIdValueLabel.hidden=NO;
        orderIdValueLabel.text = appDelegate.salesOrder.systemOrderId;
        if(appDelegate.salesOrder.Type==SalesOrderBlind)
        {
            NSString* installationCostStr = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrder*)appDelegate.salesOrder).installation];
            installationTextField.text=installationCostStr;
            NSString* transportationCostStr = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrder*)appDelegate.salesOrder).transportation];
            transportationTextField.text=transportationCostStr;
        }
    }else
    {
        orderIdLabel.hidden = YES;
        orderIdValueLabel.hidden=YES;
        /*orderIdValueLabel.text = appDelegate.salesOrder.systemOrderId;
        if(appDelegate.salesOrder.Type==SalesOrderBlind)
        {
            NSString* installationCostStr = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrder*)appDelegate.salesOrder).installation];
            installationTextField.text=installationCostStr;
            NSString* transportationCostStr = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrder*)appDelegate.salesOrder).transportation];
            transportationTextField.text=transportationCostStr;
        }*/
    }
    
    //[self.mainScrolView addSubview:imgMainView];
    /*[self.mainScrolView addSubview:lblcustomerid];
    [self.mainScrolView addSubview:lblcustomerTel]; 
    [self.mainScrolView addSubview:lblcustomername];
    [self.mainScrolView addSubview:lblcustomerCompanyName];
    [self.mainScrolView addSubview:lblcusRemark];
    [self.mainScrolView addSubview:txtcusRemark];
    [self.mainScrolView addSubview:_tableView];
    [self.mainScrolView addSubview:imgline];
    [self.mainScrolView addSubview:lbldeliveryDate];
    [self.mainScrolView addSubview:DeliveryDateTextField];
    [self.mainScrolView addSubview:lblinstallationName];
    [self.mainScrolView addSubview:installationNameTextField];
    [self.mainScrolView addSubview:lblinstallationAddress];
    [self.mainScrolView addSubview:installationAddressTextView];
    [self.mainScrolView addSubview:lblorderref];
    [self.mainScrolView addSubview:txtorderref];
    [self.mainScrolView addSubview:shippingAddress];
    [self.mainScrolView addSubview:ShippingAddressTextView];
    [self.mainScrolView addSubview:lbldate];
    [self.mainScrolView addSubview:dateTextField];
    [self.mainScrolView addSubview:lbltime];
    [self.mainScrolView addSubview:timeTextField];
    [self.mainScrolView addSubview:lbltel];
    [self.mainScrolView addSubview:telTextField];
    [self.mainScrolView addSubview:lblfax];
    [self.mainScrolView addSubview:faxTextField];
    [self.mainScrolView addSubview:lblcontact];
    [self.mainScrolView addSubview:emailTextField];
    [self.mainScrolView addSubview:lblDescription];
    [self.mainScrolView addSubview:DescripttionTextView];
    [self.mainScrolView addSubview:orderIdLabel];
    [self.mainScrolView addSubview:orderIdValueLabel];
    [self.mainScrolView addSubview:lblCalculator];
    [self.mainScrolView addSubview:txtDiscountValue];
    [self.mainScrolView addSubview:lblsymbolpercent];
    [self.mainScrolView addSubview:BtnCalculator];
    [self.mainScrolView addSubview:lblPO];
    [self.mainScrolView addSubview:txtPO];
    [self.mainScrolView addSubview:lblremark];
    [self.mainScrolView addSubview:remarkTextView];
    [self.mainScrolView addSubview:lbltotal];
    [self.mainScrolView addSubview:totalPriceLabel];
    [self.mainScrolView addSubview:lbldiscount];
    [self.mainScrolView addSubview:totalDiscountTextField];
    [self.mainScrolView addSubview:changeDiscountTypeButton];
    [self.mainScrolView addSubview:lbltransportation];
    [self.mainScrolView addSubview:transportationTextField];
    [self.mainScrolView addSubview:lblinstallation];
    [self.mainScrolView addSubview:installationTextField];
    [self.mainScrolView addSubview:lblgrandprin];
    [self.mainScrolView addSubview:grandTotalLabel];
    [self.mainScrolView addSubview:btnconfirm];
    [self.mainScrolView addSubview:btncancel];
    [self.mainScrolView addSubview:alertUser];
    [self.mainScrolView addSubview:btnupdatecart];
    [self.mainScrolView addSubview:btnContinueOrder];
    [self.mainScrolView addSubview:logoyourcart];
    [self.mainScrolView addSubview:bluebar];
    
    [self.mainScrolView addSubview:lbldescRemark];
    [self.mainScrolView addSubview:lblcontrolblind];
    [self.mainScrolView addSubview:lblquantityblind];
    [self.mainScrolView addSubview:lblsqftblind];
    [self.mainScrolView addSubview:lbltotalsqftblind];
    [self.mainScrolView addSubview:lblunitprincblind];
    [self.mainScrolView addSubview:lbldiscountblind];
    [self.mainScrolView addSubview:lbltotaltbl];
    
    [self.mainScrolView addSubview:lblremarkdescmain];
    [self.mainScrolView addSubview:lblqtymain];
    [self.mainScrolView addSubview:lblunitpricemain];
    [self.mainScrolView addSubview:lbldiscountmain];
    [self.mainScrolView addSubview:lbltotalmain];*/


    btnordersummary.hidden=YES;

    
    self.mainScrolView.contentSize=CGSizeMake(self.mainScrolView.frame.size.width, 1000);

    //[_tableView reloadData];
    [self updateCart];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (salesoderTypes==1) 
    {
        if (textField == installationNameTextField) {
            [textField resignFirstResponder];
            [dateTextField becomeFirstResponder];
        }
        else if (textField == dateTextField) {
            [textField resignFirstResponder];
            [timeTextField becomeFirstResponder];
        }
        else if (textField == timeTextField) {
            [textField resignFirstResponder];
            [telTextField becomeFirstResponder];
        }
        else if (textField == telTextField) {
            [textField resignFirstResponder];
            [faxTextField becomeFirstResponder];
        }
        else if (textField == faxTextField) {
            [textField resignFirstResponder];
            [emailTextField becomeFirstResponder];
        }
        else if (textField == emailTextField) {
            [textField resignFirstResponder];
            [txtdeliveryblind becomeFirstResponder];
            
        }else if (textField == txtdeliveryblind) {
            [textField resignFirstResponder];
            [txtPO becomeFirstResponder];
            
        }
        else if (textField == txtPO) {
            [textField resignFirstResponder];
            [totalDiscountTextField becomeFirstResponder];
            
        }
        else if (textField == totalDiscountTextField) {
            [textField resignFirstResponder];
            [transportationTextField becomeFirstResponder];
        }
        else if (textField == transportationTextField) 
        {
            [textField resignFirstResponder];
            [installationTextField becomeFirstResponder];
            
        }else if (textField == installationTextField) 
        {
            [textField resignFirstResponder];
            
        }
        
    }else 
    //{
        if (textField == DeliveryDateTextField) {
            [textField resignFirstResponder];
            [txtorderref becomeFirstResponder];
        }
        else if (textField == txtorderref) {
            [textField resignFirstResponder];
            [totalDiscountTextField becomeFirstResponder];
        }
        else if (textField == totalDiscountTextField) {
            [textField resignFirstResponder];
            [ShippingAddressTextView becomeFirstResponder];
        }
        else if (textField == ShippingAddressTextView) {
            [textField resignFirstResponder];
            //[quantityTextField becomeFirstResponder];
        }
    //}
    
	
    
	return NO;
}

-(void)loadDataFinished
{
    if(numberOfRows != 0)
    {
        
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:numberOfRows-1 inSection:0];
        [self._tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        [self updateCart];

    }
    
}
- (void)viewDidUnload
{
    /*[self setLbltransportation:nil];
    [self setLblinstallation:nil];
    [self setLblinstallationName:nil];
    [self setLblinstallationAddress:nil];
    [self setLbldate:nil];
    [self setLbltime:nil];
    [self setLbltel:nil];
    [self setLblfax:nil];
    [self setLblcontact:nil];
    [self setDeliveryDateTextField:nil];
    [self setLbldeliveryDate:nil];
    [self setDescripttionTextView:nil];
    [self setLblDescription:nil];
    [self setLblcustomerCompanyName:nil];
    [self setLblcustomerTel:nil];
    [self setShippingAddress:nil];
    [self setLblorderref:nil];
    [self setTxtorderref:nil];
    [self setLblcusRemark:nil];
    [self setTxtcusRemark:nil];
    [self setShippingAddressTextView:nil];
    [self setChangeDiscountTypeButton:nil];
    [self setTxtDiscountValue:nil];
    [self setLblsymbolpercent:nil];
    [self setLblCalculator:nil];
    [self setBtnCalculator:nil];
    [self setCalculatorbg:nil];
    [self setTxtPO:nil];
    [self setLblPO:nil];
    [self setMainScrolView:nil];
    [self setLblcustomerid:nil];
    [self setLblcustomername:nil];
    [self setImgline:nil];
    [self setLblremark:nil];
    [self setLbltotal:nil];
    [self setLbldiscount:nil];
    [self setLblgrandprin:nil];
    [self setBtnconfirm:nil];
    [self setBtncancel:nil];
    [self setBtnupdatecart:nil];
    [self setBtnContinueOrder:nil];
    [self setLogoyourcart:nil];
    [self setBtnordersummary:nil];
    [self setBluebar:nil];
    [self setLbldescRemark:nil];
    [self setLblcontrolblind:nil];
    [self setLblquantityblind:nil];
    [self setLblsqftblind:nil];
    [self setLbltotalsqftblind:nil];
    [self setLblunitprincblind:nil];
    [self setLbldiscountblind:nil];
    [self setLbltotaltbl:nil];
    [self setLbldeliveryblind:nil];
    [self setTxtdeliveryblind:nil];
    [self setLblremarkdescmain:nil];
    [self setLblqtymain:nil];
    [self setLblunitpricemain:nil];
    [self setLbldiscountmain:nil];
    [self setLbltotalmain:nil];
    [EditTag release];
    [gpriceTextField release];
    [UpdaCartPrice release];
    [EditCartSmallView release];
    [installationNameTextField release];
    [UpdateCartUIView release];

    [orderItem release];
    [orderItemsArr release];
    [backButton release];
    [reviewTableView release];
    [reviewTableViewController release];
    [reviewUIView release];
    [_tableView release];
    //[backgroundImageView release];
    [tableViewHeader release];
    [installationTextField release];
    [cityTextField release];
    [stateTextField release];
    [dateTextField release];
    [timeTextField release];
    [telTextField release];
    [faxTextField release];
    [emailTextField release];
    [transportationTextField release];
    [installationAddressTextView release];
    [remarkTextView release];
    [grandTotalLabel release];
    [orderIdLabel release];
    [orderIdValueLabel release];
    [totalPriceLabel release];
    [CurrentSaleOrder release];
    [totalDiscountTextField release];
    [lbltransportation release];
    [lblinstallation release];
    [lblinstallationName release];
    [lblinstallationAddress release];
    [lbldate release];
    [lbltime release];
    [lbltel release];
    [lblfax release];
    [lblcontact release];
    [DeliveryDateTextField release];
    [lbldeliveryDate release];
    [DescripttionTextView release];
    [lblDescription release];
    [lblcustomerCompanyName release];
    [lblcustomerTel release];
    [shippingAddress release];    
    [lblorderref release];
    [txtorderref release];
    [lblcusRemark release];
    [txtcusRemark release];
    [ShippingAddressTextView release];
    [changeDiscountTypeButton release];
    [txtDiscountValue release];
    [lblsymbolpercent release];
    [lblCalculator release];
    [BtnCalculator release];
    [Calculatorbg release];
    [txtPO release];
    [lblPO release];
    [mainScrolView release];
    [lblcustomerid release];
    [lblcustomername release];
    [imgline release];
    [lblremark release];
    [lbltotal release];
    [lbldiscount release];
    [lblgrandprin release];
    [btnconfirm release];
    [btncancel release];
    [btnupdatecart release];
    [btnContinueOrder release];
    [logoyourcart release];
    [btnordersummary release];
    [bluebar release];
    [lbldescRemark release];
    [lblcontrolblind release];
    [lblquantityblind release];
    [lblsqftblind release];
    [lbltotalsqftblind release];
    [lblunitprincblind release];
    [lbldiscountblind release];
    [lbltotaltbl release];
    [lbldeliveryblind release];
    [txtdeliveryblind release];
    [lblremarkdescmain release];
    [lblqtymain release];
    [lblunitpricemain release];
    [lbldiscountmain release];
    [lbltotalmain release];*/
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [super viewDidUnload];
    
}
-(void)keyboardWillShow:(NSNotification*)n
{
    iskeyboardDisplayed=YES;
    
}
-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    iskeyboardDisplayed=NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(void)tableCell:(UITableViewCell*)cell RemovedOrderItemAtIndex:(int)orderItemIndex
{
    GridTableView* griViewCell=(GridTableView*)cell;
    float orderItemPrice=[griViewCell.priceTextField.text floatValue] * [griViewCell.quantityTextField.text floatValue];
    totalPricce-=orderItemPrice;
    shouldUpdateCart=YES;
    totalPricce=0.0;
    [_tableView reloadData];
}
-(void)updateAlertText
{
    alertUser.hidden = NO;
    
}
/*-(void)scrollToItem:(NSIndexPath *)indexPath
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.4];
    reviewUIView.alpha = 0.0;
    [UIView commitAnimations];
    [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    //[_tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    
}
-(void)focusOnCell:(NSIndexPath*)indexPath
{
    for(int i=0;i<[_tableView visibleCells].count;i++)
    {
        if(i == indexPath.row)
        {
            GridTableView* cell = [[_tableView visibleCells] objectAtIndex:i];
            cell.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.75 alpha:0.6];
        }
    }
}


- (NSIndexPath *)indexPathForCellController:(id)cellController
{
    float sumOfOrderItemPrice = 0.0;
	NSInteger sectionIndex;
	NSInteger sectionCount = [orderItemsArr count];
	for (sectionIndex = 0; sectionIndex < sectionCount; sectionIndex++)
	{
		NSArray *section = [orderItemsArr objectAtIndex:sectionIndex];
		NSInteger rowIndex;
		NSInteger rowCount = [section count];
		for (rowIndex = 0; rowIndex < rowCount; rowIndex++)
		{
			NSArray *row = [section objectAtIndex:rowIndex];
			if ([row isEqual:cellController])
			{
				return [NSIndexPath indexPathForRow:rowIndex inSection:sectionIndex];
                sumOfOrderItemPrice += salesOrderItem.TotalPrice;
                NSLog(@"sumOfOrderItemPrice--->%@",sumOfOrderItemPrice);
			}
		}
	}
	
	return nil;
}*/

-(void)updateCart 
{
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    self.orderItemsArr=[appDelegate.salesOrder order_Items];
    //NSLog(@"update cart Category--->%d",appDelegate.salesOrder.Type);
    salesoderTypes=appDelegate.salesOrder.Type;
    
    float sumOfOrderItemPrice = 0.00;
    float transportationCost=0.00;
    float installationCost=0.00;
    float sumOfOrderItemPrice8 = 0.00;
    float sumOfOrderItemPriceedit=0.00; 
    
    for(int i=0;i<orderItemsArr.count;i++)
    {
        //GridTableView* cell=[_tableView cellForRowAtIndexPath: [(GridTableView *)tableView.delegate indexPathForCellController:self]];
        //salesOrderItem.TotalPrice; [_tableView cellForRowAtIndexPath:i]
        //NSString *orderItemsArrTOTAL=orderItemsArr;
        //sumOfOrderItemPrice += ;
        
        SalesOrderItem* orderItem2=[[appDelegate.salesOrder order_Items] objectAtIndex:i];
        BlindSalesOrderItem* blindorderItem2=[[appDelegate.salesOrder order_Items] objectAtIndex:i];
        //SalesOrder* SorderItemType=[[appDelegate.salesOrder order_Items] objectAtIndex:i];
        /*NSLog(@"Unit_Price--->%.2f",orderItem2.Unit_Price);
         NSLog(@"Quantity--->%.2f",orderItem2.Quantity);
         NSLog(@"TotalPrice--->%.2f",orderItem2.TotalPrice);
         NSLog(@"Discount_Amount--->%.2f",orderItem2.Discount_Amount);
         NSLog(@"Discount_Percent--->%.2f",orderItem2.Discount_Percent);
         NSLog(@"TotalPrice--->%.2f",orderItem2.TotalPrice);*/
        
        //NSLog(@"Category--->%@",orderItem2.item.Category);
        
        //NSLog(@"SorderItemType--->%@",blindorderItem2.item.Category);
        
        
        
        
        if (orderItem2.Unit_Price) 
        {
            if ([self.EditTag isEqualToString:@"edittag"]) 
            {
                
                if (salesoderTypes==0) 
                {
                    
                    
                    sumOfOrderItemPrice8 = (orderItem2.Quantity*orderItem2.Unit_Price);
                    
                    if (orderItem2.Discount_Percent != 0.0) 
                    {
                        //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                        totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*orderItem2.Quantity)/100;
                        sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                    }else if(orderItem2.Discount_Amount != 0.0)
                    {
                        //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                        totalDiscountOfItems=orderItem2.Discount_Amount;
                        sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                    }else 
                    {
                        sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                        //sumOfOrderItemPrice += orderItem2.TotalPrice;
                    }
                    sumOfOrderItemPrice+=sumOfOrderItemPriceedit;
                    
                    
                }else if (salesoderTypes==1)
                {
                    
                    //NSLog(@"Quantity_in_Set--->%.2f",blindorderItem2.Quantity_in_Set);
                    
                    if (blindorderItem2.Quantity_in_Set != 0) 
                    {
                        NSString* blindorderItem2Quantity_in_Set = [[NSString alloc] initWithFormat:@"%.2f", blindorderItem2.Quantity_in_Set];
                        NSString* orderItem2Unit_Price = [[NSString alloc] initWithFormat:@"%.2f", orderItem2.Unit_Price];
                        
                        sumOfOrderItemPrice8 = [blindorderItem2Quantity_in_Set floatValue] *[orderItem2Unit_Price floatValue] ;
                        
                        
                        sumOfOrderItemPrice8=sumOfOrderItemPrice8*orderItem2.Quantity;

                        
                        if (orderItem2.Discount_Percent != 0.0) 
                        {
                            //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                            totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*blindorderItem2.Quantity_in_Set)/100;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else if(orderItem2.Discount_Amount != 0.0)
                        {
                            //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                            totalDiscountOfItems=orderItem2.Discount_Amount;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else 
                        {
                            sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                            //sumOfOrderItemPrice += orderItem2.TotalPrice;
                        }
                        sumOfOrderItemPrice+=sumOfOrderItemPriceedit;
                        
                        [blindorderItem2Quantity_in_Set release];
                        [orderItem2Unit_Price release];

                    }else {
                        sumOfOrderItemPrice8 = (blindorderItem2.Quantity*orderItem2.Unit_Price);
                        
                        if (orderItem2.Discount_Percent != 0.0) 
                        {
                            //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                            totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*blindorderItem2.Quantity)/100;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else if(orderItem2.Discount_Amount != 0.0)
                        {
                            //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                            totalDiscountOfItems=orderItem2.Discount_Amount;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else 
                        {
                            sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                            //sumOfOrderItemPrice += orderItem2.TotalPrice;
                        }
                        sumOfOrderItemPrice+=sumOfOrderItemPriceedit;

                    }
                    
                                        
                }
                
                
            }else if (![self.EditTag isEqualToString:@"edittag"]) 
            {
                if (salesoderTypes==0) 
                {
                    float sumOfOrderItemPriceedit=0.0;
                    sumOfOrderItemPrice8 = (orderItem2.Quantity*orderItem2.Unit_Price);
                    
                    if (orderItem2.Discount_Percent != 0.0) 
                    {
                        //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                        totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*orderItem2.Quantity)/100;
                        sumOfOrderItemPriceedit= sumOfOrderItemPrice8 - totalDiscountOfItems;
                    }else if(orderItem2.Discount_Amount != 0.0)
                    {
                        //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                        totalDiscountOfItems=orderItem2.Discount_Amount;
                        sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                    }else 
                    {
                        sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                        //sumOfOrderItemPrice += orderItem2.TotalPrice;
                    }
                    sumOfOrderItemPrice+=sumOfOrderItemPriceedit;
                    
                }else if (salesoderTypes==1)
                {
                    //float blindorderItem2Quantity_in_Set=0.00;
                    //float orderItem2Unit_Price=0.00;
                    
                    //NSLog(@"Quantity_in_Set--->%.2f",blindorderItem2.Quantity_in_Set);
                    if (blindorderItem2.Quantity_in_Set != 0) 
                    {
                        float sumOfOrderItemPriceedit=0.0f;
                        
                        NSString* blindorderItem2Quantity_in_Set = [[NSString alloc] initWithFormat:@"%.2f", blindorderItem2.Quantity_in_Set];
                        NSString* orderItem2Unit_Price = [[NSString alloc] initWithFormat:@"%.2f", orderItem2.Unit_Price];
                        
                        sumOfOrderItemPrice8 = [blindorderItem2Quantity_in_Set floatValue] *[orderItem2Unit_Price floatValue] ;
                        
                        
                        sumOfOrderItemPrice8=sumOfOrderItemPrice8*orderItem2.Quantity;

                        
                        if (orderItem2.Discount_Percent != 0.0) 
                        {
                            //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                            totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*blindorderItem2.Quantity_in_Set)/100;
                            sumOfOrderItemPriceedit= sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else if(orderItem2.Discount_Amount != 0.0)
                        {
                            //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                            totalDiscountOfItems=orderItem2.Discount_Amount;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else 
                        {
                            sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                            //sumOfOrderItemPrice += orderItem2.TotalPrice;
                        }
                        sumOfOrderItemPrice+=sumOfOrderItemPriceedit;
                        
                        [blindorderItem2Quantity_in_Set release];
                        [orderItem2Unit_Price release];

                    }else {
                        float sumOfOrderItemPriceedit=0.0;
                        sumOfOrderItemPrice8 = (blindorderItem2.Quantity*orderItem2.Unit_Price);
                        
                        if (orderItem2.Discount_Percent != 0.0) 
                        {
                            //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                            totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*blindorderItem2.Quantity)/100;
                            sumOfOrderItemPriceedit= sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else if(orderItem2.Discount_Amount != 0.0)
                        {
                            //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                            totalDiscountOfItems=orderItem2.Discount_Amount;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else 
                        {
                            sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                            //sumOfOrderItemPrice += orderItem2.TotalPrice;
                        }
                        sumOfOrderItemPrice+=sumOfOrderItemPriceedit;

                    }
                                        
                }
                
                
            }
            
        }
        
    }
    
    totalPricce = sumOfOrderItemPrice;

    NSString* totalPriceWithDiscountStr = [[NSString alloc] initWithFormat:@"%.2f", totalPricce];
    
    
    
    if (sumOfOrderItemPrice) {
        
        
        
        
        if(![transportationTextField.text isEqualToString:@""])
        {
            transportationCost=[transportationTextField.text floatValue];
        }
        
        if(![installationTextField.text isEqualToString:@""])
        {
            installationCost=[installationTextField.text floatValue];
        }
        
        if([totalDiscountTextField.text floatValue]>totalPricce)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning!" message:@"The Amount of Total Discount is more than the Total Price." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            totalDiscountTextField.text = [NSString stringWithFormat:@"%d", 0];
            
            discountOnTotalPrice=[totalDiscountTextField.text floatValue];
            
        }else if([totalDiscountTextField.text floatValue]<totalPricce)
        {
            discountOnTotalPrice=[totalDiscountTextField.text floatValue];
        }
        
        appDelegate.salesOrder.order_discount=discountOnTotalPrice;
        
        
        totalPriceLabel.text = totalPriceWithDiscountStr;
        
        grandTotalLabel.text = totalPriceWithDiscountStr;
        
        grandPrice = totalPricce - discountOnTotalPrice+transportationCost+installationCost;
        NSString* grandPriceStr = [[NSString alloc] initWithFormat:@"%.2f", grandPrice];
        grandTotalLabel.text = grandPriceStr;
        shouldUpdateCart=NO;
        [totalPriceWithDiscountStr release];
        [grandPriceStr release];
    }
    
}
-(IBAction)changeDiscountType_ButtonTapped:(id)sender
{
    if(isDiscountAmount)
    {
        [((UIButton*)sender) setBackgroundImage:[UIImage imageWithContentsOfFile:@"Btn_Discount_Amount.png"] forState:UIControlStateNormal];
        //self.discountTypeLabel.textColor=[UIColor blackColor];
        //self.discountTypeLabel.text = @"In %";
        
        txtDiscountValue.hidden=YES;
        lblsymbolpercent.hidden=YES;
        lblCalculator.hidden=YES;
        BtnCalculator.hidden=YES;
        Calculatorbg.hidden=YES;
        
        isDiscountAmount = NO;
        
    }
    else
    {
        [((UIButton*)sender) setBackgroundImage:[UIImage imageWithContentsOfFile:@"Button_Amount_Percentage.png"] forState:UIControlStateNormal];
        //self.discountTypeLabel.textColor=[UIColor blackColor];
        //self.discountTypeLabel.text = @"In amount";
        txtDiscountValue.hidden=NO;
        lblsymbolpercent.hidden=NO;
        lblCalculator.hidden=NO;
        BtnCalculator.hidden=NO;
        Calculatorbg.hidden=NO;
        
        isDiscountAmount = YES;
    }
    
}



/*-(void)setIsDiscountAmount:(BOOL)tisDiscountAmount
{
    isDiscountAmount=tisDiscountAmount;
    NSString *imageName=@"Btn_Discount_Amount.png";
    if(isDiscountAmount)
    {
        imageName=@"Button_Amount_Percentage.png";
    }
    [self.changeDiscountTypeButton setBackgroundImage:[UIImage imageWithContentsOfFile:imageName] forState:UIControlStateNormal];
}*/


-(IBAction)submitDiscountPercentValue
{
    float discounttotal;
    if (![txtDiscountValue.text isEqualToString:@""]) 
    {
        discounttotal=([txtDiscountValue.text floatValue]*totalPricce)/100;
         NSString* totalPriceWithDiscountStr = [NSString stringWithFormat:@"%.2f", discounttotal];
        totalDiscountTextField.text=totalPriceWithDiscountStr;
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField.tag == 90) {
        int lengtha = [installationNameTextField.text length];
        //NSLog(@"lenghta = %d",lengtha);
        if (lengtha >= MAXBRANDsixty && ![string isEqualToString:@""]) {
            installationNameTextField.text = [installationNameTextField.text substringToIndex:MAXBRANDsixty];
            return NO;
        }
        return YES;
    } else if (textField.tag == 98) {
        int lengthb = [dateTextField.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXQTYthirty && ![string isEqualToString:@""]) {
            dateTextField.text = [dateTextField.text substringToIndex:MAXQTYthirty];
            return NO;
        }
        return YES;
    } else if (textField.tag == 99) {
        int lengthc = [timeTextField.text length];
        //NSLog(@"lenghtc = %d",lengthc);
        if (lengthc >= MAXQTYthirty && ![string isEqualToString:@""]) {
            timeTextField.text = [timeTextField.text substringToIndex:MAXQTYthirty];
            return NO;
        }
        return YES;
    } else if (textField.tag == 101) {
        int lengthd = [telTextField.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXQTYthirty && ![string isEqualToString:@""]) {
            telTextField.text = [telTextField.text substringToIndex:MAXQTYthirty];
            return NO;
        }
        return YES;
    }else if (textField.tag == 102) {
        int lengthd = [faxTextField.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXQTYthirty && ![string isEqualToString:@""]) {
            faxTextField.text = [faxTextField.text substringToIndex:MAXQTYthirty];
            return NO;
        }
        return YES;
    }else if (textField == txtPO) {
        int lengthd = [txtPO.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXBRANDfifty && ![string isEqualToString:@""]) {
            txtPO.text = [txtPO.text substringToIndex:MAXBRANDfifty];
            return NO;
        }
        return YES;
    }else if (textField.tag == 103) {
        int lengthd = [emailTextField.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXBRANDsixty && ![string isEqualToString:@""]) {
            emailTextField.text = [emailTextField.text substringToIndex:MAXBRANDsixty];
            return NO;
        }
        return YES;
    }
    else if (textField.tag == 104) {
        int lengthd = [txtDiscountValue.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXBRANDtwo && ![string isEqualToString:@""]) {
            txtDiscountValue.text = [txtDiscountValue.text substringToIndex:MAXBRANDtwo];
            return NO;
        }
        return YES;
    }
    else if (textField.tag == kTotalDiscount) {
        int lengthd = [totalDiscountTextField.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXSIZEtwelve && ![string isEqualToString:@""]) {
            totalDiscountTextField.text = [totalDiscountTextField.text substringToIndex:MAXSIZEtwelve];
            return NO;
        }
        return YES;
    }else if (textField.tag == 106) {
        int lengthd = [transportationTextField.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXSIZEtwelve && ![string isEqualToString:@""]) {
            transportationTextField.text = [transportationTextField.text substringToIndex:MAXSIZEtwelve];
            return NO;
        }
        return YES;
    }else if (textField.tag == 107) {
        int lengthd = [installationTextField.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXSIZEtwelve && ![string isEqualToString:@""]) {
            installationTextField.text = [installationTextField.text substringToIndex:MAXSIZEtwelve];
            return NO;
        }
        return YES;
    }else if (textField.tag == 108) {
        int lengthd = [txtcusRemark.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXLENGTHo && ![string isEqualToString:@""]) {
            txtcusRemark.text = [txtcusRemark.text substringToIndex:MAXLENGTHo];
            return NO;
        }
        return YES;
    }else if (textField.tag == 109) {
        int lengthd = [DeliveryDateTextField.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXBRANDfifty && ![string isEqualToString:@""]) {
            DeliveryDateTextField.text = [DeliveryDateTextField.text substringToIndex:MAXBRANDfifty];
            return NO;
        }
        return YES;
    }else if (textField.tag == 1091) {
        int lengthd = [txtdeliveryblind.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXBRANDfifty && ![string isEqualToString:@""]) {
            DeliveryDateTextField.text = [txtdeliveryblind.text substringToIndex:MAXBRANDfifty];
            return NO;
        }
        return YES;
    }else if (textField.tag == 110) {
        int lengthd = [txtorderref.text length];
        //NSLog(@"lenghtd = %d",lengthd);
        if (lengthd >= MAXBRANDfifty && ![string isEqualToString:@""]) {
            txtorderref.text = [txtorderref.text substringToIndex:MAXBRANDfifty];
            return NO;
        }
        return YES;
    }
   
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)string
{
    if (textView.tag == 201) {
        int lengtha = [installationAddressTextView.text length];
        //NSLog(@"lenghta = %d",lengtha);
        if (lengtha >= MAXLENGTHto && ![string isEqualToString:@""]) {
            installationAddressTextView.text = [installationAddressTextView.text substringToIndex:MAXLENGTHto];
            return NO;
        }
        return YES;
    } else if (textView.tag == 202) {
        int lengthb = [ShippingAddressTextView.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHto && ![string isEqualToString:@""]) {
            ShippingAddressTextView.text = [ShippingAddressTextView.text substringToIndex:MAXLENGTHto];
            return NO;
        }
        return YES;
    } else if (textView.tag == 203) {
        int lengthb = [DescripttionTextView.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHo && ![string isEqualToString:@""]) {
            DescripttionTextView.text = [DescripttionTextView.text substringToIndex:MAXLENGTHo];
            return NO;
        }
        return YES;
    } else if (textView.tag == 204) {
        int lengthb = [remarkTextView.text length];
        //NSLog(@"lenghtb = %d",lengthb);
        if (lengthb >= MAXLENGTHo && ![string isEqualToString:@""]) {
            remarkTextView.text = [remarkTextView.text substringToIndex:MAXLENGTHo];
            return NO;
        }
        return YES;
    } 
    return YES;
}

/*- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [installationNameTextField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    return newLength <= 10;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger insertDelta = string.length - range.length;
    
    if (installationNameTextField.text.length + insertDelta > 11)
    {
        return NO; // the new string would be longer than MAX_LENGTH
    }else if (dateTextField.text.length + insertDelta > 10)
    {
        return NO; // the new string would be longer than MAX_LENGTH
    }
    else {
        return YES;
    }
}*/

/*-(void)updateCart
{
        KDSAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
        totalDiscountOfItems=0.0;
        totalPricce=0.0;
    
        NSLog(@"_tableView indexPathsForSelectedRows--->%d",[_tableView indexPathsForSelectedRows].count);
    
    int cc = orderItemsArr.count;
    
    NSLog(@"orderItemsArr-->%d",cc);
    
        float sumOfOrderItemPrice = 0.0;
        for(int i=0;i<[_tableView visibleCells].count;i++)
        {
            //GridTableView* cell=[_tableView cellForRowAtIndexPath: [(GridTableView *)tableView.delegate indexPathForCellController:self]];
            
            GridTableView* cell=[_tableView cellForRowAtIndexPath:self];
            //static NSString *CellIdentifier = @"Cell";
            //GridTableView *cell = [self tableView:table cellForRowAtIndexPathde:indexPati];
            //GridTableView *cell = [[self tableView] cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            
            SalesOrderItem* orderItem=[[appDelegate.salesOrder order_Items] objectAtIndex:i];
            
            orderItem.Unit_Price=[cell.priceTextField.text floatValue];
            orderItem.Quantity=[cell.quantityTextField.text floatValue];
            orderItem.Comment = cell.descriptionTextView.text;
            
            float orderQuantity = [cell.quantityTextField.text floatValue];
            float orderPrice = [cell.priceTextField.text floatValue];
            sumOfOrderItemPrice += (orderQuantity*orderPrice);
            orderQuantity = 0;
            if(cell.isDiscountAmount)
            {
                
                NSString* alertStr;
                if([cell.discountTextField.text floatValue] >= orderItem.Quantity*orderItem.Unit_Price)
                {
                    if(i==0)
                    {
                        alertStr=[NSString stringWithFormat:@"The Discount Amount for First Order Item can't be more than total price of that order item."];
                    }
                    else
                    {
                        alertStr = [NSString stringWithFormat:@"The Discount Amount for %d%@ Order Item can't be more than total price of that order item.", i+1, @"nd"];
                    }
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:alertStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    [alert release];
                    cell.discountTextField.text = [NSString stringWithFormat:@"%d", 0];
                }
                else
                {
                    orderItem.Discount_Amount=[cell.discountTextField.text floatValue];
                    totalDiscountOfItems+=[cell.discountTextField.text floatValue];
                }
            }
            else
            {
                if([cell.discountTextField.text floatValue] == 100)
                {
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"100% Discount is not allowed." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    [alert release];
                    cell.discountTextField.text = [NSString stringWithFormat:@"%d", 0];
                }
                else
                {
                    orderItem.Discount_Percent=[cell.discountTextField.text floatValue];
                    totalDiscountOfItems+=(orderItem.Discount_Percent*orderItem.Unit_Price*orderItem.Quantity)/100;
                }
            }
        }
    
    
    
        totalPricce = sumOfOrderItemPrice - totalDiscountOfItems;
        float transportationCost=0.0f;
        float installationCost=0.0f;
        if(![transportationTextField.text isEqualToString:@""])
        {
            transportationCost=[transportationTextField.text floatValue];
        }
        if(![installationTextField.text isEqualToString:@""])
        {
            installationCost=[installationTextField.text floatValue];
        }
        if([totalDiscountTextField.text floatValue]>totalPricce)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning!" message:@"The Amount of Total Discount is more than the Total Price." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            totalDiscountTextField.text = [NSString stringWithFormat:@"%d", 0];
        }
        else if([totalDiscountTextField.text floatValue]<totalPricce)
        {
            discountOnTotalPrice=[totalDiscountTextField.text floatValue];
        }
        appDelegate.salesOrder.order_discount=discountOnTotalPrice;
        
        NSString* totalPriceWithDiscountStr = [NSString stringWithFormat:@"%.2f", totalPricce];
        totalPriceLabel.text = totalPriceWithDiscountStr;
        
        grandTotalLabel.text = totalPriceWithDiscountStr;
        
        grandPrice = totalPricce - discountOnTotalPrice+transportationCost+installationCost;
        NSString* grandPriceStr = [NSString stringWithFormat:@"%.2f", grandPrice];
        grandTotalLabel.text = grandPriceStr;
        shouldUpdateCart=NO;
}*/





@end
