//
//  UOMAndConversion.m
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UOMAndConversion.h"


@implementation UOMAndConversion

@synthesize Uom = _Uom, Converstion = _Converstion;
-(void)dealloc
{
    [_Uom release];
    [_Converstion release];
    [super dealloc];
}

@end
