//
//  KDSDataItem.m
//  KDS
//
//  Created by Tiseno on 12/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KDSDataItem.h"


@implementation KDSDataItem
@synthesize runBatch;
-(void)dealloc
{
    [super dealloc];
}
-(DataBaseInsertionResult)insertItem:(Item*)tItem
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    } 
    if(connectionIsOpenned)
    {
        NSString *insertSQL;
         
        if((![tItem.Category isEqualToString:kBlindCategory]) && (![tItem.Category isEqualToString:kblindACategoryA]))
        {
            NSString *itemDescription=[tItem.Description stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"]; 
            NSString *itemNo=[tItem.Item_No stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
            insertSQL = [NSString stringWithFormat:@"insert into Item(category,description,fmt_item_no,image_path,item_no,stock_unit) values('%@','%@','%@','%@','%@','%@');",tItem.Category,itemDescription,tItem.FMT_Item_No,tItem.Image_Path,itemNo,tItem.Stock_Unit];
            
            
            NSLog(@"tItem.Description--->%@",tItem.Description );
        }
        else
        {
            BlindItem* blindItem = (BlindItem*)tItem;
            NSString *itemDescription=[blindItem.Description stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
            NSString *itemNo=[blindItem.Item_No stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
            insertSQL = [NSString stringWithFormat:@"insert into Item(min_sqft,min_height,category,description,fmt_item_no,image_path,item_no,stock_unit) values(%f,%f,'%@','%@','%@','%@','%@','%@');",blindItem.min_SqFt,blindItem.min_Height,blindItem.Category,itemDescription,blindItem.FMT_Item_No,blindItem.Image_Path,itemNo,blindItem.Stock_Unit];
            
            NSLog(@"tItem.Descriptionbbbb--->%@",tItem.Description );
        } 
        //NSString *itemDescription=[tItem.Description stringByReplacingOccurrencesOfString:@"'" withString:@"\""];
        //NSString *insertSQL = [NSString stringWithFormat:@"insert into Item(category,description,fmt_item_no,image_path,item_no,stock_unit) values('%@','%@','%@','%@','%@','%@');",tItem.Category,itemDescription,tItem.FMT_Item_No,tItem.Image_Path,tItem.Item_No,tItem.Stock_Unit];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            int itemID=sqlite3_last_insert_rowid(dbKDS);
            for(UOMAndConversion* uom in tItem.UOMAndConversion)
            {
                [self insertUomAndConversion:uom forItem:itemID];
            }
            for(DefaultPriceAndUOM* defaultPrice in tItem.Default_Price)
            {
                [self insertDefaultPriceAndUOM:defaultPrice forItem:itemID];
            }
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        //NSLog(@"%@",insertSQL);
        //NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}
-(DataBaseInsertionResult)insertUomAndConversion:(UOMAndConversion*)tUOM forItem:(int)tItem_id
{
    NSString *insertSQL = [NSString stringWithFormat:@"insert into UOMAndConversion(item_id,conversion,uom) values(%d,'%@','%@');",tItem_id,tUOM.Converstion,tUOM.Uom];
    sqlite3_stmt *statement;
    const char *insert_stmt = [insertSQL UTF8String];
    
    sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
    if(sqlite3_step(statement) == SQLITE_DONE)
    {
        sqlite3_finalize(statement);
        return DataBaseInsertionSuccessful;
    }
    NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
    sqlite3_finalize(statement);
    return DataBaseInsertionFailed;
}
-(DataBaseInsertionResult)insertDefaultPriceAndUOM:(DefaultPriceAndUOM*)tDefaultPrice forItem:(int)tItem_id
{
    NSString *insertSQL = [NSString stringWithFormat:@"insert into DefaultPriceAndUOM(item_id,default_price,uom) values(%d,'%@','%@');",tItem_id,tDefaultPrice.Default_Price,tDefaultPrice.Uom];
    sqlite3_stmt *statement;
    const char *insert_stmt = [insertSQL UTF8String];
    
    sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
    if(sqlite3_step(statement) == SQLITE_DONE)
    {
        sqlite3_finalize(statement);
        return DataBaseInsertionSuccessful;
    }
    NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
    sqlite3_finalize(statement);
    return DataBaseInsertionFailed;
}
-(DataBaseDeletionResult)deletItem:(Item*)tItem 
{
    NSString *insertSQL = [NSString stringWithFormat:@"delete from Item where item_id=%d;",tItem.Item_id];
    sqlite3_stmt *statement;
    const char *insert_stmt = [insertSQL UTF8String];
     
    sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
    if(sqlite3_step(statement) == SQLITE_DONE)
    {
        sqlite3_finalize(statement);
        if([self deletUOMAndConversionForItem:tItem]!=DataBaseDeletionSuccessful)
        {
            return DataBaseDeletionFailed;
        }
        if([self deletDefaultPriceAndUOMForItem:tItem]!=DataBaseDeletionSuccessful)
        {
            return DataBaseDeletionFailed;
        }
        return DataBaseDeletionSuccessful;
    }
    NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
    sqlite3_finalize(statement);
    return DataBaseDeletionFailed;
}
-(DataBaseDeletionResult)deletUOMAndConversionForItem:(Item*)tItem
{
    NSString *insertSQL = [NSString stringWithFormat:@"delete from UOMAndConversion where item_id=%d;",tItem.Item_id];
    sqlite3_stmt *statement;
    const char *insert_stmt = [insertSQL UTF8String];
    
    sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
    if(sqlite3_step(statement) == SQLITE_DONE)
    {
        sqlite3_finalize(statement);
        return DataBaseDeletionSuccessful;
    }
    NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
    sqlite3_finalize(statement);
    return DataBaseDeletionFailed;
}
-(DataBaseDeletionResult)deletDefaultPriceAndUOMForItem:(Item*)tItem
{
    NSString *insertSQL = [NSString stringWithFormat:@"delete from DefaultPriceAndUOM where item_id=%d;",tItem.Item_id];
    sqlite3_stmt *statement;
    const char *insert_stmt = [insertSQL UTF8String];
    
    sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
    if(sqlite3_step(statement) == SQLITE_DONE)
    {
        sqlite3_finalize(statement);
        return DataBaseDeletionSuccessful;
    }
    NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
    sqlite3_finalize(statement);
    return DataBaseDeletionFailed;
}
 
-(void)deleteAllitems
{
    NSArray *existingItems=[self selectItem];
    
    for(Item* eItem in existingItems)
    {
        [self deletItem:eItem];
    }

}

-(void)deleteMainitems
{
    NSArray *existingItems=[self selectremoveItemMain];
    
    for(Item* eItem in existingItems)
    {
        [self deletItem:eItem];
    }
    
}

-(void)deleteBlinditems
{
    NSArray *existingItems=[self selectremoveItemBlind];
    
    for(Item* eItem in existingItems)
    {
        [self deletItem:eItem];
    }
    
}

-(DataBaseInsertionResult)insertItemArray:(NSArray*)itemArr
{
    self.runBatch=YES; 
    DataBaseInsertionResult insertionResult=DataBaseInsertionSuccessful;
    NSArray *existingItems=[self selectItem];
    [self openConnection];
    for(Item* item in itemArr)
    {
        BOOL foundAMatch=NO;
        Item* matchedItem=nil;
        for(Item* eItem in existingItems)
        {
            if([eItem.Item_No isEqualToString:item.Item_No])
            {
                foundAMatch=YES;
                matchedItem=eItem;
                if(matchedItem!=nil)
                {
                    if(matchedItem.UOMAndConversion.count!=item.UOMAndConversion.count || 
                       matchedItem.Default_Price.count!=item.Default_Price.count ||
                       ![matchedItem.Description isEqualToString:item.Description] ||
                       ![matchedItem.Image_Path isEqualToString:item.Image_Path])
                    {
                        [self deletItem:matchedItem];
                        [self insertItem:item];
                    }
                }
                break;
            }
        }
        if(!foundAMatch)
        {
            insertionResult=[self insertItem:item];
            if(insertionResult==DataBaseInsertionFailed)
            {
                break;
            }
        }/*
        else
        {
            if(matchedItem!=nil)
            {
                if(matchedItem.UOMAndConversion.count!=item.UOMAndConversion.count || 
                   matchedItem.Default_Price.count!=item.Default_Price.count ||
                   ![matchedItem.Description isEqualToString:item.Description] ||
                   ![matchedItem.Image_Path isEqualToString:item.Image_Path])
                {
                    [self deletItem:matchedItem];
                    [self insertItem:item];
                }
            }
        }*/
    }
    [self closeConnection];
    return insertionResult;
}

-(NSArray*)selectUpdateItem:(Item*)item
{
    NSMutableArray* itemarr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        itemarr=[[[NSMutableArray alloc] init] autorelease];
        //NSString *selectSQL=[NSString stringWithFormat:@"select item.item_id,item.category,item.description,item.fmt_item_no,item.image_path,item.item_no,item.stock_unit, UOMAndConversion.conversion,UOMAndConversion.uom,item.min_height,item.min_sqft from Item,UOMAndConversion where item.item_id=UOMAndConversion.item_id"];
        
        NSString *selectSQL=[NSString stringWithFormat:@"select item.item_id,item.category,item.description,item.fmt_item_no,item.image_path,item.item_no,item.stock_unit, UOMAndConversion.conversion,UOMAndConversion.uom,item.min_height,item.min_sqft from Item LEFT JOIN UOMAndConversion ON item.item_id=UOMAndConversion.item_id WHERE item.item_id='%d'", item.Item_id];
        
        //SELECT Name, Day FROM Customers LEFT JOIN Reservations ON Customers.CustomerID=Reservations.CustomerId;
        
        
        //NSString *selectSQL=[NSString stringWithFormat:@"select last_selling_price,last_selling_price_date,last_selling_price_uom,min_height,min_sqft from LastSellingPrice where salesperson_code='%@' and customer_id='%@' and item_no='%@' group by last_selling_price_uom", salesperson.SalesPerson_Code,customer.Customer_Id,item.Item_No];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        Item *prevItem=nil;
        int prevItemID=-1;
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            int curItemID=sqlite3_column_int(statement, 0);
            if(prevItemID!=curItemID)
            {
                Item *tItem;
                NSString* itemCategory = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                if((![itemCategory isEqualToString:kBlindCategory]) && (![itemCategory isEqualToString:kblindACategoryA])) 
                {
                    tItem = [[Item alloc] init];
                    
                    //NSLog(@"arr tItem.Item_No-->%@",tItem.Item_No);
                    //NSLog(@"arr tItem.Category-->%@",tItem.Category);
                }
                else
                {
                    tItem = [[BlindItem alloc] init];
                    ((BlindItem*)tItem).min_Height = sqlite3_column_double(statement, 9);
                    ((BlindItem*)tItem).min_SqFt = sqlite3_column_double(statement, 10);
                }
                tItem.Item_id=curItemID;
                UOMAndConversion *tUom = [[UOMAndConversion alloc] init];
                
                int itemID=sqlite3_column_int(statement, 0);
                tItem.Item_id=itemID;
                NSString *category=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                tItem.Category = category;
                
                NSString *description=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
                NSString *descriptiono=[description stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                
                tItem.Description = descriptiono;
                
                NSString *fmt_item_no=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
                NSString *fmt_item_noo=[fmt_item_no stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                tItem.FMT_Item_No = fmt_item_noo;
                
                NSString *image_path=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 4)];
                tItem.Image_Path = image_path;
                NSString *item_no=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 5)];
                NSString *item_noo=[item_no stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                tItem.Item_No = item_noo;
                NSString *stock_unit=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)];
                tItem.Stock_Unit = stock_unit;
                
                NSString *conversion=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
                tUom.Converstion = conversion;
                NSString *uom=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
                tUom.Uom = uom;
                
                NSMutableArray *tUOMArr=[[NSMutableArray alloc] init];
                [tUOMArr addObject:tUom];
                tItem.UOMAndConversion=tUOMArr;
                [tUOMArr release];
                [itemarr addObject:tItem];
                prevItem=tItem;
                prevItemID=curItemID;
                [tItem release];
                [tUom release];
            }
            else
            {
                UOMAndConversion* tUom=[[UOMAndConversion alloc] init];
                NSString *conversion=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
                tUom.Converstion = conversion;
                NSString *uom=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
                tUom.Uom = uom;
                [prevItem.UOMAndConversion addObject:tUom];
                [tUom release];
            }
        } 
        for(Item* item in itemarr)
        {
            if(item.Default_Price==nil)
            {
                NSMutableArray *tDefaultPriceArr=[[NSMutableArray alloc] init];
                item.Default_Price=tDefaultPriceArr;
                [tDefaultPriceArr release];
            }
            [self addDefaultPriceToItem:item];
        }
        [self closeConnection];
    }
    return itemarr;
} 


-(NSArray*)selectremoveItemMain
{
    NSMutableArray* itemarr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        itemarr=[[[NSMutableArray alloc] init] autorelease];
        //NSString *selectSQL=[NSString stringWithFormat:@"select item_id, category from Item where category ='BLIND' OR category ='BLINDA' "];
        NSString *selectSQL=[NSString stringWithFormat:@"select item_id, category from Item where NOT category ='BLIND' AND NOT category ='BLINDA' "];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        Item *prevItem=nil;
        int prevItemID=-1;
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            int curItemID=sqlite3_column_int(statement, 0);
            if(prevItemID!=curItemID)
            {
                Item *tItem;
                NSString* itemCategory = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                if((![itemCategory isEqualToString:kBlindCategory]) && (![itemCategory isEqualToString:kblindACategoryA]))
                {
                    tItem = [[Item alloc] init];
                    
                    
                }
                else
                {
                    tItem = [[BlindItem alloc] init];
                    
                    
                    //NSString *catexyzgory=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                    
                    
                    //NSLog(@"catexyzgory-->%@",catexyzgory);
                }
                tItem.Item_id=curItemID;
                UOMAndConversion *tUom = [[UOMAndConversion alloc] init];
                
                int itemID=sqlite3_column_int(statement, 0);
                tItem.Item_id=itemID;
                NSString *category=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                tItem.Category = category;
                
                [itemarr addObject:tItem];
                prevItem=tItem;
                prevItemID=curItemID;
                [tItem release];
                [tUom release];
                
                //NSLog(@"tItem.Item_No-->%@",tItem.Item_No);
                //NSLog(@"tItem.Category-->%@",tItem.Category);
            }
            else
            {
                UOMAndConversion* tUom=[[UOMAndConversion alloc] init];
                
                
                [prevItem.UOMAndConversion addObject:tUom];
                [tUom release];
            }
        }
        for(Item* item in itemarr)
        {
            if(item.Default_Price==nil)
            {
                NSMutableArray *tDefaultPriceArr=[[NSMutableArray alloc] init];
                item.Default_Price=tDefaultPriceArr;
                [tDefaultPriceArr release];
            }
            [self addDefaultPriceToItem:item];
        }
        [self closeConnection];
    }
    return itemarr;
}

-(NSArray*)selectremoveItemBlind
{ 
    NSMutableArray* itemarr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        itemarr=[[[NSMutableArray alloc] init] autorelease];
        NSString *selectSQL=[NSString stringWithFormat:@"select item_id, category from Item where category ='BLIND' OR category ='BLINDA' "];
        //NSString *selectSQL=[NSString stringWithFormat:@"select item_id, category from Item where NOT category ='BLIND' AND NOT category ='BLINDA' "];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        Item *prevItem=nil;
        int prevItemID=-1;
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            int curItemID=sqlite3_column_int(statement, 0);
            if(prevItemID!=curItemID)
            {
                Item *tItem;
                NSString* itemCategory = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                if((![itemCategory isEqualToString:kBlindCategory]) && (![itemCategory isEqualToString:kblindACategoryA]))
                {
                    tItem = [[Item alloc] init];
                    
                    
                }
                else
                {
                    tItem = [[BlindItem alloc] init];

                    
                    //NSString *catexyzgory=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                    
                    
                    //NSLog(@"catexyzgory-->%@",catexyzgory);
                }
                tItem.Item_id=curItemID;
                UOMAndConversion *tUom = [[UOMAndConversion alloc] init];
                
                int itemID=sqlite3_column_int(statement, 0);
                tItem.Item_id=itemID;
                NSString *category=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                tItem.Category = category;

                [itemarr addObject:tItem];
                prevItem=tItem;
                prevItemID=curItemID;
                [tItem release];
                [tUom release];
                
                //NSLog(@"tItem.Item_No-->%@",tItem.Item_No);
                //NSLog(@"tItem.Category-->%@",tItem.Category);
            }
            else 
            {
                UOMAndConversion* tUom=[[UOMAndConversion alloc] init];


                [prevItem.UOMAndConversion addObject:tUom];
                [tUom release];
            }
        }
        for(Item* item in itemarr)
        {
            if(item.Default_Price==nil)
            {
                NSMutableArray *tDefaultPriceArr=[[NSMutableArray alloc] init];
                item.Default_Price=tDefaultPriceArr;
                [tDefaultPriceArr release];
            }
            [self addDefaultPriceToItem:item];
        }
        [self closeConnection];
    }
    return itemarr;
}

-(NSArray*)selectItem
{
    NSMutableArray* itemarr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        itemarr=[[[NSMutableArray alloc] init] autorelease];
        NSString *selectSQL=[NSString stringWithFormat:@"select item.item_id,item.category,item.description,item.fmt_item_no,item.image_path,item.item_no,item.stock_unit, UOMAndConversion.conversion,UOMAndConversion.uom,item.min_height,item.min_sqft from Item,UOMAndConversion where item.item_id=UOMAndConversion.item_id"];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        Item *prevItem=nil;
        int prevItemID=-1;
        while(sqlite3_step(statement) == SQLITE_ROW)
        { 
            int curItemID=sqlite3_column_int(statement, 0);
            if(prevItemID!=curItemID)
            {
                Item *tItem;
                NSString* itemCategory = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                if((![itemCategory isEqualToString:kBlindCategory]) && (![itemCategory isEqualToString:kblindACategoryA]))
                {
                    tItem = [[Item alloc] init];
                    
                    
                }
                else
                {
                    tItem = [[BlindItem alloc] init];
                    ((BlindItem*)tItem).min_Height = sqlite3_column_double(statement, 9);
                    ((BlindItem*)tItem).min_SqFt = sqlite3_column_double(statement, 10);
                    
                    //NSString *catexyzgory=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                    
                    
                    //NSLog(@"catexyzgory-->%@",catexyzgory);
                }
                tItem.Item_id=curItemID;
                UOMAndConversion *tUom = [[UOMAndConversion alloc] init];
                
                int itemID=sqlite3_column_int(statement, 0);
                tItem.Item_id=itemID;
                NSString *category=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                tItem.Category = category;
                NSString *description=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
                NSString *descriptiono=[description stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                tItem.Description = descriptiono;
                NSString *fmt_item_no=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
                NSString *fmt_item_noo=[fmt_item_no stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                tItem.FMT_Item_No = fmt_item_noo;
                NSString *image_path=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 4)];
                tItem.Image_Path = image_path;
                NSString *item_no=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 5)];
                NSString *item_noo=[item_no stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                tItem.Item_No = item_noo;
                NSString *stock_unit=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)];
                tItem.Stock_Unit = stock_unit;
                
                NSString *conversion=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
                tUom.Converstion = conversion;
                NSString *uom=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
                tUom.Uom = uom;
                
                NSMutableArray *tUOMArr=[[NSMutableArray alloc] init];
                [tUOMArr addObject:tUom];
                tItem.UOMAndConversion=tUOMArr;
                [tUOMArr release];
                [itemarr addObject:tItem];
                prevItem=tItem;
                prevItemID=curItemID;
                [tItem release];
                [tUom release];
                
                //NSLog(@"tItem.Item_No-->%@",tItem.Item_No);
                //NSLog(@"tItem.Category-->%@",tItem.Category);
            }
            else
            {
                UOMAndConversion* tUom=[[UOMAndConversion alloc] init];
                NSString *conversion=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
                tUom.Converstion = conversion;
                NSString *uom=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
                tUom.Uom = uom;
                [prevItem.UOMAndConversion addObject:tUom];
                [tUom release];
            }
        }
        for(Item* item in itemarr)
        {
            if(item.Default_Price==nil)
            {
                NSMutableArray *tDefaultPriceArr=[[NSMutableArray alloc] init];
                item.Default_Price=tDefaultPriceArr;
                [tDefaultPriceArr release];
            }
            [self addDefaultPriceToItem:item];
        }
        [self closeConnection];
    }
    return itemarr;
}


-(NSArray*)selectLastSellingPriceForItem:(Item*)item andCustomer:(Customer*)customer andSalesperson:(SalesPerson*)salesperson
{
    NSMutableArray* lastSellingPriceArr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        lastSellingPriceArr=[[[NSMutableArray alloc] init] autorelease];
        NSString *selectSQL=[NSString stringWithFormat:@"select last_selling_price,last_selling_price_date,last_selling_price_uom,min_height,min_sqft from LastSellingPrice where salesperson_code='%@' and customer_id='%@' and item_no='%@' group by last_selling_price_uom", salesperson.SalesPerson_Code,customer.Customer_Id,item.Item_No];
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            LastSellingPrice *lastSellingPrice=[[LastSellingPrice alloc] init];
            lastSellingPrice.Price=(float)sqlite3_column_double(statement, 0);
            NSString *priceDateStr=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
            NSDateFormatter* formatter=[[NSDateFormatter alloc] init];
            //[formatter setDateFormat:@"YYYY/MM/dd hh:mm:ss"];
            [formatter setDateFormat:@"yyyyMMdd"];
            NSDate *priceDate=[formatter dateFromString:priceDateStr];
            [formatter release];
            lastSellingPrice.Date = priceDate;
            NSString *uom=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            lastSellingPrice.UOM = uom;
            lastSellingPrice.Min_Hight=(float)sqlite3_column_double(statement, 3);
            lastSellingPrice.Min_SQFT=(float)sqlite3_column_double(statement, 4);
            [lastSellingPriceArr addObject:lastSellingPrice];
            [lastSellingPrice release];
        }
        [self closeConnection];
    }
    return lastSellingPriceArr;
}


-(NSArray*)selectEditItem:(NSString *)editItem
{
    NSMutableArray* itemarr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        itemarr=[[[NSMutableArray alloc] init] autorelease];
        NSString *selectSQL=[NSString stringWithFormat:@"select item.item_id,item.category,item.description,item.fmt_item_no,item.image_path,item.item_no,item.stock_unit, UOMAndConversion.conversion,UOMAndConversion.uom,item.min_height,item.min_sqft from Item,UOMAndConversion where item.item_id=UOMAndConversion.item_id and item.item_no=%@", editItem];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        //NSLog(@"%@",selectSQL);
        //NSLog(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
         
        
        Item *prevItem=nil;
        //int prevItemID=-1;
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            int curItemID=sqlite3_column_int(statement, 0);
            if(curItemID)
            {
                Item *tItem;
                NSString* itemCategory = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                if((![itemCategory isEqualToString:kBlindCategory]) || (![itemCategory isEqualToString:kblindACategoryA])) 
                {
                    tItem = [[Item alloc] init];
                    
                    NSLog(@"last sellingtItem.Item_No-->%@",tItem.Item_No);
                    NSLog(@"last selling tItem.Category-->%@",tItem.Category);
                }
                else
                {
                    tItem = [[BlindItem alloc] init];
                    ((BlindItem*)tItem).min_Height = sqlite3_column_double(statement, 9);
                    ((BlindItem*)tItem).min_SqFt = sqlite3_column_double(statement, 10);
                }
                //tItem.Item_id=curItemID;
                UOMAndConversion *tUom = [[UOMAndConversion alloc] init];
                
                int itemID=sqlite3_column_int(statement, 0);
                tItem.Item_id=itemID;
                NSString *category=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                tItem.Category = category;
                NSString *description=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
                NSString *descriptiono=[description stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                tItem.Description = descriptiono;
                NSString *fmt_item_no=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
                NSString *fmt_item_noo=[fmt_item_no stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                tItem.FMT_Item_No = fmt_item_noo;
                NSString *image_path=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 4)];
                tItem.Image_Path = image_path;
                NSString *item_no=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 5)];
                tItem.Item_No = item_no;
                NSString *stock_unit=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)];
                tItem.Stock_Unit = stock_unit;
                
                NSString *conversion=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
                tUom.Converstion = conversion;
                NSString *uom=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
                tUom.Uom = uom;
                
            
                NSMutableArray *tUOMArr=[[NSMutableArray alloc] init];
                [tUOMArr addObject:tUom];
                tItem.UOMAndConversion=tUOMArr;
                [tUOMArr release];
                [itemarr addObject:tItem];
                
                [tItem release];
                [tUom release];
            }
            /**/else
            {
                UOMAndConversion* tUom=[[UOMAndConversion alloc] init];
                NSString *conversion=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
                tUom.Converstion = conversion;
                NSString *uom=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
                tUom.Uom = uom;
                [prevItem.UOMAndConversion addObject:tUom];
                [tUom release];
            }
        }
        for(Item* item in itemarr)
        {
            if(item.Default_Price==nil)
            {
                NSMutableArray *tDefaultPriceArr=[[NSMutableArray alloc] init];
                item.Default_Price=tDefaultPriceArr;
                [tDefaultPriceArr release];
            }
            [self addDefaultPriceToItem:item];
        }
        [self closeConnection];
    }
    return itemarr;
}

-(void)addDefaultPriceToItem:(Item *)tItem
{
    NSString *selectSQL=[NSString stringWithFormat:@"select DefaultPriceAndUOM.default_price,DefaultPriceAndUOM.uom from DefaultPriceAndUOM where DefaultPriceAndUOM.item_id=%d", tItem.Item_id];
    
    sqlite3_stmt *statement;
    const char *select_stmt = [selectSQL UTF8String];
    sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
    
    while(sqlite3_step(statement) == SQLITE_ROW)
    {
        DefaultPriceAndUOM *defaultPrice=[[DefaultPriceAndUOM alloc] init];
        NSString *default_price=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 0)];
        defaultPrice.Default_Price = default_price;
        NSString *uom=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
        defaultPrice.Uom = uom;
        [tItem.Default_Price addObject:defaultPrice];
        [defaultPrice release];
    }
}

-(NSArray*)selectItemUOM:(Item*)item
{ 
    NSMutableArray* itemarr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        itemarr=[[[NSMutableArray alloc] init] autorelease];
          
        NSString *selectSQL=[NSString stringWithFormat:@"select item.item_id,item.category,item.description,item.fmt_item_no,item.image_path,item.item_no,item.stock_unit, UOMAndConversion.conversion,UOMAndConversion.uom,item.min_height,item.min_sqft from Item LEFT JOIN UOMAndConversion ON item.item_id=UOMAndConversion.item_id WHERE item.item_id='%d'", item.Item_id];
        
          
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        Item *prevItem=nil;
        int prevItemID=-1;
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            int curItemID=sqlite3_column_int(statement, 0);
            if(prevItemID!=curItemID)
            {
                Item *tItem;
                NSString* itemCategory = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                if((![itemCategory isEqualToString:kBlindCategory]) && (![itemCategory isEqualToString:kblindACategoryA]))
                {
                    tItem = [[Item alloc] init];
                    NSLog(@" uom tItem.Item_No-->%@",tItem.Item_No);
                    NSLog(@"uom tItem.Category-->%@",tItem.Category);
                }
                else
                {
                    tItem = [[BlindItem alloc] init];
                    ((BlindItem*)tItem).min_Height = sqlite3_column_double(statement, 9);
                    ((BlindItem*)tItem).min_SqFt = sqlite3_column_double(statement, 10);
                }
                tItem.Item_id=curItemID;
                UOMAndConversion *tUom = [[UOMAndConversion alloc] init];
                
                int itemID=sqlite3_column_int(statement, 0);
                tItem.Item_id=itemID;
                NSString *category=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                tItem.Category = category;
                NSString *description=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
                NSString *descriptiono=[description stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                tItem.Description = descriptiono;
                NSString *fmt_item_no=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
                NSString *fmt_item_noo=[fmt_item_no stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                tItem.FMT_Item_No = fmt_item_noo;
                NSString *image_path=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 4)];
                tItem.Image_Path = image_path;
                NSString *item_no=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 5)];
                NSString *item_noo=[item_no stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                tItem.Item_No = item_noo
                ;
                NSString *stock_unit=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)];
                tItem.Stock_Unit = stock_unit;
                
                NSString *conversion=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
                tUom.Converstion = conversion;
                NSString *uom=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
                tUom.Uom = uom;
                
                NSMutableArray *tUOMArr=[[NSMutableArray alloc] init];
                [tUOMArr addObject:tUom];
                tItem.UOMAndConversion=tUOMArr;
                [tUOMArr release];
                [itemarr addObject:tItem];
                prevItem=tItem;
                prevItemID=curItemID;
                [tItem release];
                [tUom release];
            }
            else
            { 
                UOMAndConversion* tUom=[[UOMAndConversion alloc] init];
                NSString *conversion=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
                tUom.Converstion = conversion;
                NSString *uom=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
                tUom.Uom = uom;
                [prevItem.UOMAndConversion addObject:tUom];
                [tUom release];
            }
        }
        for(Item* item in itemarr)
        {
            if(item.Default_Price==nil)
            {
                NSMutableArray *tDefaultPriceArr=[[NSMutableArray alloc] init];
                item.Default_Price=tDefaultPriceArr;
                [tDefaultPriceArr release];
            }
            [self addDefaultPriceToItem:item];
        }
        [self closeConnection];
    }
    return itemarr;
}

@end
