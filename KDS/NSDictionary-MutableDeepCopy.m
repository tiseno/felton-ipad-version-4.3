

#import "NSDictionary-MutableDeepCopy.h"

@implementation NSDictionary (MutableDeepCopy)
 
-(NSMutableDictionary*) MutableDeepCopy
{
    NSMutableDictionary *ret = [[[NSMutableDictionary alloc] initWithCapacity:[self count]]autorelease];
    NSArray *keys = [[NSArray alloc]initWithArray:[self allKeys]];
    //keys = [self allKeys];
    
    for(id key in keys)
    {
        id oneValue = [self valueForKey:key];
        id oneCopy = nil;
        
        if([oneValue respondsToSelector:@selector(MutableDeepCopy)])
        {
            oneCopy = [oneValue MutableDeepCopy];
        }
        else if ([oneValue respondsToSelector:@selector(mutableCopy)])
        {
            oneCopy = [[oneValue mutableCopy]autorelease];
        }else if(oneCopy == nil)
        {
            oneCopy = [[oneValue copy]autorelease];
        }
        [ret setValue:oneCopy forKey:key];
    }
    [keys release];
    return ret; 
}

@end