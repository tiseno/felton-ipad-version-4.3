//
//  UIButtonWithImageFromURL.h
//  Holiday Villa
//
//  Created by Jermin Bazazian on 5/18/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface UIButtonWithImageFromURL : UIButton<AsyncImageViewDelegate> {
    
}
@end
