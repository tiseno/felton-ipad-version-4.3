//
//  BlindSalesOrder.h
//  KDS
//
//  Created by Jermin Bazazian on 12/8/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SalesOrder.h"


@interface BlindSalesOrder : SalesOrder {
    Address* _Installation_Address;
    NSString* _Installation_Phone;
    NSString* _Installation_Fax;
    NSString* _Installation_Email;
    NSString* _Installation_Name;
    float installation;
    float transportation;
    NSString *_Installation_City;
    NSString *_Installation_State;
    NSString *_Installationorder_Address;
}
@property (nonatomic, retain) NSString *Installation_Email,*Installation_Phone,*Installation_Fax,*Installation_Name;
@property (nonatomic, retain) Address* Installation_Address;
@property (nonatomic,retain) NSString *Installation_City, *Installation_State, *Installationorder_Address;
@property (nonatomic) float installation;
@property (nonatomic) float transportation;
@end
