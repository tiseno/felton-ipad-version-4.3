//
//  TransferedOrderTableViewCell.m
//  KDS
//
//  Created by Tiseno on 11/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TransferedOrderTableViewCell.h"
#define cell1Width 91
#define cell2Width 80
#define cell3Width 480
#define cell4Width 195
#define cellHeight 45
@implementation TransferedOrderTableViewCell

@synthesize lineColor, topCell,salesOrder;
@synthesize deleteButton, pdfButton, delegate,editButton;
@synthesize nameLabel, priceLabel, codeLabel, isRemoved;
@synthesize lblTransferredDate,lblCreatedDate;
@synthesize PackageIDLabel, pdfquoButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        topCell = NO;
        isRemoved =NO;
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 846, 183);
        
        /**/ 
        
        UIImage *editButtonImage = [UIImage imageNamed:@"btn_light_blue.png"];
        UIButton *teditButton = [[UIButton alloc] initWithFrame:CGRectMake(20, (cellHeight/2)-(editButtonImage.size.height/2), 51, 30)];
        [teditButton setTitle:@"View"  forState:UIControlStateNormal ];
        teditButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [teditButton setBackgroundImage:editButtonImage forState:UIControlStateNormal];
        [teditButton addTarget:self action:@selector(edit_ButtonTapped:)
              forControlEvents:UIControlEventTouchDown];
        self.editButton = teditButton;
        [teditButton release];
        [self addSubview:editButton];
        
        UIImage *pdfButtonImage = [UIImage imageNamed:@"btn_po.png"];
        UIButton *tPdfButton = [[UIButton alloc] initWithFrame:CGRectMake(100, (cellHeight/2)-(pdfButtonImage.size.height/2), 35, 35)];
        [tPdfButton setBackgroundImage:pdfButtonImage forState:UIControlStateNormal];
        [tPdfButton addTarget:self action:@selector(pdf_ButtonTapped:) 
             forControlEvents:UIControlEventTouchDown];
        self.pdfButton = tPdfButton;
        [tPdfButton release];
        [self addSubview:pdfButton];
        
        UIImage *deleteButtonImage = [UIImage imageNamed:@"btn_delete_2.png"];
        UIButton *tDeleteButton = [[UIButton alloc] initWithFrame:CGRectMake(145, (cellHeight/2)-(deleteButtonImage.size.height/2), 30, 30)];
        [tDeleteButton setBackgroundImage:deleteButtonImage forState:UIControlStateNormal];
        [tDeleteButton addTarget:self action:@selector(delete_ButtonTapped:)
                forControlEvents:UIControlEventTouchDown];
        self.deleteButton = tDeleteButton;
        [tDeleteButton release];
        [self addSubview:deleteButton];   
        
        UIImage *pdfquoButtonImage = [UIImage imageNamed:@"btn_quo.png"];
        UIButton *tPdfquoButton = [[UIButton alloc] initWithFrame:CGRectMake(520, (cellHeight/2)-(pdfButtonImage.size.height/2), 35, 35)];
        [tPdfquoButton setBackgroundImage:pdfquoButtonImage forState:UIControlStateNormal];
        [tPdfquoButton addTarget:self action:@selector(pdfQUO_ButtonTapped:)
                forControlEvents:UIControlEventTouchDown];
        self.pdfquoButton = tPdfquoButton;
        [tPdfquoButton release];
        [self addSubview:pdfquoButton];
        
        UILabel *tNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, (cellHeight/2)-cellHeight/2, 300, cellHeight)];
        //tNameLabel.text = @"Name here";
        tNameLabel.textAlignment = UITextAlignmentLeft;
        tNameLabel.backgroundColor = [UIColor clearColor];
        tNameLabel.adjustsFontSizeToFitWidth = YES;
        tNameLabel.textColor = [UIColor blackColor];
        tNameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
        self.nameLabel = tNameLabel;
        [tNameLabel release];
        [self addSubview:nameLabel];
        
        UILabel *tTransferredDate = [[UILabel alloc] initWithFrame:CGRectMake(580, 22, 150,21)];
        //tTransferredDate.text = @"Name here";
        tTransferredDate.textAlignment = UITextAlignmentLeft;
        tTransferredDate.backgroundColor = [UIColor clearColor];
        tTransferredDate.textColor = [UIColor blackColor];
        tTransferredDate.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
        self.lblTransferredDate = tTransferredDate;
        [tTransferredDate release];
        [self addSubview:lblTransferredDate];
        
        UILabel *tcreatedDate = [[UILabel alloc] initWithFrame:CGRectMake(580, 4, 150,21)];
        tcreatedDate.textAlignment = UITextAlignmentLeft;
        tcreatedDate.backgroundColor = [UIColor clearColor];
        tcreatedDate.textColor = [UIColor blackColor];
        tcreatedDate.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
        self.lblCreatedDate = tcreatedDate;
        [tcreatedDate release]; 
        [self addSubview:lblCreatedDate];
         
        UILabel *tPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(734, 4, cell4Width, 21)];
        tPriceLabel.textAlignment = UITextAlignmentLeft;
        tPriceLabel.backgroundColor = [UIColor clearColor];
        tPriceLabel.textColor = [UIColor blackColor];
        self.priceLabel = tPriceLabel;
        [tPriceLabel release];
        [self addSubview:priceLabel];
        
        UILabel *tPackageIDLabel = [[UILabel alloc] initWithFrame:CGRectMake(734, 22, cell4Width, 21)];
        tPackageIDLabel.textAlignment = UITextAlignmentLeft;
        tPackageIDLabel.backgroundColor = [UIColor clearColor];
        tPackageIDLabel.textColor = [UIColor blueColor];
        tPackageIDLabel.font=[UIFont fontWithName:@"Helvetica-Bold" size:12];
        self.PackageIDLabel = tPackageIDLabel;
        [tPackageIDLabel release];
        [self addSubview:PackageIDLabel];
        
        
    }
    return self;
}

-(IBAction)edit_ButtonTapped:(id)sender
{
    //NSLog(@"hello~"); 
    
    /**/KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
    NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:self.salesOrder];
    [dataSalesOrderItem release];
    if([self.salesOrder NumberOfOderItems]==0)
    {
        for(SalesOrderItem* orderItem in orderItemArr)
        {
            [self.salesOrder addOrderItem:orderItem];
        }
    } 
    
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    appDelegate.TransferOrdersalesOrder=self.salesOrder;
    appDelegate.OrderTransfercurrentCustomer=self.salesOrder.customer;
    [self.delegate editDeleteSaleOrderForEditButton];  
}

-(IBAction)pdf_ButtonTapped:(id)sender
{
    KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:self.salesOrder];
    
    [dataSalesOrderItem release];
    
    if([self.salesOrder NumberOfOderItems]==0)
    {
        for(SalesOrderItem* orderItem in orderItemArr)
        {
            [self.salesOrder addOrderItem:orderItem];
        }
        
        //NSLog(@"pdf tapped!!");
        
        appDelegate.PdfEmailOrdersalesOrder=self.salesOrder;
        appDelegate.PDFTransfercurrentCustomer=self.salesOrder.customer;
        
        //NSLog(@"%d", self.salesOrder.Order_id);
        
        [self.delegate editTransferredSaleOrderPDFButton:(SalesOrder*)salesOrder SalesPersonCode:appDelegate.loggedInSalesPerson];
        
    }
    else
    {
        appDelegate.PdfEmailOrdersalesOrder=self.salesOrder;
        appDelegate.PDFTransfercurrentCustomer=self.salesOrder.customer;
        
        NSLog(@"FULL--->%d", self.salesOrder.Order_id);
        
        [self.delegate editTransferredSaleOrderPDFButton:(SalesOrder*)salesOrder SalesPersonCode:appDelegate.loggedInSalesPerson];
    }
    
}

-(IBAction)pdfQUO_ButtonTapped:(id)sender
{
    KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:self.salesOrder];
    
    [dataSalesOrderItem release];
    
    if([self.salesOrder NumberOfOderItems]==0)
    {
        for(SalesOrderItem* orderItem in orderItemArr)
        {
            [self.salesOrder addOrderItem:orderItem];
        }
        
        //NSLog(@"pdf tapped!!");
        
        appDelegate.PdfEmailOrdersalesOrder=self.salesOrder;
        appDelegate.PDFTransfercurrentCustomer=self.salesOrder.customer;
        
        //NSLog(@"%d", self.salesOrder.Order_id);
        
        [self.delegate editTransferredSaleOrderPDFQuoButton:(SalesOrder*)salesOrder SalesPersonCode:appDelegate.loggedInSalesPerson];
        
    }
    else
    {
        appDelegate.PdfEmailOrdersalesOrder=self.salesOrder;
        appDelegate.PDFTransfercurrentCustomer=self.salesOrder.customer;
        
        NSLog(@"FULL--->%d", self.salesOrder.Order_id);
        
        [self.delegate editTransferredSaleOrderPDFQuoButton:(SalesOrder*)salesOrder SalesPersonCode:appDelegate.loggedInSalesPerson];
    }
    
}

-(IBAction)delete_ButtonTapped:(id)sender
{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingAlertTag;
    [alert show];
    [alert release];
    
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == kConfirmingAlertTag)
    {
        if(buttonIndex == 1)
        {
            KDSDataSalesOrder* dataSaleOrder=[[KDSDataSalesOrder alloc] init];
            self.salesOrder.isDeleted=YES;
            [dataSaleOrder updateSaleOrderIsDeleted:salesOrder];
            [dataSaleOrder release];
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"The order has been deleted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=kConfirmedAlertTag;
            [alert show];
            [alert release];
            [delegate reloadTransferedTable];
        }
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
    [pdfquoButton release];
    [PackageIDLabel release];
    [lblCreatedDate release];
    [lblTransferredDate release];
    [salesOrder release];
    [lineColor release];
    [deleteButton release];
    [pdfButton release];
    [nameLabel release];
    [priceLabel release];
    [delegate release];
    [codeLabel release];
}
-(void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context, lineColor.CGColor); 
    
    // CGContextSetLineWidth: The default line width is 1 unit. When stroked, the line straddles the path, with half of the total width on either side.
	// Therefore, a 1 pixel vertical line will not draw crisply unless it is offest by 0.5. This problem does not seem to affect horizontal lines.
	CGContextSetLineWidth(context, 1.0);
    
	// Add the vertical lines
	/*CGContextMoveToPoint(context, cell1Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+0.5, rect.size.height);
    
	CGContextMoveToPoint(context, cell1Width+cell2Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+0.5, rect.size.height);
    
    CGContextMoveToPoint(context, cell1Width+cell2Width+cell3Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+cell3Width+0.5, rect.size.height);
    
    //CGContextMoveToPoint(context, cell1Width+cell2Width+cell3Width+cell4Width+0.5, 0);
     //CGContextAddLineToPoint(context, cell1Width+cell2Width+cell3Width+cell4Width+0.5, rect.size.height);
    
    
	// Add bottom line
	CGContextMoveToPoint(context, 0, rect.size.height);
	CGContextAddLineToPoint(context, rect.size.width, rect.size.height-0.5);
	
	// If this is the topmost cell in the table, draw the line on top
	if (topCell)
	{
		CGContextMoveToPoint(context, 0, 0);
		CGContextAddLineToPoint(context, rect.size.width, 0);
	}*/
	
	// Draw the lines
	CGContextStrokePath(context); 
}
-(void)setTopCell:(BOOL)newTopCell
{
    topCell = newTopCell;
}


@end
