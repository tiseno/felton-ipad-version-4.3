//
//  CustomersBySalespersonIDResponse.h
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/6/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"


@interface CustomersBySalespersonIDResponse : XMLResponse {
    NSArray *customers;
}
@property (nonatomic, retain) NSArray *customers;
@end
