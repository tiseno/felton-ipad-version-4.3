//
//  AsyncImageViewDelegate.h
//  KDS
//
//  Created by Jermin Bazazian on 12/15/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AsyncImageView;
@protocol AsyncImageViewDelegate <NSObject>
-(void)handleRecieveImage:(UIImage*)image sender:(AsyncImageView*)sender;
@end
