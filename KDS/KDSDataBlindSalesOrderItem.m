//
//  KDSDataBlindSalesOrderItem.m
//  KDS
//
//  Created by Tiseno on 12/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KDSDataBlindSalesOrderItem.h"

 
@implementation KDSDataBlindSalesOrderItem
 
-(NSString*)generateInsertQueryForSaleOrderItem:(SalesOrderItem*)salesOrederItem AndSalesOrderID:(int)salesOrderID
{
    BlindSalesOrderItem* blindSalesOrderItem = (BlindSalesOrderItem*)salesOrederItem;
    NSString *salesOrederItemComment=[salesOrederItem.Comment stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *Description=[salesOrederItem.item.Description stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *Item_No=[salesOrederItem.item.Item_No stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *SpecialItemDescription=[salesOrederItem.SpecialItemDescription stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *Control=[blindSalesOrderItem.Control stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *Color=[blindSalesOrderItem.Color stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    
      
    NSString *insertSQL = [NSString stringWithFormat:@"insert into SalesOrderItem(salesorder_id,item_no,item_description,quantity,unit_price,order_uom,discount_percent,discount_amount,comment,width,height,quantity_in_set,control,color,SpecialItemDesc) values(%d,'%@','%@',%f,%f,'%@',%f,%f,'%@',%f,%f,%f,'%@','%@','%@');", salesOrderID,Item_No,Description,salesOrederItem.Quantity,salesOrederItem.Unit_Price,salesOrederItem.Order_UOM,salesOrederItem.Discount_Percent,salesOrederItem.Discount_Amount,salesOrederItemComment,blindSalesOrderItem.Width,blindSalesOrderItem.Height,blindSalesOrderItem.Quantity_in_Set,Control,Color,SpecialItemDescription];
    return insertSQL;
}
@end
