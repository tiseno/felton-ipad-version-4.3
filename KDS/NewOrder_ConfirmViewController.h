//
//  NewOrder_ConfirmViewController.h
//  KDS
//
//  Created by Tiseno on 11/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YourCartViewController.h"
#import "KDSAppDelegate.h"
#import "UOMAndConversion.h"
#import "SalesOrder.h"
#import "BlindSalesOrder.h"
#import "KDSDataItem.h"
#import "UIImageViewWithImageFromURL.h"
#import "LoginViewController.h"
#import "SalesOrderType.h"
#import "BlindItem.h"
#import "NewOrderViewController.h"

#define kHeightTextField 0
#define kWidthTextField 1
#define kQuantityTextField 2
#define kPriceTextFeield 3

#define MAXLENGTHtwelve 12
#define MAXLENGTHfive 10
#define MAXLENGTHfif 15
#define MAXLENGTHsix 10
#define MAXLENGTHfifty 60
#define MAXLENGTHCOLORCONTROL 25

@interface NewOrder_ConfirmViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate> {
    
    UILabel *customerNameLabel;
    UILabel *companyNameLabel;
    UILabel *telLabel;
    UILabel *faxLabel;
    UILabel *emailLabel;
    UILabel *totalLengthLabel;
    UILabel *standardPriceLabel;
    UILabel *productNameLabel;
    UILabel *descriptionLabel;
    UILabel *measureLabel;
    UILabel *typesOfmeasurementsLabel;
    UILabel *lblSelectedUOM;
    UILabel *quantityOrderedLabel;
    UITextField *discountTextField;
    UILabel *formulaLabel;
    UIButton *changeDiscountTypeButton;
    
    UITextField *priceTextField;
    UITextField *heightTextField;
    UITextField *widthTextField;
    UITextField *controlTextField;
    UITextField *colorTextField;
    UITextField *quantityTextField;
    UILabel *discountTypeLabel;
    
    UITextView *remarkTextView;
    
    UIButton* changeMeasurementButton;
    //UIPickerView *typesOfmeasurementPickerView;
    
    BOOL isPickerHidden;
    BOOL isOrderMixed;
    
    Customer *currentSelectedCustomer;
    Item* currentSelectedItem;
    Item* SelectedItemUpdate;
    BlindItem* currentSelectedBlindItem;
    
    NSArray *measurementArray;
    int heightOfEditedView;
    int heightOffset;
    BOOL isDiscountAmount;
    BOOL isOverAmount;
    
    UIImageView *backGroundImageView;
    UIImageViewWithImageFromURL *productImageView;
    float currentConversionRate;
    float defaultConversionRate;
    float currentPrice;
    NSString *currentUOM;
    float totalLength;
    //NSString *UpDatetag;
    float totalDiscountOfItems;
    NSString *currentcatItem;
    UIView *tableViewHeader;
    UISegmentedControl *orderTypeSegmentedControl;
    UITableView *customerDetailtable;
}

@property (retain, nonatomic) IBOutlet UITableView *customerDetailtable;
@property (nonatomic) float totalDiscountOfItems;
@property (nonatomic) float totalLength;
@property (nonatomic, retain) BlindItem* currentSelectedBlindItem;
@property (nonatomic) BOOL isPickerHidden;
@property (nonatomic) BOOL isOrderMixed;
@property (nonatomic) BOOL isOverAmount;
@property (nonatomic, retain) IBOutlet UILabel *customerNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *quantityOrderedLabel;
@property (nonatomic, retain) IBOutlet UILabel *companyNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *telLabel;
@property (nonatomic, retain) IBOutlet UILabel *formulaLabel;
@property (nonatomic, retain) IBOutlet UILabel *faxLabel;
@property (nonatomic, retain) IBOutlet UILabel *emailLabel;
@property (nonatomic, retain) IBOutlet UILabel *totalLengthLabel;
@property (nonatomic, retain) IBOutlet UILabel *standardPriceLabel;
@property (nonatomic, retain) IBOutlet UILabel *productNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain) IBOutlet UILabel *typesOfmeasurementsLabel;
@property (nonatomic, retain) IBOutlet UILabel *lblSelectedUOM;

@property (nonatomic, retain) IBOutlet UITextField *priceTextField;
@property (nonatomic, retain) IBOutlet UITextField *heightTextField;
@property (nonatomic, retain) IBOutlet UITextField *widthTextField;
@property (nonatomic, retain) IBOutlet UITextField *controlTextField;
@property (nonatomic, retain) IBOutlet UITextField *colorTextField;
@property (nonatomic, retain) IBOutlet UITextField *quantityTextField;
@property (nonatomic, retain) IBOutlet UITextView *remarkTextView;

@property (nonatomic, retain) IBOutlet UISegmentedControl *orderTypeSegmentedControl;

@property (nonatomic, retain) IBOutlet UIPickerView *typesOfmeasurementPickerView;
@property (nonatomic, retain) IBOutlet UIButton *changeMeasurementButton;

@property (nonatomic, retain) NSArray *measurementArray;

@property (nonatomic, retain) IBOutlet UIImageView *backGroundImageView;
@property (nonatomic, retain) UIImageViewWithImageFromURL *productImageView;
@property (nonatomic, retain) Customer *currentSelectedCustomer;
@property (nonatomic, retain) Item* currentSelectedItem;
//@property (nonatomic,retain) NSString *UpDatetag;
@property (nonatomic, retain) Item* SelectedItemUpdate;
-(void)setCurrentUOM:(NSString*)iCurrentUOM;
-(void)hidePicker;
-(void)initializeVariables;
-(void)initializeItem;
-(void)initializeProductImage;
//-(IBAction)changeDiscountType_ButtonTapped:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *discountTextField;
@property (retain, nonatomic) IBOutlet UILabel *discountTypeLabel;
@property (retain, nonatomic) IBOutlet UIButton *changeDiscountTypeButton;
@property (retain,nonatomic) NSString *currentcatItem;

@property (retain, nonatomic) IBOutlet UILabel *lblcolor;
@property (retain, nonatomic) IBOutlet UILabel *lblcontrol;
@property (retain, nonatomic) IBOutlet UILabel *lblheight;
@property (retain, nonatomic) IBOutlet UILabel *lblwidth;
@property (retain, nonatomic) IBOutlet UILabel *lblX;
@property (retain, nonatomic) IBOutlet UILabel *lblDescription;
@property (retain, nonatomic) IBOutlet UILabel *lblequel;
@property (nonatomic, retain) UIView *tableViewHeader;

@property (retain, nonatomic) IBOutlet UITextView *blindremarktextview;
@property (retain, nonatomic) IBOutlet UILabel *lblitemdesc;
@property (retain, nonatomic) IBOutlet UITextView *blinditemDesctextView;







@end








