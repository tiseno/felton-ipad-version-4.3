//
//  SalesOrderItem.h
//  KDS
//
//  Created by Jermin Bazazian on 12/8/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SalesOrderType.h"
#import "Item.h"
#import "GDataXMLNode.h"
#import "BlindItem.h"


@interface SalesOrderItem : NSObject {
    Item* item;
    float _Quantity;
    float _Unit_Price;
     NSString* _Order_UOM;
    float _Discount_Percent;
    float _Discount_Amount;
    NSString* _Comment; 
    float TotalPrice;
     NSString* SpecialItemDescription;
    BlindItem* blinditems;
   
}
@property (nonatomic, retain) NSString* SpecialItemDescription;
@property (nonatomic, retain) BlindItem* blinditems;
@property (nonatomic, retain) Item* item;
@property (nonatomic) float Quantity,Unit_Price,Discount_Percent,Discount_Amount;
@property (nonatomic) float TotalPrice;
@property (nonatomic, retain) NSString* Comment,*Order_UOM;
-(void)addInheritanceDetails:(GDataXMLElement*)salesOrderItemNode;
-(void)addInheritancePDFDetails:(GDataXMLElement*)salesOrderItemNode;
@end
