//
//  OrderPackagingRequest.m
//  KDS
//
//  Created by Tiseno Mac 2 on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrderPackagingRequest.h"

@implementation OrderPackagingRequest
//@synthesize ItemType;
@synthesize SalespersonID,datestart,dateend;


/*-(id)initWithItemTypes:(NSString*)iItemType;
{
    self=[super init];
    if(self)
    {
        self.webserviceURL=@"http://219.94.43.102/KDSItemImage/service.asmx";
        self.SOAPAction=@"getItemImage";
        self.requestType=WebserviceRequest;
        self.ItemType=iItemType;
    }
    return self;
}*/

-(id)initWithSalespersonID:(NSString *)isaleOrderID Salesdatestart:(NSString *)isalesdatestart Salesdateend:(NSString *)isalesdateend
{
    
    self=[super init];
    if(self)
    {
        self.webserviceURL=@"http://219.94.43.102/KDSPackagingID/service.asmx";
        self.SOAPAction=@"getPackagingID";
        self.requestType=WebserviceRequest;
        self.SalespersonID=isaleOrderID;
        self.datestart=isalesdatestart;
        self.dateend=isalesdateend;
    }
    
    return self;
}

-(void)dealloc
{
    [SalespersonID release];
    [datestart release];
    [dateend release];
    
    [super dealloc]; 
}

-(NSString*) generateHTTPPostMessage
{
    NSString *postMsg = [NSString stringWithFormat:@"SalespersonID=%@&datestart=%@&dateend=%@",self.SalespersonID,self.datestart,self.dateend];
    
    return postMsg;
    
}
@end

