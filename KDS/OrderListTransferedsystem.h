//
//  OrderListTransferedsystem.h
//  KDS
//
//  Created by Jermin Bazazian on 5/28/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "NewOrderTableEditDelegate.h"
#import "SalesOrder.h"
#import "SalesOrderType.h"
#import "NetworkHandler.h"
#import "XMLResponse.h"
#import "Reachability.h"
#import "DeletedCartViewController.h"
#import "OrderListTransferedCellViewController.h"
#import "KDSAppDelegate.h"
#import "KDSDataSalesOrder.h"
#import "NewOrderTransferRequest.h" 
#import "LoadingView.h"
#import "OrderListTransferedDelegate.h"
#import "TransferOrderTableViewController.h"

@interface OrderListTransferedsystem : UIViewController<NetworkHandlerDelegate,OrderListTransferedDelegate>{
    
    LoadingView *loadingView;
    SalesOrder* ssalesOrder;
    NSArray *newolistSalesOrderArr;
    UIView *_tableViewHeader;
    
}
-(void)reloadSalesOrderArray;
-(void)reloadTransferedTable;
@property (nonatomic, retain) UIView *_tableViewHeader;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) SalesOrder* ssalesOrder;
@property (retain, nonatomic) IBOutlet UITableView *_tableView;
@property (nonatomic, retain) NSArray *newolistSalesOrderArr;
@property (nonatomic, retain) TransferOrderTableViewController *TnewOrderTableViewController;
@property (nonatomic, retain) IBOutlet UITableView *transferedOrderTableView;
@property (retain, nonatomic) IBOutlet UIButton *btnBigTransfer;

@end
