//
//  Customer.m
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Customer.h"


@implementation Customer
   
@synthesize Customer_Id = _Customer_Id, Customer_Name = _Customer_Name, Contact_Name = _Contact_Name, Phone_1 = _Phone_1, Phone_2 = _Phone_2, Email = _Email, salesperson_Code = _salesperson_Code;
@synthesize Address = _Address;
//@synthesize customer_remark;
-(void)dealloc
{
    //[customer_remark release];
    [_Customer_Id release];
    [_Customer_Name release];
    [_Phone_1 release];
    [_Phone_2 release];
    [_Email release];
    [_salesperson_Code release];
    [_Address release];
    [super dealloc];
}
@end
