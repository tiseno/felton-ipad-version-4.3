//
//  ResponseTranslator.h
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IResponseTranslator.h"
#import "GDataXMLNode.h"

#import "AllSalesPeopleResponse.h"
#import "SalesPerson.h"

#import "CustomersBySalespersonIDResponse.h"
#import "Customer.h"

#import "AllItemDetailsResponse.h"
#import "Item.h"
#import "BlindItem.h"
#import "UOMAndConversion.h"
#import "DefaultPriceAndUOM.h"
#import "AsyncImageView.h"
#import "AppWideConstants.h"
#import "TransferOrderPDFResponse.h"
#import "QuotationFormPdfResponse.h"
#import "NewOrderTransferResponse.h"
#import "DownloadItemImagesReaponse.h"
#import "DownloadImages.h"
#import "OrderPackagingResponse.h"
#import "OrderPackaging.h"
#import "SalesOrder.h"

@interface ResponseTranslator : NSObject<IResponseTranslator> {
    
}
-(XMLResponse*)translate:(NSData*) xmlData;
-(XMLResponse*)translateOrderPackage:(NSData*) xmlData;
-(XMLResponse*)translateAllSalesPeople:(NSData*) xmlData;
-(XMLResponse*)translateAllItemDetails:(NSData *)xmlData;
-(XMLResponse*)translateCustomersBySalespersonID:(NSData *)xmlData;
-(XMLResponse*) translategetPDFOrder:(NSData *)xmlData;
-(XMLResponse*)translateOrder:(NSData*) xmlData;
-(XMLResponse*)translateImagePath:(NSData *)xmlData;
-(XMLResponse*)translateMainItemDetails:(NSData *)xmlData;
-(XMLResponse*)translateBlindItemDetails:(NSData *)xmlData;
-(XMLResponse*) translategetPDFQuotationr:(NSData *)xmlData;
@end
