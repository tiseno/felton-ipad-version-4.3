//
//  DownloadItemImagesRequest.m
//  KDS
//
//  Created by Tiseno Mac 2 on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DownloadItemImagesRequest.h"

@implementation DownloadItemImagesRequest
@synthesize ItemType;

-(id)initWithItemTypes:(NSString*)iItemType;
{
    self=[super init];
    if(self)
    {
        //self.webserviceURL=@"http://219.94.43.102/KDSItemImage/service.asmx";
        self.webserviceURL=@"http://219.94.43.102/KDSItemImage/service.asmx";
        self.SOAPAction=@"getItemImage";
        self.requestType=WebserviceRequest;
        self.ItemType=iItemType;
    }
    return self;
}

-(void)dealloc
{
    [ItemType release];
     
    [super dealloc];
}

-(NSString*) generateHTTPPostMessage
{
    NSString *postMsg = [NSString stringWithFormat:@"ItemType=%@",self.ItemType];
    
    return postMsg;

}
@end
