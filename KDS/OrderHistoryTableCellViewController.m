//
//  OrderHistoryTableCellViewController.m
//  KDS
//
//  Created by Tiseno on 12/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OrderHistoryTableCellViewController.h"
#define cell1Width 313
#define cell2Width 100
#define cellHeight 50

@implementation OrderHistoryTableCellViewController

@synthesize lineColor, orderDateLabel, orderPriceLabel, orderIdPrefix;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 411, 407);
        
        UILabel *tOrderIdPrefixLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, cell1Width/2, cellHeight)];
        tOrderIdPrefixLabel.textAlignment = UITextAlignmentLeft;
        tOrderIdPrefixLabel.adjustsFontSizeToFitWidth = YES;
        tOrderIdPrefixLabel.backgroundColor = [UIColor clearColor];
        tOrderIdPrefixLabel.textColor = [UIColor colorWithRed:0.21 green:0.38 blue:0.43 alpha:1.0]; 
        tOrderIdPrefixLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:14];
        self.orderIdPrefix = tOrderIdPrefixLabel;
        [tOrderIdPrefixLabel release];
        [self addSubview:orderIdPrefix];
        
        UILabel *tOrderDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, cell1Width/2, cellHeight)];
        tOrderDateLabel.textAlignment = UITextAlignmentLeft;
        tOrderDateLabel.adjustsFontSizeToFitWidth = YES;
        tOrderDateLabel.backgroundColor = [UIColor clearColor];
        tOrderDateLabel.textColor = [UIColor colorWithRed:0.21 green:0.38 blue:0.43 alpha:1.0]; 
        tOrderDateLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:14];
        self.orderDateLabel = tOrderDateLabel;
        [tOrderDateLabel release];
        [self addSubview:orderDateLabel];
        
        UILabel *tOrderPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(cell1Width, 0, cell2Width, cellHeight)];
        tOrderPriceLabel.textAlignment = UITextAlignmentCenter;
        tOrderPriceLabel.adjustsFontSizeToFitWidth = YES;
        tOrderPriceLabel.backgroundColor = [UIColor clearColor];
        tOrderPriceLabel.textColor = [UIColor blackColor]; 
        tOrderPriceLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
        self.orderPriceLabel = tOrderPriceLabel;
        [tOrderPriceLabel release];
        [self addSubview:orderPriceLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [lineColor release];
    [orderIdPrefix release];
    [orderDateLabel release];
    //[orderedProductNameLabel release];
    [orderPriceLabel release];
    [super dealloc];
}

@end
