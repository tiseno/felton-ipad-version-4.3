//
//  BlindSalesOrder.m
//  KDS
//
//  Created by Jermin Bazazian on 12/8/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "BlindSalesOrder.h"


@implementation BlindSalesOrder
@synthesize Installation_Email=_Installation_Email,Installation_Address=_Installation_Address,Installation_Phone=_Installation_Phone,Installation_Fax=_Installation_Fax, Installation_Name = _Installation_Name,installation,transportation;
@synthesize Installation_City=_Installation_City, Installation_State=_Installation_State,Installationorder_Address=_Installationorder_Address;
-(id)init
{
    self=[super init];
    if(self)
    {
        isDeleted=NO;
        _Type=SalesOrderBlind;
    }
    return  self;
}
-(void)dealloc
{
    [_Installation_City release];
    [_Installation_State release];
    [_Installationorder_Address release];
    [_Installation_Email release];
    [_Installation_Address release];
    [_Installation_Phone release];
    [_Installation_Fax release];
    [_Installation_Name release];
    [super dealloc];
}
-(BOOL)isMainOrderType:(SalesOrderItem*)orderItem
{
    if([orderItem isKindOfClass:[BlindSalesOrderItem class]])
    {
        return YES;
    }
    return NO;
}
-(void)addInheritanceDetails:(GDataXMLElement*)salesOrderNode
{
    GDataXMLElement* addressElement=[GDataXMLNode elementWithName:@"InstallationAddress"];
    
    NSString *Street_1s=[self.Installation_Address.Street_1 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* addressStreet1Element=[GDataXMLNode elementWithName:@"Street1" stringValue:Street_1s];
    [addressElement addChild:addressStreet1Element];
    
    NSString *Street_2s=[self.Installation_Address.Street_2 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* addressStreet2Element=[GDataXMLNode elementWithName:@"Street2" stringValue:Street_2s];
    [addressElement addChild:addressStreet2Element];
    
    NSString *Street_3s=[self.Installation_Address.Street_3 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* addressStreet3Element=[GDataXMLNode elementWithName:@"Street3" stringValue:Street_3s];
    [addressElement addChild:addressStreet3Element];
    
    NSString *Street_4s=[self.Installation_Address.Street_4 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* addressStreet4Element=[GDataXMLNode elementWithName:@"Street4" stringValue:Street_4s];
    [addressElement addChild:addressStreet4Element];
    
    NSString *Citys=[self.Installation_Address.City stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* addressCityElement=[GDataXMLNode elementWithName:@"City" stringValue:Citys];
    [addressElement addChild:addressCityElement];
    
    NSString *PostalCodes=[self.Installation_Address.PostalCode stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* addressPostalCodeElement=[GDataXMLNode elementWithName:@"PostalCode" stringValue:PostalCodes];
    [addressElement addChild:addressPostalCodeElement];
    
    NSString *States=[self.Installation_Address.State stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* addressStateElement=[GDataXMLNode elementWithName:@"State" stringValue:States];
    [addressElement addChild:addressStateElement];
    
    NSString *Countrys=[self.Installation_Address.Country stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* addressCountryElement=[GDataXMLNode elementWithName:@"Country" stringValue:Countrys];
    [addressElement addChild:addressCountryElement];
    [salesOrderNode addChild:addressElement];
    
    NSString *Installation_Phones=[self.Installation_Phone stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* installationPhoneElement=[GDataXMLNode elementWithName:@"InstallationPhone" stringValue:Installation_Phones];
    [salesOrderNode addChild:installationPhoneElement];
    
    NSString *Installation_Faxs=[self.Installation_Fax stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* installationFaxElement=[GDataXMLNode elementWithName:@"InstallationFax" stringValue:Installation_Faxs];
    [salesOrderNode addChild:installationFaxElement];
    
    NSString *Installation_Emails=[self.Installation_Email stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* installationEmailElement=[GDataXMLNode elementWithName:@"InstallationEmail" stringValue:Installation_Emails];
    [salesOrderNode addChild:installationEmailElement];
    
    NSString *Installation_Names=[self.Installation_Name stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* installationNameElement=[GDataXMLNode elementWithName:@"InstallationName" stringValue:Installation_Names];
    [salesOrderNode addChild:installationNameElement];
    
    GDataXMLElement* installationCostElement=[GDataXMLNode elementWithName:@"InstallationCost" stringValue:[NSString stringWithFormat:@"%.2f",self.installation]];
    [salesOrderNode addChild:installationCostElement];
    GDataXMLElement* transportationCostElement=[GDataXMLNode elementWithName:@"TransportationCost" stringValue:[NSString stringWithFormat:@"%.2f",self.transportation]];
    [salesOrderNode addChild:transportationCostElement];
}

-(void)addInheritancePDFDetails:(GDataXMLElement*)salesOrderNode
{
    NSString *Installationorder_Addresss=[self.Installationorder_Address stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    /**/GDataXMLElement* addressElement=[GDataXMLNode elementWithName:@"InstallationAddress" stringValue:Installationorder_Addresss];
    
    //GDataXMLElement* addressStreet1Element=[GDataXMLNode elementWithName:@"Street1" stringValue:self.Installation_Address.Street_1];
    //GDataXMLElement* addressStreet2Element=[GDataXMLNode elementWithName:@"Street2" stringValue:self.Installation_Address.Street_2];
    //GDataXMLElement* addressStreet3Element=[GDataXMLNode elementWithName:@"Street3" stringValue:self.Installation_Address.Street_3];
     //GDataXMLElement* addressStreet4Element=[GDataXMLNode elementWithName:@"Street4" stringValue:self.Installation_Address.Street_4];
    
    NSString *Installation_Citys=[self.Installation_City stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* addressCityElement=[GDataXMLNode elementWithName:@"InstallationDate" stringValue:Installation_Citys];
    //GDataXMLElement* addressPostalCodeElement=[GDataXMLNode elementWithName:@"PostalCode" stringValue:self.Installation_Address.PostalCode];
    
    NSString *Installation_States=[self.Installation_State stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* addressStateElement=[GDataXMLNode elementWithName:@"InstallationTime" stringValue:Installation_States];
    //GDataXMLElement* addressCountryElement=[GDataXMLNode elementWithName:@"Country" stringValue:self.Installation_Address.Country];
    
    
    NSString *Installation_Phones=[self.Installation_Phone stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    NSString *Installation_Faxs=[self.Installation_Fax stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    NSString *Installation_Emails=[self.Installation_Email stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    NSString *Installation_Names=[self.Installation_Name stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    
    GDataXMLElement* installationPhoneElement=[GDataXMLNode elementWithName:@"InstallationPhone" stringValue:Installation_Phones];
    GDataXMLElement* installationFaxElement=[GDataXMLNode elementWithName:@"InstallationFax" stringValue:Installation_Faxs];
    GDataXMLElement* installationEmailElement=[GDataXMLNode elementWithName:@"InstallationEmail" stringValue:Installation_Emails];
    GDataXMLElement* installationNameElement=[GDataXMLNode elementWithName:@"InstallationName" stringValue:Installation_Names];
    
    
    
     
    
    
    /*[addressElement addChild:addressStreet1Element];
    
    [addressElement addChild:addressStreet2Element];
    
    [addressElement addChild:addressStreet3Element];
   
    [addressElement addChild:addressStreet4Element];
    
    [addressElement addChild:addressPostalCodeElement];
    
    [addressElement addChild:addressStateElement];
    
    [addressElement addChild:addressCountryElement];*/
    
    
    [salesOrderNode addChild:addressCityElement];
    [salesOrderNode addChild:addressStateElement];
    
    [salesOrderNode addChild:addressElement];
    
    [salesOrderNode addChild:installationPhoneElement];
    
    [salesOrderNode addChild:installationFaxElement];
    
    [salesOrderNode addChild:installationEmailElement]; 
    
    [salesOrderNode addChild:installationNameElement];
    
    GDataXMLElement* transportationCostElement=[GDataXMLNode elementWithName:@"Transportation" stringValue:[NSString stringWithFormat:@"%.2f",self.transportation]];
    GDataXMLElement* installationCostElement=[GDataXMLNode elementWithName:@"Installation" stringValue:[NSString stringWithFormat:@"%.2f",self.installation]];
    
    [salesOrderNode addChild:transportationCostElement];
    [salesOrderNode addChild:installationCostElement];
    
}

@end
