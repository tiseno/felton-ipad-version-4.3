//
//  TransferOrderPdfRequest.m
//  KDS
//
//  Created by Tiseno Mac 2 on 3/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TransferOrderPdfRequest.h"

@implementation TransferOrderPdfRequest
@synthesize salesPersonTransfer,newTransferSalesOrder;

-(id)initWithSalesOrder:(SalesOrder*)isaleOrder SalesPersonTransfer:(SalesPerson*)isalesPersonTransfer
{
self=[super init];
if(self)
{
    self.webserviceURL=@"http://219.94.43.102/KDSOrderPDFConvertor/service.asmx";
    //self.webserviceURL=@"http://175.139.179.90/OrderWebservice/Service.asmx";
    
    self.SOAPAction=@"OrderPDF";
    self.requestType=WebserviceRequest;
    self.newTransferSalesOrder=isaleOrder;
    self.salesPersonTransfer=isalesPersonTransfer;
}
return self;
}
-(NSString*) generateHTTPPostMessage
{ 
    NSString* xmlRequest=[self.newTransferSalesOrder convertToXMLWithSalesPersonPDF:self.salesPersonTransfer];
    //NSString *xmlStrsend=[xmlRequest stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"]; 
    NSString *xmlStr=[NSString stringWithFormat:@"XMLRequest=%@",xmlRequest];
    //NSLog(@"%@",xmlStr);
    return xmlStr;

 
}
-(void) dealloc
{
[newTransferSalesOrder release];
[salesPersonTransfer release];
[super dealloc];
}
@end
