//
//  NewOrderCategoryViewController.m
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewOrderCategoryViewController.h"


@implementation NewOrderCategoryViewController
//@synthesize lblprefix;
//@synthesize prefixonoff;
@synthesize btnViewYourCart;
@synthesize btnCancelOrder;
@synthesize customerDetailtable;
@synthesize listGallery;
//@synthesize searchMethodSc;
@synthesize toRemove;
//@synthesize search;
@synthesize allNames,names,keyss,tableViewHeader;
@synthesize productsArr,searchById,productsArr_Copied,selectedItem,categoryitem;
@synthesize _CountDownTime,selfView,loadingView;
@synthesize customerNameLabel, companyNameLabel, telLabel, faxLabel, emailLabel, currentSelectedCustomer, categoryScrollView,scCategories;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        loadData=NO;
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        if(appDelegate.salesOrder!=nil)
        {
            [appDelegate clearSalesOrder];
        }
        self.currentSelectedCustomer = ((KDSAppDelegate*)[UIApplication sharedApplication].delegate).currentCustomer;
        
        [self initializeCategoryButtonsWithCategoryType:kAllCategory];
    } 
    return self;
}

- (void)dealloc
{
    //[tableViewHeader release];
    [loadingView release];
    [keyss release];
    [names release];
    [allNames release];
    [categoryitem release];
    [productsArr release];
    [productsArr_Copied release];
    [customerNameLabel release];
    [companyNameLabel release];
    [telLabel release];
    [faxLabel release];
    [emailLabel release];
    [categoryScrollView release];
    [currentSelectedCustomer release];
    [scCategories release];
    //[searchMethodSc release];
    //[search release];
    [listGallery release];
    [btnViewYourCart release];
    [btnCancelOrder release];
    //[lblprefix release];
    //[prefixonoff release];
    [customerDetailtable release];
    [super dealloc];
}
-(IBAction)viewYourCartButtonTapped
{
    YourCartViewController *yourCartViewController=[[YourCartViewController alloc] initWithNibName:@"YourCartViewController" bundle:nil];
    yourCartViewController.title=@"Felton";
    [self.navigationController pushViewController:yourCartViewController animated:YES];
    [yourCartViewController release];
}
-(IBAction)cancelYourCartButtonTapped
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"Please Confirm cancelling your order." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
    [alert release];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        [appDelegate clearSalesOrder];
        
        UIViewController* newOrderCategoryViewController=nil;
        for(UIViewController* viewController in self.navigationController.viewControllers)
        {
            if([viewController isKindOfClass:[MenuViewController class]])
            {
                newOrderCategoryViewController=viewController;
                break;
            }
        }
        [self.navigationController popToViewController:newOrderCategoryViewController animated:YES];
    }
    
}

-(IBAction)orderTypeControlTapped
{
    
}


/*-(void)initializeCategoryButtons:(NSArray*) itemArr
 {
 //NSLog(@"%@",itemArr);
 int widthOffset = 0;
 int yOffset=0;
 int xOffset=2;
 for(int i=0;i<itemArr.count;i++)
 {
 
 if(i!=0 && (i%2)==0)
 {
 yOffset=0;
 xOffset+=150;
 }
 Item* item=[itemArr objectAtIndex:i];
 ButtonWithProduct *categoryButton_up = [[[ButtonWithProduct alloc] init] autorelease];
 if(item.Image_Path!=nil && ![item.Image_Path isEqualToString:@""])
 {
 NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString* documentsPath = [paths objectAtIndex:0];
 NSString* filePath = [documentsPath stringByAppendingPathComponent:item.Image_Path];
 NSData* pngData = [NSData dataWithContentsOfFile:filePath];
 UIImage* image = [UIImage imageWithData:pngData];
 
 [categoryButton_up setBackgroundImage:image forState:UIControlStateNormal];
 //NSLog(@"image loaded");
 }
 categoryButton_up.item = item;
 //categoryButton_up.frame = CGRectMake(1+widthOffset*xOffset, yOffset, 137, 137);
 categoryButton_up.frame = CGRectMake(xOffset, 2+widthOffset*yOffset, 137, 137);
 [categoryButton_up addTarget:self action:@selector(productImageTapped:) forControlEvents:UIControlEventTouchDown];
 
 UILabel *orderIdLabel_up = [[UILabel alloc] initWithFrame:CGRectMake(xOffset, 2+widthOffset*yOffset+137, 137, 21)];
 orderIdLabel_up.adjustsFontSizeToFitWidth = YES;
 orderIdLabel_up.font = [UIFont fontWithName:@"Helvetica-Bold" size:9];
 orderIdLabel_up.minimumFontSize=7;
 orderIdLabel_up.textAlignment=UITextAlignmentCenter;
 orderIdLabel_up.textColor = [UIColor blackColor];
 orderIdLabel_up.backgroundColor = [UIColor clearColor];
 orderIdLabel_up.text=item.Item_No;
 
 UILabel *categoryLabel_up = [[UILabel alloc] initWithFrame:CGRectMake(xOffset, 2+widthOffset*yOffset+155, 137, 21)];
 categoryLabel_up.adjustsFontSizeToFitWidth = YES;
 categoryLabel_up.minimumFontSize=7;
 categoryLabel_up.textAlignment=UITextAlignmentCenter;
 categoryLabel_up.textColor = [UIColor blackColor];
 categoryLabel_up.backgroundColor = [UIColor clearColor];
 categoryLabel_up.text=item.Description;
 widthOffset=187;
 //xOffset++;
 yOffset++;
 
 [self.itemScrollView addSubview:categoryButton_up];
 [self.itemScrollView addSubview:orderIdLabel_up];
 [self.itemScrollView addSubview:categoryLabel_up];
 
 [categoryLabel_up release];
 //[orderIdLabel_up release];
 //[categoryButton_up release];
 
 }
 self.itemScrollView.contentSize=CGSizeMake(xOffset+150, self.itemScrollView.frame.size.height);
 
 }*/
-(void)initializeCategoryButtonsWithCategoryType:(NSString*)categoryType
{
    /**/for(UIView* uiview in self.categoryScrollView.subviews)
    {
        [uiview removeFromSuperview];
    }
    KDSAppDelegate* appDelegate = (KDSAppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSMutableDictionary *itemCategoryDictionary=[[NSMutableDictionary alloc]init];
    itemCategoryDictionary=appDelegate.itemCategoryDictionary;
    
    if(itemCategoryDictionary == nil)
    {
        [appDelegate loadProducts]; 
    }else {
        NSLog(@"no loadProducts cat"); 
    }
    NSArray *keys = [itemCategoryDictionary allKeys];
    int widthOffset = 0;
    int yOffset=0;
    int xOffset=2;
    int loopCounter=0;
    for(int i=0;i<keys.count;i++)
    {
        
        NSString* category = [keys objectAtIndex:i];
        //NSLog(@"category---?%@",category);
        if([self doesCategory:category belongToCategoryType:categoryType])
        { 
            if(loopCounter!=0 && (loopCounter%2)==0)
            { 
                yOffset=0;
                xOffset+=180;
            }
            
            NSArray *itemArr = [itemCategoryDictionary objectForKey:category];
            
            Item* item=[itemArr objectAtIndex:0];
            //UIButton *categoryButton_up = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            
            ButtonWithProduct *categoryButton_up = [[[ButtonWithProduct alloc] init]autorelease];
            if(item.Image_Path!=nil && ![item.Image_Path isEqualToString:@""])
            {
                NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString* documentsPath = [paths objectAtIndex:0];
                NSString* filePath = [documentsPath stringByAppendingPathComponent:item.Image_Path];
                NSData* pngData = [NSData dataWithContentsOfFile:filePath];
                UIImage* image = [UIImage imageWithData:pngData];
                
                [categoryButton_up setBackgroundImage:image forState:UIControlStateNormal];
                //NSLog(@"image loaded--->%@",paths);
            }
            else {
                UIImage* image = [UIImage imageNamed:@"blinds.png"];
                [categoryButton_up setBackgroundImage:image forState:UIControlStateNormal];
            }
            
            [categoryButton_up setTitle:category forState:UIControlStateNormal];
            [categoryButton_up.titleLabel setFont:[UIFont systemFontOfSize:1]];
            //categoryButton_up.frame = CGRectMake(xOffset, 2+widthOffset*yOffset, 137, 137);
            categoryButton_up.frame = CGRectMake(xOffset+3, 2+widthOffset*yOffset+2, 131, 133);
            [categoryButton_up addTarget:self action:@selector(categoryImageTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            UILabel *orderIdLabel_up = [[UILabel alloc] initWithFrame:CGRectMake(xOffset, 2+widthOffset*yOffset+137, 137, 21)];
            orderIdLabel_up.adjustsFontSizeToFitWidth = YES;
            orderIdLabel_up.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
            orderIdLabel_up.minimumFontSize=7;
            orderIdLabel_up.textAlignment=UITextAlignmentCenter;
            orderIdLabel_up.textColor = [UIColor blackColor];
            orderIdLabel_up.backgroundColor = [UIColor clearColor];
            orderIdLabel_up.text=category;
            
            //UIImage* image_base = [UIImage imageNamed:@"image_base.png"];
            
            UIImageView *imgview =[[UIImageView alloc]initWithFrame:CGRectMake(xOffset, 2+widthOffset*yOffset, 137, 137)];
            imgview.image=[UIImage imageNamed:@"image_base.png"];
            
            
            widthOffset=180;
            yOffset++;
            loopCounter++;
            
            [self.categoryScrollView addSubview:imgview];
            [self.categoryScrollView addSubview:categoryButton_up];
            [self.categoryScrollView addSubview:orderIdLabel_up];
        }
    }
    
    //self.categoryScrollView.contentSize=CGSizeMake(self.categoryScrollView.frame.size.width, yOffset+142);
    self.categoryScrollView.contentSize=CGSizeMake(xOffset+180, self.categoryScrollView.frame.size.height);
    //self.itemScrollView.contentSize=CGSizeMake(xOffset+150, self.itemScrollView.frame.size.height);
    
}

-(BOOL)doesCategory:(NSString*)category belongToCategoryType:(NSString*)categoryType
{
    
    if([categoryType isEqualToString:kAllCategory])
    {
        return YES;
    }
    else if([categoryType isEqualToString:kBlindCategory])
    {
        if([category isEqualToString:kBlindCategory])
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else if([categoryType isEqualToString:kMainCategory])
    {
        if(![category isEqualToString:kBlindCategory])
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    return NO;
}
-(IBAction)categoryImageTapped:(id)sender
{
    /*for (UIView* uiview in self.view.subviews) {
        [uiview removeFromSuperview];
    }*/
    
    NewOrder_ProductViewController *orderProductViewController=[[NewOrder_ProductViewController alloc] initWithNibName:@"NewOrder_ProductViewController" bundle:nil];
    orderProductViewController.category=[((UIButton*)sender) titleForState:UIControlStateNormal];
    orderProductViewController.title=@"Felton";
    
    /*UIViewController* newOrderCategoryViewController=nil;
    for(UIViewController* viewController in self.navigationController.viewControllers)
    {
        if([viewController isKindOfClass:[NewOrder_ProductViewController class]])
        {
            newOrderCategoryViewController=orderProductViewController;
            break;
        }
    }*/
    
    
    
    
    //[self.view addSubview:orderProductViewController.view];
    
    [self.navigationController pushViewController:orderProductViewController animated:YES];
    [orderProductViewController release];
} 


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */



- (void)updateTimer{ //Happens every time updateTimer is called. Should occur every second.
    
    counterA -= 1;
    _CountDownTime = [NSString stringWithFormat:@"%i", counterA];
    
    if (counterA < 0) // Once timer goes below 0, reset all variables.
    {
        [timerA invalidate];
        startA = TRUE;
        //counterA = 1;
        loadData=YES;
        NSLog(@"%@",_CountDownTime); 
        
        if (loadData==YES) 
        {
            [self initializeCategoryButtonsWithCategoryType:kAllCategory];
        }
        
        [self.loadingView removeView];
        
        
    }
    
    
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    btnViewYourCart.hidden=YES;
    btnCancelOrder.hidden=YES;
    //searchMethodSc.hidden=YES;
    //search.hidden=YES;
    //lblprefix.hidden=YES; 
    //prefixonoff.hidden=YES;
    scCategories.hidden=YES;
    listGallery.hidden=YES;
    
    customerNameLabel.text = currentSelectedCustomer.Contact_Name;
    companyNameLabel.text = currentSelectedCustomer.Customer_Name;
    telLabel.text = currentSelectedCustomer.Phone_1;
    faxLabel.text = currentSelectedCustomer.Phone_2;
    emailLabel.text = currentSelectedCustomer.Email;
    //[self initializeCategoryButtonsWithCategoryType:kAllCategory];
    
    ///self.listGallery.selectedSegmentIndex=1;
    
    
    
    counterA = 1;
    startA = TRUE;
    
    if(startA == TRUE) //Check that another instance is not already running.
    {
        _CountDownTime = @"1";
        startA = FALSE;
        timerA = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
        
        
        
    }
    //UIView *selfView=self.view;
    selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    

    
    
    
    
    
    /**/if([[NSUserDefaults standardUserDefaults] boolForKey:@"Remember Password"])
    {
        self.listGallery.selectedSegmentIndex = 0;
    }
    else
    {
        self.listGallery.selectedSegmentIndex = 1;
    }
    
    //[self searchView];
    
    /**/UIView *TransferedOrdertableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(customerDetailtable.frame.origin.x, customerDetailtable.frame.origin.y-30, self.customerDetailtable.frame.size.width, 30)];
    TransferedOrdertableHeaderView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0];
    
    UILabel *TransferedOrderTableHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 5, self.customerDetailtable.frame.size.width/2, 21)];
    TransferedOrderTableHeaderLabel.text = @"Customer";
    TransferedOrderTableHeaderLabel.backgroundColor = [UIColor clearColor];
    TransferedOrderTableHeaderLabel.textColor = [UIColor whiteColor];
    TransferedOrderTableHeaderLabel.textAlignment = UITextAlignmentLeft;
    TransferedOrderTableHeaderLabel.font=[UIFont boldSystemFontOfSize:17];
    [TransferedOrdertableHeaderView addSubview:TransferedOrderTableHeaderLabel];
    tableViewHeader = TransferedOrdertableHeaderView;
    [self.view addSubview:tableViewHeader];
    
    
    [TransferedOrdertableHeaderView release];
    [TransferedOrderTableHeaderLabel release];
}


- (void)viewDidUnload
{
    
    //[self setListGallery:nil];
    /*[self setBtnViewYourCart:nil];
    [self setBtnCancelOrder:nil];
    [self setLblprefix:nil];
    [self setPrefixonoff:nil];
    [self setCustomerDetailtable:nil];*/
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
/**/-(void)viewDidDisappear:(BOOL)animated
{
    if([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        //order information will be removed here
        //order information will be removed here
        //order information will be removed here
        //order information will be removed here
        NSLog(@"order information will be removed here11");
        
    }
    //else {
    //    NSLog(@"order information will be removed here22");
    //}
    //[self.navigationController viewDidDisappear:animated];
    [super viewDidDisappear:animated];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(IBAction) segmentedControlIndexChanged
{
    
    if(self.scCategories.selectedSegmentIndex==0)
    {
        [self initializeCategoryButtonsWithCategoryType:kAllCategory];
    }
    else if(self.scCategories.selectedSegmentIndex==1)
    {
        [self initializeCategoryButtonsWithCategoryType:kMainCategory];
    }
    else if(self.scCategories.selectedSegmentIndex==2)
    {
        [self initializeCategoryButtonsWithCategoryType:kBlindCategory];
    }
    /*for(UIView* uiview in self.categoryScrollView.subviews)
     {
     [uiview removeFromSuperview];
     }*/
    
}

-(IBAction)ListTapped
{
    KDSDataListGallery *dataItem=[[KDSDataListGallery alloc] init];
    [dataItem updateLogin:@"list"];
    
    NewOrderViewController *newOrderViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
    newOrderViewController.title=@"iOrder";
    [self.navigationController pushViewController:newOrderViewController animated:YES];
    [newOrderViewController release];
}

-(IBAction)ListGalleryTapped
{
    if(self.listGallery.selectedSegmentIndex==0)
    {
        
        NewOrderViewController *newOrderViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
        newOrderViewController.title=@"iOrder";
        [self.navigationController pushViewController:newOrderViewController animated:YES];
        [newOrderViewController release];
        
        /*UIViewController* newOrderCategoryViewController=nil;
         for(UIViewController* viewController in self.navigationController.viewControllers)
         {
         if([viewController isKindOfClass:[NewOrderViewController class]])
         {
         newOrderCategoryViewController=viewController;
         break;
         }
         }
         //[self.navigationController popToViewController:newOrderCategoryViewController animated:YES];
         
         [self.navigationController pushViewController:newOrderCategoryViewController animated:YES];*/
        
    }
    else if(self.listGallery.selectedSegmentIndex==1)
    {
        
        /*UIViewController* newOrderCategoryViewController=nil;
         for(UIViewController* viewController in self.navigationController.viewControllers)
         {
         if([viewController isKindOfClass:[NewOrderCategoryViewController class]])
         {
         newOrderCategoryViewController=viewController;
         break;
         }
         }
         //[self.navigationController popToViewController:newOrderCategoryViewController animated:YES];
         [self.navigationController pushViewController:newOrderCategoryViewController animated:YES];
         */
        
        NewOrderCategoryViewController *newOrderViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];
        newOrderViewController.title=@"iOrder";
        [self.navigationController pushViewController:newOrderViewController animated:YES];
        [newOrderViewController release];
        
        
        
    }
}

/*==================================== search product =============================================*/



@end
