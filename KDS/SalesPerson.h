//
//  SalesPerson.h
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SalesPerson : NSObject{
    NSString *_SalesPerson_Code;
    NSString *_SalesPerson_Name;
    NSString *_SalesPerson_Username;
    NSString *_SalesPerson_Password;
    NSString *orderId_Prefix;
}

@property (nonatomic, retain) NSString* SalesPerson_Code, *SalesPerson_Name, *SalesPerson_Username, *SalesPerson_Password, *orderId_Prefix;


@end
