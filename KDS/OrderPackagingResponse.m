//
//  OrderPackagingResponse.m
//  KDS
//
//  Created by Tiseno Mac 2 on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrderPackagingResponse.h"

@implementation OrderPackagingResponse
@synthesize OrderPackage;

-(void)dealloc
{
    [OrderPackage release];
    [super dealloc];
}

@end
