//
//  SalesPerson.m
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SalesPerson.h"


@implementation SalesPerson

@synthesize SalesPerson_Code = _SalesPerson_Code, SalesPerson_Name = _SalesPerson_Name, SalesPerson_Password = _SalesPerson_Password, SalesPerson_Username = _SalesPerson_Username, orderId_Prefix;

-(void)dealloc
{
    [orderId_Prefix release];
    [_SalesPerson_Code release];
    [_SalesPerson_Name release];
    [_SalesPerson_Password release];
    [_SalesPerson_Username release];
    [super dealloc];
}
@end
