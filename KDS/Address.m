//
//  Address.m
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Address.h"


@implementation Address

@synthesize Street_1 = _Street_1, Street_2 = _Street_2, Street_3 = _Street_3, Street_4 = _Street_4, City = _City, ZipCode = _ZipCode, PostalCode = _PostalCode, Country = _Country, State = _State;
-(void)dealloc
{
    [_Street_1 release];
    [_Street_2 release];
    [_Street_3 release];
    [_Street_4 release];
    [_City release];
    [_ZipCode release];
    [_PostalCode release];
    [_Country release];
    [_State release];
    [super dealloc];
}
@end
