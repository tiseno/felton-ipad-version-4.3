//
//  ReviewTableViewController.h
//  KDS
//
//  Created by Tiseno on 1/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReviewTableViewCell.h"
#import "ReviewTableViewCellBlind.h"
#import "KDSAppDelegate.h"
#import "ReviewTableViewDelegate.h"


@interface ReviewTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource> {
    
    int numberOfRows;
    id<ReviewTableViewDelegate> scrollDelegate;
}
@property (nonatomic) int numberOfRows;
@property (nonatomic, retain) id<ReviewTableViewDelegate> scrollDelegate;
@end
