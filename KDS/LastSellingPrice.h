//
//  LastSellingPrice.h
//  KDS
//
//  Created by Jermin Bazazian on 12/9/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LastSellingPrice : NSObject {
    float _Price;
    NSDate* _Date;
    NSString* _UOM;
    float _Min_Hight;
    float _Min_SQFT;
}
@property (nonatomic) float Price,Min_Hight,Min_SQFT;
@property (nonatomic, retain) NSDate* Date;
@property (nonatomic, retain) NSString* UOM;
@end
