//
//  KDSDataListGallery.h
//  KDS
//
//  Created by Tiseno Mac 2 on 8/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KDSDataLayerBase.h"
#import "listgallery.h"

@interface KDSDataListGallery : KDSDataLayerBase{
    
    BOOL runBatch;
}
@property (nonatomic)BOOL runBatch;
-(NSArray*)selectItem;
-(DataBaseUpdateResult)updateLogin:(NSString*)tllistg;
-(DataBaseInsertionResult)insertItem:(NSString*)tlistGstatus;
@end
