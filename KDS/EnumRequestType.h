//
//  EnumRequestType.h
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//
typedef enum {
    FileRequest,
    WebserviceRequest
} EnumRequestType;