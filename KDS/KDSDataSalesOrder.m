//
//  KDSDataSalesOrder.m
//  KDS
//
//  Created by Tiseno on 12/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KDSDataSalesOrder.h"


@implementation KDSDataSalesOrder
@synthesize runBatch;
@synthesize orderPackagesArr;

-(void)dealloc
{
    [orderPackagesArr release];
    [super dealloc];
}

-(NSString*)generateInsertQueryForSaleOrder:(SalesOrder*)salesOreder AndSalesperson:(SalesPerson*)salesPerson
{
    int orderType=1;
    if(salesOreder.Type==SalesOrderBlind)
    {
        orderType=2;
    }
     
    //NSLog(@" orderType-->%d",orderType);
    NSDateFormatter* formatter=[[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"YYYY/MM/dd hh:mm:ss"];
    [formatter setDateFormat:@"yyyyMMdd"];
    NSDate* currentDate=[NSDate date];
    NSString* currentDateStr=[formatter stringFromDate:currentDate];
    //NSLog(@"currentDateStr------------->%@",currentDateStr);
    [formatter release]; 
    
    NSString *orderRemark=[salesOreder.remark stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *orderDeliveryDate=[salesOreder.DeliveryDate stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *orderShipping_Address=[salesOreder.Shipping_Address stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *orderOrder_Reference=[salesOreder.Order_Reference stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *orderOrder_CashRemarks=[salesOreder.CashRemarks stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *OrderDescription=[salesOreder.OrderDescription stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    
    NSString *insertSQL = [NSString stringWithFormat:@"insert into SalesOrder(customer_id,salesperson_code,order_type,order_date,order_location,is_salesorder_uploaded,grand_price,order_discount,is_deleted, remark, OrderDescription, DeliveryDate, installation_address, orderReference, CashRemark) values('%@','%@',%d,'%@','%@',%d,%f,%f,%d,'%@','%@','%@','%@','%@', '%@');", salesOreder.customer.Customer_Id,salesPerson.SalesPerson_Code,orderType,currentDateStr,@"",salesOreder.Is_Uploaded,salesOreder.grand_Price,salesOreder.order_discount,salesOreder.isDeleted,orderRemark, OrderDescription, orderDeliveryDate, orderShipping_Address, orderOrder_Reference, orderOrder_CashRemarks];
    //NSLog(@"%@", insertSQL);  
    return insertSQL;
}
-(void)insertOrderItems:(NSArray*)orderItemArr forSalesOrderID:(int)salesOrderID
{
    KDSDataSalesOrderItem* dataItem=[[KDSDataSalesOrderItem alloc] initWithDataBase:dbKDS];
    dataItem.runBatch=YES;
    for(SalesOrderItem* item in orderItemArr)
    {
        [dataItem insertSalesOrderItem:item ForSalesOrderID:salesOrderID];
    }
    [dataItem release];
}
-(DataBaseInsertionResult)insertSalesOrder:(SalesOrder*)tSalesOrder forSalesperson:(SalesPerson*)salesPerson
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *insertSQL=[self generateInsertQueryForSaleOrder:tSalesOrder AndSalesperson:salesPerson];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            int salesOrderID=sqlite3_last_insert_rowid(dbKDS);
            tSalesOrder.Order_id=salesOrderID;
            [self insertOrderItems:[tSalesOrder order_Items] forSalesOrderID:salesOrderID];
            NSArray* orderItemArr=[tSalesOrder order_Items];
            for(SalesOrderItem *orderItem in orderItemArr)
            {
                LastSellingPrice* lastSellingPrice=[[LastSellingPrice alloc] init];
                lastSellingPrice.Price=orderItem.Unit_Price;
                lastSellingPrice.UOM=orderItem.Order_UOM;
                NSDate* currentDate=[NSDate date];
                lastSellingPrice.Date=currentDate;
                if([self insertLastSellingPrice:lastSellingPrice ForCustomer:tSalesOrder.customer AndSalePerson:salesPerson andItem:orderItem.item]!=DataBaseInsertionSuccessful)
                {
                    [lastSellingPrice release]; 
                    return DataBaseInsertionFailed;
                }
                [lastSellingPrice release];
            }
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}
-(DataBaseInsertionResult)insertLastSellingPrice:(LastSellingPrice*)lastSellingPrice ForCustomer:(Customer*)customer AndSalePerson:(SalesPerson*)salesPerson andItem:(Item*)item
{
    NSDateFormatter* formatter=[[NSDateFormatter alloc] init]; 
    //[formatter setDateFormat:@"YYYY/MM/dd hh:mm:ss"];
    [formatter setDateFormat:@"yyyyMMdd"];
    NSString* lastSellingPriceDate=[formatter stringFromDate:lastSellingPrice.Date];
    [formatter release];
    NSString *Item_Nog=[item.Item_No stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    
    NSString *insertSQL = [NSString stringWithFormat:@"insert into LastSellingPrice(salesperson_code,customer_id,item_no,last_selling_price,last_selling_price_date,last_selling_price_uom,min_height,min_sqft) values('%@','%@','%@',%f,'%@','%@',%f,%f);", salesPerson.SalesPerson_Code,customer.Customer_Id,Item_Nog,lastSellingPrice.Price,lastSellingPriceDate,lastSellingPrice.UOM,lastSellingPrice.Min_Hight,lastSellingPrice.Min_SQFT];
    sqlite3_stmt *statement;
    const char *insert_stmt = [insertSQL UTF8String];
     
    sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
    if(sqlite3_step(statement) == SQLITE_DONE)
    {
        sqlite3_finalize(statement);
        
        return DataBaseInsertionSuccessful;
    }
    NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
    sqlite3_finalize(statement);
    return DataBaseInsertionFailed;
}
-(DataBaseUpdateResult)markSalesOrderAsUploaded:(SalesOrder*)salesOrder 
{ 
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *orderOrder_Reference=[salesOrder.Order_Reference stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *updateSQL = [NSString stringWithFormat:@"update SalesOrder set is_salesorder_uploaded=1, orderReference='%@' where salesorder_id=%d", orderOrder_Reference ,salesOrder.Order_id];
        salesOrder.Is_Uploaded=YES;
        sqlite3_stmt *statement;
        const char *update_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, update_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseUpdateFailed;
}

-(NSMutableArray*)selectSalesOrdersforSalesPerson:(SalesPerson*)salesPerson IsUploaded:(BOOL)isUploaded IsDeleted:(BOOL)isDeleted
{ 
    
    //KDSDateOrderPackage *dataSalesOrder=[[KDSDateOrderPackage alloc] init];
    //NSArray* tTransferecSalesOrderPackageArr=[dataSalesOrder selectOrderPackageID];
     
    //KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    //appDelegate.orderPackagingArr=tTransferecSalesOrderPackageArr;
    
    NSMutableArray* ordersarr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        ordersarr=[[[NSMutableArray alloc] init] autorelease];
        NSString *selectSQL=[NSString stringWithFormat:@"select SalesOrder.salesorder_id,SalesOrder.order_type,SalesOrder.order_date,SalesOrder.order_location,SalesOrder.is_salesorder_uploaded,SalesOrder.grand_price,SalesOrder.installation,SalesOrder.transportation,SalesOrder.order_discount,SalesOrder.system_order_id,Customer._id,SalesOrder.remark,SalesOrder.installation_name,SalesOrder.installation_address,SalesOrder.installation_city,SalesOrder.installation_state,SalesOrder.installation_phone,SalesOrder.installation_fax,SalesOrder.installation_email, SalesOrder.OrderDescription, SalesOrder.DeliveryDate, Customer.name, Customer.street_1, Customer.street_2, Customer.street_3,Customer.street_4, Customer.phone_1, Customer.phone_2, SalesOrder.orderReference, SalesOrder.CashRemark, Customer.contact_name, SalesOrder.packageID from SalesOrder,Customer where SalesOrder.customer_id=Customer._id and SalesOrder.salesperson_code='%@' and is_salesorder_uploaded=%d and is_deleted=%d order by SalesOrder.salesorder_id DESC",salesPerson.SalesPerson_Code, isUploaded,isDeleted];
         
        //NSString *selectSQL=[NSString stringWithFormat:@"select SalesOrder.salesorder_id,SalesOrder.order_type,SalesOrder.order_date,SalesOrder.order_location,SalesOrder.is_salesorder_uploaded,SalesOrder.grand_price,SalesOrder.installation,SalesOrder.transportation,SalesOrder.order_discount,SalesOrder.system_order_id,Customer._id,SalesOrder.remark, Customer.name from SalesOrder,Customer where SalesOrder.customer_id=Customer._id and SalesOrder.salesperson_code='%@' and is_salesorder_uploaded=%d",salesPerson.SalesPerson_Code, isUploaded];
 
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        while(sqlite3_step(statement) == SQLITE_ROW) 
        {
            SalesOrder* salesOrder=nil;
            int orderType=sqlite3_column_int(statement, 1);
            if(orderType==1)
            {
                salesOrder=[[SalesOrder alloc] init];
                
                
            }
            else if(orderType==2)
            {
                salesOrder=[[BlindSalesOrder alloc] init];
                float installationCost = sqlite3_column_double(statement, 6);
                ((BlindSalesOrder*)salesOrder).installation = installationCost;
                float transportationCost = sqlite3_column_double(statement, 7);
                ((BlindSalesOrder*)salesOrder).transportation = transportationCost;
                NSString *installationName=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 12)];
                NSString *installationNamesd=[installationName stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_Name = installationNamesd;
                
                NSString *installationaddress=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 13)];
                NSString *installationaddressd=[installationaddress stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installationorder_Address= installationaddressd;
                
                NSString *installationCity=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 14)];
                //((BlindSalesOrder*)salesOrder).Installation_Address.City = installationCity;
                NSString *installationCityd=[installationCity stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_City = installationCityd;
                
                NSString *installationState=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 15)];
                NSString *installationStated=[installationState stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_State = installationStated;
                
                NSString *installationPhone=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 16)];
                NSString *installationPhoned=[installationPhone stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_Phone = installationPhoned;
                
                NSString *installationFax=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 17)];
                NSString *installationFaxd=[installationFax stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_Fax = installationFaxd;
                
                NSString *installationemail=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 18)];
                NSString *installationemaild=[installationemail stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_Email = installationemaild;
            }
            
            int orderID=sqlite3_column_int(statement, 0);
            salesOrder.Order_id=orderID;
            
            /*NSDate *today = [NSDate date];
             NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
             [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
             NSString *dateString = [dateFormat stringFromDate:today];
             //NSLog(@"date: %@", dateString);
             [dateFormat release];*/
            
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            //[formatter setDateFormat:@"YYYY/MM/dd hh:mm:ss"];
            [formatter setDateFormat:@"yyyyMMdd"]; 
            NSString *orderDateStr=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            NSDate *orderDate=[formatter dateFromString:orderDateStr];
            salesOrder.Date = orderDate;
            //NSLog(@"orderDateStr>>>>>>>>>>>>%@",orderDate);
            [formatter release];
            
            NSString *orderLocation=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
            NSString *orderLocationd=[orderLocation stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            salesOrder.Location = orderLocationd;
            
            int isUploaded=sqlite3_column_int(statement, 4);
            salesOrder.Is_Uploaded=isUploaded;
            float grandPrice=sqlite3_column_double(statement, 5);
            salesOrder.grand_Price=grandPrice;
            float order_discount=sqlite3_column_double(statement, 8);
            salesOrder.order_discount = order_discount;
            if((char*)sqlite3_column_text(statement, 9)!= NULL)
            {
                NSString* orderIdPrefix = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 9)];
                salesOrder.systemOrderId = orderIdPrefix;
            }
            
            if(orderType==1)
            {
                if((char*)sqlite3_column_text(statement, 13)!= NULL)
                {
                    
                    NSString *shippingaddress=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 13)];
                    NSString *orderShipping_Address=[shippingaddress stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                    salesOrder.Shipping_Address= orderShipping_Address;
                }
                
            }
            
             
            
            Customer* customer=[[Customer alloc] init];
            NSString *customerID=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 10)];
            customer.Customer_Id = customerID;
            //customer.Contact_Name
            char* c_remark=(char*)sqlite3_column_text(statement, 11);
            if(c_remark!=NULL)
            {
                NSString *remark=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 11)];
                NSString *orderRemark=[remark stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.remark = orderRemark;
                
            }
            char* c_OrderDescriptions=(char*)sqlite3_column_text(statement, 19);
            if(c_OrderDescriptions!=NULL)
            {
                NSString *OrderDescriptions=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 19)];
                NSString *OrderDescriptionss=[OrderDescriptions stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.OrderDescription = OrderDescriptionss;
            }
            
            char* c_DeliveryDates=(char*)sqlite3_column_text(statement, 20);
            if(c_DeliveryDates!=NULL)
            {
                NSString *DeliveryDates=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 20)];
                NSString *orderDeliveryDate=[DeliveryDates stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.DeliveryDate = orderDeliveryDate;
            }
            
            
            NSString *customerName=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 21)];
            NSString *customerNamed=[customerName stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            customer.Customer_Name = customerNamed;
            //customer.Customer_Name = customerName;
            
            
            if((char*)sqlite3_column_text(statement, 28)!=NULL)
            {  
                NSString *orderRef=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 28)];
                NSString *orderOrder_Reference=[orderRef stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.Order_Reference = orderOrder_Reference; 
            }
            
            /**/if((char*)sqlite3_column_text(statement, 29)!=NULL)
            {
                NSString *cashrmk=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 29)];
                NSString *cashrmks=[cashrmk stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.CashRemarks = cashrmks;                    
            } 
            
            if((char*)sqlite3_column_text(statement, 30)!=NULL)
            {
                NSString *cuscontactname=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 30)];
                NSString *cuscontactnamed=[cuscontactname stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                customer.Contact_Name = cuscontactnamed;                    
            } 
            
            if((char*)sqlite3_column_text(statement, 31)!=NULL)
            {  
                NSString *orderPackage=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 31)];
                //NSString *orderOrder_Reference=[orderRef stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.OrderPackageID = orderPackage; 
            }
            
            
            Address* C_address=[[Address alloc]init];
            NSString *Street_1s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 22)];
            NSString *Street_1ss=[Street_1s stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            C_address.Street_1 = Street_1ss;
            //((Address*)salesOrder).Street_1 = Street_1s;
            
            /*NSLog(@"Street_1s--->%@",Street_1s);
            NSLog(@"C_address.Street_1--->%@",C_address.Street_1);*/
            
            NSString *Street_2s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 23)];
            NSString *Street_2sd=[Street_2s stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            C_address.Street_2 = Street_2sd;
            
            NSString *Street_3s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 24)];
            NSString *Street_3ss=[Street_3s stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            C_address.Street_3 = Street_3ss;
            
            NSString *Street_4s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 25)];
            NSString *Street_4sd=[Street_4s stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            C_address.Street_4 = Street_4sd;
            salesOrder.Caddress=C_address;
            [C_address release];
            
            NSString *Phone_1s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 26)];
            NSString *Phone_1ss=[Phone_1s stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            customer.Phone_1 = Phone_1ss;
            
            NSString *Phone_2s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 27)];
            NSString *Phone_2ss=[Phone_2s stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            customer.Phone_2 = Phone_2ss;
            
            
            
            /*KDSDateOrderPackage *dataSalesOrder=[[KDSDateOrderPackage alloc] init];
            NSArray* tTransferecSalesOrderPackageArr=[dataSalesOrder selectOrderPackageID];
            
            for(OrderPackaging* orderpItem in tTransferecSalesOrderPackageArr)
            {
                //[self.salesOrder addOrderItem:orderItem]; 
                NSLog(@"orderItem-->%@",orderpItem.orderID);
            }
            [dataSalesOrder release];
            
            
            if (appDelegate.orderPackagingArr !=nil) {
                
                NSLog(@"appDelegate.orderPackagingArr-->%@",appDelegate.orderPackagingArr);
                
                for(OrderPackaging* orderpItem in appDelegate.orderPackagingArr)
                { 
                    NSLog(@"orderpItem--->%@",orderpItem);
                    
                    //[self.salesOrder addOrderItem:orderItem]; 
                    NSLog(@"orderItem-->%@",orderpItem.orderID);
                }
            }else { 
            
                NSLog(@"orderItem nil");
                
            }
            
            
            
            //orderPackagesArr=[appDelegate.apporderPackaging Order_Package];
            orderPackagesArr=appDelegate.apporderPackaging.orderPackagearr;
            for(OrderPackaging* orderpItem in orderPackagesArr)
            { 
                //NSLog(@"orderpItem--->%@",orderpItem);
                
                //[self.salesOrder addOrderItem:orderItem]; 
                NSLog(@"orderItem-->%@",orderpItem.orderID);
            }*/ 
                  
                
            
            
            
                        
            salesOrder.customer=customer;
            [customer release];
            
            
            [ordersarr addObject:salesOrder];
            [salesOrder release];
        }
    }
    
    //[dataSalesOrder release];
    
    NSSortDescriptor* sortDescriptor;
    sortDescriptor =[[NSSortDescriptor alloc] initWithKey:@"Date" ascending:NO];
    NSArray* sortedDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [ordersarr sortUsingDescriptors:sortedDescriptors];
    [sortDescriptor release];
    return ordersarr;
}

-(DataBaseUpdateResult)updateOrderPackageArray:(NSArray*)iorderpackage 
{
    self.runBatch=YES;
    DataBaseUpdateResult insertionResult=DataBaseUpdateSuccessful;
    //NSArray *existingCustomers=[self selectCustomerWithSalespersonID:salesPersonID];
    [self openConnection];
    
    for (SalesOrder* orderp in iorderpackage) 
    {
        
        insertionResult=[self updateSaleOrderPackageID:orderp];
        
        if(insertionResult==DataBaseUpdateFailed)
        {
            break;
        }
    } 
    
    [self closeConnection];
    return insertionResult;
} 

-(DataBaseUpdateResult)updateSaleOrderPackageID:(SalesOrder*)saleOrder
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch) 
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    } 
    if(connectionIsOpenned)
    {
         
        NSLog(@"saleOrder.OrderPackageID---%@",saleOrder.OrderPackageID);
        NSString* updateSQL=[NSString stringWithFormat:@"update SalesOrder set packageID='%@' where system_order_id='%@';", saleOrder.OrderPackageID,saleOrder.systemOrderId];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            { 
                [self closeConnection];
            }
            return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        //[self closeConnection];
        //return DataBaseUpdateSuccessful;
        if(!runBatch)
        {
            [self closeConnection];
        } 
    }
    return DataBaseUpdateFailed;
    
} 


-(DataBaseInsertionResult)insertPackage:(OrderPackaging*)tPackage
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch) 
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    } 
    if(connectionIsOpenned)
    {
        NSString *insertSQL;
        
        
        //NSString *itemDescription=[tItem.Description stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"]; 
        //NSString *itemNo=[tItem.Item_No stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        insertSQL = [NSString stringWithFormat:@"insert into SalesOrderPackage(orderID, packageID) values('%@','%@');",tPackage.orderID, tPackage.packageID];
        
        
        NSLog(@"tItem.Description--->%@",tPackage.orderID );
        
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        //NSLog(@"%@",insertSQL);
        //NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}

-(NSMutableArray*)selectOneSalesOrdersforSalesPerson:(SalesPerson*)salesPerson IsUploaded:(BOOL)isUploaded IsDeleted:(BOOL)isDeleted IsSalesOrderID:(SalesOrder*)issalesOrder
{
    NSMutableArray* ordersarr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        ordersarr=[[[NSMutableArray alloc] init] autorelease];
        NSString *selectSQL=[NSString stringWithFormat:@"select SalesOrder.salesorder_id,SalesOrder.order_type,SalesOrder.order_date,SalesOrder.order_location,SalesOrder.is_salesorder_uploaded,SalesOrder.grand_price,SalesOrder.installation,SalesOrder.transportation,SalesOrder.order_discount,SalesOrder.system_order_id,Customer._id,SalesOrder.remark,SalesOrder.installation_name,SalesOrder.installation_address,SalesOrder.installation_city,SalesOrder.installation_state,SalesOrder.installation_phone,SalesOrder.installation_fax,SalesOrder.installation_email, SalesOrder.OrderDescription, SalesOrder.DeliveryDate, Customer.name, Customer.street_1, Customer.street_2, Customer.street_3,Customer.street_4, Customer.phone_1, Customer.phone_2, SalesOrder.orderReference, SalesOrder.CashRemark, Customer.contact_name from SalesOrder,Customer where SalesOrder.customer_id=Customer._id and SalesOrder.salesperson_code='%@' and is_salesorder_uploaded=%d and is_deleted=%d and SalesOrder.salesorder_id=%d",salesPerson.SalesPerson_Code, isUploaded,isDeleted, issalesOrder.Order_id];
        //NSString *selectSQL=[NSString stringWithFormat:@"select SalesOrder.salesorder_id,SalesOrder.order_type,SalesOrder.order_date,SalesOrder.order_location,SalesOrder.is_salesorder_uploaded,SalesOrder.grand_price,SalesOrder.installation,SalesOrder.transportation,SalesOrder.order_discount,SalesOrder.system_order_id,Customer._id,SalesOrder.remark, Customer.name from SalesOrder,Customer where SalesOrder.customer_id=Customer._id and SalesOrder.salesperson_code='%@' and is_salesorder_uploaded=%d",salesPerson.SalesPerson_Code, isUploaded];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        while(sqlite3_step(statement) == SQLITE_ROW) 
        {
            SalesOrder* salesOrder=nil;
            int orderType=sqlite3_column_int(statement, 1);
            if(orderType==1)
            {
                salesOrder=[[SalesOrder alloc] init];
                
                
            }
            else if(orderType==2)
            {
                salesOrder=[[BlindSalesOrder alloc] init];
                float installationCost = sqlite3_column_double(statement, 6);
                ((BlindSalesOrder*)salesOrder).installation = installationCost;
                float transportationCost = sqlite3_column_double(statement, 7);
                ((BlindSalesOrder*)salesOrder).transportation = transportationCost;
                NSString *installationName=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 12)];
                NSString *installationNames=[installationName stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_Name = installationNames;
                
                NSString *installationaddress=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 13)];
                NSString *installationaddresss=[installationaddress stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installationorder_Address= installationaddresss;
                
                NSString *installationCity=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 14)];
                NSString *installationCityd=[installationCity stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                //((BlindSalesOrder*)salesOrder).Installation_Address.City = installationCity;
                ((BlindSalesOrder*)salesOrder).Installation_City = installationCityd;
                
                NSString *installationState=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 15)];
                NSString *installationStates=[installationState stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_State = installationStates;
                
                NSString *installationPhone=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 16)];
                NSString *installationPhones=[installationPhone stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_Phone = installationPhones;
                NSString *installationFax=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 17)];
                NSString *installationFaxs=[installationFax stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_Fax = installationFaxs;
                NSString *installationemail=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 18)];
                NSString *installationemails=[installationemail stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                ((BlindSalesOrder*)salesOrder).Installation_Email = installationemails;
            }
            
            int orderID=sqlite3_column_int(statement, 0);
            salesOrder.Order_id=orderID;
            
            /*NSDate *today = [NSDate date];
             NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
             [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
             NSString *dateString = [dateFormat stringFromDate:today];
             //NSLog(@"date: %@", dateString);
             [dateFormat release];*/
            
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            //[formatter setDateFormat:@"YYYY/MM/dd hh:mm:ss"];
            [formatter setDateFormat:@"yyyyMMdd"];
            NSString *orderDateStr=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            NSDate *orderDate=[formatter dateFromString:orderDateStr];
            salesOrder.Date = orderDate;
            NSLog(@"salesOrder.Date55555>>>>>>>>>>>>%@",salesOrder.Date);
            [formatter release];
            
            

            NSString *orderLocation=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
            NSString *orderLocations=[orderLocation stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            salesOrder.Location = orderLocations;
            int isUploaded=sqlite3_column_int(statement, 4);
            salesOrder.Is_Uploaded=isUploaded;
            float grandPrice=sqlite3_column_double(statement, 5);
            salesOrder.grand_Price=grandPrice;
            float order_discount=sqlite3_column_double(statement, 8);
            salesOrder.order_discount = order_discount;
            if((char*)sqlite3_column_text(statement, 9)!= NULL)
            {
                NSString* orderIdPrefix = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 9)];
                salesOrder.systemOrderId = orderIdPrefix;
            }

            
            if(orderType==1)
            {
                if((char*)sqlite3_column_text(statement, 13)!= NULL)
                {
                    NSString *shippingaddress=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 13)];
                    NSString *orderShipping_Address=[shippingaddress stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                    salesOrder.Shipping_Address= orderShipping_Address;
                }
 
            }
            
            if((char*)sqlite3_column_text(statement, 28)!=NULL)
                {
                    NSString *orderRef=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 28)];
                    NSString *orderOrder_Reference=[orderRef stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                    salesOrder.Order_Reference = orderOrder_Reference;                    
                } 
            
            Customer* customer=[[Customer alloc] init];
            NSString *customerID=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 10)];
            customer.Customer_Id = customerID;
            char* c_remark=(char*)sqlite3_column_text(statement, 11);
            if(c_remark!=NULL)
            {
                NSString *remark=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 11)];
                NSString *orderRemark=[remark stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.remark = orderRemark;
            }
            char* c_OrderDescriptions=(char*)sqlite3_column_text(statement, 19);
            if(c_OrderDescriptions!=NULL)
            {
                NSString *OrderDescriptions=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 19)];
                NSString *OrderDescriptionss=[OrderDescriptions stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.OrderDescription = OrderDescriptionss;
            }
            
            char* c_DeliveryDates=(char*)sqlite3_column_text(statement, 20);
            if(c_DeliveryDates!=NULL)
            {
                NSString *DeliveryDates=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 20)];
                NSString *orderDeliveryDate=[DeliveryDates stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.DeliveryDate = orderDeliveryDate;
            }
            
            
            NSString *customerName=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 21)];
            NSString *customerNamed=[customerName stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            customer.Customer_Name = customerNamed;
            //customer.Customer_Name = customerName;
            
            /**/if((char*)sqlite3_column_text(statement, 29)!=NULL)
            {
                NSString *cashrmk=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 29)];
                NSString *orderOrder_CashRemarks=[cashrmk stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.CashRemarks = orderOrder_CashRemarks;                    
            } 
            
            if((char*)sqlite3_column_text(statement, 30)!=NULL)
            {
                NSString *cuscontactname=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 30)];
                NSString *cuscontactnames=[cuscontactname stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                customer.Contact_Name = cuscontactnames;                    
            } 
            
            
            Address* C_address=[[Address alloc]init];
            NSString *Street_1s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 22)];
            NSString *Street_1ss=[Street_1s stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            C_address.Street_1 = Street_1ss;
            //((Address*)salesOrder).Street_1 = Street_1s;
            
            /*NSLog(@"Street_1s--->%@",Street_1s);
             NSLog(@"C_address.Street_1--->%@",C_address.Street_1);*/
            
            NSString *Street_2s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 23)];
            NSString *Street_2sd=[Street_2s stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            C_address.Street_2 = Street_2sd;
            
            NSString *Street_3s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 24)];
            NSString *Street_3sd=[Street_3s stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            C_address.Street_3 = Street_3sd;
            
            NSString *Street_4s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 25)];
            NSString *Street_4sd=[Street_4s stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            C_address.Street_4 = Street_4sd;
            salesOrder.Caddress=C_address;
            [C_address release];
            
            NSString *Phone_1s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 26)];
            customer.Phone_1 = Phone_1s;
            
            NSString *Phone_2s=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 27)];
            customer.Phone_2 = Phone_2s;
            
            
            
            
            salesOrder.customer=customer;
            [customer release];
            
            
            [ordersarr addObject:salesOrder];
            [salesOrder release];
        }
    }
    NSSortDescriptor* sortDescriptor;
    sortDescriptor =[[NSSortDescriptor alloc] initWithKey:@"Date" ascending:NO];
    NSArray* sortedDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [ordersarr sortUsingDescriptors:sortedDescriptors];
    [sortDescriptor release];
    return ordersarr;
}
-(NSMutableArray*)selectSalesOrderForCustomer:(Customer*)customer IsDeleted:(BOOL)isDeleted
{
    NSMutableArray* orderArr = nil;
    if([self openConnection]== DataBaseConnectionOpened)
    {
        orderArr = [[[NSMutableArray alloc] init] autorelease];
        NSString *selectSQL=[NSString stringWithFormat:@"select SalesOrder.salesorder_id,SalesOrder.order_type,SalesOrder.order_date,SalesOrder.order_location,SalesOrder.is_salesorder_uploaded,SalesOrder.grand_price,SalesOrder.system_order_id,Customer._id,Customer.name, SalesOrder.CashRemark from SalesOrder,Customer where SalesOrder.customer_id=Customer._id and Customer._id='%@' and SalesOrder.is_deleted=%d",customer.Customer_Id,isDeleted];

        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            SalesOrder* salesOrder=nil;
            int orderType=sqlite3_column_int(statement, 1);
            if(orderType==1)
            {
                salesOrder=[[SalesOrder alloc] init];
            }
            else if(orderType==2)
            {
                salesOrder=[[BlindSalesOrder alloc] init];
            }
            int orderID=sqlite3_column_int(statement, 0);
            salesOrder.Order_id=orderID;
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            //[formatter setDateFormat:@"YYYY/MM/dd hh:mm:ss"];
            [formatter setDateFormat:@"yyyyMMdd"];
            NSString *orderDateStr=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            NSDate *orderDate=[formatter dateFromString:orderDateStr];
            [formatter release];
            salesOrder.Date = orderDate;
            NSString *orderLocation=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
            NSString *orderLocationd=[orderLocation stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            salesOrder.Location = orderLocationd;
            int isUploaded=sqlite3_column_int(statement, 4);
            salesOrder.Is_Uploaded=isUploaded;
            float grandPrice=sqlite3_column_double(statement, 5);
            salesOrder.grand_Price=grandPrice;
            NSString* orderIdPrefix = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)];
            salesOrder.systemOrderId = orderIdPrefix;
            Customer* customer=[[Customer alloc] init];
            NSString *customerID=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
            customer.Customer_Id = customerID;
            NSString *customerName=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
            
            /**/if((char*)sqlite3_column_text(statement, 9)!=NULL)
            {
                NSString *cusRemark=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 9)];
                NSString *cusRemarks=[cusRemark stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                salesOrder.CashRemarks = cusRemarks;    
            }  
            
            customer.Customer_Name = customerName;
            salesOrder.customer=customer;
            [customer release];
            [orderArr addObject:salesOrder];
            [salesOrder release];
        }
    }
    return orderArr;
}

/*-(DataBaseUpdateResult)updateCustomerRemark:(Customer*)tCustomer
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *tCustomercustomer_remark=[tCustomer.customer_remark stringByReplacingOccurrencesOfString:@"'" withString:@"\""];
        
        NSString *updateSQL = [NSString stringWithFormat:@"update Customer set Remarks='%@' where _id='%@'",tCustomercustomer_remark,tCustomer.Customer_Id];
        sqlite3_stmt *statement;
        const char *update_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, update_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        { 
            [self closeConnection];
        }
    }
    return DataBaseUpdateFailed;
}*/
 

-(DataBaseDeletionResult)deleteSaleOrder:(SalesOrder *)saleOrder
{
    KDSDataSalesOrderItem* salesOrderItem=[[KDSDataSalesOrderItem alloc] init];
    if([salesOrderItem deleteFromSalesOrderItemForOrder:saleOrder]==DataBaseDeletionSuccessful)
    {
        if([self openConnection]==DataBaseConnectionOpened)
        {
            NSString* deleteSQL=[NSString stringWithFormat:@"delete from SalesOrder where salesorder_id=%d", saleOrder.Order_id];
            sqlite3_stmt *statement;
            const char *insert_stmt = [deleteSQL UTF8String];
            
            sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
            if(sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
                [self closeConnection];
                [salesOrderItem release]; 
                return DataBaseDeletionSuccessful;
            }
            [self closeConnection];
            NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
            sqlite3_finalize(statement);
        }
    }
    [salesOrderItem release];
    return DataBaseDeletionFailed;
}

-(DataBaseUpdateResult)updateSaleOrderIsDeleted:(SalesOrder*)saleOrder
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        
        
        NSString* updateSQL=[NSString stringWithFormat:@"update SalesOrder set is_deleted=%d where salesorder_id=%d", saleOrder.isDeleted,saleOrder.Order_id];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateSuccessful;
    }
    else
    { 
        return DataBaseUpdateFailed;
    }
    
}


-(DataBaseUpdateResult)updateSaleOrder:(SalesOrder*)saleOrder ForSaleOrderId:(SalesPerson*)salePerson
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString* updateSQL=[NSString stringWithFormat:@"update SalesOrder set system_order_id='%@' where salesorder_id=%d", salePerson.orderId_Prefix,saleOrder.Order_id];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateSuccessful;
    }
    else
    { 
        return DataBaseUpdateFailed;
    }
}
@end
