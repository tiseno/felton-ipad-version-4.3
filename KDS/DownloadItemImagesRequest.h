//
//  DownloadItemImagesRequest.h
//  KDS
//
//  Created by Tiseno Mac 2 on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"

@interface DownloadItemImagesRequest : XMLRequest{
    NSString *ItemType;
}

@property (nonatomic,retain) NSString *ItemType;
-(id)initWithItemTypes:(NSString*)iItemType;

@end
