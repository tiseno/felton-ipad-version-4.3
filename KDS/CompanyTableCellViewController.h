//
//  CompanyTableCellViewController.h
//  KDS
//
//  Created by Tiseno on 12/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderHistoryTableGeneratorDelegate.h"
#import "Customer.h"

@interface CompanyTableCellViewController : UITableViewCell<UITableViewDelegate>{
    

    UILabel *companyNameLabel;
    UILabel *companyNameIDLabel;
    UILabel *picLabel;
    
    UIButton *orderHistoryButton;
    id<OrderHistoryTableGeneratorDelegate> orderHistoryGeneratorDelegate;
    
    Customer *customer;
    UIColor *lineColor;
    BOOL topCell;
}
@property (nonatomic) BOOL topCell;
@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic, retain) UILabel *companyNameLabel;
@property (nonatomic, retain) UILabel *companyNameIDLabel;
@property (nonatomic, retain) UILabel *picLabel;
@property (nonatomic, retain) UIButton *orderHistoryButton;
@property (nonatomic, retain) id<OrderHistoryTableGeneratorDelegate> orderHistoryGeneratorDelegate;
@property (nonatomic, retain) Customer *customer;
@end
