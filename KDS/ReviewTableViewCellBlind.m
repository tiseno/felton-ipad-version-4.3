//
//  ReviewTableViewCellBlind.m
//  KDS
//
//  Created by Tiseno on 4/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ReviewTableViewCellBlind.h"


@implementation ReviewTableViewCellBlind
@synthesize itemIdLabel, itemNameLabel, itemWidthDrop, itemSqrFeet, itemQuantityLabel, orderPriceLabel, totalPriceLabel, orderDiscount, itemArea,orderControl,orderColor;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) 
    {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 740, 35);
        
        /*UILabel *tItemIdLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, 100, self.frame.size.height)];
        tItemIdLabel.textAlignment = UITextAlignmentCenter;
        tItemIdLabel.backgroundColor = [UIColor clearColor];
        tItemIdLabel.textColor = [UIColor blackColor];
        self.itemIdLabel = tItemIdLabel;
        [tItemIdLabel release];
        [self addSubview:itemIdLabel];*/
        
        /*UILabel *tItemSqrFeetLabel = [[UILabel alloc] initWithFrame:CGRectMake(400, self.frame.origin.y, 480, self.frame.size.height)];
         tItemSqrFeetLabel.textAlignment = UITextAlignmentCenter;
         tItemSqrFeetLabel.backgroundColor = [UIColor clearColor];
         tItemSqrFeetLabel.textColor = [UIColor blackColor];
         self.itemSqrFeet = tItemSqrFeetLabel;
         [tItemSqrFeetLabel release];
         [self addSubview:itemSqrFeet];*/
        
        UILabel *tItemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.origin.y, 150, self.frame.size.height)];
        tItemNameLabel.textAlignment = UITextAlignmentCenter;
        tItemNameLabel.backgroundColor = [UIColor clearColor];
        tItemNameLabel.textColor = [UIColor blackColor];
        self.itemNameLabel = tItemNameLabel;
        [tItemNameLabel release];
        [self addSubview:itemNameLabel];
        
        UILabel *tItemWidthDropLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, self.frame.origin.y, 210, self.frame.size.height)];
        tItemWidthDropLabel.textAlignment = UITextAlignmentCenter;
        tItemWidthDropLabel.backgroundColor = [UIColor clearColor];
        tItemWidthDropLabel.textColor = [UIColor blackColor];
        self.itemWidthDrop = tItemWidthDropLabel;
        [tItemWidthDropLabel release];
        [self addSubview:itemWidthDrop];
        
        
        UILabel *tItemQuantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, self.frame.origin.y, 330, self.frame.size.height)];
        tItemQuantityLabel.textAlignment = UITextAlignmentCenter;
        tItemQuantityLabel.backgroundColor = [UIColor clearColor];
        tItemQuantityLabel.textColor = [UIColor blackColor];
        self.itemQuantityLabel = tItemQuantityLabel;
        [tItemQuantityLabel release];
        [self addSubview:itemQuantityLabel];
        
        UILabel *tItemAreaLabel = [[UILabel alloc] initWithFrame:CGRectMake(320, self.frame.origin.y, 270, self.frame.size.height)];
        tItemAreaLabel.textAlignment = UITextAlignmentCenter;
        tItemAreaLabel.backgroundColor = [UIColor clearColor];
        tItemAreaLabel.textColor = [UIColor blackColor];
        self.itemArea = tItemAreaLabel;
        [tItemAreaLabel release];
        [self addSubview:itemArea];
        
        
        
        UILabel *tOrderPrice = [[UILabel alloc] initWithFrame:CGRectMake(340, self.frame.origin.y, 390, self.frame.size.height)];
        tOrderPrice.textAlignment = UITextAlignmentCenter;
        tOrderPrice.backgroundColor = [UIColor clearColor];
        tOrderPrice.textColor = [UIColor blackColor];
        self.orderPriceLabel = tOrderPrice;
        [tOrderPrice release];
        [self addSubview:orderPriceLabel];
        
        UILabel *tOrderControl = [[UILabel alloc] initWithFrame:CGRectMake(400, self.frame.origin.y, 450, self.frame.size.height)];
        tOrderControl.textAlignment = UITextAlignmentCenter;
        tOrderControl.backgroundColor = [UIColor clearColor];
        tOrderControl.textColor = [UIColor blackColor];
        self.orderControl = tOrderControl;
        [tOrderControl release];
        [self addSubview:orderControl];
        
        UILabel *tOrderColor = [[UILabel alloc] initWithFrame:CGRectMake(460, self.frame.origin.y, 510, self.frame.size.height)];
        tOrderColor.textAlignment = UITextAlignmentCenter;
        tOrderColor.backgroundColor = [UIColor clearColor];
        tOrderColor.textColor = [UIColor blackColor];
        self.orderColor = tOrderColor;
        [tOrderColor release];
        [self addSubview:orderColor];
        
        UILabel *tOrderDiscount = [[UILabel alloc] initWithFrame:CGRectMake(520, self.frame.origin.y, 570, self.frame.size.height)];
        tOrderDiscount.textAlignment = UITextAlignmentCenter;
        tOrderDiscount.backgroundColor = [UIColor clearColor];
        tOrderDiscount.textColor = [UIColor blackColor];
        self.orderDiscount = tOrderDiscount;
        [tOrderDiscount release];
        [self addSubview:orderDiscount];
        
        UILabel *tTotalPrice = [[UILabel alloc] initWithFrame:CGRectMake(580, self.frame.origin.y, 630, self.frame.size.height)];
        tTotalPrice.textAlignment = UITextAlignmentCenter;
        tTotalPrice.backgroundColor = [UIColor clearColor];
        tTotalPrice.textColor = [UIColor blackColor];
        self.totalPriceLabel = tTotalPrice;
        [tTotalPrice release];
        [self addSubview:totalPriceLabel];
    }
    
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc
{
    [orderPriceLabel release];
    [itemQuantityLabel release];
    [itemNameLabel release];
    [itemIdLabel release];
    [super dealloc];
}

@end
