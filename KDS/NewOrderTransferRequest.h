//
//  NewOrderTransferRequest.h
//  KDS
//
//  Created by Tiseno on 2/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"
#import "SalesOrder.h"
#import "SalesPerson.h"
 

@interface NewOrderTransferRequest : XMLRequest {
    SalesOrder* newSalesOrder;
    SalesPerson* salesPersonCode;
}
@property(nonatomic, retain) SalesOrder* newSalesOrder;
@property(nonatomic, retain) SalesPerson* salesPersonCode;

-(id)initWithSalesOrder:(SalesOrder*)isaleOrder SalesPersonCode:(SalesPerson*)isalesPersonCode;
@end
