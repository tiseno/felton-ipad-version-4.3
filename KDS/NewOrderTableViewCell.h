//
//  NewOrderTableViewCell.h
//  KDS
//
//  Created by Tiseno on 11/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesOrder.h"
#import "KDSAppDelegate.h"
#import "KDSDataSalesOrderItem.h"
#import "NewOrderTableEditDelegate.h"
#import "KDSDataSalesOrder.h"
//#import "NetworkHandler.h"

#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1
@interface NewOrderTableViewCell : UITableViewCell <UIAlertViewDelegate>  {
    UIColor *lineColor;
    BOOL topCell;
    
    UIButton *editButton;
    UIButton *pdfButton;
    UIButton *deleteButton;
    
    UILabel *codeLabel;
    UILabel *nameLabel;
    UILabel *priceLabel;
    UILabel *currencyLabel;
    UILabel *orderIdLabel;
    
    SalesOrder* salesOrder;
    id<NewOrderTableEditDelegate> delegate;
}
@property (nonatomic, retain) UIColor *lineColor;

@property (nonatomic) BOOL topCell;
@property (nonatomic, retain) SalesOrder* salesOrder;
@property (nonatomic, retain) id<NewOrderTableEditDelegate> delegate;
@property (nonatomic, retain)  UIButton *editButton;
@property (nonatomic, retain)  UIButton *pdfButton;
@property (nonatomic, retain)  UIButton *deleteButton;
@property (nonatomic, retain)  UIButton *pdfquoButton;

@property (nonatomic, retain) UILabel *orderIdLabel;
@property (nonatomic, retain) UILabel *codeLabel;
@property (nonatomic, retain) UILabel *nameLabel;
@property (nonatomic, retain) UILabel *priceLabel;
@property (nonatomic, retain) UILabel *currencyLabel;
@end
