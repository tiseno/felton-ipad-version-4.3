//
//  EditCartView.m
//  KDS
//
//  Created by Tiseno Mac 2 on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EditCartView.h"
#define cell1Width 115
#define cell2Width 306
#define cell3Width 110

@implementation EditCartView
@synthesize descriptionTextView,quantityTextField,priceTextField,discountTextField;
@synthesize deleteButton;
@synthesize changeDiscountTypeButton;
@synthesize quantityLabel, productNameLabel, priceLabel, descriptionLabel,discountLabel,discountTypeLabel;
@synthesize productImageView;




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
}

-(void)showDetailFrame
{
    UIImage *deleteButtonImage = [UIImage imageNamed:@"btn_delete.png"];
    UIButton *tdeleteButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 90, 89, 34)];
    [tdeleteButton setBackgroundImage:deleteButtonImage forState:UIControlStateNormal];
    [tdeleteButton addTarget:self action:@selector(delet_ButtonTapped:)forControlEvents:UIControlEventTouchDown];
    self.deleteButton = tdeleteButton;
    [tdeleteButton release];
    [self.view addSubview:deleteButton];
    
    UILabel *tquantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(14, 10, 89, 25)];
    tquantityLabel.text = @"Quantity:";
    tquantityLabel.textAlignment = UITextAlignmentLeft;
    tquantityLabel.backgroundColor = [UIColor clearColor];
    tquantityLabel.textColor = [UIColor brownColor];
    self.quantityLabel = tquantityLabel;
    [tquantityLabel release];
    [self.view  addSubview:quantityLabel];
    
    UITextField *tquantityTextField = [[UITextField alloc] initWithFrame:CGRectMake(10, 45, 89, 34)];
    tquantityTextField.delegate = self;
    tquantityTextField.textColor = [UIColor blackColor];
    tquantityTextField.textAlignment = UITextAlignmentCenter;
    tquantityTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    tquantityTextField.backgroundColor = [UIColor lightGrayColor];
    self.quantityTextField = tquantityTextField;
    [tquantityTextField release];
    [self.view  addSubview:quantityTextField];
    
    UIImageViewWithImageFromURL *tproductImageView = [[UIImageViewWithImageFromURL alloc] init];
    tproductImageView.frame = CGRectMake(120, 0, 110, 130);
    self.productImageView = tproductImageView;
    [tproductImageView release];
    [self.view  addSubview:productImageView];
    
    UILabel *tproductNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, 15, cell2Width-productImageView.frame.size.width-25, 25)];
    tproductNameLabel.textAlignment = UITextAlignmentLeft;
    tproductNameLabel.adjustsFontSizeToFitWidth = YES;
    tproductNameLabel.backgroundColor = [UIColor clearColor];
    tproductNameLabel.textColor = [UIColor blackColor];
    self.productNameLabel = tproductNameLabel;
    [tproductNameLabel release];
    [self.view  addSubview:productNameLabel];
    
    UILabel *tdescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, 36, cell2Width-productImageView.frame.size.width-25, 21)];
    tdescriptionLabel.text = @"Description:";
    tdescriptionLabel.textAlignment = UITextAlignmentLeft;
    tdescriptionLabel.backgroundColor = [UIColor clearColor];
    tdescriptionLabel.textColor = [UIColor brownColor];
    self.descriptionLabel = tdescriptionLabel;
    [tdescriptionLabel release];
    [self.view  addSubview:descriptionLabel];
    
    UITextView *tdescriptionTextView = [[UITextView alloc] initWithFrame:CGRectMake(cell1Width+productImageView.frame.size.width+5, 57, cell2Width-productImageView.frame.size.width-25, 70)];
    tdescriptionTextView.delegate=self;
    tdescriptionTextView.textColor = [UIColor blackColor];
    tdescriptionTextView.textAlignment = UITextAlignmentLeft;
    tdescriptionTextView.backgroundColor = [UIColor lightGrayColor];
    self.descriptionTextView = tdescriptionTextView;
    [tdescriptionTextView release];
    [self.view  addSubview:descriptionTextView];
    
    
    UILabel *tpriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(cell1Width+cell2Width+10, 25, 150, 25)];
    tpriceLabel.text = @"Price (RM):";
    tpriceLabel.textAlignment = UITextAlignmentLeft;
    tpriceLabel.backgroundColor = [UIColor clearColor];
    tpriceLabel.textColor = [UIColor brownColor];
    self.priceLabel = tpriceLabel;
    [tpriceLabel release];
    [self.view  addSubview:priceLabel];
    
    UITextField *tpriceTextField = [[UITextField alloc] initWithFrame:CGRectMake(cell1Width+cell2Width+10, 55, 89, 34)];
    //tpriceTextField.delegate=self;
    //tpriceTextField.tag = kPriceTextField;
    tpriceTextField.textColor = [UIColor blackColor];
    tpriceTextField.textAlignment = UITextAlignmentCenter;
    tpriceTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    tpriceTextField.backgroundColor = [UIColor lightGrayColor];
    self.priceTextField = tpriceTextField;
    [tpriceTextField release];
    [self.view  addSubview:priceTextField];
    
    UILabel *tDiscountLabel = [[UILabel alloc] initWithFrame:CGRectMake(cell1Width+cell2Width+cell3Width+20, 10, 89, 25)];
    tDiscountLabel.text = @"Discount:";
    tDiscountLabel.textAlignment = UITextAlignmentCenter;
    tDiscountLabel.backgroundColor = [UIColor clearColor];
    tDiscountLabel.textColor = [UIColor brownColor];
    self.discountLabel = tDiscountLabel;
    [tDiscountLabel release];
    [self.view  addSubview:discountLabel];
    
    UITextField *tDiscountTextField = [[UITextField alloc] initWithFrame:CGRectMake(cell1Width+cell2Width+cell3Width+20, 40, 89, 34)];
    tDiscountTextField.delegate=self;
    tDiscountTextField.textColor = [UIColor blackColor];
    tDiscountTextField.textAlignment = UITextAlignmentCenter;
    tDiscountTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    tDiscountTextField.backgroundColor = [UIColor lightGrayColor];
    self.discountTextField = tDiscountTextField;
    [tDiscountTextField release];
    [self.view  addSubview:discountTextField];
    
    UIImage *changeDiscountButtonImage = [UIImage imageNamed:@"Button_Amount_Percentage.png"];
    UIButton *tchangeDiscountTypeButton = [[UIButton alloc] initWithFrame:CGRectMake(cell1Width+cell2Width+cell3Width+20, 80, 92, 34)];
    [tchangeDiscountTypeButton setBackgroundImage:changeDiscountButtonImage forState:UIControlStateNormal];
    [tchangeDiscountTypeButton addTarget:self action:@selector(changeDiscountType_ButtonTapped:)forControlEvents:UIControlEventTouchDown];
    self.changeDiscountTypeButton = tchangeDiscountTypeButton;
    [tchangeDiscountTypeButton release];
    [self.view addSubview:changeDiscountTypeButton];
    
    UILabel *tDiscountTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(cell1Width+cell2Width+cell3Width+20, 120, 89, 20)];
    tDiscountTypeLabel.text = @"In amount";
    tDiscountTypeLabel.textAlignment = UITextAlignmentCenter;
    tDiscountTypeLabel.backgroundColor = [UIColor clearColor];
    tDiscountTypeLabel.textColor = [UIColor brownColor];
    self.discountTypeLabel = tDiscountTypeLabel;
    [tDiscountTypeLabel release];
    [self.view  addSubview:discountTypeLabel];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
